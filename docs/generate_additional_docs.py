#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import settings

invalid_fields = {"description", "short_desc", "validator", "validators", "parser"}


with open("algorithms.rst", "w") as file:
    file.write("""
Algorithms
**********

""")
    for name, config in sorted(settings.algorithms.items()):
        header = "{} - ({})".format(config.get("name", name), name)
        file.write(header)
        file.write("\n{}\n\n".format("="*len(header)))
        file.write(config.get("description", ""))
        file.write("\n")

        if "parameters" in config:
            file.write("""
Parameters
^^^^^^^^^^
""")
            for pname, param in config["parameters"].items():
                header = "{} - {}".format(pname, param.get("short_desc"))
                file.write("\n{}\n{}\n\n".format(header, "\""*len(header)))
                if "description" in param:
                    file.write(param["description"])
                    file.write("\n\n")
                for key, value in param.items():
                    if key[-5:] == "_html":
                        continue
                    if key in invalid_fields:
                        continue
                    file.write("- **{}**: {}\n".format(key, str(value)))
                file.write("\n")
        file.write("\n")

with open("distances.rst", "w") as file:
    file.write("""
Distance-Measures
*****************

""")
    for name, config in sorted(settings.distances.items()):
        print("##############")
        print(name,config)
        header = "{} - ({})".format(config.get("name", name), name)
        file.write(header)
        file.write("\n{}\n\n".format("="*len(header)))
        file.write(config.get("description", ""))
        file.write("\n")

        if "parameters" in config:
            file.write("""
Parameters
^^^^^^^^^^
""")
            for pname, param in config["parameters"].items():
                header = "{} - {}".format(pname, param.get("short_desc"))
                file.write("\n{}\n{}\n\n".format(header, "\""*len(header)))
                if "description" in param:
                    file.write(param["description"])
                    file.write("\n\n")
                for key, value in param.items():
                    if key[-5:] == "_html":
                        continue
                    if key in invalid_fields:
                        continue
                    file.write("- **{}**: {}\n".format(key, str(value)))
                file.write("\n")
        file.write("\n")