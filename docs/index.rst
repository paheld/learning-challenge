.. CI learning-challenge documentation master file, created by
   sphinx-quickstart on Mon Jul 14 15:49:21 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CI learning-challenge's documentation!
=================================================

Contents:

.. toctree::
   :maxdepth: 2

   readme
   algorithms
   distances
   config
   webapp
   backend
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

