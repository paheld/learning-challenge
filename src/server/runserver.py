#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import sys
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.django_settings")

import settings
import daemon

import logging
logging.basicConfig(level=settings.LOG_LEVEL)

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

import django
django.setup()

import django.core.handlers.wsgi

import tornado.httpserver
import tornado.ioloop
import tornado.wsgi
import tornado.web
import tornado.websocket
import json
from threading import RLock, Thread


ws_connections = set()
ws_connections_lock = RLock()

class WebSocketHandler(tornado.websocket.WebSocketHandler):
    lock = RLock()

    def open(self):
        with ws_connections_lock:
            ws_connections.add(self)
        self.send_event({"msg":"hello"})

    def on_message(self, message):
        self.send_event({"msg":"echo", "data":message})

    def send_event(self, evt):
        with self.lock:
            self.write_message(json.dumps(evt))

    def on_close(self):
        with ws_connections_lock:
            ws_connections.remove(self)


def process_messages():
    while True:
        msg = settings.msg_queue.get(True)
        #print(msg)
        with ws_connections_lock:
            for con in ws_connections:
                try:
                    con.send_event(msg)
                except:
                    pass


class MyStaticHandler(tornado.web.StaticFileHandler):
    def get_content_type(self):
        if self.absolute_path.split(".")[-1] == "arff":
            return "text/plain"
        return tornado.web.StaticFileHandler.get_content_type(self)


def run():
    msg_processor = Thread(target=process_messages)
    msg_processor.setDaemon(True)
    msg_processor.start()

    wsgi_app = tornado.wsgi.WSGIContainer(django.core.handlers.wsgi.WSGIHandler())
    tornado_app = tornado.web.Application([
        (r'/websocket/', WebSocketHandler),
        (r'/static/(.*)', MyStaticHandler, {'path': settings.DJANGO_STATIC_ROOT}),
        (r'/media/(.*)', MyStaticHandler, {'path': settings.DJANGO_MEDIA_ROOT}),
        ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
      ])
    server = tornado.httpserver.HTTPServer(tornado_app)

    server.listen(settings.TORNADO_PORT)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    if len(sys.argv) == 2:
        d = daemon.Daemon(settings.TORNADO_PIDFILE, run)
        daemon.deamon_manager(d)
    else:
        run()