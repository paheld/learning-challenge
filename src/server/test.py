#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import backend_helpers as bh
from api import API
import settings
import signal

api = API()

#print(bh.get_job_info(1))

#print(api.add_job_picture(7,"giraffe_2.jpg"))


#print(bh.post_result(24,
#                0.8, 0.5,
#                [1,2,3], [4,5,6], [7,8,9],
#                extra_scores_test={"time":1}, extra_scores_validation={"time":2},
#                message="yippie",
#                pictures=["giraffe_2.jpg"],
#                success=True))

#print(api.get_datasets())
#print(bh.get_arff_file(5,"test"))
#print(bh.load_dataset(4))

def handler(signum, frame):
    print(signum, frame)
    raise TimeoutError()

signal.signal(signal.SIGALRM, handler)
signal.alarm(1)

i=0
try:
    while True:
        i+=1
except TimeoutError:
    print(i)