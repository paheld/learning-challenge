#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import logging
import numpy as np

logger = logging.getLogger(__name__)

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


def process(dataset, *_, **__):
    dataset = list(dataset)
    for i in range(0,len(dataset), 2):
        raw_data = np.matrix(dataset[i])
        if raw_data.shape[0] == 0 or raw_data.shape[1] == 0:
            continue

        new_data = np.matrix(np.zeros(raw_data.shape))

        new_data[:,0] = raw_data[:,0]
        new_data[:,1:] = raw_data[:,1:] - raw_data[:,:-1]

        dataset[i] = np.array(new_data)

    return dataset
