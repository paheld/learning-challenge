__author__ = 'cbraune'

from sklearn.preprocessing import Normalizer


def process(dataset, norm='L1', **_):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    if norm == 'L1':
        scaler = Normalizer(norm='l1')
    else:
        scaler = Normalizer(norm='l2')

    if len(training_data) > 0:
        training_data = scaler.transform(training_data)
    test_data = scaler.transform(test_data)
    validation_data = scaler.transform(validation_data)

    return training_data, training_label, test_data, test_label, validation_data, validation_label