__author__ = 'cbraune'

from sklearn.preprocessing import MinMaxScaler
import numpy as np


def process(dataset, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    if 'min' not in kwargs:
        kwargs['min'] = 0
    minim = float(kwargs['min'])

    if 'max' not in kwargs:
        kwargs['max'] = kwargs['min'] + 1
    maxim = float(kwargs['max'])

    minim, maxim = min((minim, maxim)), max((minim, maxim))

    newrange = (minim, maxim)
    scaler = MinMaxScaler(feature_range=newrange)

    if not len(training_data) > 0:  # cluster data set...
        if 'dimensions' in kwargs:
            for dim in kwargs['dimensions']:
                scaler.fit(test_data[:, dim])  # fit scaler to the current dimension
                # Transform each data set individually with the fitted transformation
                test_data[:, dim] = scaler.transform(test_data[:, dim])
                validation_data[:, dim] = scaler.transform(validation_data[:, dim])
    else:
        if 'dimensions' in kwargs:
            for dim in kwargs['dimensions']:
                scaler.fit(training_data[:, dim])  # fit scaler to the current dimension
                # Transform each data set individually with the fitted transformation
                training_data[:, dim] = scaler.transform(training_data[:, dim])
                test_data[:, dim] = scaler.transform(test_data[:, dim])
                validation_data[:, dim] = scaler.transform(validation_data[:, dim])
    return training_data, training_label, test_data, test_label, validation_data, validation_label