__author__ = 'cbraune'

from sklearn.preprocessing import StandardScaler


def process(dataset, dimensions=None, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    if dimensions is None:
        return dataset

    scaler = StandardScaler()

    if not len(training_data) > 0:
        scaler.fit(test_data[:, dimensions])
        test_data[:, dimensions] = scaler.transform(test_data[:, dimensions])
        validation_data[:, dimensions] = scaler.transform(validation_data[:, dimensions])
    else:
        scaler.fit(training_data[:, dimensions])
        training_data[:, dimensions] = scaler.transform(training_data[:, dimensions])
        test_data[:, dimensions] = scaler.transform(test_data[:, dimensions])
        validation_data[:, dimensions] = scaler.transform(validation_data[:, dimensions])

    return training_data, training_label, test_data, test_label, validation_data, validation_label