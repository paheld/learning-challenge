__author__ = 'cbraune'

from sklearn.decomposition import PCA
import numpy as np


def process(dataset, **_):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    pca = PCA()
    if len(training_data) > 0:
        pca.fit(training_data)
        training_data = pca.transform(training_data)
    else:  # cluster data set...
        pca.fit(test_data)
    test_data = pca.transform(test_data)
    validation_data = pca.transform(validation_data)
    return training_data, training_label, test_data, test_label, validation_data, validation_label