from __future__ import print_function, division, unicode_literals, absolute_import, generators
from sklearn.decomposition import KernelPCA

__author__ = 'cbraune'

def process(dataset, kernel='rbf', **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    ndim = 5  # chosen by an unfair dice roll
    if len(training_data) > 0:
        _, ndim = training_data.shape
    else:
        _, ndim = test_data.shape

    pca = None
    if kernel == 'poly':
        pca = KernelPCA(n_components=ndim, kernel='poly', degree=int(kwargs['kernel_degree']), coef0=float(kwargs['kernel_coef0']), gamma=float(kwargs['kernel_gamma']))
    elif kernel == 'linear':
        pca = KernelPCA(n_components=ndim, kernel='linear')
    elif kernel == 'rbf':
        pca = KernelPCA(n_components=ndim, kernel='rbf', gamma=float(kwargs['kernel_gamma']))
    elif kernel == 'sigmoid':
        pca = KernelPCA(n_components=ndim, kernel='sigmoid', coef0=float(kwargs['kernel_coef0']))

    if pca is not None:
        if len(training_data) > 0:
            pca.fit(training_data)
            training_data = pca.transform(training_data)
        else:  # cluster data set...
            pca.fit(test_data)
        test_data = pca.transform(test_data)
        validation_data = pca.transform(validation_data)
    return training_data, training_label, test_data, test_label, validation_data, validation_label