__author__ = 'cbraune'

from sklearn.preprocessing import MinMaxScaler
import numpy as np


def process(dataset, dimensions=None, **_):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    if 'dimensions' is not None:
        if len(training_data) > 0:
            training_data = np.array(training_data)[:, dimensions]
        test_data = np.array(test_data)[:, dimensions]
        validation_data = np.array(validation_data)[:, dimensions]

    return training_data, training_label, test_data, test_label, validation_data, validation_label