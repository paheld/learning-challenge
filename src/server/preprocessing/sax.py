#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import logging
import numpy as np

logger = logging.getLogger(__name__)

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


def process(dataset, symbols=10, *_, **__):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    dataset = list(dataset)
    symbols = int(symbols)

    if len(training_data) > 0:
        t_data = training_data

    else:
        t_data = test_data

    data = t_data.flatten()

    borders = list()
    for x in range(1, symbols):
        borders.append(np.percentile(data, x * 100 / symbols))

    borders.append(np.inf)

    intervals = list()
    intervals.append((-np.inf, borders[0]))
    for i in range(symbols-1):
        intervals.append((borders[i], borders[i+1]))

    for i in range(0,len(dataset), 2):
        raw_data = np.matrix(dataset[i])
        if raw_data.shape[0] == 0 or raw_data.shape[1] == 0:
            continue

        new_data = np.matrix(np.zeros(raw_data.shape), dtype=np.int32)

        for sym, (low, high) in enumerate(intervals):
            new_data[np.logical_and(low <= raw_data, raw_data < high)] = sym

        dataset[i] = np.array(new_data)

    return dataset
