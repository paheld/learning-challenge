#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import logging
import numpy as np

logger = logging.getLogger(__name__)

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


def process(dataset, bins=10, *_, **__):
    # (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    dataset = list(dataset)
    bins = int(bins)

    for i in range(0,len(dataset), 2):
        raw_data = np.matrix(dataset[i])
        if raw_data.shape[0] == 0 or raw_data.shape[1] == 0:
            continue
        means = np.mean(raw_data, axis=1)
        raw_data -= means * np.ones((1, raw_data.shape[1]))
        stds = np.std(raw_data, axis=1)
        raw_data /= stds * np.ones((1, raw_data.shape[1]))

        dims = raw_data.shape[1]

        binned = np.matrix(np.zeros((raw_data.shape[0], bins)))
        for b in range(bins):
            start = int(1.0 * b * dims / bins)
            end = int(1.0 * (b+1) * dims / bins)
            if b == bins-1:
                end = dims
            idx = list(range(start, end))
            if idx:
                binned[:,b] = np.mean(raw_data[:,idx], axis=1)

        dataset[i] = np.array(binned)

    return dataset
