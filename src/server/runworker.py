#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import multiprocessing as mp
import os, os.path
from time import sleep
import signal, sys, daemon, atexit

import matplotlib
matplotlib.use('Agg')

import settings
from backend_helpers import api, process_job, process_preprocessing_job

def signal_handler(*args):
    """Handles the SIGINT signal, to tell workers to stop after finishing there current job.
    """
    try:
        os.remove(settings.BACKEND_RUNFILE)
        print("Quit process started. Waiting for current jobs to be done")
    except:
        pass


class JobCanceledException(Exception):
    pass


def signal_alarm(*_):
    global current_job
    if not api.ping_job(current_job):
        raise JobCanceledException
    try:
        signal.alarm(3)
    except AttributeError:
        pass


signal.signal(signal.SIGINT, signal_handler)
atexit.register(signal_handler)


def worker(delay):
    """Runs a single Worker process.

    This is implemented by an infinity loop which pings the server and processes jobs.
    """
    global current_job
    try:
        signal.signal(signal.SIGALRM, signal_alarm)
    except AttributeError:
        print("Your system does not support SIGALRM")
    while os.path.isfile(settings.BACKEND_RUNFILE):
        jobid = api.ping()
        print("PROCESS JOB: ", jobid)
        if jobid:
            try:
                current_job = jobid
                try:
                    signal.alarm(3)
                except AttributeError:
                    pass
                process_job(jobid)
            except JobCanceledException:
                pass
            try:
                signal.alarm(0)
            except AttributeError:
                pass
            current_job = None
        else:
            if delay > 1:
                for _ in range(int(delay)):
                    if os.path.isfile(settings.BACKEND_RUNFILE):
                        sleep(1)
            else:
                sleep(delay)


def worker_preprocessing(delay):
    """Runs a single Worker process.

    This is implemented by an infinity loop which pings the server and processes jobs.
    """
    while os.path.isfile(settings.BACKEND_RUNFILE):
        ppid = api.ping_preprocessing()
        print("PROCESS PREPROCESSING JOB: ", ppid)
        if ppid:
            process_preprocessing_job(ppid)
        else:
            if delay > 1:
                for _ in range(int(delay)):
                    if os.path.isfile(settings.BACKEND_RUNFILE):
                        sleep(1)
            else:
                sleep(delay)


def run():
    f = open(settings.BACKEND_RUNFILE, 'w+')
    f.write("running\n")
    f.close()
    processes = []
    for x in range(mp.cpu_count()):
        p = mp.Process(target=worker, args=(settings.BACKEND_PING_TIME * mp.cpu_count(),))
        p.start()
        processes.append(p)

        p = mp.Process(target=worker_preprocessing, args=(settings.BACKEND_PING_TIME * mp.cpu_count(),))
        p.start()
        processes.append(p)

        for _ in range(int(settings.BACKEND_PING_TIME)):
            if os.path.isfile(settings.BACKEND_RUNFILE):
                sleep(1)
    for p in processes:
        p.join()

if __name__ == "__main__":
    if len(sys.argv) == 2:
        d = daemon.Daemon(settings.BACKEND_PIDFILE, run)
        daemon.deamon_manager(d)
    else:
        run()