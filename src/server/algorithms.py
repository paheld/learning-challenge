#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators
import learners
import cluster
import preprocessing

__author__ = "Pascal Held, Christian Braune"
__email__ = "paheld@gmail.com, christian.braune79@gmail.com"


###############
#
# (in-)validators
#
###############

"""
Invalidators are check functions which should return an string as error message or a false value if everything is ok

general signature:
invalidator(value, *args, **kwargs)

All parameter settings are propagated as named arguments. Also the ``dataset`` is propagated.
"""


def invalid_optional(value, optional=False, **_):
    """Checking for optional parameters.
    """
    if optional:
        return False
    if value is None or value == "":
        return "Parameter required"
    return False


def invalid_range(value, mini=None, maxi=None, **_):
    """Checking for min and max values.
    """
    if mini is not None and mini > value:
        return "Should be at least {}".format(mini)

    if maxi is not None and maxi < value:
        return "Should be maximal {}".format(maxi)
    return False


def invalid_strictly_larger_than(value, mini=None, **_):
    """Checking whether value is strictly larger than min.
    """
    if mini is not None and mini >= value:
        return "Should be strictly larger than {}".format(min)

    return False


def invalid_list(value, values=None, **_):
    """Checks if value in values.
    """
    if values is not None and value not in values:
        return "Should be one of: {}".format(", ".join([val for val in values]))
    return False


def invalid_multi_list(value, values=None, **_):
    """Checks if value in values.
    """
    if values is not None:
        for v in value:
            if v not in values:
                return "Should be one of: {}".format(", ".join([val for val in values]))
    return False


def invalid_weights(value, dataset=None, **_):
    """Checks whether the right number of weights was specified for the dataset.
    """
    l = len(value.split(","))
    if l < dataset.dimensions:
        return "Too few weights specified. Need %i additional." % (dataset.dimensions - l)
    elif l > dataset.dimensions:
        return "Too many weights specified. Remove %i weights." % (l - dataset.dimensions)
    return False


def invalid_number_of_centers(value, dataset=None, **_):
    """Checks whether the right number of centers was specified for the dataset.
    """
    if value > dataset.nr_validation_points-1:
        return "Too many cluster centers!. At most %i possible." % (dataset.nr_validation_points-1)
    else:
        return False


def invalid_priors(value, dataset=None, **_):
    """Checks whether the right number of priors was specified for the dataset. And if they sum up to one-
    """
    n_cl = dataset.nr_classes
    l = len(value.split(","))
    if l < n_cl:
        return "Too few priors specified. Need %i additional." % (n_cl - l)
    elif l > n_cl:
        return "Too many priors specified. Remove %i priors." % (l - n_cl)
    priors = [float(x) for x in value.split(",")]
    s = sum(priors)
    if abs(s-1.0) > 0.0001:
        return "Priors sum up %2.3f instead of 1.0! Please correct this!" % s
    if any([x <= 0.0 for x in priors]):
        return "All priors must be strictly positive!"

    return False


distances = {
    "euclidean": {
        "name": "Euclidean Distance",
        "short_desc": "The common euclidean distance measure",
        "description": "The distance is calculated as :math:`d(x,y) = \\sqrt{\\sum_{i=0}^n (x_i-y_i)^2}`."
    },
    "weuclidean": {
        "name": "Weighted Euclidean Distance",
        "short_desc": "The weighted euclidean distance measure",
        "description": "The distance is calculated as :math:`d(x,y) = \\sqrt{\\sum_{i=0}^n w_i(x_i-y_i)^2}`.",
        "parameters": {
            "weights": {
                "name": "Weigths",
                "short_desc": "Comma-separated list of weights for each dimension.",
                "type": "string",
                "validator": invalid_weights
            }
        }
    },
    "seuclidean": {
        "name": "Standardized Euclidean Distance",
        "short_desc": "The standardized euclidean distance measure",
        "description": "The distance is calculated as "
                       ":math:`d(x,y) = \\sqrt{\\frac{\\sum_{i=0}^n (x_i-y_i)^2}{s^2_i}}`, "
                       "where :math:`s^2_i` is the standard deviation between the :math:`x_i` and :math:`y_i`"
                       " in the data set."
    },
    "sqeuclidean": {
        "name": "Squared Euclidean Distance",
        "short_desc": "The squared euclidean distance measure",
        "description": "The distance is calculated as :math:`d(x,y) = \\sum_{i=0}^n (x_i-y_i)^2`."
    },
    "hamming": {
        "name": "Hamming Distance",
        "short_desc": "The hamming distance measure",
        "description": "The distance is calculated as the number of components which differ over "
                       "the number of components."
    },
    "manhattan": {
        "name": "Manhattan / Cityblock Distance",
        "short_desc": "The manhattan (or cityblock) distance measure",
        "scipy_name": "cityblock",
        "description": "The distance is calculated as :math:`d(x,y) = \\sum_{i=0}^n |x_i-y_i|`."
    },
    "chebyshev": {
        "name": "Chebyshev Distance / Maximum Norm",
        "short_desc": "The Chebyshev (maximum norm) distance measure",
        "description": "The distance is calculated as :math:`d(x,y) = \\max_{i} |x_i-y_i|`."
    },
    "correlation": {
        "name": "Correlation",
        "short_desc": "Correlation based distance",
        "description": "The distance is calculated as :math:`d(x,y) = 1- \\frac{(u-\\bar{u})\\cdot(v - \\bar{v})}{||(u-\\bar{u})||_2\\cdot ||(v-\\bar{v})||_2}`."
    },
    "minkowski": {
        "name": "Minkowski Distance",
        "short_desc": "The Minkowski distance with parameter ```p```. To turn this into euclidean distance,"
                      " choose p=2.",
        "description": "The distance is calculated as :math:`d(x,y) = (\\sum_{i=0}^n (x_i-y_i)^p)^{1/p}`.",
        "parameters": {
            "p": {
                "name": "p",
                "short_desc": "Parameter of the Minkowski distance (exponent).",
                "type": "float",
                "default": 2,
                "min": 0.001,
            }
        }
    },
    "wminkowski": {
        "name": "Weighted Minkowski Distance",
        "short_desc": "The Minkowski distance with parameter p. To turn this into euclidean distance, choose p=2.",
        "description": "The distance is calculated as :math:`d(x,y) = (\\sum_{i=0}^n (x_i-y_i)^p)^{1/p}`.",
        "parameters": {
            "p": {
                "name": "p",
                "short_desc": "Parameter of the Minkowski distance (exponent).",
                "type": "float",
                "default": 2,
                "min": 0.001,
            },
            "weights": {
                "name": "Weights",
                "short_desc": "Comma-separated list of weights for each dimension.",
                "type": "string",
                "validator": invalid_weights
            }
        }
    }
}

algorithms = {
    "kmeans": {
        "is_cluster": True,
        "name": "k-means",
        "short_desc": "Iteratively calculates ```k``` centers of gravity and assigns points to the nearest center.",
        "description": "This version uses only a single run and thus may get stuck in a local optimum.\n\n"
                       "[WPkmeans]_ k-means clustering is a method of vector quantization, originally from signal "
                       "processing, that is popular for cluster analysis in data mining. k-means clustering aims to "
                       "partition n observations into k clusters in which each observation belongs to the cluster with "
                       "the nearest mean, serving as a prototype of the cluster. This results in a partitioning of the "
                       "data space into Voronoi cells.\n\n"
                       "The problem is computationally difficult (NP-hard); however, there are efficient heuristic "
                       "algorithms that are commonly employed and converge quickly to a local optimum. These are "
                       "usually similar to the expectation-maximization algorithm for mixtures of Gaussian "
                       "distributions via an iterative refinement approach employed by both algorithms. Additionally, "
                       "they both use cluster centers to model the data; however, k-means clustering tends to find "
                       "clusters of comparable spatial extent, while the expectation-maximization mechanism allows "
                       "clusters to have different shapes.\n\n"
                       "The algorithm has nothing to do with and should not be confused with k-nearest neighbor, "
                       "another popular machine learning technique.",
        "references": [
            "[WPkmeans] http://en.wikipedia.org/wiki/K-means_clustering",
        ],
        "worker": cluster.cluster_func(cluster.kmeans.cluster),
        "parameters": {
            "k": {
                "type": "int",
                "min": 2,
                "default": 3,
                "short_desc": "Number of initial cluster centers",
                "description": "The higher this value is chosen the more clusters will be generated. Points can only be"
                               "assigned to one cluster at a time.",
                "validator": invalid_number_of_centers
            },
            "init": {
                "name": "Initialisation method",
                "type": "dict",
                "values": {
                    'random': {
                        "name": "Random Initialization",
                        "parameters": {}
                    },
                    'points': {
                        "name": "Specify initial cluster centers",
                        "parameters": {
                            "points": {
                                "name": "Initial cluster Centers",
                                "short_desc": "List of List of Coordinates",
                                "description": "The format for this parameter is :math:`[[x_{11},x_{12},x_{13},...,"
                                               "x_{1d}],[x_{21},x_{22},x_{23},...,x_{2d}],...,[x_{k1},x_{k2},x_{k3},..."
                                               ",x_{kd}]]`. (You will receive error messages, if you do not adhere to "
                                               "this format. And we will tell you to RTFM!. ;)",
                                "type": "string",
                            }
                        }
                    }
                }
            }
        }
    },
    "kmedian": {
        "is_cluster": True,
        "name": "k-medians",
        "short_desc": "Iteratively calculates ```k``` centers of gravity (medians) and assigns points to the nearest "
                      "center.",
        "description": "This version uses only a single run and thus may get stuck in a local optimum.\n\n"
                       "[WPkmedians]_ In statistics and data mining, k-medians clustering is a cluster analysis "
                       "algorithm. It is a variation of k-means clustering where instead of calculating the mean for "
                       "each cluster to determine its centroid, one instead calculates the median. This has the effect "
                       "of minimizing error over all clusters with respect to the 1-norm distance metric, as opposed "
                       "to the square of the 2-norm distance metric (which k-means does).\n\n"
                       "This relates directly to the k-median problem which is the problem of finding k centers such "
                       "that the clusters formed by them are the most compact. Formally, given a set of data points "
                       "x, the k centers ci are to be chosen so as to minimize the sum of the distances from each x to "
                       "the nearest :math:`c_i`.\n\n"
                       "The criterion function formulated in this way is sometimes a better criterion than that used "
                       "in the k-means clustering algorithm, in which the sum of the squared distances is used. The "
                       "sum of distances is widely used in applications such as facility location.\n\n"
                       "The proposed algorithm uses Lloyd-style iteration which alternates between an expectation (E) "
                       "and maximization (M) step, making this an Expectation–maximization algorithm. In the E step, "
                       "all objects are assigned to their nearest median. In the M step, the medians are recomputed by "
                       "using the median in each single dimension.",
        "references": [
            "[WPkmedians] http://en.wikipedia.org/wiki/K-medians_clustering",
        ],
        "worker": cluster.cluster_func(cluster.kmedian.cluster),
        "parameters": {
            "k": {
                "type": "int",
                "min": 2,
                "default": 3,
                "short_desc": "Number of initial cluster centers",
                "description": "The higher this value is chosen the more clusters will be generated. Points can only be"
                               "assigned to one cluster at a time.",
                "validator": invalid_number_of_centers
            },
            "init": {
                "name": "Initialisation method",
                "type": "dict",
                "values": {
                    'random': {
                        "name": "Random Initialization",
                        "parameters": {}
                    },
                    'points': {
                        "name": "Specify initial cluster centers",
                        "parameters": {
                            "points": {
                                "name": "Initial cluster Centers",
                                "short_desc": "List of List of Coordinates",
                                "description": "The format for this parameter is :math:`[[x_{11},x_{12},x_{13},...,"
                                               "x_{1d}],[x_{21},x_{22},x_{23},...,x_{2d}],...,[x_{k1},x_{k2},x_{k3},..."
                                               ",x_{kd}]]`. (You will receive error messages, if you do not adhere to "
                                               "this format. And we will tell you to RTFM!. ;)",
                                "type": "string",
                            }
                        }
                    }
                }
            }
        }
    },
    "hac": {
        "is_cluster": True,
        "name": "Hierarchical clustering",
        "short_desc": "Assuming that every point is a cluster, starts by iteratively merging clusters until only on "
                      "cluster remains.",
        "description": "[WPhac]_ In data mining, hierarchical clustering (also called hierarchical cluster analysis or "
                       "HCA) is a method of cluster analysis which seeks to build a hierarchy of clusters. Strategies "
                       "for hierarchical clustering generally fall into two types:\n\n"
                       "Agglomerative: This is a 'bottom up' approach: each observation starts in its own cluster, and "
                       "pairs of clusters are merged as one moves up the hierarchy.\n\n"
                       "Divisive: This is a 'top down' approach: all observations start in one cluster, and splits are "
                       "performed recursively as one moves down the hierarchy.\n\n"
                       "In general, the merges and splits are determined in a greedy manner. The results of "
                       "hierarchical clustering are usually presented in a dendrogram.\n\n"
                       "In the general case, the complexity of agglomerative clustering is O(n^3), which makes them "
                       "too slow for large data sets. Divisive clustering with an exhaustive search is O(2^n), which "
                       "is even worse. However, for some special cases, optimal efficient agglomerative methods (of "
                       "complexity O(n^2)) are known: SLINK for single-linkage and CLINK for complete-linkage "
                       "clustering.",
        "references": [
            "[WPhac] http://en.wikipedia.org/wiki/Hierarchical_clustering",
        ],
        "worker": cluster.cluster_func(cluster.hac.cluster),
        "parameters": {
            "k": {
                "type": "int",
                "min": 2,
                "default": 2,
                "short_desc": "Number of clusters",
                "description": "The higher this value is chosen the more clusters will be generated. Points can only be"
                               "assigned to one cluster at a time.",
                "validator": invalid_number_of_centers
            },
            "linkage": {
                "name": "Linkage method",
                "type": "dict",
                "values": {
                    'single': {
                        "name": "Single Linkage",
                        "parameters": {
                            "distance": {
                                "name": "Distance measure",
                                "type": "dist",
                                "short_desc": "Used distance measure",
                            }
                        }
                    },
                    'complete': {
                        "name": "Complete Linkage",
                        "parameters": {
                            "distance": {
                                "name": "Distance measure",
                                "type": "dist",
                                "short_desc": "Used distance measure",
                            }
                        }
                    },
                    'average': {
                        "name": "Average Linkage",
                        "parameters": {
                            "distance": {
                                "name": "Distance measure",
                                "type": "dist",
                                "short_desc": "Used distance measure",
                            }
                        }
                    },
                    'weighted': {
                        "name": "Weighted Linkage",
                        "parameters": {
                            "distance": {
                                "name": "Distance measure",
                                "type": "dist",
                                "short_desc": "Used distance measure",
                            }
                        }
                    },
                    'centroid': {
                        "name": "Centroid Linkage",
                        "parameters": {
                            "distance": {
                                "name": "Distance measure",
                                "type": "dist",
                                "short_desc": "Used distance measure",
                                "values": ['euclidean']
                            }
                        }
                    },
                    'median': {
                        "name": "Median Linkage",
                        "parameters": {
                            "distance": {
                                "name": "Distance measure",
                                "type": "dist",
                                "short_desc": "Used distance measure",
                                "values": ['euclidean']
                            }
                        }
                    },
                    'ward': {
                        "name": "Ward Linkage",
                        "parameters": {
                            "distance": {
                                "name": "Distance measure",
                                "type": "dist",
                                "short_desc": "Used distance measure",
                                "values": ['euclidean']
                            }
                        }
                    },
                }
            }
        }
    },
    "mst": {
        "is_cluster": True,
        "name": "Minimum Spanning Tree",
        "short_desc": "Build a minimum spanning tree and remove the :math:`k-1` longest edges. The remaining connected"
                      "components are the clusters.",
        "description": "[WPmst]_ Given a connected, undirected graph, a spanning tree of that graph is a subgraph that "
                       "is a tree and connects all the vertices together. A single graph can have many different "
                       "spanning trees. We can also assign a weight to each edge, which is a number representing how "
                       "unfavorable it is, and use this to assign a weight to a spanning tree by computing the sum of "
                       "the weights of the edges in that spanning tree. A minimum spanning tree (MST) or minimum "
                       "weight spanning tree is then a spanning tree with weight less than or equal to the weight of "
                       "every other spanning tree. More generally, any undirected graph (not necessarily connected) "
                       "has a minimum spanning forest, which is a union of minimum spanning trees for its connected "
                       "components.",
        "references": [
            "[WPmst] http://en.wikipedia.org/wiki/Minimum_spanning_tree",
        ],
        "worker": cluster.cluster_func(cluster.mst.cluster),
        "parameters": {
            "k": {
                "type": "int",
                "min": 2,
                "default": 2,
                "short_desc": "Number of clusters",
                "description": "The higher this value is chosen the more clusters will be generated. Points can only be"
                               "assigned to one cluster at a time.",
                "validator": invalid_number_of_centers
            },
            "d": {
                "type": "dist",
                "short_desc": "Distance Metric",
                "description": "Edge weights are assigned according to this metric."
            },
        }
    },
    "spectral": {
        "is_cluster": True,
        "name": "Spectral Clustering",
        "short_desc": "Using normalized graph cuts clusters are constructed from the data.",
        "description": "[WPspectral]_ In multivariate statistics and the clustering of data, spectral clustering "
                       "techniques make use of the spectrum (eigenvalues) of the similarity matrix of the data to "
                       "perform dimensionality reduction before clustering in fewer dimensions. The similarity matrix "
                       "is provided as an input and consists of a quantitative assessment of the relative similarity "
                       "of each pair of points in the dataset.\n\n"
                       "In application to image segmentation, spectral clustering is known as segmentation-based "
                       "object categorization.",
        "references": [
            "[WPspectral] http://en.wikipedia.org/wiki/Spectral_clustering",
        ],
        "worker": cluster.cluster_func(cluster.spectral.cluster),
        "parameters": {
            "k": {
                "type": "int",
                "min": 2,
                "default": 2,
                "short_desc": "Number of clusters",
                "description": "The higher this value is chosen the more clusters will be generated. Points can only be"
                               "assigned to one cluster at a time.",
                "validator": invalid_number_of_centers
            },
            "affinity": {
                "type": "dict",
                "short_desc": "Affinity type",
                "description": "The affinity matrix describes the relationship of the samples to embed.",
                "values": {
                    "knn": {
                        "name": "K-NN Graph",
                        "parameters": {
                            "k": {
                                "type": "int",
                                "min": 3,
                                "max": 25,
                                "default": 7,
                                "short_desc": "Number of neighbors to use to build the neighborhood graph.",
                            }
                        }
                    },
                    "rbf": {
                        "name": "Gauss Kernel",
                        "parameters": {
                            "gamma": {
                                "type": "float",
                                "min": 0.0,
                                "default": 0.0,
                                "short_desc": "Scaling factor.",
                            }
                        }
                    }
                }
            },
            "assign": {
                "type": "list",
                "name": "Assignment Strategy",
                "short_desc": "Strategy used to assign labels.",
                "description": "The strategy to use to assign labels in the embedding space. There are two ways to "
                               "assign labels after the laplacian embedding. k-means can be applied and is a popular "
                               "choice. But it can also be sensitive to initialization. Discretization is another "
                               "approach which is less sensitive to random initialization.",
                "values": ['kmeans', 'discretize']
            }
        }
    },
    "gmm": {
        "is_cluster": True,
        "name": "Gaussian Mixture Model",
        "short_desc": "A Gaussian Mixture Model tries to fit several multi-variate distributions to the data.",
        "description": "[WPgmm]_ In statistics, a mixture model is a probabilistic model for representing the presence "
                       "of subpopulations within an overall population, without requiring that an observed data set "
                       "should identify the sub-population to which an individual observation belongs. Formally a "
                       "mixture model corresponds to the mixture distribution that represents the probability "
                       "distribution of observations in the overall population. However, while problems associated "
                       "with 'mixture distributions' relate to deriving the properties of the overall population from "
                       "those of the sub-populations, 'mixture models' are used to make statistical inferences about "
                       "the properties of the sub-populations given only observations on the pooled population, "
                       "without sub-population identity information.",
        "references": [
            "[WPgmm] http://en.wikipedia.org/wiki/Mixture_model",
        ],
        "worker": cluster.cluster_func(cluster.gmm.cluster),
        "parameters": {
            "k": {
                "type": "int",
                "min": 2,
                "default": 2,
                "short_desc": "Number of clusters",
                "description": "The higher this value is chosen the more clusters will be generated. Points can only be"
                               "assigned to one cluster at a time.",
                "validator": invalid_number_of_centers
            },
            "covariance": {
                "type": "dict",
                "short_desc": "Covariance Matrix",
                "description": "Shape of the covariance matrix.",
                "values": {
                    "spherical": {
                        "name": "Spherical",
                        "short_desc": "All variances are the same. All covariances are zero. The distribution forms a "
                                      "hypersphere."
                    },
                    "diag": {
                        "name": "Diagonal",
                        "short_desc": "All variances can be different. All covariances are zero. The distribution "
                                      "forms an axis-parallel hyper-ellipsoid."
                    },
                    "tied": {
                        "name": "Tied",
                        "short_desc": "???"
                    },
                    "full": {
                        "name": "Full",
                        "short_desc": "All variances can be different. All covariances can be non-zero. The "
                                      "distribution forms a hyper-ellipsoid."
                    }
                }
            },
            "initialisation": {
                "type": "dict",
                "name": "Initialisation",
                "short_desc": "Which parameters should be estimated for initialisation?",
                "multi": True,
                "values": {
                    "w": {
                        "name": "Weights",
                    },
                    "m": {
                        "name": "Means",
                    },
                    "c": {
                        "name": "Covariance",
                    }
                }
            }
        }
    },
    "affinity": {
        "is_cluster": True,
        "name": "Affinity Propagation",
        "short_desc": ".",
        "description": "[WPaffinity]_ In statistics and data mining, affinity propagation (AP) is a clustering "
                       "algorithm based on the concept of 'message passing' between data points. Unlike clustering "
                       "algorithms such as k-means or k-medoids, AP does not require the number of clusters to be "
                       "determined or estimated before running the algorithm. Like k-medoids, AP finds 'exemplars', "
                       "members of the input set that are representative of clusters.",
        "references": [
            "[WPaffinity] http://en.wikipedia.org/wiki/Affinity_propagation",
        ],
        "worker": cluster.cluster_func(cluster.affinity.cluster),
        "parameters": {
            "preferences": {
                "type": "float",
                "min": -1000,
                "max": -0.001,
                "default": -50,
                "short_desc": "Affinity",
                "description": "Describes the preference for each point to become a cluster examplar.",
            },
            "damping": {
                "type": "float",
                "min": 0.5,
                "max": 1.0,
                "default": 0.5,
                "step": 0.1,
                "short_desc": "Damping",
                "description": "Damping factor",
            },
            "max_iter": {
                "type": "int",
                "min": 1,
                "max": 3000,
                "default": 50,
                "short_desc": "Iterations",
                "description": "How many iterations should be calculated.",
            },
            "metric": {
                "type": "dist",
                "short_desc": "Distance Metric",
                "description": "Affinities are  calculated according to this metric."
            },
        }
    },
    "dbscan": {
        "is_cluster": True,
        "name": "DBScan",
        "short_desc": "Finds dense regions of points in a data set. Regions are dense, if at least '''minPts''' points"
                      "lie near (maximal distance: '''epsilon''') a point.",
        "description": "[WPdbscan]_ Density-based spatial clustering of applications with noise (DBSCAN) is a data "
                       "clustering algorithm proposed by Martin Ester, Hans-Peter Kriegel, Jörg Sander and Xiaowei Xu "
                       "in 1996. It is a density-based clustering algorithm: given a set of points in some space, it "
                       "groups together points that are closely packed together (points with many nearby neighbors), "
                       "marking as outliers points that lie alone in low-density regions (whose nearest neighbors are "
                       "too far away). DBSCAN is one of the most common clustering algorithms and also most cited in "
                       "scientific literature.\n\n "
                       "In 2014, the algorithm was awarded the test of time award (an award given to algorithms which "
                       "have received substantial attention in theory and practice) at the leading data mining "
                       "conference, KDD.",
        "references": [
            "[WPdbscan] http://en.wikipedia.org/wiki/DBSCAN",
        ],
        "worker": cluster.cluster_func(cluster.dbscan.cluster),
        "parameters": {
            "eps": {
                "type": "float",
                "min": 0.000001,
                "default": 0.5,
                "step": 1.0,
                "short_desc": "Epsilon",
                "description": "Distance until which a point is considered to be '''close''' to another point.",
            },
            "minpts": {
                "type": "int",
                "min": 1,
                "max": 1000,
                "default": 5,
                "short_desc": "Minimum Points",
                "description": "Determines how many points must lie with a certain radius of a point for it to become a"
                               " cluster core.",
            },
            "metric": {
                "type": "dist",
                "short_desc": "Distance Metric",
                "values": ['manhattan', 'euclidean', 'seuclidean', 'sqeuclidean', 'minkowski', 'correlation'],
                "description": "Distances are calculated according to this metric."
            },
        }
    },
    "optics": {
        "is_cluster": True,
        "name": "OPTICS",
        "short_desc": "OPTICS inverses the density notion of DBSCAN, thus asking: How little density can I accept?",
        "description": "[WPoptics]_ Ordering points to identify the clustering structure (OPTICS) is an algorithm for "
                       "finding density-based clusters in spatial data. It was presented by Mihael Ankerst, Markus M. "
                       "Breunig, Hans-Peter Kriegel and Jörg Sander. Its basic idea is similar to DBSCAN, but it "
                       "addresses one of DBSCAN's major weaknesses: the problem of detecting meaningful clusters in "
                       "data of varying density. In order to do so, the points of the database are (linearly) ordered "
                       "such that points which are spatially closest become neighbors in the ordering. Additionally, a "
                       "special distance is stored for each point that represents the density that needs to be "
                       "accepted for a cluster in order to have both points belong to the same cluster. ",
        "references": [
            "[WPoptics] http://en.wikipedia.org/wiki/OPTICS",
        ],
        "worker": cluster.cluster_func(cluster.optics.cluster),
        "parameters": {
            "eps": {
                "type": "float",
                "min": 0.000001,
                "default": 0.5,
                "step": 1.0,
                "short_desc": "Epsilon",
                "description": "Distance until which a point is considered to be '''close''' to another point.",
            },
            "minpts": {
                "type": "int",
                "min": 1,
                "max": 1000,
                "default": 5,
                "short_desc": "Minimum Points",
                "description": "Determines how many points must lie with a certain radius of a point for it to become a"
                               " cluster core.",
            }
        }
    },
    "meanshift": {
        "is_cluster": True,
        "name": "MeanShift",
        "short_desc": "Mean shift is a non-parametric feature-space analysis technique for locating "
                      "the maxima of a density function, a so-called mode-seeking algorithm. Application domains "
                      "include cluster analysis in computer vision and image processing.",
        "description": "[WPmeanshift]_ Mean shift is a non-parametric feature-space analysis technique for locating "
                       "the maxima of a density function, a so-called mode-seeking algorithm. Application domains "
                       "include cluster analysis in computer vision and image processing.",
        "references": [
            "[WPmeanshift] http://en.wikipedia.org/wiki/Mean_shift",
        ],
        "worker": cluster.cluster_func(cluster.meanshift.cluster),
        "parameters": {
            "bandwidth": {
                "name": "Bandwidth",
                "type": "float",
                "min": 0.000001,
                "default": 0.5,
                "step": 1.0,
                "short_desc": "Bandwidth used in the RBF kernel.",
                "description": "Distance until which a point is considered to be '''close''' to another point.",
            },
            "clusterall": {
                "name": "Cluster all points",
                "type": "bool",
                "default": True,
                "short_desc": "All points should belong to a cluster?",
                "description": "If not checked, outliers will become noise.",
            },
            "binseeding": {
                "name": "Use bins for initialisation",
                "type": "bool",
                "default": True,
                "short_desc": "If true, initial kernel locations are not locations of all points, but rather the "
                              "location of the discretized version of points, where points are binned onto a grid "
                              "whose coarseness corresponds to the bandwidth. ",
            },
            "minbinfreq": {
                "name": "Minimum number of points per bin",
                "type": "int",
                "default": 1,
                "min": 1,
                "short_desc": "Only applicable if bins are used for initialisation!!!",
                "description": "To speed up the algorithm, accept only those bins with at least this many points as "
                              "seeds.",
            },
        }
    },
    "triclust": {
        "is_cluster": True,
        "name": "TRICLUST",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": cluster.cluster_func(cluster.triclust.cluster),
        "parameters": {
            "a": {
                "name": "A",
                "short_desc": "Weight for Mean(P)",
                "type": "float",
                "min": 0.000001,
                "default": 1,
                "step": 1.0,
            },
            "b": {
                "name": "B",
                "short_desc": "Weight for DM(P)",
                "type": "float",
                "min": 0.000001,
                "default": 1,
                "step": 1.0,
            },
            "c": {
                "name": "C",
                "short_desc": "Weight for PDM(P)",
                "type": "float",
                "min": 0.000001,
                "default": 1,
                "step": 1.0,
            },
            "k": {
                "name": "Number of Bins",
                "type": "int",
                "min": 2,
                "default": 100,
                "step": 1,
            },
            "th": {
                "name": "Threshholding method",
                "type": "dict",
                "values": {
                    "arithmetic": {
                        "name": "Arithmetic Mean"
                    },
                    "harmonic": {
                        "name": "Harmonic Mean"
                    }
                },
                "default": "arithmetic"
            },
        }
    },
    "diana": {
        "is_cluster": True,
        "author": "Anja Felgenhauer, Björn Golla",
        "name": "DIANA",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.diana.cluster),
        "parameters": {
            "k": {
                "type": "float",
                "min": 0,
                "default": 3,
                "short_desc": "Either the number of clusters (if >= 1) or the divisive coefficient."
            },
            "metric": {
                "name": "Distance measure",
                "type": "dist",
                "short_desc": "Used distance measure",
                "values": ['euclidean', 'manhattan', 'seuclidean', 'sqeuclidean', 'chebyshev']
            },
        }
    },
    "comeans": {
        "is_cluster": True,
        "author": "Sebastian Steuber, Michael Kropp",
        "name": "co-Means",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.comeans.cluster),
        "parameters": {
            "k": {
                "type": "float",
                "min": 0,
                "default": 3,
                "short_desc": "Number of clusters"
            },
            "maxiter": {
                "name": "Maximum no. of iterations",
                "type": "int",
                "default": 100,
                "min": 1
            },
            "mustlink": {
                "name": "Must-Link constraints",
                "type": "string",
                "optional": True,
            },
            "cannotlink": {
                "name": "Cannot-Link constraints",
                "type": "string",
                "optional": True,
            }
        }
    },
    "fuzzyhac": {
        "is_cluster": True,
        "author": "Fabian Witt, Steven Brandt",
        "needs_ARFF_File": True,
        "name": "fuzzy-HACs",
        "short_desc": "",
        "description": "",
        "worker": cluster.fuzzyhac.cluster,
        "parameters": {
            "nocluster": {
                "type": "float",
                "min": 0,
                "default": 3,
                "short_desc": "Number of clusters"
            },
            "k": {
                "name": "Magic Variable",
                "type": "int",
                "default": 2,
                "min": 1
            },
            "linkage": {
                "name": "Linkage criterion",
                "type": "list",
                "values": ['single', 'complete', 'average']
            }
        }
    },
    "snn": {
        "is_cluster": True,
        "author": "Patrick und Dominik Schön",
        "name": "Shared Nearest Neighbour",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.snn.cluster),
        "parameters": {
            "k": {
                "name": "K",
                "type": "int",
                "min": 2,
                "default": 3,
                "short_desc": "Number of nieghbours"
            },
            "eps": {
                "name": "Epsilon",
                "type": "float",
                "default": 1,
                "min": 0.01,
                "step": 0.01
            },
            "minpts": {
                "name": "Min Points",
                "type": "int",
                "min": 2,
                "default": 10,
                "short_desc": "Minimum number of Points"
            }
        }
    },
    "fuzzydbscan": {
        "is_cluster": True,
        "author": "Chris Long, Arne Schneck",
        "name": "Fuzzy DBSCAN",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.fuzzydbs.cluster),
        "parameters": {
            "eps": {
                "name": "Epsilon",
                "type": "float",
                "default": 1,
                "min": 0.01,
                "step": 0.01
            },
            "minptsmin": {
                "name": "Minimum number of Min Points",
                "type": "int",
                "min": 2,
                "default": 5,
                "short_desc": "Minimum number of Points"
            },
            "minptsmax": {
                "name": "Maximal number of Min Points",
                "type": "int",
                "min": 2,
                "default": 10,
                "short_desc": "Maximum number of Points"
            }
        }
    },
    "fuzzydbscan2016": {
        "is_cluster": True,
        "author": "Joel Dierkes, Magnus Rafn Tiedemann",
        "name": "Fuzzy DBSCAN 2016",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.fuzzy_dbscan_2016.cluster),
        "parameters": {
            "dist": {
                "name": "Distance",
                "type": "dist",
                "default": "euclidean",
            },
            "eps": {
                "name": "Epsilon",
                "type": "float",
                "default": 1,
                "min": 0.01,
                "step": 0.01
            },
            "minmin": {
                "name": "Minimum number of Min Points",
                "type": "int",
                "min": 1,
                "default": 1,
                "short_desc": "Minimum number of Points"
            },
            "minmax": {
                "name": "Maximal number of Min Points",
                "type": "int",
                "min": 1,
                "default": 5,
                "short_desc": "Maximum number of Points"
            }
        }
    },
    "som2016": {
        "is_cluster": True,
        "author": "Elke Grabe, Jan Schumacher",
        "name": "SOM 2016",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.som_2016.som_clustering_withPlot),
        "parameters": {
            "tMax": {
                "name": "Number of iterations",
                "type": "int",
                "default": 1000,
                "min": 1,
                "step": 1,
            },
            "numberOfNeurons": {
                "name": "Number of neurons",
                "type": "int",
                "min": 1,
                "default": 100,
            },
            "eStart": {
                "name": "eStart",
                "type": "float",
                "min": 0,
                "default": 0.8,
            },
            "eEnd": {
                "name": "eEnd",
                "type": "float",
                "min": 0,
                "default": 0.5,
            },
        }
    },
    "nscabdt2016": {
        "is_cluster": True,
        "author": "David Magnus Henriques",
        "name": "NSCABDT 2016",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.nscabdt_2016.clustering),
        "parameters": {
            "plot": {
                "name": "Plot images",
                "type": "bool",
            },
        }
    },
    "ascdt2016": {
        "is_cluster": True,
        "author": "David Bögelsack, Alexander Wilhelm",
        "name": "ASCDT 2016",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.ascdt_2016.cluster),
        "parameters": {
            "beta": {
                "name": "beta",
                "type": "float",
                "short_desc": "This is :math:`\\beta`",
                "description": "This is Patrick!",
                "default": 0,
                "step": 0.1
            },
        }
    },
    "ant2016": {
        "is_cluster": True,
        "author": "Philipp Bergt, Martin Wieczorek",
        "name": "ANT 2016",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.ant_2016.cluster),
        "parameters": {
            "num_iterations": {
                "name": "num iterations",
                "type": "int",
                "default": 2500,
                "min": 1,
                "max": 10000,
                "step": 1,
            },
            "kp": {
                "name": "kp",
                "type": "float",
                "default": 0.05,
                "step": 0.01,
                "min": 0,
                "max": 1,
            },
            "kc": {
                "name": "kc",
                "type": "float",
                "default": 0.4,
                "step": 0.1,
                "min": 0,
                "max": 1,
            },
            "alpha": {
                "name": "alpha",
                "type": "float",
                "default": 1.5,
                "step": 0.1,
            },
            "number_ants": {
                "name": "number_ants",
                "type": "int",
                "default": 20,
                "step": 1,
                "min": 1,
                "max": 1000,
            },
            "alpha1": {
                "name": "alpha1",
                "type": "float",
                "default": 0.3,
                "step": 0.1,
            },
            "s": {
                "name": "s",
                "type": "int",
                "default": 100,
                "step": 1
            },
        }
    },
    #i=100, k=5, s=20, t=0.4
    "spc2016": {
        "is_cluster": True,
        "author": "Dominik Hamann, Mitch Köhler",
        "name": "SPC 2016",
        "short_desc": "",
        "description": "",
        "worker": cluster.spc_2016.cluster,
        "needs_ARFF_File": True,
        "parameters": {
            "i": {
                "name": "iteration",
                "type": "int",
                "default": 100,
                "step": 1,
                "min": 1,
            },
            "k": {
                "name": "k-Nearest Neighbors",
                "type": "int",
                "default": 5,
                "step": 1,
                "min": 1,
            },
            "s": {
                "name": "spins",
                "type": "int",
                "default": 5,
                "step": 1,
                "min": 1,
            },
            "t": {
                "name": "threshold",
                "type": "float",
                "default": 0.4,
                "step": 0.1,
                "min": 0,
                "max": 1,
            },
        }
    },
    "studentem": {
        "is_cluster": True,
        "author": "Lars Grotehenne, Frederick Sander",
        "needs_ARFF_File": True,
        "name": "EM with student-t distribution",
        "short_desc": "",
        "description": "",
        "worker": cluster.studentem.cluster,
        "parameters": {
            "k": {
                "name": "K",
                "type": "int",
                "min": 2,
                "default": 3,
                "short_desc": "Number of Clusters"
            },
            "maxiter": {
                "name": "Maximal number of iterations",
                "type": "int",
                "min": 1,
                "default": 150,
            }
        }
    },
    "spc": {
        "is_cluster": True,
        "author": "Andreas Pfohl",
        "name": "Super Paramagnetic Clustering",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.spc.cluster),
        "parameters": {
            "neighbors": {
                "name": "Neighbors",
                "type": "int",
                "min": 2,
                "default": 11,
                "short_desc": "Number of Neighbors"
            },
            "spins": {
                "name": "Number of different spin states",
                "type": "int",
                "min": 2,
                "default": 20,
            },
            "iterations": {
                "name": "Number of iterations",
                "type": "int",
                "min": 2,
                "default": 20,
            },
            "temperature": {
                "name": "Temperature",
                "type": "float",
                "default": 0.05,
                "min": 0.01,
                "step": 0.01
            },
            "metric": {
                "name": "Distance measure",
                "type": "dist",
                "short_desc": "Used distance measure",
                "values": ['euclidean', 'manhattan', 'seuclidean', 'sqeuclidean', 'chebyshev']
            }
        }
    },
    "kNN": {
        "is_cluster": False,
        "name": "k-Nearest Neighbors",
        "short_desc": "Uses the ```k``` nearest neighbors and select the most probable class.",
        "description": "[WPknn]_ In pattern recognition, the k-Nearest Neighbors algorithm (or k-NN for short) is a "
                       "non-parametric method used for classification and regression. In both cases, the input consists"
                       " of the k closest training examples in the feature space. k-NN is a type of instance-based"
                       " learning, or lazy learning, where the function is only approximated locally and all "
                       "computation is deferred until classification. The k-NN algorithm is among the simplest of all "
                       "machine learning algorithms.",
        "worker": learners.knn.do_stuff,
        "references": [
            "[WPknn] http://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm",
        ],
        "parameters": {
            "k": {
                "type": "int",
                "min": 1,
                "default": 3,
                "short_desc": "Number of neighbors",
                "description": "The higher this value is chosen the more neighboring points can ```vote``` for the "
                               "class label of the target instance. Too low values may be prone to noise, too high "
                               "values may be prone to unbalanced class distributions."
            },
            "distance": {
                "name": "Distance measure",
                "type": "dist",
                "values": [x for x in distances if x is not 'weuclidean'], # Strange sklearn reasons
                "short_desc": "Used distance measure",
                "description": "Based on this metric the ```k``` nearest neighbors are chosen."
            },
            "weights": {
                "name": "Weighting method",
                "type": "list",
                "short_desc": "Method used for instance weighting.",
                "description": "If ```uniform``` is used, each of the nearest neighbours will have equal weight in the "
                               "prediction of the new class label while ```weighted``` uses the inverses of their "
                               "respective distances as weights.",
                "values": ['uniform', 'distance']
            }
        }
    },
    "dtree": {
        "is_cluster": False,
        "name": "Decision Trees",
        "short_desc": "Uses axis-aligned decision borders to classify data.",
        "description": "(wikipedia) A decision tree is a decision support tool that uses a tree-like graph or model of "
                       "decisions and their possible consequences, including chance event outcomes, resource costs, "
                       "and utility. It is one way to display an algorithm."
                       "Decision trees are commonly used in operations research, specifically in decision analysis, to "
                       "help identify a strategy most likely to reach a goal."
                       "\n\n"
                       "See also: [QR1986]_ and [QR1987]_",
        "worker": learners.dtree.do_stuff,
        "references": [
            "[QR1986] Quinlan, J. R. Ross. Induction of decision trees. Machine learning, 1986, 1. Jg., Nr. 1, S. "
            "81-106.",
            "[QR1987] Quinlan, J. R. Ross. Simplifying decision trees. International journal of man-machine studies, "
            "1987, 27. Jg., Nr. 3, S. 221-234."
            ],
        "parameters": {
            "criterion": {
                "name": "Split criterion",
                "type": "list",
                "short_desc": "Measure of split quality",
                "description": "```gini``` uses Gini's impurity measure, ```entropy``` uses the Information Gain.",
                "values": ['gini', 'entropy']
            },
            "splitter": {
                "name": "Splitting strategy",
                "type": "list",
                "short_desc": "Which strategy should be used to choose the next split attribute?",
                "description": "```Best``` always chooses the attribut that maximizes the score obtained via the split "
                               "quality criterion, ```random``` just chooses a ... random... criterion.",
                "values": ['best', 'random']
            },
            "max_depth": {
                "name": "Maximum tree depth",
                "type": "int",
                "min": 0,
                "short_desc": "Maximum depth of a tree.",
                "description": "If 0 then leaves are expanded until completely pure or all attributes have been used. "
                               "Otherwise tree growth will stop after the set amount of attributes has been used."
            }
        }
    },
    "bayesnet": {
        "is_cluster": False,
        "name": "Bayesian Network",
        "short_desc": "Instead of assuming complete independency of all attributes a network structure of conditional "
                      "dependencies is learned and used to classify data.",
        "description": "[WPbayesnet]_ A Bayesian network, Bayes network, belief network, Bayes(ian) model or "
                       "probabilistic directed acyclic graphical model is a probabilistic graphical model "
                       "(a type of statistical model) that represents a set of random variables and their conditional "
                       "dependencies via a directed acyclic graph (DAG). "
                       "For example, a Bayesian network could represent the probabilistic relationships between "
                       "diseases and symptoms. Given symptoms, the network can be used to "
                       "compute the probabilities of the presence of various diseases.\\n\\n"
                       "Formally, Bayesian networks are DAGs whose nodes represent random variables in the Bayesian "
                       "sense: they may be observable quantities, latent variables, "
                       "unknown parameters or hypotheses. Edges represent conditional dependencies; nodes that are not "
                       "connected represent variables that are conditionally "
                       "independent of each other. Each node is associated with a probability function that takes as "
                       "input a particular set of values for the node's parent variables "
                       "and gives the probability of the variable represented by the node. For example, if the parents "
                       "are m Boolean variables then the probability function could be "
                       "represented by a table of 2^m entries, one entry for each of the 2^m possible combinations of "
                       "its parents being true or false. Similar ideas may be applied to "
                       "undirected, and possibly cyclic, graphs; such are called Markov networks.",
        "references": [
            "[WPbayesnet] http://en.wikipedia.org/wiki/Bayesian_network",
        ],
        "worker": learners.bayesnet.do_stuff,
        "needs_ARFF_File": True,
        "parameters": {
            "structure": {
                "name": "Structure learning algorithm",
                "type": "dict",
                "values": {
                    'hill': {
                        "name": "Hill Climber",
                        "parameters": {}
                    },
                    'K2': {
                        "parameters": {
                            "maxparents": {
                                "short_desc": "Maximum number of parents in the network",
                                "type": "int",
                                "default": 2,
                                "min": 1
                            },
                            "defaultstructure": {
                                "short_desc": "Use Naive Bayes as default structure",
                                "type": "list",
                                "values": ["empty", "NaiveBayes"],
                                "default": "empty"
                            }
                        }
                    },
                    'tan': {
                        "name": "Tree Augmented Network",
                        "parameters": {}
                    },
                    'tabu': {
                        "name": "Tabu Search",
                        "parameters": {}
                    },
                    'score': {
                        "name": "Score-Based Search Algorithm",
                        "parameters": {}
                    }
                },
                "default": 'K2',
                "short_desc": "Which algorithm should be used to learn the dependency structure. Different algorithms "
                              "may use significantly different amounts of time."
            },
            "scorer": {
                "name": "Scoring function",
                "type": "list",
                "values": ['bayes', 'mdl', 'entropy', 'aic'],
                "default": "entropy",
                "short_desc": "Which scoring function should be used to evaluate the network structure?",
                "description": "Bayes (BIC) = Bayesian Information Criterion\nMDL: Minimum Description Length\n"
                               "Entropy: Entropy\nAIC: Akaike's Information Criterion"
            }
        }
    },
    "naivebayes": {
        "is_cluster": False,
        "name": "Naive Bayes Classifier",
        "short_desc": "Assumes complete independency of all attributes and uses the conditional probabilities to "
                      "classify data.",
        "description": "[WPnaive]_ In machine learning, naive Bayes classifiers are a family of simple probabilistic "
                       "classifiers based on applying Bayes' theorem with strong (naive) independence assumptions "
                       "between the features.\n\n Naive Bayes models are also known under a variety of names in the "
                       "literature, including simple Bayes and independence Bayes. All these names reference the use "
                       "of Bayes' theorem in the classifier's decision rule, but naive Bayes is not (necessarily) a "
                       "Bayesian method;[1] Russell and Norvig note that \"[naive Bayes] is sometimes called a "
                       "Bayesian classifier, a somewhat careless usage that has prompted true Bayesians to call it the "
                       "idiot Bayes model.\"",
        "references": [
            "[WPnaive] http://en.wikipedia.org/wiki/Naive_Bayes_classifier",
        ],
        "worker": learners.naivebayes.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "distribution": {
                "name": "Distribution Assumption",
                "type": "list",
                "values": ['gaussian', 'multinomial'],
                "default": "gaussian"
            }
        }
    },
    "ldaqda": {
        "is_cluster": False,
        "name": "Discriminant Analysis",
        "short_desc": "A classifier with a linear or quadratic decision boundary, generated by fitting class "
                      "conditional densities to the data and using Bayes’ rule.",
        "description": "[WPLDA]_ Linear discriminant analysis (LDA) and the related Fisher's linear discriminant are "
                       "methods used in statistics, pattern recognition and machine learning to find a linear "
                       "combination of features which characterizes or separates two or more classes of objects or "
                       "events. The resulting combination may be used as a linear classifier, or, more commonly, for "
                       "dimensionality reduction before later classification."
                       "\n\n[WPQuad]_ A quadratic classifier is used in machine learning and statistical "
                       "classification to separate measurements of two or more classes of objects or events by a "
                       "quadric surface. It is a more general version of the linear classifier.",
        "references": [
            "[WPLDA] http://en.wikipedia.org/wiki/Linear_discriminant_analysis",
            "[WPQuad] http://en.wikipedia.org/wiki/Quadratic_classifier"
        ],
        "worker": learners.ldaqda.do_stuff,
        #"worker": lambda: None,
        "needs_ARFF_File": False,
        "parameters": {
            "type": {
                "name": "Algorithm",
                "type": "dict",
                "values": {
                    "lda": {
                        "name": "Linear Discriminant Analysis",
                        "parameters": {}
                    },
                    "qda": {
                        "name": "Quadratic Discriminant Analysis",
                        "parameters": {
                            "regularisation": {
                                "name": "Regularisation parameter",
                                "short_desc": "Regularises the covariance estimate as :math:`(1-reg_param)*Sigma + "
                                              "reg_param*np.eye(n_features)`",
                                "type": "float",
                                "optional": True,
                                "default": 0.0,
                                "min": 0.0,
                                "max": 1.0,
                                "step": 0.05
                            }
                        }
                    }
                }
            },
            "priors": {
                "name": "A-Priori Probabilities",
                "short_desc": "Gives the probabilities of each class to occur.",
                "description": "Specify the probabilities as comma-separated values. Probabilities must sum up to one.",
                "type": "string",
                "validator": invalid_priors
            }
        }
    },
    "svm": {
        "is_cluster": False,
        "name": "Support Vector Machine",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.svm.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "penalty": {
                "name": "Penalty parameter",
                "short_desc": "Penalty parameter C of the error term.",
                "type": "float",
                "default": 1.0,
                "min": 0.0,
            },
            "kernel": {
                "name": "Kernel",
                "short_desc": "Specifies the kernel type.",
                "description": "Either ``linear``, ``rbf`` (radial basis function), ``poly`` (polynomial) or "
                               "``sigmoid`` are available.",
                "type": "dict",
                "values": {
                    "rbf": {
                        "name": "Radial Basis Function",
                        "parameters": {
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.
                            }
                        }
                    },
                    "poly": {
                        "name": "Polynomial",
                        "parameters": {
                            "degree": {
                                "name": "Polynomial degree",
                                "short_desc": "Degree of the polynomial to be used.",
                                "type": "int",
                                "min": 1,
                                "default": 3
                            },
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.
                            },
                            "coef0": {
                                "name": "Independent term in kernel function",
                                "type": "float",
                                "default": 0.
                            }
                        }
                    },
                    "sigmoid": {
                        "name": "Sigmoid",
                        "parameters": {
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.
                            },
                            "coef0": {
                                "name": "Independent term in kernel function",
                                "type": "float",
                                "default": 0.
                            }
                        }
                    },
                    "linear": {
                        "name": "Linear",
                        "parameters": {}
                    }
                }
            }
        }
    },
    "labelpropagation": {
        "is_cluster": False,
        "name": "Label Propagation",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.labels.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "algo": {
                "name": "Algorithm",
                "short_desc": "Which algorithm should be used?",
                "description": "Label Propagation uses the raw similarity matrix, Label Spreading minimizes a loss "
                               "function.",
                "type": "dict",
                "values": {
                    "propagation": {
                        "name": "Label Propagation",
                        "parameters": {}
                    },
                    "spreading": {
                        "name": "Label Spreading",
                        "parameters": {}
                    }
                }
            },
            "kernel": {
                "name": "Kernel",
                "short_desc": "Specifies the kernel type.",
                "description": "Either ``knn``, ``rbf`` (radial basis function) are available.",
                "type": "dict",
                "values": {
                    "rbf": {
                        "name": "Radial Basis Function",
                        "parameters": {
                            "gamma": {
                              "name": "Kernel coefficient",
                              "short_desc": "Needs to be strictly larger than 0.",
                              "description": "Somehow something like the standard deviation used in a normal "
                                             "distribution.",
                              "type": "float",
                              "default": 1.,
                              "step": 0.25,
                              "min": 0.,
                              "validator": invalid_strictly_larger_than
                            }
                        }
                    },
                    "knn": {
                        "name": "Nearest Neighbor Graph",
                        "parameters": {
                            "neighbors": {
                                "name": "Neighbors",
                                "short_desc": "Needs to be strictly larger than 0.",
                                "description": "Number of nearest neighbors used in constructing the nearest-"
                                               "neighborhood graph.",
                                "type": "int",
                                "default": 3,
                                "step": 1,
                                "min": 0,
                                "validator": invalid_strictly_larger_than
                            }
                        }
                    }
                }
            },
            "alpha": {
                "name": "Clamping factor",
                "short_desc": "",
                "description": "",
                "type": "float",
                "default": 1.0,
                "step": 0.1,
                "min": 0.,
                "max": 1.0,
                "validator": invalid_strictly_larger_than
            }
        }
    },

    "s3vm": {
        "is_cluster": False,
        "name": "Semi-Supervised Support Vector Machine",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.s3vm.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "penalty_1": {
                "name": "Penalty for labeled instances",
                "short_desc": "Penalty parameter C of the error term.",
                "type": "float",
                "default": 0.001,
                "min": 0.0,
                "step": 0.001,
                "validator": invalid_strictly_larger_than
            },
            "penalty_2": {
                "name": "Penalty for unlabeled instances",
                "short_desc": "Penalty parameter C of the error term.",
                "type": "float",
                "default": 1.0,
                "min": 0.0,
                "validator": invalid_strictly_larger_than
            },
            "kernel": {
                "name": "Kernel",
                "short_desc": "Specifies the kernel type.",
                "description": "Either ``linear``, ``rbf`` (radial basis function) are available.",
                "type": "dict",
                "values": {
                    "rbf": {
                        "name": "Radial Basis Function",
                        "parameters": {
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.,
                                "validator": invalid_strictly_larger_than
                            }
                        }
                    },
                    "linear": {
                        "name": "Linear",
                        "parameters": {},
                    },
                }
            }
        }
    },
    "activesvm": {
        "is_cluster": False,
        "name": "Active Learning (Support Vector Machine)",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.al.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "queries": {
                "name": "Oracle Queries",
                "short_desc": "Number of queries.",
                "type": "int",
                "default": 10,
                "min": 0,
                "step": 1
            },
            "penalty": {
                "name": "Penalty parameter",
                "short_desc": "Penalty parameter C of the error term.",
                "type": "float",
                "default": 1.0,
                "min": 0.0,
            },
            "kernel": {
                "name": "Kernel",
                "short_desc": "Specifies the kernel type.",
                "description": "Either ``linear``, ``rbf`` (radial basis function), ``poly`` (polynomial) or "
                               "``sigmoid``"
                               " are available.",
                "type": "dict",
                "values": {
                    "rbf": {
                        "name": "Radial Basis Function",
                        "parameters": {
                            "gamma": {
                              "name": "Kernel coefficient",
                              "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                            "instead.",
                              "type": "float",
                              "default": 0.,
                              "step": 0.25,
                              "min": 0.
                            }
                        }
                    },
                    "poly": {
                        "name": "Polynomial",
                        "parameters": {
                            "degree": {
                                "name": "Polynomial degree",
                                "short_desc": "Degree of the polynomial to be used.",
                                "type": "int",
                                "min": 1,
                                "default": 3
                            },
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.
                            },
                            "coef0": {
                                "name": "Independent term in kernel function",
                                "type": "float",
                                "default": 0.
                            }
                        }
                    },
                    "sigmoid": {
                        "name": "Sigmoid",
                        "parameters": {
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.
                            },
                            "coef0": {
                                "name": "Independent term in kernel function",
                                "type": "float",
                                "default": 0.
                            }
                        }
                    },
                    "linear": {
                        "name": "Linear",
                        "parameters": {}
                    }
                }
            }
        }
    },
    "ensemble": {
        "is_cluster": False,
        "name": "Ensemble Methods",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ensemble.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "enstype": {
                "name": "Ensemble Type",
                "short_desc": "Which ensemble classifier should be used.",
                "type": "dict",
                "values": {
                    "random": {
                        "name": "Random Forest"
                    },
                    "extra": {
                        "name": "Extra Random Forest"
                    },
                    "ada": {
                        "name": "Ada-boosted Decision Tree",
                        "parameters": {
                            "maxdepth": {
                                "name": "Maximum depth of decision trees",
                                "short_desc": "(for AdaBoost only!)",
                                "type": "int",
                                "default": 3,
                                "min": 0,
                            }
                        }
                    }
                },
                "default": "random"
            },
            "n": {
                "name": "Number of Estimators",
                "short_desc": "Number of estimators to be used",
                "type": "int",
                "default": 3,
                "min": 0,
            },
        }
    },
    "ensemble2": {
        "is_cluster": False,
        "name": "Majority of 3",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.bestofthree.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "algo1": {
                "name": "First Classifier",
                "type": "algo",
                "values": ["kNN", "dtree", "svm", "naivebayes", "ldaqda"],
            },
            "algo2": {
                "name": "Second Classifier",
                "type": "algo",
                "values": ["kNN", "dtree", "svm", "naivebayes", "ldaqda"],
            },
            "algo3": {
                "name": "Third Classifier",
                "type": "algo",
                "values": ["kNN", "dtree", "svm", "naivebayes", "ldaqda"],
            }
        }
    },
    "ensemble3": {
        "is_cluster": False,
        "author": "Stephan Besecke, Christian Harnisch",
        "name": "Ensemble Project",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ensemble_project.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "k": {
                "name": "k",
                "type": "int",
                "default": 3,
                "min": 1
            }
        }
    },
    "dtreemulti": {
        "is_cluster": False,
        "author": "Fabian Witt, Steven Brandt",
        "name": "Decision Tree Multiclass",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.decision_tree_multiclass.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "n": {
                "name": "Number of Estimators",
                "type": "int",
                "default": 3,
                "min": 1
            },
            "maxdepth": {
                "name": "Maximal Depth",
                "type": "int",
                "default": 3,
                "min": 1
            },
            "percexamples": {
                "name": "Perc_Examples",
                "type": "float",
                "default": 0.25,
                "max": 1.0,
                "min": 0.0000001
            },
            "percattributes": {
                "name": "Perc_Attributes",
                "type": "float",
                "default": 0.25,
                "max": 1.0,
                "min": 0.0000001
            }
        }
    },
    "gradientboosting": {
        "is_cluster": False,
        "name": "Gradient Boosting",
        "author": "Pascal Krenckel, Sebastian Steuber",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.gradient_boosting.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "maxleafs": {
                "name": "MaxLeafs",
                "type": "int",
                "default": 15,
                "min": 1
            },
            "iter": {
                "name": "Iterations",
                "type": "int",
                "default": 5,
                "min": 1
            },
            "f0": {
                "name": "F0",
                "type": "float",
                "default": 3.141592653589793,
                "min":0.00000001
            },
            "loss": {
                "name": "Loss Function",
                "type": "dict",
                "values": {
                    "1": {
                        "name": "Linear"
                    },
                    "2": {
                        "name": "Quadratisch"
                    },
                    "3": {
                        "name": "Logaritmisch"
                    }
                },
                "default": "2"
            },
            "mct": {
                "name": "Multiclass type",
                "type": "dict",
                "values": {
                    "OvA": {
                        "name": "One Vs. All"
                    },
                    "OvO": {
                        "name": "One Vs. One"
                    },
                    "Regression": {
                        "name": "Regression"
                    }
                },
                "default": "OvA"
            },
        }
    },
    "evodtree": {
        "name": "Evolutionary Decision Trees",
        "author": "Florian Bannier, David Magnus Henriques",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.evodtree.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "gensize": {
                "name": "Individuals per Generation",
                "type": "int",
                "default": 100,
                "min": 1,
                "max": 1000,
            },
            "generations": {
                "name": "# of Generations",
                "type": "int",
                "default": 100,
                "min": 1,
                "max": 1000,
            },
            "treesizebias": {
                "name": "Tree Size Bias",
                "type": "float",
                "default": 0.1,
                "min":0.00000001,
                "max":1.0
            }
        }
    },
    "kdeclassify": {
        "name": "KDE classify",
        "author": "Johannes Gaetjen",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.kdeclassify.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "bandwidthmethod": {
                "name": "Bandwidth method",
                "type": "dict",
                "values": {
                    'manual': {
                        'name': "Manual",
                        'parameters':{
                            'bandwidth': {
                                'name': 'Bandwidth',
                                'type': 'float',
                                'default': 0.1,
                                'min': 0.000001,
                            }
                        }
                    },
                    'silverman': {"name": "Silverman's Heuristic"},
                    'plugin': {"name": "Plugin Heuristic"}
                }
            },
            "kernel": {
                "name": "Kernel",
                "type": "dict",
                "values": {
                    "gaussian": {"name": "Gaussian Kernel"},
                    "epanechnikov": {"name": "Epanechnikov Kernel"},
                    "uniform": {"name": "Uniform Kernel"},
                    "triangular": {"name": "Triangular Kernel"},
                    "quartic": {"name": "Quartic Kernel"}
                }
            },
            "metric": {
                "name": "Metric",
                "type": "dist",
                "values": ['euclidean', 'seuclidean', 'sqeuclidean', 'hamming', 'manhattan', 'chebyshev']
            }
        }
    },
    "alusuds": {
        "name": "Active Learning (US, UDS)",
        "author": "Dominik Lang",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.al_us_uds.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "classifier": {
                "name": "Classifier",
                "type": "dict",
                "values": {
                    'DT': {'name': "Decision Tree"},
                    'NB': {'name': "Naive Bayes"}
                }
            },
            "sampling": {
                "name": "Sampling Method",
                "type": "dict",
                "values": {
                    'US': {'name': "Uncertainty Sampling"},
                    'UDS': {'name': "Uncertainty Diversity Sampling"}
                }
            },
            "method": {
                "name": "Method",
                "type": "dict",
                "values": {
                    'margin': {'name': "Margin"},
                    'entropy': {'name': "Entropy"},
                    # 'random': {'name': "Random"}
                }
            },
            "budget": {
                "name": "Number of Queries",
                "type": "int",
                "min": 0,
                "default": 20
            },
            "bw": {
                "name": "Bandwidth",
                "type": "float",
                "min": 0.000000001,
                "default": 1.0
            },
        }
    },
    "dbscanlabelprop": {
        "name": "DBSCAN LP",
        "author": "Jan Schumacher, Chris Long",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.dbscan_label_propagation.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "eps": {
                "name": "Epsilon",
                "type": "float",
                "default": 1.,
                "step": 0.25,
                "min": 0.,
                "validator": invalid_strictly_larger_than
            },
            "minpts": {
                "name": "MinPTS",
                "type": "int",
                "default": 3,
                "step": 1,
                "min": 0,
                "validator": invalid_strictly_larger_than
            }
        }
    },
    "vfdt": {
        "name": "VFDT",
        "author": "Arne Schneck",
        "short_desc": "This function applies the VFDT algorithm on data. Inserted data is expected to be discretized. "
                      "The VFDT algorithm is applied on training data and evaluation data.",
        "description": "",
        "references": [],
        "worker": learners.VFDT.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "nbins": {
                "name": "Number of Bins",
                "short_desc": "This is the number of possible values of each attribute. Attributes are expected to "
                              "be discretized as described for parameter trainingData. Must be at least 1.",
                "type": "int",
                "default": 2,
                "step": 1,
                "min": 1,
            },
            "nmin": {
                "name": "N min",
                "short_desc": "Number of training instances that have to have arrived in a node between each "
                              "calculation of split quality. See VFDT paper for further explanation. This value has to "
                              "be at least 1, but consider: This mode will be not active for 1 as input! Default value "
                              "is 10.",
                "type": "int",
                "default": 10,
                "step": 1,
                "min": 1,
            },
            "delta": {
                "name": "Delta",
                "short_desc": "Accepted error that a split is not done for the best possible attribute. It is to be "
                              "in (0,1]. The smaller, the later decisions / splits are done and therefore the longer "
                              "the algorithm needs to adapt the model. If this parameter is not set appropriately, an "
                              "error message is returned. Default value is 0.05",
                "type": "float",
                "default": 0.5,
                "step": 0.01,
                "min": 0.0,
                "max": 1.0,
                "validator": invalid_strictly_larger_than
            },
            "tiethr": {
                "name": "Tie Breaking Threshold",
                "short_desc": "Parameter to enforce splits if the algorithm can't settle down on deciding what "
                              "attribute is to be chosen for splitting a node if several attributes are equally strong "
                              "in their split quality. This is done to prevent the algorithm from taking too much time "
                              "/ instances to decide. This parameter can have any value but consider values <= 0 will "
                              "switch off tie breaking. See paper 'Tie Breaking in Hoeffding Trees' by Geoffrey Holmes,"
                              " Richard Kirkby, and Bernhard Pfahringer for further details on the issue of tie "
                              "breaking. Default value is 0.2 .",
                "type": "float",
                "default": 0.2,
                "step": 0.1,
            },
        }
    },
    "lvq": {
        "name": "LVQ",
        "is_cluster": False,
        "author": "Simon Parlow",
        "short_desc": "It's a undocumented student project.",
        "description": "please ask Simon",
        "references": [],
        "worker": learners.ws1617.LVQ.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "lrate": {
                "name": "Lernrate",
                "short_desc": "Größe des Lernrate",
                "type": "float",
                "default": 0.3,
                "step": 0.1,
                "min": 0,
            },
            "epochs": {
                "name": "Epochen",
                "short_desc": "Anzahl der Epochen",
                "type": "int",
                "default": 10,
                "step": 1,
                "min": 1,
            },
            "n_codebooks": {
                "name": "Codebooks",
                "short_desc": "Anzahl der Codebooks",
                "type": "int",
                "default": 10,
                "step": 1,
                "min": 1,
            },
            "distance": {
                "name": "Distanz",
                "short_desc": "Distanzmaß",
                "type": "dict",
                "values": {
                    'euclidean': {'name': "Euklidische Distanz"},
                    'cityblock': {'name': "Cityblock/Manhattan Distanz"}
                },
                "default": "euclidean"
            },
            "codebook": {
                "name": "Codebook Initialisierung",
                "short_desc": "Initialisierungsfunktion für Codebooks",
                "type": "dict",
                "values": {
                    'random': {'name': "Zufällig"},
                    'class': {'name': "Zufällig klassenbasiert"}
                },
                "default": "random"
            },

        }
    },
    "nbtree": {
        "name": "Naive Bayes Tree",
        "is_cluster": False,
        "author": "Mitch Köhler, Michelle Bieber",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ws1617.NBTree.do_stuff,
        "needs_ARFF_File": True,
        "parameters": {
            "t": {
                "name": "Treshold",
                "short_desc": "See long desc.",
                "description": """Represents
                                  the minimum entropy reduction (in PERCENT)
                                  required to create leafs. If the entropy
                                  reduction is smaller than the given
                                  threshold, a leaf node will be created. The
                                  higher the threshold, the more does the tree
                                  generalize.""",
                "type": "float",
                "default": 0,
                "step": 1,
                "min": 0,
                "max": 100,
            },
            "dm": {
                "name": "decision measure",
                "type": "dict",
                "values": {
                    'utility': {'name': "Utility"},
                    'entropy': {'name': "Entropy"}
                },
                "default": "entropy"
            },
            "cv": {
                "name": "5-fold cross-validation",
                "type": "bool",
                "default": False
            }
        }
    },
    "adtree": {
        "name": "ADTree",
        "is_cluster": False,
        "author": "Martin Wieczorek, Philipp Bergt",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ws1617.adtree.do_stuff,
        "needs_ARFF_File": True,
        "parameters": {
            "iterations": {
                "name": "Iterations",
                "type": "int",
                "default": 1,
                "step": 1,
                "min": 1,
            }
        }
    },
    "kde2": {
        "name": "KDE",
        "is_cluster": False,
        "author": "Alexander-Mourice Schmidt, David Schulte",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ws1617.kde2.do_stuff,
        "needs_ARFF_File": True,
        "parameters": {
            "kernel_mode": {
                "name": "Kernel Mode",
                "type": "dict",
                "values": {
                    '0': {'name': "gaussian"},
                    '1': {'name': "bisquare"},
                    '2': {'name': "epanechnikov"},
                    '3': {'name': "cauchy"},
                    '4': {'name': "picard"},
                },
                "default": "2"
            },
            "metric": {
                "name": "decision measure",
                "type": "dict",
                "values": {
                    '0': {'name': "euclidian"},
                    '1': {'name': "squared_euclidian"},
                    '2': {'name': "chebyshev"},
                    '3': {'name': "manhattan"},
                },
                "default": "3"
            },
            "bandwidth_mode": {
                "name": "decision measure",
                "type": "dict",
                "values": {
                    '0': {'name': "silverman"},
                    '1': {'name': "solve_the_equation"},
                },
                "default": "0"
            },
        }
    },
    "extendedLDA": {
        "name": "Erweiterte LDA",
        "is_cluster": False,
        "author": "Annika Niemann",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ws1617.extendedLDA.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "art": {
                "name": "Art",
                "short_desc": "See long desc.",
                "description": """Represents
                                  the minimum entropy reduction (in PERCENT)
                                  required to create leafs. If the entropy
                                  reduction is smaller than the given
                                  threshold, a leaf node will be created. The
                                  higher the threshold, the more does the tree
                                  generalize.""",
                "type": "dict",
                "values": {
                    'lda': {'name': "LDA"},
                    'qda': {'name': "QDA"},
                    'cos': {'name': "COS"},
                    'log': {'name': "LOG"},
                },
                "default": "lda"
            }
        }
    },
    "ParzenWindows": {
        "name": "Parzen Windows Classifier",
        "is_cluster": False,
        "author": "Angelika Ophagen, Florian Bethe",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ws1617.kde.kde.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "kdeKernel": {
                "name": "Kernel",
                "short_desc": "kernel to use",
                "type": "dict",
                "values": {
                    '0': {'name': "multivariate Gauss"},
                    '1': {'name': "multiplicative Epanechnikov"},
                    '2': {'name': "multiplicative Picard"},
                },
                "default": "0"
            },
            "estimator": {
                "name": "bandwidthEstimator",
                "short_desc": "bandwidth estimator to use",
                "type": "dict",
                "values": {
                    '0': {'name': "Markov Chain"},
                    '1': {'name': "Silverman"},
                },
                "default": "0"
            },
            "shape": {
                "name": "priorsShape",
                "short_desc": "parameter controlling the shape of prior bandwidth distribution (only for markov chain",
                "type": "float",
                "default": 1.0,
                "step": 0.1,
                "min": 0,
            }
        }
    },
    "nn": {
        "name": "Neural Network",
        "is_cluster": False,
        "author": "Christian Buss",
        "short_desc": "",
        "description": "",
        "references": [],
        "worker": learners.ws1617.nn.do_stuff,
        "needs_ARFF_File": False,
        "parameters": {
            "neuron_list": {
                "name": "Anzahl Neuronen",
                "short_desc": "Komma getrennte Liste der Neuronen pro Schicht",
                "type": "string",
                "default": "10,10,10"
            },
            "epsilon": {
                "name": "Lernrate",
                "type": "float",
                "default": 0.01,
                "min": 0
            }
        }
    },
    "kbcht_2017": {
        "is_cluster": True,
        "author": "Tim Sabsch, Cornelius Styp von Rekowski",
        "name": "KBCHT 2017",
        "short_desc": "",
        "description": """The algorithm in this program is based on work from the following paper:
                Kmeans-Based Convex Hull Triangulation Clustering Algorithm [Abubaker, Hamad, 2012]

                It can be found here: http://www.globalcis.org/rnis/ppl/RNIS105PPL.pdf""",
        "worker": cluster.cluster_func(cluster.kbcht_2017.tolles_clustering_mit_visualisierung),
        "needs_ARFF_File": False,
        "parameters": {
            "k": {
                "name": "k-Means k",
                "type": "int",
                "default": 10,
                "step": 1,
                "min": 1,
            },
            "shrinking_threshold": {
                "name": "Shrinking Threshold",
                "type": "float",
                "default": 2,
                "step": 0.1,
                "min": 0,
                "validator": invalid_strictly_larger_than
            }
        }
    },
    "ea_2017": {
        "is_cluster": True,
        "author": "Jan-Ole Perschewski und Tetiana Lavynska",
        "name": "EA 2017",
        "short_desc": "",
        "description": "",
        "worker": cluster.cluster_func(cluster.ea_2017.cluster),
        "needs_ARFF_File": False,
        "parameters": {
            "cluster": {
                "name": "number of clusters",
                "type": "int",
                "default": 3,
                "step": 1,
                "min": 1,
            },
            "generations": {
                "name": "number of generations",
                "type": "int",
                "default": 100,
                "step": 1,
                "min": 1,
            },
            "size": {
                "name": "population size",
                "type": "int",
                "default": 25,
                "step": 1,
                "min": 1,
            },
            "sample_size": {
                "name": "sample size",
                "type": "int",
                "default": 25,
                "step": 1,
                "min": 1,
            },
            "fit": {
                "name": "Fitness function",
                "type": "dict",
                "default": "centroid-distance",
                "values": {
                    "centroid-distance": {
                        "name": "centroid",
                        "parameters": {}
                    },
                    "convex_hull_volume": {
                        "name": "Convex Hull Volume",
                        "parameters": {}
                    },
                    "davies_bouldin": {
                        "name": "Davies-Bouldin Index",
                        "parameters": {}
                    },
                    "min-clique": {
                        "name": "Minimum Clique",
                        "parameters": {
                            "mink_dist_const": {
                                "name": "Minkowski-Distance Constant",
                                "type": "int",
                                "min": 1,
                                "default": 1,
                            }
                        }
                    },
                    "knn_sum": {
                        "name": "kNN-Sum",
                        "parameters": {
                            "knnk": {
                                "name": "kNN-K",
                                "type": "int",
                                "min": 1,
                                "default": 1,
                            }
                        }
                    },
                    "density_sum": {
                        "name": "density-Sum",
                        "parameters": {
                            "mink_dist_const": {
                                "name": "Minkowski-Distance Constant",
                                "type": "int",
                                "min": 1,
                                "default": 1,
                            },
                            "cut_dist": {
                                "name": "Cut-Distance",
                                "type": "float",
                                "min": 0,
                                "default": 0,
                                "validator": invalid_strictly_larger_than,
                            },
                        }
                    },
                }
            },
            "sel": {
                "name": "Selection strategy",
                "type": "dict",
                "default": "tournament_selection",
                "values": {
                    "tournament_selection": {
                        "name": "Tournament Selection",
                        "parameters": {
                            "size": {
                                "name": "Tournament size",
                                "type": "int",
                                "validator": invalid_number_of_centers,
                                "min": 2,
                                "default": 2,
                            }
                        }
                    },
                    "random_selection": {
                        "name": "Random Selection",
                        "parameters": {}
                    },
                }
            },
            "mut": {
                "name": "Mutation Operator",
                "type": "dict",
                "default": "change_one_assignment_fixed",
                "values": {
                    "change_one_assignment_fixed": {
                        "name": "Change one assignment (fixed)",
                        "parameters": {
                            "amount": {
                                "name": "Amount of children",
                                "type": "int",
                                "min": 0,
                                "default": 0,
                            }
                        }
                    },
                    "change_one_assignment_probability": {
                        "name": "Change one assignment (probability)",
                        "parameters": {
                            "amount": {
                                "name": "Mutation probability",
                                "type": "float",
                                "min": 0.,
                                "max": 1.,
                                "step": 0.01,
                                "default": 0,
                            }
                        }
                    },
                }
            },
            "rek": {
                "name": "Recombination Operator",
                "type": "dict",
                "default": "n_point_crossover",
                "values": {
                    "n_point_crossover": {
                        "name": "N-Point-Crossover",
                        "parameters": {
                            "cuts": {
                                "name": "Number of Cuts",
                                "type": "int",
                                "min": 0,
                                "default": 0,
                            },
                            "children": {
                                "name": "Number of Children",
                                "type": "int",
                                "min": 0,
                                "default": 0,
                            },
                            "mapping": {
                                "name": "Using Mapping",
                                "type": "int",
                                "min": 0,
                                "max": 1,
                                "default": 0,
                            },
                        }
                    },
                    "uniform-crossover": {
                        "name": "Uniform Crossover",
                        "parameters": {
                            "probability": {
                                "name": "Swapping Probability",
                                "type": "float",
                                "min": 0.,
                                "max": 1.,
                                "step": 0.01,
                                "default": 0,
                            },
                            "children": {
                                "name": "Number of Children",
                                "type": "int",
                                "min": 0,
                                "default": 0,
                            },
                            "mapping": {
                                "name": "Using Mapping",
                                "type": "int",
                                "min": 0,
                                "max": 1,
                                "default": 0,
                            },
                        }
                    },
                },
            },
        }
    },
    "flame_2017": {
        "is_cluster": True,
        "author": "Arne Herdick und Dustin Böttcher",
        "name": "FLAME 2017",
        "short_desc": "",
        "description": """FLAME Clustering algorithm implementation in python
                    * Wikipedia: [FLAME clustering](https://en.wikipedia.org/wiki/FLAME_clustering)
                    * C-Source: [Google Code Archive](https://code.google.com/archive/p/flame-clustering/source/default/source)
                    * Paper: [biomedcentral](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-8-3)""",
        "worker": cluster.cluster_func(cluster.flame_2017.flame_cluster),
        "needs_ARFF_File": False,
        "parameters": {
            "k": {
                "name": "k",
                "desc": "the amount of neighbours to use for the initial knn graph",
                "type": "int",
                "default": 10,
                "step": 1,
                "min": 1,
            },
            "outlier_threshold": {
                "name": "outlier threshold",
                "desc": "the maximum density an outlier can have",
                "type": "float",
                "default": 2,
                "step": 0.1,
                "min": 0,
            },
            "distance_measure": {
                "name": "distance measure",
                "type": "list",
                "values": [
                    "braycurtis",
                    "canberra",
                    "chebyshev",
                    "cityblock",
                    "correlation",
                    "cosine",
                    "dice",
                    "euclidean",
                    "hamming",
                    "jaccard",
                    "kulsinski",
                    "mahalanobis",
                    "matching",
                    "minkowski",
                    "rogerstanimoto",
                    "russellrao",
                    "seuclidean",
                    "sokalmichener",
                    "sokalsneath",
                    "sqeuclidean",
                    "yule",
                ],
                "default": "euclidean",
            },
            "iterations": {
                "name": "iterations",
                "desc": "number of updates based on membership of nearest neighbors",
                "type": "int",
                "default": 1,
                "step": 1,
                "min": 1,
            },
            "minkowski_p": {
                "name": "minkowski_p",
                "desc": "The p-norm to apply. Mandatory for Minkowski distance. Ignored otherwise.",
                "type": "int",
                "default": 2,
                "step": 1,
                "min": 1,
            },

        }
    },
    "som_2017": {
        "is_cluster": True,
        "author": "Simon Parlow und Viola Hauffe",
        "name": "SOM 2017",
        "short_desc": "",
        "worker": cluster.cluster_func(cluster.som_2017.cluster),
        "needs_ARFF_File": False,
        "parameters": {
            "x": {
                "name": "x",
                "desc": "grid size in x direction",
                "type": "int",
                "default": 4,
                "step": 1,
                "min": 1,
            },
            "y": {
                "name": "y",
                "desc": "grid size in y direction",
                "type": "int",
                "default": 4,
                "step": 1,
                "min": 1,
            },
            "max_samples": {
                "name": "max samples",
                "desc": "Number of samples to draw from training set",
                "type": "int",
                "default": 100,
                "step": 1,
                "min": 1,
            },
            "max_iterations": {
                "name": "max iteration",
                "desc": "Number of iterations to train the network",
                "type": "int",
                "default": 3,
                "step": 1,
                "min": 1,
            },
            "n_clusters": {
                "name": "k",
                "desc": "Number of (estimated) clusters",
                "type": "int",
                "default": 4,
                "step": 1,
                "min": 1,
            },
            "sampling_method_name": {
                "name": "sampling method",
                "type": "list",
                "values": [
                    "linear",
                    "random",
                ],
                "default": "random",
            },
            "second_level_cluster_name": {
                "name": "Label assignment strategy",
                "type": "dict",
                "values": {
                    "hac": {
                        "name": "HAC",
                        "parameters": {
                            "linkage": {
                                "type": "list",
                                "values": [
                                    "ward", "complete", "average"
                                ],
                                "default": "complete",
                            }
                        }
                    },
                    "kmean": {
                        "name": "k-means"
                    },
                },
                "default": "kmean",
            },
            "lrate": {
                "name": "learning rate",
                "desc": "",
                "type": "float",
                "default": 0.05,
                "step": 0.01,
                "min": 0,
                "max": 1,
            },

        }
    },
    "birch_2017": {
        "is_cluster": True,
        "author": "David Schulte, Jonathan Kloss",
        "name": "BIRCH 2017",
        "short_desc": "",
        "worker": cluster.cluster_func(cluster.birch_2017.databirch),
        "needs_ARFF_File": False,
        "parameters": {
            "threshold": {
                "name": "threshold",
                "desc": "Ab welchem Durchmesser wird gesplittet",
                "type": "float",
                "default": 2,
                "step": 1,
                "min": 0,
            },
            "b": {
                "name": "b",
                "desc": "wieviele Nodes dürfen die inneren Nodes halten",
                "type": "int",
                "default": 1500,
                "step": 1,
                "min": 0,
            },
            "cluster": {
                "name": "k",
                "desc": "gewuenschte clusteranzahl  ( nimmt von oben so viele Cluster bis er genug hat)",
                "type": "int",
                "default": 2,
                "step": 1,
                "min": 1,
            },
            "metric": {
                "name": "metric",
                "type": "list",
                "values": [
                    "euclidian",
                    "seuclidian",
                    "manhattan",
                ],
                "default": "euclidean",
            }
        }
    },
    "khopca_2017": {
        "is_cluster": True,
        "author": "Hans-Martin Wulfmeyer, Markus Hempel",
        "name": "KHOPCA 2017",
        "short_desc": "",
        "worker": cluster.cluster_func(cluster.khopca_2017.cluster),
        "needs_ARFF_File": False,
        "parameters": {
            "knn": {
                "name": "knn",
                "desc": "kanten zwischen datenpuntken werden über knn gebildet (sollte unter 500 bleiben wegen der zeit)",
                "type": "int",
                "default": 300,
                "step": 1,
                "min": 1,
            },
            "kmax": {
                "name": "kmax",
                "desc": "wert zum anpassen für den khopca algorithmus",
                "type": "int",
                "default": 50,
                "step": 1,
                "min": 0,
                "validator": invalid_strictly_larger_than,
            }
        }
    },
    "cure_2017": {
        "is_cluster": True,
        "author": "Daniel Micheel, Philipp Thoms",
        "name": "CURE 2017",
        "short_desc": "",
        "worker": cluster.cluster_func(cluster.cure_2017.cure_clustering),
        "needs_ARFF_File": False,
        "parameters": {
            "number_of_clusters": {
                "name": "number_of_clusters",
                "desc": "Anzahl der Cluster",
                "type": "int",
                "default": 1,
                "step": 1,
                "min": 1,
            },
            "alpha": {
                "name": "Alphawert",
                "desc": "bestimmt die Form der Cluster",
                "type": "float",
                "default": 1,
                "step": 0.1,
                "min": 0,
            },
            "c": {
                "name": "c",
                "desc": "Clusterrepraesentanten",
                "type": "int",
                "default": 1,
                "step": 1,
            }
        }
    },

}


preprocessing = {
    "min_max": {
        "name": "Min-Max Scaling",
        "worker": preprocessing.minmax.process,
        "dimensions": True,
        "dimensions_out": "dimensions_in",
        "parameters": {
            "min": {
                "name": "Minimum",
                "type": "float",
                "default": 0
            },
            "max": {
                "name": "Maximum",
                "type": "float",
                "default": 1
            }
        }
    },
    "pca": {
        "name": "PCA",
        "worker": preprocessing.pca.process,
        "dimensions": False,
        "dimensions_out": "dimensions_in",
        "parameters": {},
    },
    "kernelpca": {
        "name": "Kernel PCA",
        "worker": preprocessing.kernelpca.process,
        "dimensions": False,
        "dimensions_out": "dimensions_in",
        "parameters": {
            "kernel": {
                "name": "Kernel",
                "short_desc": "Specifies the kernel type.",
                "description": "Either ``linear``, ``rbf`` (radial basis function), ``poly`` (polynomial) or "
                               "``sigmoid``"
                               " are available.",
                "type": "dict",
                "values": {
                    "rbf": {
                        "name": "Radial Basis Function",
                        "parameters": {
                            "gamma": {
                              "name": "Kernel coefficient",
                              "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                            "instead.",
                              "type": "float",
                              "default": 0.,
                              "step": 0.25,
                              "min": 0.
                            }
                        }
                    },
                    "poly": {
                        "name": "Polynomial",
                        "parameters": {
                            "degree": {
                                "name": "Polynomial degree",
                                "short_desc": "Degree of the polynomial to be used.",
                                "type": "int",
                                "min": 1,
                                "default": 3
                            },
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.
                            },
                            "coef0": {
                                "name": "Independent term in kernel function",
                                "type": "float",
                                "default": 0.
                            }
                        }
                    },
                    "sigmoid": {
                        "name": "Sigmoid",
                        "parameters": {
                            "gamma": {
                                "name": "Kernel coefficient",
                                "short_desc": "If left at '0.0', :math:`\\frac{1}{number of features}` will be used "
                                              "instead.",
                                "type": "float",
                                "default": 0.,
                                "step": 0.25,
                                "min": 0.
                            },
                            "coef0": {
                                "name": "Independent term in kernel function",
                                "type": "float",
                                "default": 0.
                            }
                        }
                    },
                    "linear": {
                        "name": "Linear",
                        "parameters": {}
                    }
                }
            }
        },
    },
    "projection": {
        "name": "Projections",
        "worker": preprocessing.projection.process,
        "dimensions": True,
        "dimensions_out": "dimensions_selected",
        "parameters": {},
    },
    "paa": {
        "name": "Piecewise Aggregate Approximation",
        "worker": preprocessing.paa.process,
        "dimensions": False,
        "dimensions_out": "parseInt(step.bins)",
        "parameters": {
            "bins": {
                "name": "Number of intervals",
                "type": "int",
                "min": 1,
                "default": 10,
            },
        },
    },
    "sax": {
        "name": "Symbolic Aggregate approXimation",
        "worker": preprocessing.sax.process,
        "dimensions": False,
        "dimensions_out": "dimensions_in",
        "parameters": {
            "symbols": {
                "name": "Number of symbols",
                "type": "int",
                "min": 1,
                "default": 10,
            },
        },
    },
    "derivation": {
        "name": "Derivation",
        "worker": preprocessing.derivation.process,
        "dimensions": False,
        "dimensions_out": "dimensions_in",
        "parameters": {},
    },
    "normalizer": {
        "name": "Normalizer",
        "worker": preprocessing.normalizer.process,
        "dimensions": False,
        "parameters": {
            "norm": {
                "name": "Norm",
                "type": "list",
                "values": ['L1', 'L2']
            },
        }
    },
    "standardizer": {
        "name": "Standardizer",
        "worker": preprocessing.standardizer.process,
        "dimensions": True,
        "dimensions_out": "dimensions_in",
        "parameters": {},
    },
}
"""

Parameterspezifikation
======================

The following parameter types could be used.

Integer
-------
type: int
min: Minimal Value (optional)
max: Maximal Value (optional)
step: Step size vor input (optional, no strict checks)

Floats
------
type: float
min: Minimal Value (optional)
max: Maximal Value (optional)
step: Step size vor input (optional, no strict checks)

List
----
type: list
multi: is multi select possible
values: list of possible values

String
------
type: string

Boolean
-------
type: bool
label: label right of the checkbox

Algorithm
---------
type: algo
values: list of algorithms (important, please do NOT create cycles or infinity loops)

Distance-Measure
----------------
type: dist
values: list of allowed measures, should be a subset of defined measures (optional)

Additional optional Parameters for all Types
--------------------------------------------
default: default value
optional: boolean, default False
short_desc: string with short description
description: string with detailed description
"""
###########
#
# add default parser and validators to parameters
# add default value
# add default name
# replace dist type with dict object
#
###########


def attach_validators_to_parameters(parameters, prefix=""):
    for paramid, param in parameters.items():

        if param["type"] == "dist":
            param["type"] = "dict"
            if "values" in param:
                values = {}
                for dist in param["values"]:
                    if dist in distances:
                        values[dist] = distances[dist]
                    else:
                        values[dist] = {}
                    if "scipy_name" not in distances[dist]:  # hack scipy names for cityblock/manhattan distance
                        values[dist]['scipy_name'] = dist
                param["values"] = values
            else:
                param["values"] = distances

        if param["type"] == "algo":
            if "values" in param:
                param["type"] = "dict"
                values = {}
                for algorithm in param["values"]:
                    if algorithm in algorithms:
                        values[algorithm] = algorithms[algorithm]
                    else:
                        values[algorithm] = {}
                param["values"] = values
            else:
                param["type"] = "list"
                param["values"] = algorithms.keys()

        validators = [invalid_optional]
        default = ""
        if param["type"] == "int":
            validators.append(invalid_range)
            param["parser"] = lambda x: int(float(x))
        elif param["type"] == "float":
            validators.append(invalid_range)
            param["parser"] = float
        elif param["type"] in ("list", "dict"):
            if "multi" in param and param["multi"]:
                validators.append(invalid_multi_list)
            else:
                validators.append(invalid_list)
        if "validator" in param:
            validators.append(param["validator"])
        param["validators"] = validators

        if "default" not in param:
            param["default"] = default

        if "name" not in param:
            param["name"] = prefix+paramid

        if "references" in param:
            if "description" not in param:
                param["description"] = ""
            for reference in algorithms["references"]:
                param["description"] += "\n\n.. "+reference

        if param["type"] == "dict" and "values" in param and isinstance(param["values"], dict):
            for val in param["values"].values():
                if "parameters" in val:
                    attach_validators_to_parameters(val["parameters"], prefix+paramid+"-")


for algorithm in algorithms.values():
    if "references" in algorithm:
            if "description" not in algorithm:
                algorithm["description"] = ""
            for ref in algorithm["references"]:
                algorithm["description"] += "\n\n.. "+ref
    if "parameters" in algorithm:
        attach_validators_to_parameters(algorithm["parameters"])
