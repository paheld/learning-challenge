from django.contrib import admin

from webapp.models import *
from django.contrib.auth.models import User

admin.site.register(Group)
admin.site.register(Round)
admin.site.register(Dataset)
admin.site.register(DatasetPoint)
admin.site.register(Job)
admin.site.register(JobPicture)
admin.site.register(Preprocessing)
admin.site.register(Worker)
admin.site.register(DatasetResultPoints)
admin.site.register(RoundResultPoints)