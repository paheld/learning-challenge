#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

from django import template

import json

register = template.Library()

class Context2JsNode(template.Node):
    def __init__(self, variable):
        self.vars = variable.split_contents()[1:]

    def render(self, context):
        parts = ["<script>"]
        for var in self.vars:
            try:
                if var in context:
                    json_text = json.dumps(context[var])
                    json_text = json_text.replace("\\", "\\\\")
                    json_text = json_text.replace("\n", "\\\n")
                    json_text = json_text.replace("'", "\\'")

                    parts.append("{} = JSON.parse('{}');".format(var, json_text))
                else:
                    parts.append("// Error: {} not in context".format(var))
            except Exception as e:
                parts.append("// {} - Exception: {}".format(var, e))
        parts.append("</script>")
        return "\n".join(parts)


@register.tag("context2js")
def do_context2js(parser, token):
    return Context2JsNode(token)


@register.filter
def separated(value, separator=","):
    return separator.join([str(x) for x in value])