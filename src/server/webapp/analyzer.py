#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

from webapp.models import *

import settings

def update_round_dataset_ranking(round_id, dataset_id):
    jobs = Job.objects.filter(round_id=round_id,
                              dataset_id=dataset_id,
                              is_selected=True,
                              user__is_superuser=False
                              ).only("pk", "score_validation", "user")
    best_jobs = {}
    for job in jobs:
        if not job.user_id in best_jobs:
            best_jobs[job.user_id] = job
        elif best_jobs[job.user_id].score_validation < job.score_validation:
            best_jobs[job.user_id] = job

    best_job_list = sorted(best_jobs.values(), key=lambda x: x.score_validation)

    score_objs = []

    tmp_score = 0
    tmp_points = 0
    tmp_beats_nbr = 0

    for job in best_job_list:
        score = DatasetResultPoints()
        score.user_id = job.user_id
        score.score = job.score_validation
        score.round_id = round_id
        score.dataset_id = dataset_id
        score.job = job

        tmp_beats_nbr += 1
        if job.score_validation > tmp_score:
            tmp_points = tmp_beats_nbr
            tmp_score = job.score_validation

        score.points = tmp_points

        score_objs.append(score)

    DatasetResultPoints.objects.filter(round_id=round_id, dataset_id=dataset_id).delete()
    for obj in score_objs:
        obj.save()

    settings.msg_queue.put({
        "msg": "update_dataset_points_list",
        "list": [obj.get_jsonable() for obj in score_objs]
    })
    update_round_total_ranking(round_id)


def update_round_total_ranking(round_id):
    points = DatasetResultPoints.objects.filter(round_id=round_id)
    r = Round.objects.get(id=round_id)

    users = {}

    for point in points:
        print(point)
        if not point.user_id in users:
            rrp = RoundResultPoints()
            rrp.user_id = point.user_id
            rrp.round_id = round_id
            rrp.points = point.points
            users[point.user_id] = rrp
        else:
            users[point.user_id].points += point.points

    best_list = sorted(users.values(), key=lambda x: x.points)

    tmp_score = 0
    tmp_stars = 0
    tmp_beats_nbr = 0

    for p in best_list:
        tmp_beats_nbr += 1
        if p.points > tmp_score:
            tmp_stars = tmp_beats_nbr
            tmp_score = p.points

        p.stars = tmp_stars * r.star_factor

    RoundResultPoints.objects.filter(round_id=round_id).delete()
    for obj in best_list:
        obj.save()

    settings.msg_queue.put({
        "msg": "update_stars",
        "list": [obj.get_jsonable() for obj in best_list]
    })