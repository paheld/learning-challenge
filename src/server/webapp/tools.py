#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import calendar
import hashlib
import server.django_settings as ds
import os.path


def timestamp(datetime_obj):
    if datetime_obj:
        return calendar.timegm(datetime_obj.timetuple())
    return None


def get_cached_url(path, include_file_link=False):
    md5 = hashlib.md5()
    md5.update(path.encode("utf-8"))
    md5.update(ds.SECRET_KEY.encode("utf8"))
    hash = md5.hexdigest()

    ext = ""
    if "." in path:
        ext = ".{}".format(path.split(".")[-1])
        ext = ext.replace("/", "")

    url = "{}cache/{}{}".format(ds.MEDIA_URL, hash, ext)

    if include_file_link:
        dir = "{}cache/".format(ds.MEDIA_ROOT)
        path = "{}{}{}".format(dir, hash, ext)
        return url, path, dir

    return url


def replace_with_cached_url(ref):
    url, path, dir = get_cached_url(ref, True)

    if os.path.isfile(path):
        return url

    return ref
