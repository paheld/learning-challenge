content_update_timeout = null

last_page = ''
last_group = null
last_round = null
last_create_job_dataset = null
last_create_job_algorithm = null
last_job_id = null
current_dataset_pk = null
current_dataset_data = null
current_dataset_series_data = null
current_dataset_series = null
current_dataset_labels = null
csrftoken = null
jobs = Object()
round_jobs = Object()
job_history = Object()
set_job_params = null
scores = Object()
scores_total = Object()
TIME_DELTA = 0
job_limits = Object()
HISTORY_SHOW_ALL = true
preprocessing_config = []
preprocessing_dimensions = 1
preprocessing_id = null
awaiting_preprocessing = null
job_list_view_counter = 0

COLORS = [
  "#1f77b4",
  "#ff7f0e",
  "#2ca02c",
  "#d62728",
  "#9467bd",
  "#8c564b",
  "#e377c2",
  "#7f7f7f",
  "#bcbd22",
  "#17becf",
  "#aec7e8",
  "#ffbb78",
  "#98df8a",
  "#ff9896",
  "#c5b0d5",
  "#c49c94",
  "#f7b6d2",
  "#c7c7c7",
  "#dbdb8d",
  "#9edae5",
]

###
  Web Database
###

window.database = null

open_database = (success=null, error=null) ->
  console.log(username)
  request = window.indexedDB.open(username, 11)
  request.onupgradeneeded = () ->
    db = this.result
    for s in db.objectStoreNames
      db.deleteObjectStore(s)
    stores = {jobs: "pk"}
    for s, key of stores
      db.createObjectStore(s, {
        keyPath: key
      })
    console.log(this)
  request.onsuccess = (e) ->
    window.database = this.result
    console.log(this)
    if success != null
      success(e)
  request.onerror = (e) ->
    console.log(e)
    if error != null
      error(e)
  return true

window.open_database = open_database

set_job = (jobdata, success=null, error=null) ->
  #console.log(JSON.stringify(jobdata), window.database)
  if window.database == null
    if error != null
      error()
    return false

  #labeldata = {
  #  pk: jobdata.pk
  #}

  #for key in ["labels_test", "labels_training", "labels_validation"]
  #  if jobdata[key] != undefined
  #    labeldata[key] = jobdata[key]
  #    delete jobdata[key]

  trans = window.database.transaction(["jobs"], 'readwrite')
  trans.oncomplete = () ->
    success(jobdata)
  trans.onerror = error
  store = trans.objectStore("jobs")
  req = store.put(jobdata)
  return req


#  console.log(JSON.stringify(jobdata), store)

#  console.log(req.readyState)
#  trans2 = window.database.transaction(["labels"], 'readwrite')
#  store2 = trans2.objectStore("labels")
  #req2 = store2.put(labeldata)

  #req.onsuccess = () ->
  #  console.log(this)

  #  if success != null
  #    req2.onsuccess = success
  #  if error != null
  #    req2.onerror = error
  #if error != null
  #  req.onerror = error


get_job = (jobid, success=null, error=null) ->
  if window.database == null
    if error != null
      error()
    return false

  trans = window.database.transaction(["jobs"], 'readonly')
  store = trans.objectStore('jobs')
  req = store.get(jobid)
  if success != null
    req.onsuccess = () ->
      success(this.result)
  req.onerror = error


init_job_history = () ->
  if window.database == null
    if error != null
      error()
    return false

  trans = window.database.transaction(["jobs"], 'readonly')
  store = trans.objectStore('jobs')
  store.openCursor().onsuccess = (evt) ->
    cursor = evt.target.result
    if cursor
      jobdata = cursor.value

      r = jobdata.round
      if jobdata.user == username or ADMIN
        if job_history[r] == undefined
          job_history[r] = [jobdata.id]
        else if jobdata.pk not in job_history[r]
          job_history[r].push(jobdata.pk)
          job_history[r] = job_history[r].sort((a,b) ->
            a-b
          )

          cursor.continue()


window.get_job = get_job

get_job_with_labels = get_job
#get_job_with_labels = (jobid, success=null, error=null) ->
#  if window.database == null
#    if error != null
#      error()
#    return false
#
#  trans = window.database.transaction(["jobs"], 'readonly')
#  store = trans.objectStore('jobs')
#  req = store.get(jobid)
#  if success != null
#    req.onsuccess = () ->
#      job = this.result
#      if job == undefined
#        return
#      trans2 = window.database.transaction(["labels"], 'readonly')
#      store2 = trans2.objectStore('labels')
#      console.log(job)
#      req2 = store2.get(job.pk)
#      if success != null
#        req2.onsuccess = () ->
#          label_obj = this.result
#          for key, val of label_obj
#            job[key] = val
#          success(job)
#        req2.onerror = () ->
#          success(job)
#  req.onerror = error


get_jobs = (jobids, success=null, error=null) ->
  console.log("start_get_jobs", Date())
  if window.database == null
    if error != null
      error()
    return false

  trans = window.database.transaction(["jobs"], 'readonly')
  store = trans.objectStore('jobs')
  tmp_jobs = []
  range = IDBKeyRange.bound(Math.min.apply(null, jobids), Math.max.apply(null, jobids))
  store.openCursor(range).onsuccess = (e) ->
    cursor = e.target.result
    if cursor
      if cursor.key in jobids
        tmp_jobs.push(cursor.value)
      cursor.continue()
    else
      success(tmp_jobs)

#  for jobid in jobids
#    req = store.get(jobid)
#    req.onsuccess = () ->
#      tmp_jobs.push(this.result)
#    req.onerror = () ->
#      tmp_jobs.push(null)
#  check = () ->
#    if tmp_jobs.length == jobids.length
#      console.log("finish_get_jobs", Date())
#      success(tmp_jobs)
#    else
#      setTimeout(check, 50)
#  if success != null
#    check()

window.get_jobs = get_jobs

###
  Site creation
###
decode = (text) ->
  return decodeURIComponent((text+'').replace(/\+/g, '%20'))


id = (obj) ->
  obj

zeros = (number, positions) ->
  res = number.toFixed(0)
  while res.length < positions
    res = "0"+res
  return res

date_format = (dateVal) ->
  if dateVal
    d = new Date(dateVal*1000)
    return zeros(d.getFullYear(),4)+"-"+zeros(d.getMonth()+1, 2)+"-"+zeros(d.getDate(),2)+" "+zeros(d.getHours(),2)+":"+zeros(d.getMinutes(),2)
  else
    return "not set"

update_group = (name) ->
  $("#nav-status").removeClass().text("")
  last_group = name
  ga("set",DIMENSION_GROUP_LABEL,name)
  if name and name.length
    $(".group-label").text(name)

    ###
      Group Settings Pane
    ###
    table = $("#groupSettingsTable")
    table.html("")
    for nr in window.groups[name].rounds
      row = $("<tr></tr>")
      round = window.rounds[nr]
      row.append("<td>"+round.name+"</td>")
      row.append("<td>"+date_format(round.start_time)+"</td>")
      row.append("<td>"+date_format(round.end_time)+"</td>")
      ds_field = $("<td></td>")
      ds_list = $("<ul class='list-inline'></ul>")
      for ds in round.datasets
        link = $("<a class='dataset-preview-opener' dataset-pk='"+ds+"'>"+window.datasets[ds].name+"</a>")
        link.click(open_dataset_preview)
        field = $("<li></li>")
        field.append(link)
        ds_list.append(field)
      ds_field.append(ds_list)
      row.append(ds_field)
      alg_field = $("<td></td>")
      alg_list = $("<ul class='list-inline'></ul>")
      for alg in round.algorithms
        alg_list.append("<li>"+alg+"</li>")
      alg_field.append(alg_list)
      row.append(alg_field)
      row.append("<td class='round-status-label-"+round.id+" round-status-text-"+round.id+"'>&nbsp;</td>")
      actions = $("<div class='btn-group'></div>")
      edit_btn = $('
          <button class="btn btn-primary btn-xs edit-round" round="'+round.id+'">
              Edit
          </button>')
      edit_btn.click(edit_round)
      actions.append(edit_btn)
      start_btn = $('
                      <button class="btn btn-success btn-xs" round="'+round.id+'">
                          <span class="glyphicon glyphicon-play"></span>
                      </button>')
      start_btn.click(() ->
        $.getJSON("/actions/start_round/"+$(this).attr("round")+".json")
      )
      stop_btn = $('
                      <button class="btn btn-danger btn-xs" round="'+round.id+'">
                          <span class="glyphicon glyphicon-stop"></span>
                      </button>')
      stop_btn.click(() ->
        $.getJSON("/actions/stop_round/"+$(this).attr("round")+".json")
      )

      actions.append(start_btn)
      actions.append(stop_btn)
      actions.appendTo($("<td></td>").appendTo(row))
      table.append(row)


    ###
      Navbar
    ###
    $("#round-label").text("Rounds")
    list = $("#roundNavList")
    list.html("")
    for nr in window.groups[name].rounds
      round = window.rounds[nr]
      item = $("<li></li>")
      anchor = $("<a href='"+round.url+"' class='ajax'>"+round.name+"</a>")
      anchor.click(update_location)
      item.append(anchor)
      list.append(item)

    if window.ADMIN
      list.append('<li class="divider"></li>')
      list.append('<li><a href="#" data-toggle="modal" data-target="#editRoundModal"><span class="glyphicon glyphicon-plus"></span> Add new</a></li>')
      settings_link = $('<a href="'+window.groups[name].url+'settings/"><span class="glyphicon glyphicon-cog" class="ajax"></span> Settings</a>')
      settings_link.click(update_location)
      list.append($("<li></li>").append(settings_link))
    $("#roundNav").show()

  else
    $("#group-label").text("Groups")
    $("#roundNav").hide()

  update_group_point_list(name)
  update_round_status()


update_page = (path) ->

  if not path.length
    # Load Index
    if last_page != "index"
      $(".page").hide()
      $("#page-index").show()
      last_page = "index"
      update_group(null)
    return

  group_name = decode(path.shift())
  if group_name == "datasets"
    # Load Dataset page
    if last_page != "datasets"
      $(".page").hide()
      $("#page-datasets").show()
      last_page = "datasets"
      update_group(null)
    return

  update_group(group_name)

  if path.length == 0 or path[0] == "settings"
    # load group page

    $(".page").hide()
    $("#page-group").show()
    if path.length == 0
      $('#groupTabsLinks a[href="#group_overview"]').tab("show")
    else
      $('#groupTabsLinks a[href="#group_settings"]').tab("show")
    last_page = "group"
    $("#editRoundGroup").val(group_name)
  else
    # load round page
    round = parseInt(path.splice(0,1)[0])
    update_round(round)
    $(".page").hide()
    $("#page-round").show()

    if path.length == 0
      $('#roundTabsLinks a[href="#round_results"]').tab("show")
    else
      if path[0] == "joblist"
        update_job_list_view()
        $('#roundTabsLinks a[href="#round_job_list"]').tab("show")
      else
        $('#roundTabsLinks a[href="#round_job"]').tab("show")
        path.splice(0,1)
        if path.length > 1 and path[0] == "details"
          $("#page-round-job-detail").show()
          update_job_details(parseInt(path[1]))
        else
          if path.length > 0
            $("#createJobDatasetDetails").show()
            $("#createJobAlgorithm").prop("disabled", false)
            update_job_dataset(parseInt(path[0]))
            path.splice(0,1)
            if path.length > 0
              update_job_algorithm(path[0])
          else
            $("#createJobDatasetDetails").hide()
            $("#createJobAlgorithm").prop("disabled", true)
          $("#page-round-job-create").show()
    last_page = "round"


update_round = (round_id) ->
  check_round_jobs(round_id)
  $("#createJobRun").removeClass().addClass("btn btn-primary round-status-hider-"+round_id)
  $("#createJobRun").prop("disabled",false)
  $("#nav-status").removeClass().addClass("round-status-label-"+round_id+" round-status-text-"+round_id)
  if last_round == round_id
    return
  last_round = round_id
  update_job_history_view()
  if scores[round_id] == undefined
    fetch_score_list(round_id)
  round = rounds[round_id]
  ga("set",DIMENSION_ROUND_LABEL,last_group + " - " + rounds[round_id].name)


  $(".round-label").text(rounds[round_id].name)
  ds_selector = $("#createJobDataset")
  ds_selector.html("")
  ds_selector.append("<option value=''>please choose dataset</option>")
  for ds_id in round.datasets
    ds_selector.append("<option value='"+ds_id+"'>"+datasets[ds_id].str+"</option>")

  alg_selector = $("#createJobAlgorithm")
  alg_selector.html("")
  if round.algorithms.length > 1
    alg_selector.append("<option value=''>please choose algorithm</option>")
    for alg in round.algorithms
      alg_selector.append("<option value='"+alg+"'>"+algorithms[alg].name+"</option>")
  else
    alg_selector.append("<option value='"+round.algorithms[0]+"'>"+algorithms[round.algorithms[0]].name+"</option>")
    if algorithms[round.algorithms[0]].short_desc_html != undefined
      $("#createJobAlgorithmDesc").html(algorithms[round.algorithms[0]].short_desc_html)
    update_job_algorithm(round.algorithms[0])
  if round.preprocessing.length > 0
    $("#PreprocessingPanel").show()
  else
    $("#PreprocessingPanel").hide()
  update_result_list(round_id)
  update_hints(round_id)
  update_round_status()

update_job_dataset = (ds_id) ->
  if last_create_job_dataset != ds_id
    ds = datasets[ds_id]
    last_create_job_dataset = ds_id
    update_job_history_view()
    if ds.cluster_dataset
      $(".cluster_hidden").hide()
    else
      $(".cluster_hidden").show()
    $("#createJobDataset").val(ds_id)
    $("#createJobDatasetDimensions").text(ds.get_dimension_str)
    $("#createJobDatasetTrainingSize").text(ds.nr_training_points)
    $("#createJobDatasetTestSize").text(ds.nr_test_points)
    $("#createJobDatasetValidationSize").text(ds.nr_validation_points)
    $("#createJobDatasetClasses").text(ds.nr_classes)
    $("#createJobDatasetMsg").html("<span class='limits-view-"+last_round+"-"+ds_id+"'></span>")
    check_job_limits(last_round, ds_id)
    load_dataset(ds_id,"#createJobDatasetDetailsPreview",null, "#createJobDatasetDetailsPreviewLine")
    fill3Dsetter(ds.dimensions, "#createJob3D")
    preprocessing_config = []
    awaiting_preprocessing = null
    preprocessing_id = null
    update_preprocessing()


create_parameter_inputs = (container, parameters, prefixid="createJobParam-", prefixname="param-", classes="") ->
  for paramid, param of parameters
    form_group = $("<div class='form-group "+classes+"'></div>").appendTo(container)
    $("<label class='col-sm-2 control-label' for='"+prefixid+paramid+"'>"+param.name+"</label>").appendTo(form_group)
    col_div = $("<div class='col-sm-4'></div>").appendTo(form_group)
    if param.type in ["int", "float"]
      field = $("<input class='form-control' type='number' value='"+param.default+"' id='"+prefixid+paramid+"' name='"+prefixname+paramid+"'/>")
      for attr in ["min","max","step"]
        if param[attr] != undefined
          field.prop(attr, param[attr])
      col_div.append(field)
    else if param.type in ["string"]
      field = $("<input class='form-control' type='text' value='"+param.default+"' id='"+prefixid+paramid+"' name='"+prefixname+paramid+"'/>")
      col_div.append(field)
    else if param.type == "bool"
      field = $("<input type='checkbox' id='"+prefixid+paramid+"' name='"+prefixname+paramid+"'/>")
      label = $("<label></label>").append(field)
      if param.default
        field.prop("checked", true)
      if param.label != undefined
        label.append("&nbsp;")
        label.append(param.label)
      col_div.append(label)
    else if param.type == "list"
      if param.multi != true
        field = $("<select class='form-control' id='"+prefixid+paramid+"' name='"+prefixname+paramid+"'></select>")
        field.append("<option value=''>please choose</option>")
        for v in param.values
          field.append("<option>"+v+"</option>")
        col_div.append(field)
        field.val(param.default)
      else
        for v in param.values
          label = $("<label></label>")
          field = $("<input type='checkbox' id='"+prefixid+paramid+"_"+v+"' name='"+prefixname+paramid+"_"+v+"' />")
          if v == param.default or v in param.default
            field.prop("checked", true)
          label.append(field)
          label.append("&nbsp;")
          label.append(v)
          label.append("<br />")
          col_div.append($("<div></div>").append(label))
    else if param.type == "dict"
      if param.multi != true
        field = $("<select class='form-control' id='"+prefixid+paramid+"' name='"+prefixname+paramid+"'></select>")
        field.append("<option value=''>please choose</option>")
        for option, info of param.values
          new_classes = prefixname+paramid+"_"+option+" not-"+prefixname+paramid+"_undefined"
          for other,_ of param.values
            if other != option
              new_classes = new_classes+ " not-"+prefixname+paramid+"_"+other

          if info.name != undefined
            label = info.name
          else
            label = option
          field.append("<option value='"+option+"'>"+label+"</option>")
          if info.parameters != undefined
            create_parameter_inputs(container, info.parameters, prefixid+paramid+"_"+option+"-", prefixname+paramid+"_"+option+"-", new_classes)
        field.change(() ->
          opt = $(this)
          $("."+opt.attr("name")+"_"+opt.val()).show("fast")
          $(".not-"+opt.attr("name")+"_"+opt.val()).hide("fast")
        )
        $(".not-"+prefixname+paramid+"_undefined").hide()
        console.log()
        col_div.append(field)
        field.val(param.default).change()
      else
        for option, info of param.values
          label = $("<label></label>")
          field = $("<input type='checkbox' id='"+prefixid+paramid+"_"+option+"' name='"+prefixname+paramid+"_"+option+"' />")
          if option == param.default or option in param.default
            field.prop("checked", true)
          label.append(field)
          label.append("&nbsp;")

          new_classes = prefixname+paramid+"_"+option+" not-"+prefixname+paramid+"_undefined"
          for other,_ of param.values
            if other != option
              new_classes = new_classes+ " not-"+prefixname+paramid+"_"+other

          if info.name != undefined
            l = info.name
          else
            l = option
          label.append(l)

          if info.parameters != undefined
            create_parameter_inputs(container, info.parameters, prefixid+paramid+"_"+option+"-", prefixname+paramid+"_"+option+"-", new_classes)

          field.change(() ->
            opt = $(this)
            if opt.prop("checked")
              $("."+opt.attr("name")).show("fast")
            else
              $("."+opt.attr("name")).hide("fast")
          )
          label.append("<br />")
          col_div.append($("<div></div>").append(label))
        $(".not-"+prefixname+paramid+"_undefined").hide()
    else if param.type == "dist"
      field = $("<select class='form-control' id='"+prefixid+paramid+"' name='"+prefixname+paramid+"'></select>")
      field.append("<option value=''>please choose</option>")
      for distid, dist of distances
        if param.values != undefined and distid not in param.values
          continue
        field.append("<option>"+distid+"</option>")
      if param.values != undefined
        field.append("<option>"+val+"</option>") for val in param.values if val not in algorithms
      col_div.append(field)
    desc = $('<div class="col-sm-6 small"></div>').appendTo(form_group)
    if param.short_desc_html != undefined
      desc.append($("<div style='display: inline-block;'></div>").append($(param.short_desc_html)))
      if param.description_html != undefined
        link = $("<a style='display: inline-block;' param='"+paramid+"'> &nbsp;<span class='glyphicon glyphicon-question-sign'></span></a> ").appendTo(desc)
        link.click(() ->
          $("#paramdesc-"+$(this).attr("param")).show()
        )
        desc.append($("<div hidden='true' id='paramdesc-"+paramid+"'></div>").append(param.description_html))



update_job_algorithm = (algorithm) ->
  last_create_job_algorithm = algorithm
  $("#createJobAlgorithm").val(algorithm)
  if algorithms[algorithm].short_desc_html != undefined
    desc = $("#createJobAlgorithmDesc")
    desc.html("")
    desc.append($("<div style='display: inline-block;'></div>").append($(algorithms[algorithm].short_desc_html)))
    if algorithms[algorithm].description_html != undefined
      link = $("<a style='display: inline-block;'> &nbsp;<span class='glyphicon glyphicon-question-sign'></span></a> ").appendTo(desc)
      link.click(() ->
        $("#algodesc").show()
      )
      desc.append($("<div hidden='true' id='algodesc'></div>").append(algorithms[algorithm].description_html))
  else
    $("#createJobAlgorithmDesc").text("")
  params_div = $("#createJobAlgorithmParams")
  params_div.html("")
  create_parameter_inputs(params_div, algorithms[algorithm].parameters)
  $("#createJobRun").prop("disabled",false)
  if set_job_params != null
    set_job_params_data = JSON.parse(set_job_params)
    if set_job_params_data.preprocessing != undefined
      preprocessing_config = set_job_params_data.preprocessing
      update_preprocessing()

    set_job_param_fields = (parameters, prefixid="#createJobParam-", prefixname="") ->
      if parameters == undefined
        return
      for param, paramval of parameters
        fullparam = prefixname+param
        val = set_job_params_data[fullparam]
        if val != undefined
          if paramval.type == "bool"
            $(prefixid+param).prop("checked",val).change()
          else if paramval.multi == true
            if paramval.type=="list"
              for v in paramval.values
                $(prefixid+param+"_"+v).prop("checked",v in val).change()
            else if paramval.type=="dict"
              for v,_ of paramval.values
                $(prefixid+param+"_"+v).prop("checked",v in val).change()
          else
            $(prefixid+param).val(val).change()
          if paramval.multi != true
            if paramval.type == "dict" and paramval.values != null and paramval.values[val] != undefined
              set_job_param_fields(paramval.values[val].parameters, prefixid+param+"_"+val+"-", prefixname+param+"_")
          else
            for v in val
              if paramval.type == "dict" and paramval.values != null and paramval.values[v] != undefined
                set_job_param_fields(paramval.values[v].parameters, prefixid+param+"_"+v+"-", prefixname+param+"_")

    set_job_param_fields(algorithms[algorithm].parameters)


    for param, paramval of JSON.parse(set_job_params)
      $("#createJobParam"+param).val(paramval)
    set_job_params = null


update_preprocessing = () ->
  view = $("#createJobPreprocessingList").html("")
  if datasets[$("#createJobDataset").val()] != undefined
    preprocessing_dimensions = datasets[$("#createJobDataset").val()].dimensions
  else if current_dataset_pk != null
    preprocessing_dimensions = datasets[current_dataset_pk].dimensions
  else
    preprocessing_dimensions = 1
  if preprocessing_config.length > 0
    i = 0
    for step in preprocessing_config
      if preprocessing[step.algorithm] == undefined
        continue
      li = $("<li class='list-group-item'></li>").appendTo(view)
      alg = preprocessing[step.algorithm]
      if alg.name != undefined
        li.append("<h5>"+alg.name+"</h5>")
      else
        li.append("<h5>"+step.algorithm+"</h5>")
      dl = $("<dl class='dl-horizontal'></dl>").appendTo(li)
      for key, val of step
        if key == "algorithm"
          continue
        dl.append("<dt>"+key+"</dt>")
        dl.append("<dd>"+val+"</dd>")
      btn = $('<button type="button" class="btn btn-danger btn-xs" list-index="'+i+'"> \
                  <span class="glyphicon glyphicon-remove"></span> remove this and everything below \
               </button>').appendTo(li)
      btn.click((e) ->
        idx = $(this).attr("list-index")
        preprocessing_config = preprocessing_config[0...idx]
        update_preprocessing()
      )
      i = i+1
      if alg.dimensions_out != undefined
        console.log("DIMENSIONS_OUT", alg, step)
        dimensions_in = preprocessing_dimensions
        if alg.dimensions
          dimensions_selected = step.dimensions.length
        else
          dimensions_selected = preprocessing_dimensions
        preprocessing_dimensions = eval(alg.dimensions_out)
  else
    current_dataset_pk = null
    view.append("<li class='list-group-item'>&lt;nothing specified&gt;</li>")
    load_dataset(parseInt($("#createJobDataset").val()),"#createJobDatasetDetailsPreview", null, "#createJobDatasetDetailsPreviewLine")

  cb_view = $("#preprocessingDimensionsCheckboxField").html("")
  for i in [0...preprocessing_dimensions]
    cb_view.append('<label><input class="preprocessing-dim" id="preprocessingParam-dim'+i+'" type="checkbox" value="'+i+'">&nbsp; x<sub>'+i+' </label> ')
  all_label=$("<label>").appendTo(cb_view)
  all_cb = $("<input type='checkbox'>").appendTo(all_label)
  all_label.append("&nbsp;all")
  all_cb.change(() ->
    $(".preprocessing-dim").prop("checked", $(this).prop("checked"))
  )
  $("#preprocessingDimensionsField").hide()

add_preprocessing = () ->
  algo = $("#preprocessingAlgorithm").val()

  if algo
    obj = {
      algorithm: algo
    }
    params = preprocessing[algo].parameters
    if params
      collect_job_params(params, obj, "#preprocessingParam-", "")

    if preprocessing[algo].dimensions
      dims = []
      #for i in [0...preprocessing_dimensions]
      #  if $("#preprocessingParam-dim"+i).prop("checked")
      #    dims.push(i)
      for raw_elem in $(".preprocessing-dim")
        elem = $(raw_elem)
        if elem.prop("checked")
          dims.push(parseInt(elem.val()))
      obj.dimensions = dims
      if dims.length == 0
        alert("Please select at least one dimension.")
        return

  $("#addPreprocessingModal").modal("hide")
  preprocessing_config.push(obj)
  req = {
    dataset: last_create_job_dataset,
    params: JSON.stringify(preprocessing_config)
  }
  console.log("req_pp", req)
    
  $.post("/actions/submit_preprocessing/", req, (data) ->
    console.log("sub_pp", data)
    $("#createJobDatasetDetailsPreview").html('<div class="alert alert-warning" role="alert">Waiting for data.</div>')
    $("#createJobDatasetDetailsPreviewLine").html('<div class="alert alert-warning" role="alert">Waiting for data.</div>')
    preprocessing_id = data.id
    if data.url != undefined
      load_preprocessing(data.url)
      awaiting_preprocessing = null
    else
      awaiting_preprocessing = data.id
  , "json")
  update_preprocessing()


open_add_preprocessing = () ->
  $("#addPreprocessingModal").modal("show")
  algos = $("#preprocessingAlgorithm").html("<option value=''>please choose</option>")
  for pp in rounds[last_round].preprocessing
    label = preprocessing[pp].name
    if label == undefined
       label = pp
    algos.append("<option value='"+pp+"'>"+label+"</option>")
  update_preprocessing_algorithm()

  # addPreprocessingParams


update_preprocessing_algorithm = () ->
  algo = $("#preprocessingAlgorithm").val()
  params_div = $("#addPreprocessingParams").html("")
  console.log(algo)
  if algo.length > 0
    params = preprocessing[algo].parameters
    if params != undefined
      create_parameter_inputs(params_div, params, "preprocessingParam-", "preprocessing-")
    if preprocessing[algo].dimensions
      $("#preprocessingDimensionsField").show()
    else
      $("#preprocessingDimensionsField").hide()
  else
    $("#preprocessingDimensionsField").hide()

update_content = (state=null) ->
  clearTimeout(content_update_timeout)
  content_update_timeout = setTimeout(() ->

    # Do Page Update here ...
    path = location.pathname.split("/")
    path = path.filter(id)

    update_page(path)

    $("#text").text(location.pathname)

    ga('send', 'pageview', location.pathname);
    # done ;-)
  , 50)


goto = (location, context=null) ->
  clear_preview_cache()
  history.pushState(context, null, location)
  update_content()


update_location = (e) ->
  e.preventDefault()
  goto(this.href)


###
  Create Datasets
###
parseCSVfield = (fieldname, type = 0, force_class = null) ->
  data = $(fieldname).val()
  rows = data.split("\n")
  rows = rows.filter(id)
  rows = rows.map(((x) -> x.split(",")))
  objects = (({
    coords: row.map(((x) -> parseFloat(x)))[0 ... -1],
    class_id: if force_class==null then row[row.length - 1] else force_class,
    point_type: type
  }) for row in rows)
  return objects

createNewDatasetPreview = () ->
  if $("#createDatasetSampleMethodCluster").prop("checked")
    data = parseCSVfield("#createDatasetDataset", window.TYPE_VALIDATION)
    data = data.concat(parseCSVfield("#createDatasetDataset", window.TYPE_TEST, "0"))
    $("#createDatasetClusterDataset").val(1)
  else
    $("#createDatasetClusterDataset").val(0)
    data = parseCSVfield("#createDatasetDataset", window.TYPE_TRAINING)
    if $("#createDatasetSampleMethodGiven").prop("checked")
      data = data.concat(parseCSVfield("#createDatasetTestset", window.TYPE_TEST))
      data = data.concat(parseCSVfield("#createDatasetValidationset", window.TYPE_VALIDATION))
    else
      objs = [0...data.length]
      nr_test = $("#createDatasetTestSize").val() / 100.0 * data.length
      nr_validation = $("#createDatasetValidationSize").val() / 100.0 * data.length
      for x in [1..(nr_test)]
        pos = Math.random() * objs.length
        try
          data[objs.splice(pos, 1)].point_type = window.TYPE_TEST
        catch
          console.log("not enough data points")
      for x in [1..(nr_validation)]
        pos = Math.random() * objs.length
        try
          data[objs.splice(pos, 1)].point_type = window.TYPE_VALIDATION
        catch
          console.log("not enough data points")


  max_coords = 1
  for obj in data
    max_coords = Math.max(max_coords, obj.coords.length)

  preview_header = $("#createDatasetPreviewHeadRow")
  preview_header.html("<th>Class</th><th>Subset</th>")
  for coord in [0...max_coords]
    preview_header.append("<th>x<sub>"+coord+"</sub></th>")
  preview = $("#createDatasetPreviewBody")
  preview.html("")
  for obj in data[..300]
    row = null
    if obj.point_type == window.TYPE_TEST
      row = $("<tr class='success'></tr>")
      subset = "Test"
    else if obj.point_type == window.TYPE_VALIDATION
      row = $("<tr class='info'></tr>")
      subset = "Validation"
    else
      row = $("<tr></tr>")
      subset = "Training"
    row.append("<td>"+obj.class_id+"</td>")
    row.append("<td>"+subset+"</td>")
    for coord in obj.coords
      row.append("<td>"+coord+"</td>")
    preview.append(row)
  $("#createDatasetPoints").val(JSON.stringify(data))


$("#createDatasetDataset").change(createNewDatasetPreview)
$("#createDatasetDataset").keyup(createNewDatasetPreview)
$("#createDatasetTestset").change(createNewDatasetPreview)
$("#createDatasetTestset").keyup(createNewDatasetPreview)
$("#createDatasetValidationset").change(createNewDatasetPreview)
$("#createDatasetValidationset").keyup(createNewDatasetPreview)
$("#createDatasetTestSize").change(createNewDatasetPreview)
$("#createDatasetTestSize").keyup(createNewDatasetPreview)
$("#createDatasetValidationSize").keyup(createNewDatasetPreview)
$("#createDatasetSampleMethodCluster").change(createNewDatasetPreview)
$("#createDatasetSampleMethodGiven").change(createNewDatasetPreview)
$("#createDatasetSampleMethodRandom").change(createNewDatasetPreview)


###
  Create/Edit Round
###
er_marker_update = () ->
  datasets = JSON.parse($("#editRoundDatasets").val())
  $(".er-ds-toggle").removeClass("active")
  $("#er-ds-"+ds).addClass("active") for ds in datasets

  algs = JSON.parse($("#editRoundAlgorithms").val())
  $(".er-alg-toggle").removeClass("active")
  $("#er-alg-"+ds).addClass("active") for ds in algs

  pps = JSON.parse($("#editRoundPreprocessing").val())
  $(".er-pp-toggle").removeClass("active")
  $("#er-pp-"+ds).addClass("active") for ds in pps


er_ds_toggle = () ->
  pk = parseInt(this.id[6..])
  datasets = JSON.parse($("#editRoundDatasets").val())
  if pk in datasets
    pos = datasets.indexOf(pk)
    datasets.splice(pos,1)
  else
    datasets = datasets.concat([pk])

  $("#editRoundDatasets").val(JSON.stringify(datasets))
  er_marker_update()

er_alg_toggle = () ->
  pk = this.id[7..]
  algs = JSON.parse($("#editRoundAlgorithms").val())
  if pk in algs
    pos = algs.indexOf(pk)
    algs.splice(pos,1)
  else
    algs = algs.concat([pk])

  $("#editRoundAlgorithms").val(JSON.stringify(algs))
  er_marker_update()

er_pp_toggle = () ->
  pk = this.id[6..]
  pps = JSON.parse($("#editRoundPreprocessing").val())
  if pk in pps
    pos = pps.indexOf(pk)
    pps.splice(pos,1)
  else
    pps = pps.concat([pk])

  $("#editRoundPreprocessing").val(JSON.stringify(pps))
  er_marker_update()

window.edit_round = () ->
  round = window.rounds[$(this).attr("round")]
  $("#editRoundRound").val(round.id)
  $("#editRoundAlgorithms").val(JSON.stringify(round.algorithms))
  $("#editRoundDatasets").val(JSON.stringify(round.datasets))
  $("#editRoundPreprocessing").val(JSON.stringify(round.preprocessing))
  $("#editRoundName").val(round.name)
  if round.start_time
    start = date_format(round.start_time)
    $("#editRoundStartDate").val(start[...10])
    $("#editRoundStartTime").val(start[11...])
  else
    $("#editRoundStartDate").val("")
    $("#editRoundStartTime").val("")
  if round.end_time
    end = date_format(round.end_time)
    $("#editRoundEndDate").val(end[...10])
    $("#editRoundEndTime").val(end[11...])
  else
    $("#editRoundEndDate").val("")
    $("#editRoundEndTime").val("")

  er_marker_update()

  $("#editRoundModal").modal("show")

###
  Plot preview
###
get_color = (nr) ->
  if nr < 0
    return "black"
  else
    return COLORS[nr % COLORS.length]

get_marker_symbol = (point_type) ->
  if point_type == window.TYPE_TEST
    return "square"
  else if point_type == window.TYPE_VALIDATION
    return "diamond"
  return "circle"

get_histogram = (dataset, dimension, buckets=10, class_id=null) ->
  min = dataset[0].coords[dimension]
  max = dataset[0].coords[dimension]
  min = Math.min(min, p.coords[dimension]) for p in dataset
  max = Math.max(max, p.coords[dimension]) for p in dataset
  hist = (0 for x in [0..buckets])
  if class_id != null
    ds = (p for p in dataset when p.class_id==class_id)
  else
    ds = dataset
  hist[parseInt((p.coords[dimension]-min)/(max-min)*buckets)]+=1 for p in ds
  hist[buckets-1] += hist[buckets]
  hist.splice(buckets,1)
  return hist

process_dataset = (data, pk, field_id, labels=null, dimensionlist=null, limit=500) ->
  if pk!=null
    dimensions = datasets[pk].dimensions
    current_dataset_pk = pk
  else
    dimensions = data[0].coords.length

  if dimensionlist == null
    if pk!=null and datasets[pk].default_dimensions.length > 0
      dimensionlist = datasets[pk].default_dimensions
    else
      dimensionlist = [0...Math.min(dimensions, VISUALIZATION_DIMENSION_DEFAULT)]

  current_dataset_data = data

  tmp_data = data # (data[i] for i in index_list)

  current_dataset_labels = labels
  current_dataset_series_data = ({
    coords: p.coords,
    color: get_color(p.class_id),
    marker: {
      symbol: get_marker_symbol(p.point_type),
      radius: 2
    }

  } for p in tmp_data)

  legend = ["Training Set", "Test Set", "Validation Set"]
  current_dataset_series = ({
    name: legend[type],
    animation: false,
    color: "black",
    turboThreshold: 0,
    marker: {
      symbol: get_marker_symbol(type),
      radius: 2
    }
    data: ({
      coords: p.coords,
      dataLabels: {
        pk: p.pk
        coords: JSON.stringify(p.coords)
      },
      name: datasets[current_dataset_pk].class_labels[p.class_id],
      color: get_color(p.class_id)
    } for p in tmp_data when p.point_type == type)
  } for type in [0,1,2])

  if labels != null and labels != undefined
    for t in [0,1,2]
      if labels[t] == undefined
        break
      for p in [0...current_dataset_series[t].data.length]
        if labels[t][p] == undefined
          break
        current_dataset_series[t].data[p].marker = {
          lineWidth: 2,
          lineColor: get_color(labels[t][p])
        }

  index_list_generator = (a, b) ->
    (parseInt(i*b/a) + parseInt(b/(2*a)) for i in [0...a])

  limits = (parseInt(limit*current_dataset_series[t].data.length/tmp_data.length) for t in [0,1,2])

  index_lists = (index_list_generator(limits[t], current_dataset_series[t].data.length) for t in [0,1,2])

  for t in [0,1,2]
    current_dataset_series[t].data = (current_dataset_series[t].data[i] for i in index_lists[t])

  container = $(field_id)
  container.html("")
  sample_selector_row = $("<div class='row'></div>").appendTo(container)

  sample_selector_row.append("<div class='col-sm-12 col-md-2'><strong>Sample Size:</strong></div>")
  min_abs = Math.min(10, current_dataset_data.length)
  max_abs = current_dataset_data.length
  min_log = Math.log(min_abs)
  max_log = Math.log(max_abs)
  pos_abs = Math.min(current_dataset_data.length, limit)
  pos_log = Math.log(pos_abs)
  range_selector = $("<input id='sample_slider' class='form-control sample_slider' type='range' min='"+min_log+"' max='"+max_log+"' step='0.01' value='"+pos_log+"' />").appendTo($("<div class='col-sm-6 col-md-6'></div>").appendTo(sample_selector_row))
  range_selector.bind("input", () ->
    val = $(this).val()
    $(".sample_size").val(parseInt(Math.exp(val)))
  )
  range_selector_direct = $("<input id='sample_size' class='form-control sample_size' type='number' min='"+min_abs+"' max='"+max_abs+"' step='1' value='"+pos_abs+"'/>").appendTo($("<div class='col-sm-6 col-md-2'></div>").appendTo(sample_selector_row))
  range_selector_direct.bind("input", () ->
    val = $(this).val()
    $(".sample_slider").val(Math.log(val))
  )
  range_selector_apply = $("<button class='btn btn-primary'>Apply</button>").appendTo($("<div class='col-sm-6 col-md-2'></div>").appendTo(sample_selector_row))
  range_selector_apply.click(() ->
    limit = $("#sample_size").val()
    process_dataset(data, pk, field_id, labels, dimensionlist, limit)
  )

  selector = $("<form role='form'><strong>Dimensions: </strong></form>").appendTo($("<div class='col-sm-12'></div>").appendTo($("<div class='row'></div>").appendTo(container)))
  for dim in [0...dimensions]

    l = $("<label class='checkbox-inline'></label>").appendTo(selector)
    cb = $("<input type='checkbox' dim='"+dim+"' />").appendTo(l)
    if dim in dimensionlist
      cb.attr("checked", true)
      cb.change(() ->
        new_dim_list = (x for x in [0...dimensions] when x in dimensionlist and x!=parseInt($(this).attr("dim")))
        process_dataset(data, pk, field_id, labels, new_dim_list, limit)
      )
    else
      cb.change(() ->
        new_dim_list = (x for x in [0...dimensions] when x in dimensionlist or x==parseInt($(this).attr("dim")))
        process_dataset(data, pk, field_id, labels, new_dim_list, limit)
      )
    l.append("x<sub>"+dim+"</sub> ")

  if ADMIN and pk!=null
    save_btn = $("<button type='button' class='btn btn-primary btn-xs pull-right'>Save as default</button>").appendTo(selector)
    save_btn.click(() ->
      data = {
        dataset_id: pk,
        dimensions: JSON.stringify(dimensionlist)
      }
      $.post("/actions/update_dataset_default_dimension/", data, () ->
        datasets[pk].default_dimensions = dimensionlist
        alert("Stored as default.")
      )
    )

  width = container.width() / dimensionlist.length *0.95
  for y in dimensionlist
    for x in dimensionlist
      elem = $("<div style='width:"+width+"px; height:"+width+"px; padding:0px;margin:0px;display:inline-block'></div>")
      if x != y
        for s in current_dataset_series
          for p in s.data
            p.x = p.coords[x]
            p.y = p.coords[y]
        settings = {
          chart: {
            type: "scatter",
            animation: false,
            borderWidth: 1,
            events: {
              click: open_scatter_details
            }
          },
          credits: {
            enabled: false
          },
          title: {
            text: null,
            floating: true
          },
          yAxis: {
            title: {
              text: null
            }
          },
          legend: {
            enabled: false,
          },
          tooltip: {
            enabled: false,
          },
          series: current_dataset_series
        }
        elem.highcharts(settings)
      else
        series = ({
          data: get_histogram(current_dataset_data,x,10,class_id),
          color: get_color(class_id),
          animation: false
        } for class_id in [0...datasets[current_dataset_pk].nr_classes])
        settings = {
          chart: {
            type: 'column',
            animation: false,
            borderWidth: 1
          },
          credits: {
            enabled: false
          },
          title: {
            text: "x<sub>"+x+"</sub>",
            useHTML: true
          },
          yAxis: {
            title: {
              text: null
            }
          },
          xAxis: {
            endOnTick: false,
            labels: {
              enabled: false
            }
          }
          legend: {
            enabled: false,
          },
          tooltip: {
            enabled: false,
          },
          plotOptions: {
            column: {
              stacking: 'normal'
            },
            series: {
              shadow: false,
              borderWidth:1,
              pointPadding:0,
              groupPadding:0,
            }
          },
          series: series
        }
        elem.highcharts(settings)
      container.append(elem)

process_dataset_line = (data, pk, field_id, labels=null, dimensionlist=null, limit=20) ->
  if pk!=null
    dimensions = datasets[pk].dimensions
    current_dataset_pk = pk
  else
    dimensions = data[0].coords.length

  current_dataset_data = data

  tmp_data = data

  tmp_data_parts = ((p for p in tmp_data when p.point_type == type) for type in [0,1,2])
  d = Array()
  for p in tmp_data_parts
    d = d.concat(p)
  tmp_data = d

  # console.log("DATA", data)

  index_list_generator = (a, b) ->
    (parseInt(i*b/a) + parseInt(b/(2*a)) for i in [0...a])

  if labels != null and labels != undefined
    # console.log("LABELS:", labels)

    if labels[0].constructor == Array
      l = labels[0]
      l = l.concat(labels[1])
      l = l.concat(labels[2])
      labels = l
      # console.log("LABELS:", labels)


    for i in [0...tmp_data.length]
      tmp_data[i].class_id = labels[i]

  index_list = index_list_generator(limit, tmp_data.length)

  tmp_data = (tmp_data[i] for i in index_list)

  container = $(field_id)
  container.html("")
  sample_selector_row = $("<div class='row'></div>").appendTo(container)

  sample_selector_row.append("<div class='col-sm-12 col-md-2'><strong>Sample Size:</strong></div>")
  min_abs = Math.min(10, current_dataset_data.length)
  max_abs = current_dataset_data.length
  min_log = Math.log(min_abs)
  max_log = Math.log(max_abs)
  pos_abs = Math.min(current_dataset_data.length, limit)
  pos_log = Math.log(pos_abs)
  range_selector = $("<input id='sample_slider' class='form-control sample_slider' type='range' min='"+min_log+"' max='"+max_log+"' step='0.01' value='"+pos_log+"' />").appendTo($("<div class='col-sm-6 col-md-6'></div>").appendTo(sample_selector_row))
  range_selector.bind("input", () ->
    val = $(this).val()
    $(".sample_size").val(parseInt(Math.exp(val)))
  )
  range_selector_direct = $("<input id='sample_size' class='form-control sample_size' type='number' min='"+min_abs+"' max='"+max_abs+"' step='1' value='"+pos_abs+"'/>").appendTo($("<div class='col-sm-6 col-md-2'></div>").appendTo(sample_selector_row))
  range_selector_direct.bind("input", () ->
    val = $(this).val()
    $(".sample_slider").val(Math.log(val))
  )
  range_selector_apply = $("<button class='btn btn-primary'>Apply</button>").appendTo($("<div class='col-sm-6 col-md-2'></div>").appendTo(sample_selector_row))
  range_selector_apply.click(() ->
    limit = $("#sample_size").val()
    process_dataset_line(data, pk, field_id, labels, dimensionlist, limit)
  )

  width = container.width()
  
  # console.log("Plot colors:", (p.class_id for p in tmp_data))

  settings = {
    chart: {
      type: "line",
      animation: false,
      borderWidth: 1,
      zoomType: 'xy',
    },
    title: {
      text: null,
      floating: true
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: [0...dimensions],
    },
    yAxis: {
      title: {
        text: null
      },
    }
    legend: {
      enabled: false
    },
    tooltip: {
      enabled: false,
    },
    plotOptions: {
      line: {
        marker: {
          enabled: false,
        },
        lineWidth: .3,
      },
    },

    series:
      ({
        color: get_color(p.class_id),
        data: p.coords,
        animation: false,
      } for p in tmp_data)

  }
  elem = $("<div style='width:"+width+"px; padding:0px;margin:0px;display:inline-block'></div>")
  console.log(settings)
  elem.highcharts(settings)
  container.append(elem)


#  width = container.width() / dimensionlist.length *0.95
#  for y in dimensionlist
#    for x in dimensionlist
#      elem = $("<div style='width:"+width+"px; height:"+width+"px; padding:0px;margin:0px;display:inline-block'></div>")
#      if x != y
#        for s in current_dataset_series
#          for p in s.data
#            p.x = p.coords[x]
#            p.y = p.coords[y]
#        settings = {
#          chart: {
#            type: "scatter",
#            animation: false,
#            borderWidth: 1,
#            events: {
#              click: open_scatter_details
#            }
#          },
#          credits: {
#            enabled: false
#          },
#          title: {
#            text: null,
#            floating: true
#          },
#          yAxis: {
#            title: {
#              text: null
#            }
#          },
#          legend: {
#            enabled: false,
#          },
#          tooltip: {
#            enabled: false,
#          },
#          series: current_dataset_series
#        }
#        elem.highcharts(settings)
#      else
#        series = ({
#          data: get_histogram(current_dataset_data,x,10,class_id),
#          color: get_color(class_id),
#          animation: false
#        } for class_id in [0...datasets[current_dataset_pk].nr_classes])
#        settings = {
#          chart: {
#            type: 'column',
#            animation: false,
#            borderWidth: 1
#          },
#          credits: {
#            enabled: false
#          },
#          title: {
#            text: "x<sub>"+x+"</sub>",
#            useHTML: true
#          },
#          yAxis: {
#            title: {
#              text: null
#            }
#          },
#          xAxis: {
#            endOnTick: false,
#            labels: {
#              enabled: false
#            }
#          }
#          legend: {
#            enabled: false,
#          },
#          tooltip: {
#            enabled: false,
#          },
#          plotOptions: {
#            column: {
#              stacking: 'normal'
#            },
#            series: {
#              shadow: false,
#              borderWidth:1,
#              pointPadding:0,
#              groupPadding:0,
#            }
#          },
#          series: series
#        }

open_scatter_details = (e) ->
#  console.log(e, this)
  $("#chartDetailsModal").modal("show")
  $("#chartDetailsModalDiv").html("")
  settings = {
    chart: {
      type: "scatter",
      animation: false,
      borderWidth: 1,
      zoomType: "xy",
      height: $("#chartDetailsModalDiv").width()
    },
    credits: {
      enabled: false
    },
    title: {
      text: null,
      floating: true
    },
    yAxis: {
      title: {
        text: null
      }
    },
    tooltip: {
      enabled: true,
      pointFormat: "Class: <b>{point.name}</b><br />ID: #<b>{point.dataLabels.pk}</b><br />Coords: <b>{point.dataLabels.coords}</b><br />x: <b>{point.x}</b><br />y: <b>{point.y}.</b>"
    },
    series: ({
      name: s.name,
      animation: false,
      color: "black",
      turboThreshold: 0,
      events: {
        click: (e) ->
          window.prompt("Copy to clipboard: Ctrl+C, Enter", e.point.dataLabels.coords);
      }
      data: ({
        x: p.x,
        y: p.y,
        color: p.color,
        name: p.name,
        dataLabels: p.dataLabels,
        marker: p.marker
      } for p in s.data),
    } for s in this.series)
  }
#  console.log(this.series)
#  console.log(settings.series)

  $("#chartDetailsModalDiv").highcharts(settings)



load_dataset = (pk, field_id=null, labels=null, field_id_line=null) ->
  # console.log(pk, current_dataset_pk)
  console.log("LOAD_DS", field_id, field_id_line)

  if current_dataset_pk != pk
    $.getJSON(datasets[pk].url, null, (data) ->
      if field_id != null
        process_dataset(data, pk, field_id, labels)
      if field_id_line != null
        process_dataset_line(data, pk, field_id_line, labels)
    )
  else
    if field_id != null
      process_dataset(current_dataset_data, current_dataset_pk, field_id, labels)
    if field_id_line != null
      process_dataset_line(current_dataset_data, current_dataset_pk, field_id_line, labels)

load_dataset_default = (pk, field_id=null, labels=null) ->
  # console.log(pk, current_dataset_pk)
  console.log("LOAD_DS", field_id)

  method = process_dataset

  if datasets[pk].default_visualization == "line"
    method = process_dataset_line

  if current_dataset_pk != pk
    $.getJSON(datasets[pk].url, null, (data) ->
      method(data, pk, field_id, labels)
    )
  else
    method(current_dataset_data, current_dataset_pk, field_id, labels)

load_preprocessing = (url, field_id="#createJobDatasetDetailsPreview", labels=null, field_id_line="#createJobDatasetDetailsPreviewLine") ->
  console.log("load_pp", url)
  $.getJSON(url, null, (data) ->
    # console.log("get_pp", data)
    if field_id != null
      process_dataset(data, null, field_id, labels)
    if field_id_line != null
      process_dataset_line(data, null, field_id_line, labels)
  ).fail((data,textStatus, error) ->
    console.log("get_pp_fail", textStatus, error)

    # Dirty hack, sometime json could not parse. I don't know why!

    data = eval(data.responseText)
    if field_id != null
      process_dataset(data, null, field_id, labels)
    if field_id_line != null
      process_dataset_line(data, null, field_id_line, labels)
  )

fill3Dsetter = (dimensions, prefix) ->
  for fid in (prefix+coord for coord in ["x","y","z"])
    field = $(fid)
    field.html("<option value='-1'>please choose</option>")
    for dim in [0...dimensions]
      field.append("<option value='"+dim+"'>x"+dim+"</option>")


update3Dplot = (e) ->
  field = $(this)
  target = $(field.attr("plotdiv"))
  target.html("")
  prefix = "#"+field.attr("id")[0...-1]
  valx = parseInt($(prefix+"x").val())
  valy = parseInt($(prefix+"y").val())
  valz = parseInt($(prefix+"z").val())
  if valx < 0 or valy < 0 or valz < 0
    return

  minx = Math.min.apply(null, (Math.min.apply(null, (p.coords[valx] for p in s.data)) for s in current_dataset_series))
  maxx = Math.max.apply(null, (Math.max.apply(null, (p.coords[valx] for p in s.data)) for s in current_dataset_series))
  miny = Math.min.apply(null, (Math.min.apply(null, (p.coords[valy] for p in s.data)) for s in current_dataset_series))
  maxy = Math.max.apply(null, (Math.max.apply(null, (p.coords[valy] for p in s.data)) for s in current_dataset_series))
  minz = Math.min.apply(null, (Math.min.apply(null, (p.coords[valz] for p in s.data)) for s in current_dataset_series))
  maxz = Math.max.apply(null, (Math.max.apply(null, (p.coords[valz] for p in s.data)) for s in current_dataset_series))

  #  Set up the chart
  chart = new Highcharts.Chart({
    chart: {
      renderTo: target.attr("id"),
      margin: 100,
      type: 'scatter',
      options3d: {
        enabled: true,
        alpha: 10,
        beta: 30,
        depth: 250,
        viewDistance: 5,

        frame: {
          bottom: { size: 1, color: 'rgba(0,0,0,0.02)' },
          back: { size: 1, color: 'rgba(0,0,0,0.04)' },
          side: { size: 1, color: 'rgba(0,0,0,0.06)' }
        }
      }
    },
    credits: {
      enabled: false
    },
    title: {
      text: null,
      floating: true
    },
    plotOptions: {
      scatter: {
        width: 10,
        height: 10,
        depth: 10
      }
    },
    yAxis: {
      min: miny,
      max: maxy,
      title: null,
    },
    xAxis: {
      min: minx,
      max: maxx,
      gridLineWidth: 1
    },
    zAxis: {
      min: minz,
      max: maxz
    },
    tooltip: {
      enabled: true,
    },
    series: ({
      name: s.name,
      animation: false,
      color: "black",
      turboThreshold: 0
      data: ({
        x: p.coords[valx],
        y: p.coords[valy],
        z: p.coords[valz],
        color: p.color,
        name: p.name,
        dataLabels: p.dataLabels,
        marker: p.marker
      } for p in s.data),
    } for s in current_dataset_series)
  })


  # Add mouse events for rotation
  $(chart.container).bind('mousedown.hc touchstart.hc', (e) ->
    e = chart.pointer.normalize(e);

    posX = e.pageX
    posY = e.pageY
    alpha = chart.options.chart.options3d.alpha
    beta = chart.options.chart.options3d.beta
    sensitivity = 5 # lower is more sensitive

    $(document).bind({
      'mousemove.hc touchdrag.hc': (e) ->
        # Run beta
        newBeta = beta + (posX - e.pageX) / sensitivity
        newBeta = Math.min(100, Math.max(-100, newBeta))
        chart.options.chart.options3d.beta = newBeta

        # Run alpha
        newAlpha = alpha + (e.pageY - posY) / sensitivity
        newAlpha = Math.min(100, Math.max(-100, newAlpha))
        chart.options.chart.options3d.alpha = newAlpha

        chart.redraw(false)
      ,
      'mouseup touchend': () ->
        $(document).unbind('.hc')
    })
  )

clear_preview_cache = () ->
  current_dataset_pk = null
  current_dataset_data = null
  current_dataset_series_data = null
  current_dataset_series = null
  current_dataset_labels = null
  preprocessing_dimensions = null

###
  Job Handling
###

update_jobdata_storage_views = (jobdata, update_views=true) ->
  if update_views
    if last_job_id == jobdata.id
      update_job_views(jobdata.id)
    else
      update_job_views()


update_jobdata_storage = (jobdata, update_views=true) ->
  set_job(jobdata, () ->
    update_jobdata_storage_views(jobdata, update_views)
  )
  r = jobdata.round
  if jobdata.user == username or ADMIN
    if job_history[r] == undefined
      job_history[r] = [jobdata.id]
    else if jobdata.id not in job_history[r]
      job_history[r].push(jobdata.id)
      job_history[r] = job_history[r].sort((a,b) ->
        a-b
      )



update_job_views = (job_id = null) ->
  if job_id != null
    update_job_details(job_id)
  update_job_history_view()
  update_job_list_view()

collect_job_params = (parameters, object, prefixid="#createJobParam-", prefixname="") ->
  console.log("collect", parameters, object, prefixid, prefixname)
  for paramid, param of parameters
    if param.type == "bool"
      val = $(prefixid+paramid).prop("checked")
    else if param.type == "list" and param.multi == true
      val = (v for v in param.values when $(prefixid+paramid+"_"+v).prop("checked"))
    else if param.type == "dict" and param.multi == true
      val = (v for v,_ of param.values when $(prefixid+paramid+"_"+v).prop("checked"))
    else
      val = $(prefixid+paramid).val()
    object[prefixname+paramid] = val
    if param.type=="dict"
      if param.multi != true
        if param.values != undefined and param.values[val] != undefined
          if param.values[val].parameters != undefined
            object = collect_job_params(param.values[val].parameters, object, prefixid+paramid+"_"+val+"-", prefixname+paramid+"_")
      else
        console.log(val, param)
        for v in val
          if param.values != undefined and param.values[v] != undefined
            if param.values[v].parameters != undefined
              object = collect_job_params(param.values[v].parameters, object, prefixid+paramid+"_"+v+"-", prefixname+paramid+"_")

  return object

create_job = () ->
  $("#createJobRun").prop("disabled","disabled")
  jobdata = Object()
  jobdata.round = last_round
  jobdata.dataset = parseInt($("#createJobDataset").val())
  jobdata.algorithm = $("#createJobAlgorithm").val()
  jobdata.params = Object()
  jobdata.params = collect_job_params(algorithms[jobdata.algorithm].parameters, jobdata.params)
  #jobdata.params.preprocessing = preprocessing_config
  jobdata.params = JSON.stringify(jobdata.params)
  jobdata.preprocessing_id = preprocessing_id
  console.log("send_params", jobdata.params)
  current_dataset_pk = null
  current_dataset_data = null
  current_dataset_series_data = null
  current_dataset_series = null
  current_dataset_labels = null
  preprocessing_dimensions = null

  preprocessing_config = []
  preprocessing_dimensions = 1
  preprocessing_id = null

  $.post("/actions/create_job/", jobdata, (result) ->
    if result.status == "error"
      $("#createJobRun").prop("disabled",false)
      $("#createJobErrors").append("<div class='alert alert-danger alert-dismissible fade in submit-error' role='alert'><button type='button' class='close' data-dismiss='alert'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>"+result.msg+"</div>")
    else
      $(".submit-error").alert("close")
      jobdata.pk = result.jobid
      jobdata.id = result.jobid
      jobdata.user = username
      update_jobdata_storage(jobdata)
      update_job_limits(result.free_jobs)
      goto("/"+last_group+"/"+jobdata.round+"/job/details/"+result.jobid+"/")
  , "json"
  )

get_job_label_list = (job) ->
  if job == undefined
    return []
  liste = []
  for type in ["labels_training", "labels_test", "labels_validation"]
    l = job[type]
    if l != undefined and l != null
      liste.push(JSON.parse(l))
    else
      liste.push([])
  return liste

update_job_details_async = (job) ->
  job_id = if job != null and job != undefined then job.pk else null
  last_job_id = job_id
  page = $("#page-round-job-detail")

  if job == null
    page.html("<h3>Job Details #"+job_id)
    fetch_job_details(job_id)
    page.append('<div class="alert alert-warning" role="alert"><strong>Loading...</strong> Please wait!</div>')
    return

  page.html("")

  buttons = $("<div class='btn-group'></div>").appendTo(page)
  btn = $("<button type='button' class='btn btn-default'><span class='glyphicon glyphicon-plus'></span> Create new Job</button>")
  btn.click(() ->
    goto("/"+last_group+"/"+last_round+"/job/"+job.dataset+"/")
  )
  buttons.append(btn)
  btn = $("<button type='button' class='btn btn-default'><span class='glyphicon glyphicon-share'></span> Clone Job</button>")
  btn.click(() ->
    clone_job(job_id)
  )
  buttons.append(btn)
  if job.finished and job.success and not job.is_selected
    btn = $("<button type='button' class='btn btn-success round-status-hider-"+job.round+"'><span class='glyphicon glyphicon-ok'></span> Submit Job</button>")
    btn.click(() ->
      submit_job(job_id)
    )
    buttons.append(btn)

    btn = $("<button type='button' class='btn btn-info round-status-hider-"+job.round+"'><span class='glyphicon glyphicon-thumbs-down'></span> Strange Results</button>")
    btn.click(() ->
      rerun_job(job_id)
    )
    buttons.append(btn)

  if not job.finished
    btn = $("<button type='button' class='btn btn-danger round-status-hider-"+job.round+"'><span class='glyphicon glyphicon-remove'></span> Cancel Job</button>")
    btn.click(() ->
      cancel_job(job_id)
    )
    buttons.append(btn)



  page.append("<h3>Job Details #"+job_id)

  row = $("<div class='row'></div>").appendTo(page)
  panel=$("<div class='panel panel-default'></div>").appendTo($("<div class='col-sm-6'></div>").appendTo(row))
  panel.append("<div class='panel-heading'><h4 class='panel-title'>Setup</h4></div>")
  panel_body = $("<div class='panel-body'></div>").appendTo(panel)
  list = $('<dl class="dl-horizontal"></dl>').appendTo(panel_body)
  list.append("<dt>Dataset</td>")
  list.append("<dd>"+datasets[job.dataset].name+"</dd>")
  list.append("<dt>Algorithm</dt>")
  list.append("<dd>"+job.algorithm+"</dd>")

  if job.params
    panel=$("<div class='panel panel-default'></div>").appendTo($("<div class='col-sm-6'></div>").appendTo(row))
    panel.append("<div class='panel-heading'><h4 class='panel-title'>Algorithm Parameters</h4></div>")
    panel_body = $("<div class='panel-body'></div>").appendTo(panel)
    params = JSON.parse(job.params)
    list = $('<dl class="dl-horizontal"></dl>').appendTo(panel_body)
    for param, paramval of params
      list.append("<dt>"+param+"</dt>")
      list.append("<dd>"+JSON.stringify(paramval, null, " ")+"</dd>")

  if job.finished
    if job.images
      pics = $("<div class='row'></div>").appendTo(page)
      for pic in job.images
        pic = $('<img class="clickable" src="'+pic+'" width="100%"/>')
        pic.click(() ->
          $("#chartDetailsModal").modal("show")
          field = $("#chartDetailsModalDiv").html("")
          field.append($('<a href="'+$(this).attr("src")+'" target="_blank"><img src="'+$(this).attr("src")+'" width="100%"/></a>'))
        )
        pic.appendTo($('<div class="col-sm-4" style="margin:5px;"></div>').appendTo(pics))
    row = $('<div class="row"></div>').appendTo(page)
    page.append("<div class='row' ><div class='col-sm-12' id='job_details_plot'></div></div>")
    labels = get_job_label_list(job)
    load_dataset_default(job.dataset, '#job_details_plot', labels)

    if job.success
      col = $("<div class='col-sm-6'></div>").appendTo(row)
      panel=$("<div class='panel panel-default'></div>").appendTo(col)
      panel.append("<div class='panel-heading'><h4 class='panel-title'>Test Set</h4></div>")
      table = $("<div class='panel-body'></div>").appendTo(panel)

      dl = $("<dl class='dl-horizontal'></dl>").appendTo(table)
      if job.extra_scores_test
        for key, val of JSON.parse(job.extra_scores_test)
          dl.append("<dt>"+key+"</dt>")
          dl.append("<dd>"+val+"</dd>")
      dl.append("<dt class='lead'>Score</dt>")
      dl.append("<dd class='lead'>"+job.score_test+"</dd>")

      if job.preprocessing_data
        panel=$("<div class='panel panel-default'></div>").appendTo($("<div class='col-sm-6'></div>").appendTo(row))
        panel.append("<div class='panel-heading'><h4 class='panel-title'>Preprocessing</h4></div>")
        panel_body = $("<div class='list-group'></div>").appendTo(panel)
        for params in job.preprocessing_data
          item = $('<div class="list-group-item"></div>').appendTo(panel_body)
          list = $('<dl class="dl-horizontal"></dl>').appendTo(item)
          for param, paramval of params
            list.append("<dt>"+param+"</dt>")
            list.append("<dd>"+JSON.stringify(paramval, null, " ")+"</dd>")

      panel=$("<div class='panel panel-default'></div>").appendTo(col)
      panel.append("<div class='panel-heading'><h4 class='panel-title'>Validation Set</h4></div>")
      table = $("<div class='panel-body'></div>").appendTo(panel)
      if job.is_selected
        dl = $("<dl class='dl-horizontal'></dl>").appendTo(table)
        if job.extra_scores_validation
          for key, val of JSON.parse(job.extra_scores_validation)
            dl.append("<dt>"+key+"</dt>")
            dl.append("<dd>"+val+"</dd>")
        dl.append("<dt class='lead'>Score</dt>")
        dl.append("<dd class='lead'>"+job.score_validation+"</dd>")
      else
        table.append('<div class="alert alert-warning" role="alert">Validation results will be available after submitting this job.</div>')
        btn = $("<button type='button' class='btn btn-success round-status-hider-"+job.round+"'><span class='glyphicon glyphicon-ok'></span> Submit Job</button>")
        btn.click(() ->
          submit_job(job_id)
        )
        table.append(btn)

    else
      page.append('<div class="alert alert-danger" role="alert"><strong>Job failed.</strong></div>')
  else
    page.append('<div class="alert alert-warning" role="alert"><strong>Working...</strong> Job not finished yet. Please wait!</div>')
  if job.message
    page.append('<pre>'+job.message+"</pre>")

  buttons = $("<div class='btn-group'></div>").appendTo(page)
  btn = $("<button type='button' class='btn btn-default'><span class='glyphicon glyphicon-plus'></span> Create new Job</button>")
  btn.click(() ->
    goto("/"+last_group+"/"+last_round+"/job/"+job.dataset+"/")
  )
  buttons.append(btn)
  btn = $("<button type='button' class='btn btn-default'><span class='glyphicon glyphicon-share'></span> Clone Job</button>")
  btn.click(() ->
    clone_job(job_id)
  )
  buttons.append(btn)
  if job.finished and job.success and not job.is_selected
    btn = $("<button type='button' class='btn btn-success round-status-hider-"+job.round+"'><span class='glyphicon glyphicon-ok'></span> Submit Job</button>")
    btn.click(() ->
      submit_job(job_id)
    )
    buttons.append(btn)

    btn = $("<button type='button' class='btn btn-info round-status-hider-"+job.round+"'><span class='glyphicon glyphicon-thumbs-down'></span> Strange Results</button>")
    btn.click(() ->
      rerun_job(job_id)
    )
    buttons.append(btn)

  if not job.finished
    btn = $("<button type='button' class='btn btn-danger round-status-hider-"+job.round+"'><span class='glyphicon glyphicon-remove'></span> Cancel Job</button>")
    btn.click(() ->
      cancel_job(job_id)
    )
    buttons.append(btn)


update_job_details = (job_id) ->
  get_job_with_labels(job_id, update_job_details_async, () -> update_job_details_async(null))


update_job_history_view = () ->
  if job_history[last_round] != undefined
    console.log("start history fetch")

    get_jobs(job_history[last_round], (job_list) ->
      job_cache = Object()
      for job in job_list
        job_cache[job.pk] = job
      if last_round == null
        return false
      panel = $("#jobHistoryPanel")
      panel.html("")
      panel.append("<div class='panel-heading'><h4 class='panel-title'>Last Jobs</h4></div>")
      filter_panel = $("<div class='panel-body'></div>").appendTo(panel)
      filter_panel.append("Filter: ")
      if HISTORY_SHOW_ALL
        filter_panel.append(" <strong>last</strong> | ")
        link = $("<a href='#'>current dataset</a>").appendTo(filter_panel)
        link.click((e) ->
          HISTORY_SHOW_ALL = false
          update_job_history_view()
          return false
        )
      else
        link = $("<a href='#'>last</a>").appendTo(filter_panel)
        link.click((e) ->
          HISTORY_SHOW_ALL = true
          update_job_history_view()
          return false
        )
        filter_panel.append(" | <strong>current dataset</strong>")

      list = $("<div class='list-group'></div>").appendTo(panel)

      console.log("process history update")

      myjobsids = (job.pk \
                   for job in job_list \
                   when job != undefined and job.user == username and (job.dataset == last_create_job_dataset or HISTORY_SHOW_ALL))
  #    console.log(jobs)
      if HISTORY_SHOW_ALL
        jobids = myjobsids[-10..]
      else
        jobids = myjobsids
      jobids = jobids.sort((a,b) ->
        b-a
      )
      for jobid in jobids
        job = job_cache[jobid]
        block = $("<a class='list-group-item' href='/"+last_group+"/"+last_round+"/job/details/"+job.pk+"'></a>").appendTo(list)
        if job.is_selected == true
          block.addClass("list-group-item-info")
        else if job.success == true
          block.addClass("list-group-item-success")
        else if job.success == false
          block.addClass("list-group-item-danger")
        block.click(update_location)
        block.append("<small>#"+job.pk+"</small>&nbsp;")
        block.append("<strong>"+datasets[job.dataset].name+"&nbsp;-&nbsp;"+job.algorithm+"</strong>")
        btns = $("<div class='btn-group btn-group-xs pull-right'></div>").appendTo(block)
        if job.finished and job.success and not job.is_selected
          btn = $("<button type='button' class='btn btn-success round-status-hider-"+last_round+"' jobid='"+jobid+"'><span class='glyphicon glyphicon-ok'></span></button>")
          btn.click(() ->
            submit_job(parseInt($(this).attr("jobid")))
            return false
          )
          btns.append(btn)
        if not job.finished
          btn = $("<button type='button' class='btn btn-danger round-status-hider-"+last_round+"' jobid='"+jobid+"'><span class='glyphicon glyphicon-remove'></span></button>")
          btn.click(() ->
            cancel_job(parseInt($(this).attr("jobid")))
            return false
          )
          btns.append(btn)

        btn = $("<button type='button' class='btn btn-default' jobid='"+jobid+"'><span class='glyphicon glyphicon-share'></span></button>")
        btn.click(() ->
          clone_job(parseInt($(this).attr("jobid")))
          return false
        )
        btns.append(btn)
        block.append("<br />")
        sm = $("<small></small>").appendTo(block)
        for param, paramval of JSON.parse(job.params)
          sm.append(param+":&nbsp;<strong>"+JSON.stringify(paramval, null, " ")+"</strong><br />")
        if job.finished
          block.append("Test-Score:&nbsp;<strong>"+job.score_test+"</strong><br />")
          if job.is_selected
            block.append("Valid.-Score:&nbsp;<strong>"+job.score_validation+"</strong><br />")
      console.log("done")
    )

update_job_list_view = () ->
  if last_round == null
    return false
  job_list_view_counter = job_list_view_counter + 1
  current_job_list_counter = job_list_view_counter
  if job_history[last_round] != undefined
    get_jobs(job_history[last_round], (jobs) ->
      if current_job_list_counter != job_list_view_counter
        return
      table = $("#jobListTable")
      table.html("")

      for job in jobs
        if job == undefined or job == null
          continue
        jobid = job.pk

        row = $("<tr jobid='"+jobid+"'></tr>").appendTo(table)
        row.click((e) ->
          goto("/"+last_group+"/"+last_round+"/job/details/"+$(this).attr("jobid")+"/")
        )
        if job.is_selected == true
          row.addClass("info")
        else if job.success == false
          row.addClass("danger")

        row.append("<td>"+job.id+"</td>")
        if ADMIN
          row.append("<td>"+job.user+"</td>")
        row.append("<td>"+datasets[job.dataset].name+"</td>")
        row.append("<td>"+job.algorithm+"</td>")

        sm = $("<small></small>").appendTo($("<td></td>").appendTo(row))
        for param, paramval of JSON.parse(job.params)
          sm.append(param+":&nbsp;<strong>"+JSON.stringify(paramval, null, " ")+"</strong> ")
        if job.finished
          row.append("<td>"+job.score_test+"</td>>")
          if job.is_selected
            row.append("<td>"+job.score_validation+"</td>>")
          else
            row.append("<td>-</td>")
        else
          row.append("<td>-</td>")
          row.append("<td>-</td>")
        actions = $("<td></td>").appendTo(row)
        btn = $('<a href="/admin/webapp/job/'+jobid+'/" target="job_admin" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span> edit</a> ')
        actions.append(btn)
        btn.click((e) ->
          e.stopPropagation()
        )
        btn = $("<button type='button' class='btn btn-default btn-xs' jobid='"+jobid+"'><span class='glyphicon glyphicon-share'></span> Clone Job</button>")
        btn.click(() ->
          clone_job(parseInt($(this).attr("jobid")))
          return false
        )
        actions.append(btn)
        if job.finished and job.success and not job.is_selected
          btn = $("<button type='button' class='btn btn-success btn-xs round-status-hider-"+last_round+"' jobid='"+jobid+"'><span class='glyphicon glyphicon-ok'></span> Submit Job</button>")
          btn.click(() ->
            submit_job(parseInt($(this).attr("jobid")))
            return false
          )
          actions.append("&nbsp;")
          actions.append(btn)
        if not job.finished
          btn = $("<button type='button' class='btn btn-danger btn-xs round-status-hider-"+last_round+"' jobid='"+jobid+"'><span class='glyphicon glyphicon-remove'></span> Cancel Job</button>")
          btn.click(() ->
            cancel_job(parseInt($(this).attr("jobid")))
            return false
          )
          actions.append("&nbsp;")
          actions.append(btn)
      $("#round_job_list_table").trigger("update")
    )



fetch_job_details = (job_id) ->
  $.getJSON("/actions/get_job_details/"+job_id+".json/", null, (result) ->
    update_jobdata_storage(result)
  )

clone_job_async = (job) ->
  set_job_params = job.params

  goto("/"+last_group+"/"+job.round+"/job/"+job.dataset+"/"+job.algorithm+"/")

  if job.preprocessing != undefined and job.preprocessing_data.length > 0
    preprocessing_config = job.preprocessing_data
    console.log(preprocessing_config)
    awaiting_preprocessing = job.preprocessing
    preprocessing_id = job.preprocessing
    req = {
      dataset: job.dataset,
      params: JSON.stringify(preprocessing_config)
    }
    $.post("/actions/submit_preprocessing/", req, (data) ->
      $("#createJobDatasetDetailsPreview").html('<div class="alert alert-warning" role="alert">Waiting for data.</div>')
      $("#createJobDatasetDetailsPreviewLine").html('<div class="alert alert-warning" role="alert">Waiting for data.</div>')
      preprocessing_id = data.id
      if data.url != undefined
        load_preprocessing(data.url)
        awaiting_preprocessing = null
      else
        awaiting_preprocessing = data.id
    , "json").fail((data) ->
      console.log("submit_pp_fail", data)
    )
  else
    preprocessing_config = []
    awaiting_preprocessing = null
    preprocessing_id = null

  update_preprocessing()

clone_job = (job_id) ->
  get_job(job_id, clone_job_async)

submit_job = (job_id) ->
  $.getJSON("/actions/submit_job/"+job_id+".json/", null, (data) ->
    update_jobdata_storage(data.job)
    update_job_limits(data.free_jobs)
  )

rerun_job = (job_id) ->
  $.getJSON("/actions/rerun_job/"+job_id+".json/", null, (data) ->
    update_jobdata_storage(data.job)
    update_jobdata_storage(data.new_job)
    update_job_limits(data.free_jobs)
    goto("/"+last_group+"/"+last_round+"/job/details/"+data.new_jobid+"/")
  )

cancel_job = (job_id) ->
  $.getJSON("/actions/cancel_job/"+job_id+".json/", null, (data) ->
    update_jobdata_storage(data.job)
    update_job_limits(data.free_jobs)
  )

update_job_limits = (limits) ->
  if job_limits[limits.round_id] == undefined
    job_limits[limits.round_id] = Object()
  job_limits[limits.round_id][limits.dataset_id] = limits
  update_job_limits_view(limits.round_id, limits.dataset_id)

update_job_limits_view = (round_id, dataset_id) ->
  if job_limits[round_id] == undefined or job_limits[round_id][dataset_id] == undefined
    return
  l = job_limits[round_id][dataset_id]
  field = $(".limits-view-"+round_id+"-"+dataset_id)
  field.html(l.free_jobs+"/"+l.total_jobs+" jobs open <br /> "+l.free_submits+"/"+l.total_submits+" submits open")
  field.removeClass().addClass("limits-view-"+round_id+"-"+dataset_id)
  if l.free_submits > 0 and l.free_jobs > 0
    field.addClass("text-success")
    $(".limits-hide-"+round_id+"-"+dataset_id).show()
  else
    field.addClass("text-danger")
    $(".limits-hide-"+round_id+"-"+dataset_id).hide()

check_job_limits = (round_id, dataset_id) ->
  if job_limits[round_id] == undefined or job_limits[round_id][dataset_id] == undefined
    $.getJSON("/actions/get_free_jobs/"+round_id+"/"+dataset_id+".json/", null, update_job_limits)
  else
    update_job_limits_view(round_id, dataset_id)

###
  Handling Scores
###
store_score = (score) ->
  if scores[score.roundid] != undefined
    if scores[score.roundid][score.user] == undefined
      scores[score.roundid][score.user] = Object()
    scores[score.roundid][score.user][score.datasetid] = score

store_score_list = (list) ->
  store_score(score) for score in list
  update_result_list(last_round)
  update_hints(last_round)

fetch_score_list = (round_id) ->
  scores[round_id] = Object()
  $.getJSON("/actions/score_list/"+round_id+".json/", null, (result) ->
    store_score_list(result)
  )

store_score_total = (score) ->
  if scores_total[score.roundid] == undefined
    scores_total[score.roundid] = Object()
  scores_total[score.roundid][score.user] = score

store_score_list_total = (list) ->
  store_score_total(score) for score in list
  update_result_list(last_round)
  update_group_point_list(last_group)

fetch_score_list_total = () ->
  $.getJSON("/actions/score_list_total.json/", null, (result) ->
    store_score_list_total(result)
  )

update_result_list = (round_id) ->
  head = $("#scores_head").html("")
  head.append("<th>User</th>")
  if rounds[round_id] == undefined
    return
  for dsid in rounds[round_id].datasets
    head.append("<th class='text-center'>"+datasets[dsid].name+"</th>")
  head.append("<th class='text-center'>Sum</th>")
  head.append("<th class='text-center'>Stars</th>")
  body = $("#scores_body").html("")
  userlist = (data for _, data of scores_total[round_id])
  userlist.sort((a,b) ->
    return b.points - a.points
  )
  for usertotal in userlist
    user = usertotal.user
    realname = usertotal.realname
    badges = usertotal.badges
    if scores[round_id][user] != undefined
      userdata = scores[round_id][user]
    else
      userdata = Object()
    row = $("<tr></tr>").appendTo(body)
    row.append("<td><strong>"+user+"</strong> "+badges+"<br />"+realname+"</td>")
    for dsid in rounds[round_id].datasets
      score = userdata[dsid]
      if score != undefined
        field = $("<td class='text-center clickable' jobid='"+score.jobid+"'><small>"+score.jobid+"</small>&nbsp;-&nbsp;"+score.points+"&nbsp;Pt.<br /><strong>"+score.score+"</strong></td>").appendTo(row)
        field.click((e) ->
          goto("/"+last_group+"/"+last_round+"/job/details/"+$(this).attr("jobid")+"/")
        )
      else
        row.append("<td class='text-center'>&nbsp;-&nbsp;</td>")

    if scores_total[round_id] != undefined and scores_total[round_id][user] != undefined
      row.append("<td class='text-center'><strong>"+scores_total[round_id][user].points+"</strong></td>")
      row.append("<td class='text-center'><strong>"+scores_total[round_id][user].stars+"</strong></td>")
    else
      row.append("<td class='text-center'><strong> - </strong></td>")
      row.append("<td class='text-center'><strong> - </strong></td>")
  $("#scores_table").tablesorter()


update_group_point_list = (group) ->
  if group == null
    return
  head = $("#group_overview_table_head").html("")
  body = $("#group_overview_table_body").html("")

  userdict = Object()

  head.append("<th>User</th>")
  for roundid in groups[group].rounds
    item = $("<th class='text-center'></th>")
    anchor = $("<a href='"+rounds[roundid].url+"' class='ajax'>"+rounds[roundid].name+"</a>")
    anchor.click(update_location)
    item.append(anchor)

    head.append(item)

    if scores_total[roundid] != undefined
      for user, userdata of scores_total[roundid]
        if userdict[user] == undefined
          userdict[user] = {total: 0, user: user}
        userdict[user].total += userdata.stars
        userdict[user][roundid] = userdata
        userdict[user].realname = userdata.realname
        userdict[user].badges = userdata.badges

  userlist = (value for _, value of userdict)
  userlist.sort((a,b) ->
    return b.total - a.total
  )
  head.append("<th class='text-center'>Stars</th>")

  for userdata in userlist
    row = $("<tr></tr>").appendTo(body)
    row.append("<td><strong>"+userdata.user+"</strong> "+userdata.badges+"<br />"+userdata.realname+"</td>")
    for roundid in groups[group].rounds
      if userdata[roundid] != undefined
        row.append("<td class='text-center'>"+userdata[roundid].points+"Pt.<br /><strong>"+userdata[roundid].stars+"</strong> <span class='glyphicon glyphicon-star'></span></td>")
      else
        row.append("<td class='text-center'> - </td>")
    row.append("<td class='text-center'><strong>"+userdata.total+"</strong></td>")

update_hints = (round_id) ->

  if ADMIN
    return
  hintfield = $("#hint-field").html("")
  max_stars = 0
  if scores_total[round_id] != undefined
    for u, _ of scores_total[round_id]
      data = scores_total[round_id][u]
      max_stars = Math.max(max_stars, data.stars)
    if scores_total[round_id][username] != undefined
      field = $("<div class='text-center' style='margin:10px;'></div>").appendTo(hintfield)
      field.append("<span class='h1'>"+scores_total[round_id][username].stars+"<span class='glyphicon glyphicon-star'></span></span>")
      field.append("<span class='h4'>/ "+max_stars+"<span class='glyphicon glyphicon-star'></span></span>")

  dslist = []
  for dsid in rounds[round_id].datasets
    if scores[round_id] != undefined and scores[round_id][username] != undefined and scores[round_id][username][dsid] != undefined
      ds = {
        datasetid: dsid,
        score: scores[round_id][username][dsid].score,
        score_max: scores[round_id][username][dsid].score,
        points: scores[round_id][username][dsid].points,
        points_max: scores[round_id][username][dsid].points,
      }
    else
      ds = {
        datasetid: dsid,
        score: 0,
        score_max: 0,
        points: 0,
        points_max: 0,
      }
    if scores[round_id] != undefined
      for _, userdata of scores[round_id]
        if userdata[dsid] != undefined
          ds.score_max = Math.max(ds.score_max, userdata[dsid].score)
          ds.points_max = Math.max(ds.points_max, userdata[dsid].points)

    if ds.score_max>0
      ds.score_rel = ds.score / ds.score_max
    else
      ds.score_rel = 0

    if ds.points_max>0
      ds.points_rel = ds.points / ds.points_max
      ds.points_div = ds.points_max - ds.points
    else
      ds.points_rel = 0
      ds.points_div = 0

    dslist.push(ds)
  dslist.sort((a, b) ->
    if a.points_div != b.points_div
      return b.points_div - a.points_div
    if a.score_rel != b.score_rel
      return a.score_rel - b.score_rel
    return a.score - b.score
  )
  panel = $("<div class='panel panel-default'></div>").appendTo(hintfield)
  panel.append("<div class='panel-heading'><h4 class='panel-title'>Hints</h4></div>")
  list = $("<div class='list-group'></div>").appendTo(panel)
  for hint in dslist
    anchor = $("<a class='limits-hide-"+last_round+"-"+hint.datasetid+" list-group-item' href='/"+last_group+"/"+last_round+"/job/"+hint.datasetid+"/'></a>").appendTo(list)
    anchor.click(update_location)
    anchor.append("<div><strong>"+datasets[hint.datasetid].name+"</strong><span class='pull-right'>"+hint.score+" ("+hint.points+")</span></div>")

    anchor.append("<div class='progress' style='margin:0px;'><div class='progress-bar' role='progress' style='width: "+(hint.points_rel * 100)+"%;'><small>"+hint.points+" / "+hint.points_max+" Points</small></div></div>")
    anchor.append("<div class='progress' style='margin:0px;'><div class='progress-bar progress-bar-success' role='progress' style='width: "+(hint.score_rel * 100)+"%;'><small>"+hint.score+" / "+hint.score_max+" Precision</small></div></div>")
    anchor.append("<div class='progress' style='margin:0px;'><div class='progress-bar progress-bar-warning' role='progress' style='width: "+(hint.score * 100)+"%;'><small>"+hint.score+" Precision</small></div></div>")
    update_job_limits_view(last_round, hint.datasetid)

###
  Countdown Handling
###

seconds_to_string = (time) ->
  result = ""

  secs = time % 60
  time -= secs

  result = result+secs+"s"

  if time > 0
    time = time/60
    minutes = time % 60
    time -= minutes
    result = minutes+"min "+result

    if time > 0
      time = time/60
      hours = time %24
      time -= hours
      result = hours+"h "+result

      if time > 0
        time = time/24
        result = time+" days "+result

  return result



get_round_status = (round_id) ->
  round = rounds[round_id]

  time = Math.round((Date.now() + TIME_DELTA) / 1000)

  if round.start_time == null
    return {
      text: "disabled",
      active: false,
      class: "danger"
    }

  if time < round.start_time
    return {
      text: "start in: " + seconds_to_string(round.start_time - time),
      active: false,
      class: "info"
    }

  if round.end_time == null
    return {
      text: "open",
      active: true,
      class: "success"
    }

  if round.end_time > time
    return {
      text: "open for: " + seconds_to_string(round.end_time - time),
      active: true,
      class: "success"
    }

  return {
    text: "finished",
    active: false,
    class: "danger"
  }

update_round_status = () ->
  if last_group == null
    return

  for round_id in groups[last_group].rounds
    status = get_round_status(round_id)
    $(".round-status-text-"+round_id).removeClass("text-danger text-info text-text-success").addClass("text-"+status.class)
    $(".round-status-label-"+round_id).text(status.text)
    if status.active
      $(".round-status-hider-"+round_id).show()
    else
      $(".round-status-hider-"+round_id).hide()


###
  Initiallize site
###

open_dataset_preview = (e) ->
  ds_pk = $(this).attr("dataset-pk")
  $("#datasetPreviewModal").modal("show")
  load_dataset_default(ds_pk, "#datasetPreviewModalDiv")

open_dataset_cloner = (e) ->
  ds_pk = $(this).attr("dataset-pk")
  $("#cloneDatasetModal").modal("show")
  $("#cloneDatasetSourceName").text(datasets[ds_pk].name)
  $("#cloneDatasetSourceName").text(datasets[ds_pk].name)
  $("#cloneDatasetSource").val(ds_pk)

$(document).ready( () ->
  open_database( () ->
    init_job_history()

    $(".ajax").on("click", update_location)
    $(".er-ds-toggle").on("click", er_ds_toggle)
    $(".er-alg-toggle").on("click", er_alg_toggle)
    $(".er-pp-toggle").on("click", er_pp_toggle)
    $(window).bind("popstate", update_content)

    $("#groupTabOverviewLink").click((e) ->
      e.preventDefault()
      goto("/"+last_group+"/")
    )

    $("#groupTabSettingsLink").click((e) ->
      e.preventDefault()
      goto("/"+last_group+"/settings/")
    )

    $("#roundTabJobsLink").click((e) ->
      e.preventDefault()
      goto("/"+last_group+"/"+last_round+"/job/")
    )

    $("#roundTabJobsListLink").click((e) ->
      e.preventDefault()
      goto("/"+last_group+"/"+last_round+"/joblist/")
    )

    $("#roundTabOverviewLink").click((e) ->
      e.preventDefault()
      goto("/"+last_group+"/"+last_round+"/")
    )

    $("#createJobDataset").change((e) ->
      if this.value
        if last_create_job_algorithm
          goto("/"+last_group+"/"+last_round+"/job/"+this.value+"/"+last_create_job_algorithm+"/")
        else
          goto("/"+last_group+"/"+last_round+"/job/"+this.value+"/")
      else
        goto("/"+last_group+"/"+last_round+"/job/")
    )

    $("#createJobAlgorithm").change((e) ->
      if this.value
        goto("/"+last_group+"/"+last_round+"/job/"+last_create_job_dataset+"/"+this.value+"/")
      else
        goto("/"+last_group+"/"+last_round+"/job/"+last_create_job_dataset+"/")
    )

    $("#createJobRun").click(create_job)

    $("#create_job_form").submit((e) ->
      e.preventDefault()
      create_job()
    )

    $(".dataset-preview-opener").click(open_dataset_preview)

    $(".dataset-cloner-opener").click(open_dataset_cloner)

    $("#editRoundTimeOffset").val(new Date().getTimezoneOffset())

    $(".3dplotcontol").change(update3Dplot)

    $("#round_job_list_table").tablesorter()

    $('#addPreprocessing').click(open_add_preprocessing)
    $('#submitPreprocessingBtn').click(add_preprocessing)
    $('#clearPreprocessing').click(() ->
      preprocessing_config = []
      preprocessing_id = null
      update_preprocessing()
    )
    $('#preprocessingAlgorithm').change(update_preprocessing_algorithm)

    $('.page').hide()
    update_content()
    fetch_score_list_total()

    $("#scores_table").tablesorter()
    $('[data-toggle="tooltip"]').tooltip()
  )
)

getCookie = (name) ->
    cookieValue = null;
    if (document.cookie && document.cookie != '')
        cookies = document.cookie.split(';')
        for cookie in cookies
            cookie = jQuery.trim(cookie)
            if (cookie.substring(0, name.length + 1) == (name + '='))
                return decodeURIComponent(cookie.substring(name.length + 1))

csrftoken = getCookie('csrftoken')

csrfSafeMethod = (method) ->
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method))

$.ajaxSetup({
  beforeSend: (xhr, settings) ->
    if (!csrfSafeMethod(settings.type) && !this.crossDomain)
      xhr.setRequestHeader("X-CSRFToken", csrftoken)
})

check_round_jobs = (round_id) ->
  if round_jobs[round_id] != true
    $.getJSON("/actions/load_job_list-"+round_id+".json/", null, (result) ->
      for job in result
        update_jobdata_storage(job, false)
      update_job_views(last_job_id)
    )
    round_jobs[round_id] = true

ws = new WebSocket("ws://"+document.location.host+"/websocket/")
ws.onmessage = (evt) ->
  data = JSON.parse(evt.data)
  if data.msg == "update_job"
    if data.user == username or ADMIN
      fetch_job_details(data.jobid)
  if data.msg == "update_dataset_points_list"
    store_score_list(data.list)
  if data.msg == "update_stars"
    store_score_list_total(data.list)
  if data.msg == "update_round"
    rounds[data.roundid] = data.data
    update_group(last_group)
    update_round(last_round)
  if data.msg == "preprocessing_done"
    if data.preprocessing_id == awaiting_preprocessing
      load_preprocessing(data.url)

ws.onclose = () ->
  $("#alerts").append('<div class="alert alert-danger" role="alert"><strong>Connection loss...</strong> Try <a href="'+window.location+'">page reload</a></div>')

$.getJSON("/actions/get_time.json", null, (timestamp) ->
  TIME_DELTA = timestamp * 1000 - Date.now()
)

setInterval(update_round_status, 1000)