#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is a collection of all required views for the web frontend.
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

from datetime import datetime, timedelta
from time import timezone

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import login, authenticate
from django.http.response import HttpResponseRedirect, HttpResponseBadRequest, HttpResponseForbidden, HttpResponse
from django.middleware.csrf import get_token
from django.views.decorators.cache import cache_control
import json
import base64
import os
import os.path
import string
import logging

logger = logging.getLogger(__name__)

from six.moves.urllib.parse import quote, unquote_plus


from webapp.models import *
from webapp.processors import general_processor
from webapp.tools import *
import settings
import webapp.analyzer as analyzer
import random

# Create your views here.


JSON_MIME = "application/json"


def HttpResponseJSON(content, *args, **kwargs):
    return HttpResponse(json.dumps(content), *args, content_type=JSON_MIME, **kwargs)


def static_result(f):

    def inner(request, *args, **kwargs):

        url, path, dir = get_cached_url(request.path, True)

        if os.path.isfile(path):
            return HttpResponseRedirect(url)

        else:
            if not os.path.isdir(dir):
                os.mkdir(dir)

            result = f(request, *args, **kwargs)
            with open(path, "wb") as file:
                file.write(result.content)
            return result

    return inner

###################
#
# Helpers
#
###################

class NameMapper(object):
    id2name = None
    name2id = None

    def __init__(self):
        self.id2name = dict()
        self.name2id = dict()

    def get_id(self, name):
        if name in self.name2id:
            return self.name2id[name]

        new_id = len(self.id2name)
        self.id2name[new_id] = name
        self.name2id[name] = new_id
        return new_id

    def get_mapping(self):
        return self.id2name


###################
#
# Views
#
###################

def admin_only(user):
    """Test function to check if the user is an admin, and allowed to manage things.
    """
    if user and user.is_superuser:
        return True
    return False


def webapp(request):
    if request.user.is_authenticated():
        return render(request, "webapp.html")
    else:
        return render(request, "login.html")


@user_passes_test(admin_only)
def add_group(request):
    """Creates a new group.

    The Group parameter are read from the request.POST field.

    Parameters
    ==========
    group_name : str
        The name of the new group
    public : str
        Default is public. Private on the values  ('0', 'no', 'private', 'false')

    """
    if not "group_name" in request.POST:
        return HttpResponseBadRequest

    group_name = request.POST["group_name"]
    public = True

    try:
        if request.POST["public"] in ('0', 'no', 'private', 'false'):
            public = False
    except KeyError:
        pass

    if not Group.objects.filter(name=group_name).count():
        g=Group(name=group_name, free_to_enter=public)
        g.save()

    if "api" in request.POST:
        return HttpResponseJSON({
            "status": "ok",
            "group_id": g.pk
        })

    return HttpResponseRedirect(g.url())


@login_required()
def get_groups(request):
    context = general_processor(request)
    return HttpResponseJSON(context["groups"])


@login_required()
def get_round(request, pk):
    round = get_object_or_404(Round, pk=pk)
    return HttpResponseJSON(round.get_jsonable())


@user_passes_test(admin_only)
def add_dataset(request):
    if not "name" in request.POST or not "points" in request.POST or not request.POST["name"] or not request.POST["points"]:
        return HttpResponseBadRequest()

    points = json.loads(request.POST['points'])

    ds = Dataset(name=request.POST["name"])

    if "description" in request.POST:
        ds.description = request.POST["description"]

    if "cluster_dataset" in request.POST:
        ds.cluster_dataset = request.POST["cluster_dataset"] in ("1", 1, "true", "True", "cluster")

    ds.save()

    mapper = NameMapper()

    dimension = None
    cnt = [0, 0, 0]

    for point in points:
        p = DatasetPoint(
            dataset=ds,
            coords=json.dumps(point["coords"]),
            class_id=mapper.get_id(point["class_id"]),
            point_type=point["point_type"]
        )
        p.save()
        if dimension is None:
            dimension = len(point["coords"])
        elif dimension != len(point["coords"]):
            dimension = -1
        cnt[point["point_type"]] += 1

    ds.nr_training_points = cnt[settings.TYPE_TRAINING]
    ds.nr_test_points = cnt[settings.TYPE_TEST]
    ds.nr_validation_points = cnt[settings.TYPE_VALIDATION]

    mapping = mapper.get_mapping()
    ds.nr_classes = len(mapping)
    ds.class_labels = json.dumps(mapping)
    ds.dimensions = dimension

    ds.save()

    if "api" in request.POST:
        return HttpResponseJSON({
            "status": "ok",
            "dataset": ds.get_jsonable(request.user)
        })
    return HttpResponseRedirect("/datasets/")


@user_passes_test(admin_only)
def clone_dataset(request):
    ds_source = get_object_or_404(Dataset, pk=int(request.POST["cloneDatasetSource"]))
    min_points = int(request.POST["cloneDatasetMinTraining"])
    test_size = float(request.POST["cloneDatasetTestSize"]) / 100
    validation_size = float(request.POST["cloneDatasetValidationSize"]) / 100
    name = request.POST["name"]
    make_cluster = False
    if "cluster_dataset" in request.POST:
        print(request.POST["cluster_dataset"])
        make_cluster = request.POST["cluster_dataset"] in ("1", 1, "true", "True", "cluster")

    points_set = set()
    class_count = {}

    ds_new = Dataset(name=name)
    ds_new.save()

    if make_cluster:
        for p in ds_source.datasetpoint_set.all():
            p_new = p.get_copy()
            p_new.dataset = ds_new
            p_new.point_type = settings.TYPE_VALIDATION
            p_new.save()

            p_new = p.get_copy()
            p_new.dataset = ds_new
            p_new.point_type = settings.TYPE_TEST
            p_new.class_id = 0
            p_new.save()

        ds_new.cluster_dataset = True
    else:
        point_query = ds_source.datasetpoint_set.all()

        if ds_source.cluster_dataset:
            point_query = point_query.filter(point_type=settings.TYPE_VALIDATION)

        if min_points:
            for p in point_query:
                if not p.class_id in class_count:
                    class_count[p.class_id] = 1
                    p_new = p.get_copy()
                    p_new.dataset = ds_new
                    p_new.point_type = settings.TYPE_TRAINING
                    p_new.save()
                elif class_count[p.class_id] < min_points:
                    class_count[p.class_id] += 1
                    p_new = p.get_copy()
                    p_new.dataset = ds_new
                    p_new.point_type = settings.TYPE_TRAINING
                    p_new.save()
                else:
                    points_set.add(p)
        else:
            points_set = set(point_query)

        size = len(points_set)

        test_set = random.sample(points_set, int(test_size*size))
        for p in test_set:
            points_set.remove(p)
            p_new = p.get_copy()
            p_new.dataset = ds_new
            p_new.point_type = settings.TYPE_TEST
            p_new.save()

        validation_set = random.sample(points_set, int(validation_size*size))
        for p in validation_set:
            points_set.remove(p)
            p_new = p.get_copy()
            p_new.dataset = ds_new
            p_new.point_type = settings.TYPE_VALIDATION
            p_new.save()

        for p in points_set:
            p_new = p.get_copy()
            p_new.dataset = ds_new
            p_new.point_type = settings.TYPE_TRAINING
            p_new.save()

    ds_new.dimensions = ds_source.dimensions
    ds_new.nr_classes = ds_source.nr_classes
    ds_new.nr_test_points = ds_new.datasetpoint_set.filter(point_type=settings.TYPE_TEST).count()
    ds_new.nr_training_points = ds_new.datasetpoint_set.filter(point_type=settings.TYPE_TRAINING).count()
    ds_new.nr_validation_points = ds_new.datasetpoint_set.filter(point_type=settings.TYPE_VALIDATION).count()
    ds_new.save()

    return HttpResponseRedirect("/datasets/")


@user_passes_test(admin_only)
def edit_round(request):
    """updates the round parameters."""
    required_fields = ["name", "group", "algorithms", "datasets"]
    if not all([field in request.POST for field in required_fields]):
        return HttpResponseBadRequest()

    group = get_object_or_404(Group, name=request.POST["group"])

    if "round-pk" in request.POST and int(request.POST["round-pk"]) != -1:
        round = get_object_or_404(Round, pk=int(request.POST["round-pk"]))
    else:
        round = Round()

    round.name = request.POST["name"]
    round.group = group
    round.algorithms = request.POST["algorithms"]
    round.preprocessing = request.POST["preprocessing"]

    try:
        offset = timedelta(minutes=int(request.POST["timeoffset"]))
    except:
        offset = 0

    try:
        start_datestring = "{} {}".format(request.POST["startDate"], request.POST["startTime"])
        round.start_time = datetime.strptime(start_datestring, "%Y-%m-%d %H:%M") + offset
    except:
        round.start_time = None

    try:
        end_datestring = "{} {}".format(request.POST["endDate"], request.POST["endTime"])
        round.end_time = datetime.strptime(end_datestring, "%Y-%m-%d %H:%M") + offset
    except:
        round.end_time = None

    round.save()

    round.datasets.clear()
    for ds_pk in json.loads(request.POST["datasets"]):
        round.datasets.add(Dataset.objects.get(pk=int(ds_pk)))

    settings.msg_queue.put({
        "msg": "update_round",
        "roundid": round.pk,
        "data": round.get_jsonable()
    })

    if "api" in request.POST:
        return HttpResponseJSON({
            "status": "ok",
            "round": round.get_jsonable()
        })

    return HttpResponseRedirect(group.url()+"settings/")


@login_required()
def join_group(request, group_name):
    """Add the current user to the selected group.
    """
    group = get_object_or_404(Group, name=group_name)

    if not group.free_to_enter:
        return HttpResponseForbidden()

    group.users.add(request.user)

    if "api" in request.POST:
        return HttpResponseJSON({
            "status": "ok"
        })

    return HttpResponseRedirect(group.url())


@login_required()
def get_datasets(request):
    return HttpResponseJSON({ds.pk: ds.get_jsonable(request.user) for ds in Dataset.objects.all()})


@cache_control(max_age=86400, public=True) # cache one day
@login_required()
@static_result
def get_dataset_json(request, name="", pk=None):
    name = unquote_plus(name)
    if pk:
        dataset = get_object_or_404(Dataset, pk=pk)
    else:
        dataset = get_object_or_404(Dataset, name=name)
    points = [p.get_jsonable() for p in dataset.datasetpoint_set.exclude(point_type=settings.TYPE_VALIDATION)]
    return HttpResponseJSON(points)


@cache_control(max_age=86400, public=True) # cache one day
@login_required()
@static_result
def get_preprocessing_json(request, pk):
    pp = get_object_or_404(Preprocessing, pk=pk)
    points = json.loads(pp.points_training_and_test)
    return HttpResponseJSON(points)


@cache_control(max_age=86400, public=True) # cache one day
@user_passes_test(admin_only)
@static_result
def get_full_preprocessing_json(request, pk):
    pp = get_object_or_404(Preprocessing, pk=pk)
    points = json.loads(pp.points_training_and_test)
    points = points + json.loads(pp.points_validation)
    return HttpResponseJSON(points)


@user_passes_test(admin_only)
@static_result
def get_full_dataset_json(request, name="", pk=None):
    name = unquote_plus(name)
    if pk:
        dataset = get_object_or_404(Dataset, pk=pk)
    else:
        dataset = get_object_or_404(Dataset, name=name)
    points = [p.get_jsonable() for p in dataset.datasetpoint_set.all()]
    return HttpResponseJSON(points)


def get_env(request):
    return HttpResponseJSON({
        "csrftoken": get_token(request)
    })


@login_required()
@static_result
def get_dataset_arff(request, name="", pk=None, subset=""):
    name = unquote_plus(name)
    if pk:
        dataset = get_object_or_404(Dataset, pk=pk)
    else:
        dataset = get_object_or_404(Dataset, name=name)
    types = {
        "training": settings.TYPE_TRAINING,
        "test": settings.TYPE_TEST,
        "validation": settings.TYPE_VALIDATION
    }

    if subset == "validation" and not request.user.is_superuser:
        return HttpResponseForbidden()

    points = list(dataset.datasetpoint_set.filter(point_type=types.get(subset, -1)))
    classes = set([p.class_id for p in points])

    context = {
        "points": points,
        "subset": subset,
        "dataset": dataset,
        "attributes": range(dataset.dimensions),
        "classes_str": ",".join([str(x) for x in classes])
    }

    return render(request, "dataset.arff", context, content_type="text/plain")


@login_required()
@static_result
def get_preprocessing_arff(request, pk, subset):
    pp = get_object_or_404(Preprocessing, pk=pk)

    types = {
        "training": settings.TYPE_TRAINING,
        "test": settings.TYPE_TEST,
        "validation": settings.TYPE_VALIDATION
    }

    if subset == "validation":
        if not request.user.is_superuser:
            return HttpResponseForbidden()
        points = json.loads(pp.points_validation)

    else:
        points = [p for p in json.loads(pp.points_training_and_test) if p["point_type"] == types.get(subset, -1)]

    context = {
        "dataset": pp.dataset,
        "preprocessing": pp,
        "points": points,
        "subset": subset,
        "attributes": range(len(points[0]["coords"])),
        "classes_str": ",".join([str(x) for x in json.loads(pp.dataset.class_labels).keys()])
    }

    return render(request, "dataset.arff", context, content_type="text/plain")


def api_ok(request):
    return HttpResponseJSON({
        "status": "ok"
    })


@login_required()
def create_job(request):
    for field in ["round", "algorithm", "dataset", "params"]:
        if not field in request.POST:
            return HttpResponseJSON({
                "status": "error",
                "msg": "{} is required in POST data.".format(field)
            })

    try:
        round = Round.objects.get(pk=request.POST["round"])
    except:
        return HttpResponseJSON({
            "status": "error",
            "msg": "Round not found"
        })

    if not round.is_active():
        return HttpResponseJSON({
            "status": "error",
            "msg": "Round not active"
        })

    try:
        ds = round.datasets.get(pk=request.POST["dataset"])
    except:
        return HttpResponseJSON({
            "status": "error",
            "msg": "Dataset not in round"
        })

    if not round.get_free_jobs(request.user, ds.pk):
        return HttpResponseJSON({
            "status": "error",
            "msg": "Job limit reached"
        })

    alg = request.POST["algorithm"]
    if not alg in json.loads(round.algorithms):
        return HttpResponseJSON({
            "status": "error",
            "msg": "Algorithm not allowed"
        })

    sendparams = json.loads(request.POST["params"])
    jobparams = {}

    if "preprocessing" in sendparams:
        jobparams["preprocessing"] = sendparams["preprocessing"]
    # TODO there is no check if the specified Preprocessing is valid !!!
    # be aware of hackers

    def check_params(parameters, prefix=""):
        for pid, pdata in parameters.items():
            value = None
            fullpid = prefix+pid
            if fullpid in sendparams:
                value = sendparams[fullpid]
                if "parser" in pdata:
                    value = pdata["parser"](value)
            if "validators" in pdata:
                for validator in pdata["validators"]:
                    res = validator(value, dataset=ds, **pdata)
                    if res:
                        return "{}: {}".format(pdata["name"], res)
            jobparams[fullpid] = value

            if pdata["type"] == "dict" and "values" in pdata and isinstance(pdata["values"], dict):
                if isinstance(value, (list,tuple)):
                    for v in value:
                        if v in pdata["values"]:
                            info = pdata["values"][v]
                            if "parameters" in info:
                                res = check_params(info["parameters"], prefix+pid+"_")
                                if res:
                                    return res
                else:
                    if value in pdata["values"]:
                        info = pdata["values"][value]
                        if "parameters" in info:
                            res = check_params(info["parameters"], prefix+pid+"_")
                            if res:
                                return res

        return False

    msg = check_params(settings.algorithms[alg]["parameters"])
    if msg:
        return HttpResponseJSON({
            "status": "error",
            "msg": msg,
        })

    job = Job()
    job.user = request.user
    job.round = round
    job.dataset = ds
    job.algorithm = alg
    job.params = json.dumps(jobparams)
    try:
        if "preprocessing_id" in request.POST and request.POST["preprocessing_id"]:
            job.preprocessing = get_object_or_404(Preprocessing, pk=int(request.POST["preprocessing_id"]))
            if not job.preprocessing.points_training_and_test:
                return HttpResponseJSON({
                    "status": "error",
                    "msg": "Preprocessing should be finished first",
                })
            if job.dataset_id != job.preprocessing.dataset_id:
                return HttpResponseJSON({
                    "status": "error",
                    "msg": "Preprocessing contains wrong dataset",
                })
    except ValueError:
        pass
    job.save()

    settings.msg_queue.put({
        "msg": "update_job",
        "jobid": job.pk,
        "user": job.user.username
    })

    return HttpResponseJSON({
        "status": "ok",
        "jobid": job.pk,
        "free_jobs": {
            "dataset_id": ds.pk,
            "round_id": round.pk,
            "total_jobs": round.limit_jobs,
            "total_submits": round.limit_submit,
            "free_jobs": round.get_free_jobs(request.user, ds.pk),
            "free_submits": round.get_free_submits(request.user, ds.pk)
        }
    })


@user_passes_test(admin_only)
def get_job_params(request, jobid):
    worker = get_worker(request)
    job = get_object_or_404(Job, pk=jobid)
    job.fetch_params_ts = datetime.utcnow()
    job.worker = worker
    worker.last_job = datetime.utcnow()
    worker.save()

    data_source = job.dataset
    if job.preprocessing:
        data_source = job.preprocessing
    job.save()

    return HttpResponseJSON({
        "algorithm": job.algorithm,
        "dataset_id": job.dataset_id,
        "dataset": job.dataset.name,
        "params": json.loads(job.params),
        "preprocessing": json.loads(job.preprocessing.parameters) if job.preprocessing else [],
        "url": data_source.get_json_url(request.user),
        "url_test_arff": data_source.get_arff_url("test"),
        "url_training_arff": data_source.get_arff_url("training"),
        "url_validation_arff": data_source.get_arff_url("validation"),
    })


@user_passes_test(admin_only)
def get_preprocessing_job_params(request, ppid):
    worker = get_worker(request)
    pp = get_object_or_404(Preprocessing, pk=ppid)
    pp.fetch_params_ts = datetime.utcnow()
    pp.worker = worker
    worker.last_job = datetime.utcnow()
    worker.save()
    pp.save()
    return HttpResponseJSON({
        "dataset_id": pp.dataset_id,
        "dataset": pp.dataset.name,
        "params": json.loads(pp.parameters)
    })


@user_passes_test(admin_only)
def update_preprocessing_job_details(request, ppid):
    pp = get_object_or_404(Preprocessing, pk=ppid)
    pp.points_training_and_test = request.POST["points_training_and_test"]
    pp.points_validation = request.POST["points_validation"]
    pp.save()

    settings.msg_queue.put({
        "msg": "preprocessing_done",
        "preprocessing_id": pp.pk,
        "url": pp.get_json_url()
    })

    return HttpResponseJSON({
        "status": "ok"
    })


@login_required()
def get_job_details(request, jobid):
    job = get_object_or_404(Job, pk=jobid)
    return HttpResponseJSON(job.get_jsonable(request.user))


@login_required()
def submit_job(request, jobid):
    job = get_object_or_404(Job, pk=jobid)
    round=job.round

    if not round.get_free_submits(request.user, job.dataset_id):
        return HttpResponseForbidden("Submit limited reached.")

    if job.user == request.user or request.user.is_superuser:
        if job.round.is_active():
            job.is_selected = True
            job.save()
            settings.msg_queue.put({
                "msg": "update_job",
                "jobid": job.pk,
                "user": job.user.username
            })
            analyzer.update_round_dataset_ranking(job.round_id, job.dataset_id)
            return HttpResponseJSON({
                "job": job.get_jsonable(request.user),
                "free_jobs": {
                    "dataset_id": job.dataset_id,
                    "round_id": job.round_id,
                    "total_jobs": round.limit_jobs,
                    "total_submits": round.limit_submit,
                    "free_jobs": round.get_free_jobs(request.user, job.dataset_id),
                    "free_submits": round.get_free_submits(request.user, job.dataset_id)
                }
            })
        else:
            return HttpResponseForbidden("Related round is not active")
    return HttpResponseForbidden("You are not job owner.")


@login_required()
def rerun_job(request, jobid):
    job = get_object_or_404(Job, pk=jobid)
    round = job.round

    if job.user == request.user or request.user.is_superuser:
        if job.round.is_active():
            job.is_selected = False
            new_job = Job(user=job.user, round=job.round, dataset=job.dataset, algorithm=job.algorithm,
                          params=job.params, preprocessing=job.preprocessing)
            new_job.save()

            job.success = False
            if job.message:
                job.message = "{}\n\nStrange Results! Job restartet as #{}".format(job.message, new_job.pk)
            else:
                job.message = "Strange Results! Job restartet as #{}".format(new_job.pk)
            job.save()
            settings.msg_queue.put({
                "msg": "update_job",
                "jobid": job.pk,
                "user": job.user.username
            })
            settings.msg_queue.put({
                "msg": "update_job",
                "jobid": new_job.pk,
                "user": new_job.user.username
            })
            return HttpResponseJSON({
                "job": job.get_jsonable(request.user),
                "new_job": job.get_jsonable(request.user),
                "new_jobid": new_job.pk,
                "free_jobs": {
                    "dataset_id": job.dataset_id,
                    "round_id": job.round_id,
                    "total_jobs": round.limit_jobs,
                    "total_submits": round.limit_submit,
                    "free_jobs": round.get_free_jobs(request.user, job.dataset_id),
                    "free_submits": round.get_free_submits(request.user, job.dataset_id)
                }
            })
        else:
            return HttpResponseForbidden("Related round is not active")
    return HttpResponseForbidden("You are not job owner.")


@login_required()
def cancel_job(request, jobid):
    job = get_object_or_404(Job, pk=jobid)
    round = job.round
    if job.user == request.user or request.user.is_superuser:
        if job.success is None:
            job.is_selected = False
            job.success = False
            job.finished_ts = datetime.utcnow()
            job.message = "Job canceled by {}".format(request.user)

            job.save()
            settings.msg_queue.put({
                "msg": "update_job",
                "jobid": job.pk,
                "user": job.user.username
            })
            return HttpResponseJSON({
                "job": job.get_jsonable(request.user),
                "free_jobs": {
                    "dataset_id": job.dataset_id,
                    "round_id": job.round_id,
                    "total_jobs": round.limit_jobs,
                    "total_submits": round.limit_submit,
                    "free_jobs": round.get_free_jobs(request.user, job.dataset_id),
                    "free_submits": round.get_free_submits(request.user, job.dataset_id)
                }
            })
        else:
            return HttpResponseForbidden("Job already done.")
    return HttpResponseForbidden("You are not job owner.")


@login_required()
def load_job_list(request, roundid):
    if not request.user.is_superuser:
        jobs = [job.get_jsonable(request.user) for job in Job.objects.filter(round_id=roundid, user=request.user).prefetch_related("user", "jobpicture_set")]
    else:
        jobs = [job.get_jsonable(request.user) for job in Job.objects.filter(round_id=roundid).prefetch_related("user", "jobpicture_set")]
    return HttpResponseJSON(jobs)


@user_passes_test(admin_only)
def update_job_details(request, jobid):
    job = get_object_or_404(Job, pk=jobid)

    if job.success is not None:
        return HttpResponseJSON({
            "status": "error",
            "msg": "job already done"
        })

    try:
        if request.POST["score_test"] != "None":
            job.score_test = float(request.POST["score_test"])
        else:
            job.score_test = None
    except KeyError:
        pass
    except ValueError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "score_test has an invalid format"
        })

    try:
        if request.POST["score_validation"] != "None":
            job.score_validation = float(request.POST["score_validation"])
        else:
            job.score_validation = None
    except KeyError:
        pass
    except ValueError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "score_validation has an invalid format"
        })

    try:
        if request.POST["labels_training"] != "null" and request.POST["labels_training"] is not None:
            job.labels_training = json.dumps([int(x) for x in json.loads(request.POST["labels_training"])])
        else:
            job.labels_training = None
    except KeyError:
        pass
    except ValueError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "labels_training has an invalid format"
        })

    try:
        if request.POST["labels_test"] != "null" and request.POST["labels_test"] is not None:
            job.labels_test = json.dumps([int(x) for x in json.loads(request.POST["labels_test"])])
        else:
            job.labels_test = None
    except KeyError:
        pass
    except ValueError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "labels_test has an invalid format"
        })

    try:
        if request.POST["labels_validation"] != "null" and request.POST["labels_validation"] is not None:
            job.labels_validation = json.dumps([int(x) for x in json.loads(request.POST["labels_validation"])])
        else:
            job.labels_validation = None
    except KeyError:
        pass
    except ValueError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "labels_validation has an invalid format"
        })

    try:
        if request.POST["extra_scores_test"] != "null":
            job.extra_scores_test = json.dumps({x:y for x,y in json.loads(request.POST["extra_scores_test"]).items()})
        else:
            job.extra_scores_test = None
    except KeyError:
        pass
    except ValueError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "extra_scores_test has an invalid format"
        })

    try:
        if request.POST["extra_scores_validation"] != "null":
            job.extra_scores_validation = json.dumps({x:y for x,y in json.loads(request.POST["extra_scores_validation"]).items()})
        else:
            job.extra_scores_validation = None
    except KeyError:
        pass
    except ValueError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "extra_scores_validation has an invalid format"
        })

    try:
        if request.POST["message"] != "null":
            job.message = request.POST["message"]
        else:
            job.message = None
    except KeyError:
        pass

    try:
        if request.POST["message"] != "null":
            success_text = request.POST["success"]
            job.success = success_text.lower() in ("1", "true", "yes")
        else:
            job.success = None
    except KeyError:
        pass

    job.finished_ts = datetime.utcnow()
    worker = job.worker
    worker.jobs_done += 1
    worker.save()

    job.save()

    settings.msg_queue.put({
        "msg": "update_job",
        "jobid": job.pk,
        "user": job.user.username
    })

    return HttpResponseJSON({
        "status": "ok",
    })


@user_passes_test(admin_only)
def add_job_picture(request, jobid):
    job = get_object_or_404(Job, pk=jobid)

    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)

    filename = ''.join(c for c in request.POST["filename"] if c in valid_chars)

    try:
        path = settings.DJANGO_MEDIA_ROOT+os.path.sep+str(jobid)
        try:
            os.makedirs(path)
        except OSError:
            pass

        with open(path+os.path.sep+filename, "wb") as file:
            file.write(base64.b64decode(request.POST["data"].encode()))

        JobPicture.objects.create(job=job, filename=filename)

    except KeyError:
        return HttpResponseJSON({
            "status": "error",
            "msg": "filename and data required"
        })
    settings.msg_queue.put({
        "msg": "update_job",
        "jobid": job.pk,
        "user": job.user.username
    })
    return HttpResponseJSON({
        "status": "ok",
    })


def create_user(request):
    for param in ["username", "realname", "password1", "password2"]:
        if param not in request.POST or not request.POST[param]:
            return HttpResponseRedirect("/#"+quote("{} is required".format(param)))
    if request.POST["password1"] != request.POST["password2"]:
        return HttpResponseRedirect("/#"+quote("The given passwords are not equal."))
    if not request.POST["password1"]:
        return HttpResponseRedirect("/#"+quote("You must set a password"))

    if User.objects.filter(username=request.POST["username"]).count():
        return HttpResponseRedirect("/#"+quote("User already exists. Please choose another name."))

    User.objects.create_user(request.POST["username"], password=request.POST["password1"], first_name=request.POST["realname"])

    user = authenticate(username=request.POST["username"], password=request.POST["password1"])

    login(request, user)

    return HttpResponseRedirect("/")


def get_worker(request):
    hostname = request.META["REMOTE_HOST"] if "REMOTE_HOST" in request.META and request.META["REMOTE_HOST"] else request.META["REMOTE_ADDR"]
    return Worker.objects.get_or_create(hostname=hostname)[0]


@user_passes_test(admin_only)
def ping(request):
    worker = get_worker(request)

    job = Job.objects.filter(worker=None).first()

    if not job:
        return HttpResponseJSON(None)

    worker.last_heartbeat = datetime.utcnow()
    worker.save()

    job.worker = worker
    job.save()

    return HttpResponseJSON(job.pk)


@user_passes_test(admin_only)
def ping_preprocessing(request):
    worker = get_worker(request)

    pp = Preprocessing.objects.filter(worker=None).first()

    if not pp:
        return HttpResponseJSON(None)

    worker.last_heartbeat = datetime.utcnow()
    worker.save()

    pp.worker = worker
    pp.save()

    return HttpResponseJSON(pp.pk)


@user_passes_test(admin_only)
def ping_job(request, job_id):
    job = get_object_or_404(Job, pk=job_id)

    if datetime.utcnow() - job.fetch_params_ts > settings.BACKEND_TIMEOUT_TIME:
        job.message = "Job canceled, due to timeout limitations ({})".format(settings.BACKEND_TIMEOUT_TIME)
        job.success = False
        job.finished_ts = datetime.utcnow()
        job.save()
        settings.msg_queue.put({
            "msg": "update_job",
            "jobid": job.pk,
            "user": job.user.username
        })

    return HttpResponseJSON(job.success is None)


@login_required()
def score_list(request, round_id):
    scores = DatasetResultPoints.objects.filter(round_id=round_id).prefetch_related("user")
    objs = [obj.get_jsonable() for obj in scores]
    scores = RoundResultPoints.objects.filter(round_id=round_id).prefetch_related("user")
    for obj in scores:
        objs.append(obj.get_jsonable())

    return HttpResponseJSON(objs)


@login_required()
def score_list_total(request):
    scores = RoundResultPoints.objects.all().prefetch_related("user")
    objs = [obj.get_jsonable() for obj in scores]

    return HttpResponseJSON(objs)


def get_time(request):
    time = datetime.utcnow()
    return HttpResponseJSON(timestamp(time))


@user_passes_test(admin_only)
def start_round(request, round_id):
    round = get_object_or_404(Round, pk=round_id)
    t = datetime.utcnow()

    if not round.is_active():
        round.start_time = t
        if round.end_time and round.end_time < t:
            round.end_time = None

        if not round.start_time or round.start_time > t:
            round.start_time = t
        round.save()
        settings.msg_queue.put({
            "msg": "update_round",
            "roundid": round.pk,
            "data": round.get_jsonable()
        })

        return HttpResponseJSON({
            "status": "ok",
            "round": round.get_jsonable()
        })

    return HttpResponseJSON({
        "status": "ok",
    })


@user_passes_test(admin_only)
def stop_round(request, round_id):
    round = get_object_or_404(Round, pk=round_id)
    t = datetime.utcnow()

    if round.end_time is None or round.end_time > t:
        round.end_time = t
        round.save()
        settings.msg_queue.put({
            "msg": "update_round",
            "roundid": round.pk,
            "data": round.get_jsonable()
        })

        return HttpResponseJSON({
            "status": "ok",
            "round": round.get_jsonable()
        })

    return HttpResponseJSON({
        "status": "ok",
    })


@login_required()
def get_free_jobs(request, round_id, dataset_id):
    round = get_object_or_404(Round, pk=round_id)
    return HttpResponseJSON({
        "dataset_id": dataset_id,
        "round_id": round_id,
        "total_jobs": round.limit_jobs,
        "total_submits": round.limit_submit,
        "free_jobs": round.get_free_jobs(request.user, dataset_id),
        "free_submits": round.get_free_submits(request.user, dataset_id)
    })


@user_passes_test(admin_only)
def update_dataset_default_dimension(request):
    ds = get_object_or_404(Dataset, pk=request.POST["dataset_id"])
    data = [int(x) for x in json.loads(request.POST["dimensions"])]
    ds.default_dimensions = json.dumps(data)
    ds.save()

    return HttpResponseJSON({
        "status": "ok"
    })


@login_required()
def submit_preprocessing(request):
    params = json.loads(request.POST["params"])
    dataset_id = int(request.POST["dataset"])

    md5 = Preprocessing.get_md5(dataset_id, params)

    try:
        obj = Preprocessing.objects.get(md5=md5)
    except Preprocessing.DoesNotExist:
        obj = Preprocessing(dataset_id=dataset_id, parameters=request.POST["params"], md5=md5)
        obj.save()

    if not obj.points_training_and_test:
        if obj.worker_id:
            obj.worker = None
            obj.save()
        return HttpResponseJSON({
            "id": obj.pk,
            "status": "NOT_YET_READY"
        })

    return HttpResponseJSON({
        "id": obj.pk,
        "status": "ok",
        "url": obj.get_json_url(request.user)
    })

@login_required()
def get_preprocessing(request, preprocessing_id):
    pp = get_object_or_404(Preprocessing, pk=preprocessing_id)
    result = {
        "id": pp.pk,
        "dataset": pp.dataset_id,
        "params": json.loads(pp.parameters),
    }
    if pp.points_training_and_test:
        result["status"] = "ok"
        result["url"] = pp.get_json_url(request.user)
    else:
        result["status"] = "NOT_YET_READY"
    return HttpResponseJSON(result)

