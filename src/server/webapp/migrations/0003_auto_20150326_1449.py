# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0002_preprocessing'),
    ]

    operations = [
        migrations.AlterField(
            model_name='preprocessing',
            name='worker',
            field=models.ForeignKey(null=True, to='webapp.Worker'),
        ),
    ]
