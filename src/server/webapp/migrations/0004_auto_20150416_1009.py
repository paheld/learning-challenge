# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0003_auto_20150326_1449'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataset',
            name='cluster_dataset',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='dataset',
            name='description',
            field=models.TextField(null=True, blank=True),
        ),
    ]
