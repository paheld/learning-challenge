# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Preprocessing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('parameters', models.TextField()),
                ('md5', models.CharField(db_index=True, max_length=33)),
                ('points_training_and_test', models.TextField(null=True)),
                ('points_validation', models.TextField(null=True)),
                ('dataset', models.ForeignKey(to='webapp.Dataset')),
                ('worker', models.ForeignKey(to='webapp.Worker')),
            ],
        ),
    ]
