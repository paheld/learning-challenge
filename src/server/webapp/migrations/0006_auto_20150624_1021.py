# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0005_job_preprocessing'),
    ]

    operations = [
        migrations.AddField(
            model_name='round',
            name='star_factor',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='job',
            name='extra_scores_test',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='extra_scores_validation',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='fetch_params_ts',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='finished_ts',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='labels_test',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='labels_training',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='labels_validation',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='message',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='score_test',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='score_validation',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='job',
            name='worker',
            field=models.ForeignKey(blank=True, to='webapp.Worker', null=True),
        ),
        migrations.AlterField(
            model_name='preprocessing',
            name='points_training_and_test',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='preprocessing',
            name='points_validation',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='preprocessing',
            name='worker',
            field=models.ForeignKey(blank=True, to='webapp.Worker', null=True),
        ),
    ]
