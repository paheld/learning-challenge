# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0006_auto_20150624_1021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='round',
            name='end_time',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='round',
            name='start_time',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
