# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0004_auto_20150416_1009'),
    ]

    operations = [
        migrations.AddField(
            model_name='job',
            name='preprocessing',
            field=models.ForeignKey(to='webapp.Preprocessing', null=True, blank=True),
        ),
    ]
