# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Dataset',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(unique=True, max_length=128)),
                ('dimensions', models.IntegerField(null=True)),
                ('nr_training_points', models.IntegerField(null=True)),
                ('nr_test_points', models.IntegerField(null=True)),
                ('nr_validation_points', models.IntegerField(null=True)),
                ('nr_classes', models.IntegerField(null=True)),
                ('class_labels', models.TextField(null=True, default='{}')),
                ('default_visualization', models.CharField(max_length=128, default='scatter')),
                ('default_dimensions', models.TextField(null=True, default='[]')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DatasetPoint',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('coords', models.TextField()),
                ('class_id', models.IntegerField()),
                ('point_type', models.SmallIntegerField()),
                ('dataset', models.ForeignKey(to='webapp.Dataset')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DatasetResultPoints',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('score', models.FloatField()),
                ('points', models.IntegerField()),
                ('dataset', models.ForeignKey(to='webapp.Dataset')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(unique=True, max_length=128)),
                ('free_to_enter', models.BooleanField(default=True)),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('algorithm', models.CharField(max_length=128)),
                ('params', models.TextField(default='{}')),
                ('score_test', models.FloatField(null=True)),
                ('score_validation', models.FloatField(null=True)),
                ('labels_training', models.TextField(null=True)),
                ('labels_test', models.TextField(null=True)),
                ('labels_validation', models.TextField(null=True)),
                ('extra_scores_test', models.TextField(null=True)),
                ('extra_scores_validation', models.TextField(null=True)),
                ('message', models.TextField(null=True)),
                ('success', models.NullBooleanField()),
                ('fetch_params_ts', models.DateTimeField(null=True)),
                ('finished_ts', models.DateTimeField(null=True)),
                ('is_selected', models.BooleanField(default=False)),
                ('dataset', models.ForeignKey(to='webapp.Dataset')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='JobPicture',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('filename', models.CharField(max_length=128)),
                ('job', models.ForeignKey(to='webapp.Job')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=128)),
                ('start_time', models.DateTimeField(null=True)),
                ('end_time', models.DateTimeField(null=True)),
                ('algorithms', models.TextField()),
                ('preprocessing', models.TextField(default='[]')),
                ('limit_jobs', models.IntegerField(default=15)),
                ('limit_submit', models.IntegerField(default=3)),
                ('datasets', models.ManyToManyField(to='webapp.Dataset')),
                ('group', models.ForeignKey(to='webapp.Group')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RoundResultPoints',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('points', models.IntegerField()),
                ('stars', models.IntegerField()),
                ('round', models.ForeignKey(to='webapp.Round')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Worker',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('hostname', models.CharField(max_length=128)),
                ('last_heartbeat', models.DateTimeField(null=True)),
                ('last_job', models.DateTimeField(null=True)),
                ('jobs_done', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='job',
            name='round',
            field=models.ForeignKey(to='webapp.Round'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='job',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='job',
            name='worker',
            field=models.ForeignKey(null=True, to='webapp.Worker'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datasetresultpoints',
            name='job',
            field=models.ForeignKey(to='webapp.Job'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datasetresultpoints',
            name='round',
            field=models.ForeignKey(to='webapp.Round'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='datasetresultpoints',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
