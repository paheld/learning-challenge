#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
#TODO: add content descriptions
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import socket

import settings


from webapp.models import *

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


def general_processor(request):
    """Add some environment settings to the context variable.
    """

    context = {
        "ANALYTICS_TRACKING_ID": settings.ANALYTICS_TRACKING_ID,
        "DIMENSION_HOSTNAME_LABEL": settings.DIMENSION_HOSTNAME_LABEL,
        "DIMENSION_USERNAME_LABEL": settings.DIMENSION_USERNAME_LABEL,
        "DIMENSION_GROUP_LABEL": settings.DIMENSION_GROUP_LABEL,
        "DIMENSION_ROUND_LABEL": settings.DIMENSION_ROUND_LABEL,
        "DIMENSION_DATASET_LABEL": settings.DIMENSION_DATASET_LABEL,
        "DIMENSION_ALGORITHM_LABEL": settings.DIMENSION_ALGORITHM_LABEL,
        "VISUALIZATION_DIMENSION_DEFAULT": settings.VISUALIZATION_DIMENSION_DEFAULT,
        "HOSTNAME": socket.gethostname(),
        "datasets": {ds.id: ds.get_jsonable(request.user) for ds in Dataset.objects.all()},
        "TYPE_TRAINING": settings.TYPE_TRAINING,
        "TYPE_TEST": settings.TYPE_TEST,
        "TYPE_VALIDATION": settings.TYPE_VALIDATION,
        "algorithms": settings.algorithms_jsonable,
        "preprocessing": settings.preprocessing_jsonable,
        "distances": settings.distances,
        "rounds": {r.id: r.get_jsonable() for r in Round.objects.prefetch_related("datasets", "group").all()},
        "ADMIN": request.user.is_superuser,
        "username": request.user.username
    }

    groups = {}
    user = request.user

    for group in Group.objects.prefetch_related("users", "round_set").filter(users__pk=user.pk).order_by("name"):
        groups[group.name] = {
            "name": group.name,
            "new": False,
            "url": group.url(),
            "rounds": group.get_round_ids()
        }
    for group in Group.objects.prefetch_related("users", "round_set").filter(free_to_enter=True)\
            .exclude(users__pk=user.pk).order_by("name"):
        groups[group.name] = {
            "name": group.name,
            "new": True,
            "url": group.join_url(),
            "rounds": group.get_round_ids()
        }

    context["groups"] = groups

    return context