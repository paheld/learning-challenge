#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Some helper functions for backend usage.
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators
from api import API
from sys import argv
import random
import settings

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


api = API()
import traceback
from six.moves.urllib.parse import unquote_plus
import os.path
import tempfile
import numpy as np


def get_job_info(job_id):
    """Retrieves the information needed to process the job.

    Parameters
    ----------
    job_id : integer
        The ID of the job

    Returns
    -------
    One dict with the following fields:

    dataset : string
        Name of the dataset
    algorithm : string
        Name of the selected algorithm
    params : dict
        Dictionary with the algorithm parameters
    """

    return api.get_job_params(job_id)


def get_preprocessing_job_info(pp_id):
    """Retrieves the information needed to process the preprocessing job.

    Parameters
    ----------
    pp_id : integer
        The ID of the job

    Returns
    -------
    One dict with the following fields:

    dataset : string
        Name of the dataset
    params : dict
        Dictionary with the algorithm parameters
    """

    return api.get_preprocessing_job_params(pp_id)


def post_result(job_id,
                score_test, score_validation,
                labels_training, labels_test, labels_validation,
                extra_scores_test=None, extra_scores_validation=None,
                message=None, pictures=None,
                success=True):
    """Posts the job results to the server.

    Parameters
    ----------
    job_id : integer
        The ID of the job
    score_test : float
        The score of the test set. Should be in the interval [0,1]
    score_validation : float
        The score of the validation set. Should be in the interval [0,1]
    labels_training : list
        A list of lables for the training set
    labels_test : list
        A list of lables for the test set
    labels_validation : list
        A list of labels for the validation set
    extra_scores_test : dict (optional)
        A dictionary with further scores (just for information) from the test set.
    extra_scores_validation : dict (optional)
        A dictionary with further scores (just for information) from the validation set.
    message : string (optional)
        A message from the training set. It could contain more information about the training/test.
    pictures : list of filenames (optional)
        A list of filenames with additional visualizations of the test set.
    success : boolean (default: True)
        True if the learning process was successful

    Notes
    -----
    NOT IMPLEMENTED YET!
    """
    results = []

    labels_training = [int(x) for x in labels_training] if labels_training is not None else None
    labels_test = [int(x) for x in labels_test] if labels_test is not None else None
    labels_validation = [int(x) for x in labels_validation] if labels_validation is not None else None

    results.append(api.update_job_details(job_id,
                score_test=score_test, score_validation=score_validation,
                labels_training=labels_training,
                labels_test=labels_test,
                labels_validation=labels_validation,
                extra_scores_test=extra_scores_test, extra_scores_validation=extra_scores_validation,
                message=message,
                success=success))
    print(results)
    if pictures:
        if not isinstance(pictures, (list, set)):
            pictures = [pictures]
        for pic in pictures:
            results.append(api.add_job_picture(job_id, pic))
            if settings.BACKEND_DELETE_FILES_AFTER_UPLOAD:
                os.unlink(pic)
    return results


def mk_points(data, label, point_type):
    return [{
        "point_type": point_type,
        "class_id": int(l),
        "coords": [float(x) for x in d]
    } for d, l in zip(data, label)]


def post_preprocessing_result(pp_id,
                              training_data, training_label,
                              test_data, test_label,
                              validation_data, validation_label):
    """Posts the preprocessing job results to the server.
    """

    training_points = mk_points(training_data, training_label, settings.TYPE_TRAINING)
    test_points = mk_points(test_data, test_label, settings.TYPE_TEST)
    validation_points = mk_points(validation_data, validation_label, settings.TYPE_VALIDATION)

    tt_points = training_points + test_points

    return api.update_preprocessing_job_details(pp_id, tt_points, validation_points)


def post_exception(job_id, exception, tb=None, message=None):
    """Reports an exception to the selected task.

    This method calls the ```post_result``` method and reports the exception. The exception message is added to the
    optional message.

    Parameters
    ----------
    job_id : integer
        The ID of the job
    exception : Exception
        The thrown exception
    message : string (optional)
    """
    error = "Error occured: {}".format(exception)

    if message:
        message += "\n\n"+error
    else:
        message = error

    if tb:
        message += "\n\n"+tb

    return post_result(job_id, None, None, None, None, None, message=message, success=False)


def load_dataset(dataset_id):
    points = api.get_dataset_points(dataset_id)

    training_data = np.array([x["coords"] for x in points if x["point_type"] == 0])
    training_label = np.array([x["class_id"] for x in points if x["point_type"] == 0])

    test_data = np.array([x["coords"] for x in points if x["point_type"] == 1])
    test_label = np.array([x["class_id"] for x in points if x["point_type"] == 1])

    validation_data = np.array([x["coords"] for x in points if x["point_type"] == 2])
    validation_label = np.array([x["class_id"] for x in points if x["point_type"] == 2])

    return training_data, training_label, test_data, test_label, validation_data, validation_label


def load_dataset_from_url(url):
    points = api.load(url)

    # print(points)

    training_data = np.array([x["coords"] for x in points if x["point_type"] == 0])
    training_label = np.array([x["class_id"] for x in points if x["point_type"] == 0])

    test_data = np.array([x["coords"] for x in points if x["point_type"] == 1])
    test_label = np.array([x["class_id"] for x in points if x["point_type"] == 1])

    validation_data = np.array([x["coords"] for x in points if x["point_type"] == 2])
    validation_label = np.array([x["class_id"] for x in points if x["point_type"] == 2])

    return training_data, training_label, test_data, test_label, validation_data, validation_label


def process_job(job_id):
    """The working function for job calculations.

    Parameters
    ==========
    job_id : int
        The job id
    """

    jobdata = get_job_info(job_id)
    print(jobdata)

    try:
        if jobdata['algorithm'] in settings.algorithms:
            if "needs_ARFF_File" in settings.algorithms[jobdata['algorithm']] and settings.algorithms[jobdata['algorithm']]["needs_ARFF_File"] is True:
                arff = (
                    get_arff_file_from_url(jobdata["url_training_arff"]),
                    get_arff_file_from_url(jobdata["url_test_arff"]),
                    get_arff_file_from_url(jobdata["url_validation_arff"]),
                )
                result = settings.algorithms[jobdata['algorithm']]['worker'](arff, **jobdata['params'])
            else:
                __dataset = load_dataset_from_url(jobdata['url'])
                result = settings.algorithms[jobdata['algorithm']]['worker'](__dataset, **jobdata['params'])
            print(result)
            post_result(job_id,
                result['score_test'], result['score_validation'],
                result['training_labels'],
                result['test_labels'],
                result['validation_labels'],
                extra_scores_test=result['extra_scores_test'],
                extra_scores_validation=result['extra_scores_validation'],
                message=result['message'],
                pictures=result['pictures'],
                success=result['success'])
        else:
            print('Something went horribly wrong!!!')
    except Exception as e:
        # Ohh no something strange happens
        post_exception(job_id, e, tb=traceback.format_exc())


def process_preprocessing_job(pp_id):
    """The working function for job calculations.

    Parameters
    ==========
    pp_id : int
        The job id
    """

    ppdata = get_preprocessing_job_info(pp_id)
    print("PREPROCESSING", ppdata)

    # do some magic stuff here
    # TODO: Lernalgos HIER aufrufen, Implementation unter learners.*
    print(ppdata)
    __dataset = load_dataset(ppdata['dataset_id'])
    for pre_proc_step in ppdata['params']:
        print("Running "+pre_proc_step['algorithm']+"... ", end="")
        __dataset = settings.preprocessing[pre_proc_step['algorithm']]['worker'](__dataset, **pre_proc_step)
        print("Done!")

    result = post_preprocessing_result(pp_id, *__dataset)
    # print(result)


def get_arff_file(dataset_id, subset=None):
    """Returns the path to the ARFF file. If file not in local cache, it will be downlaoded.

    Parameters
    ==========
    dataset_id : int
        The ID of the dataset
    subset : string
        Should be one of ["training", "test", "validation"]. If no subset is given, a tripel of all arff-filenames is returned.
    """
    if subset is not None:
        ds = api.get_datasets().get(dataset_id)
        if ds is None:
            return None
        field = "url_{}_arff".format(subset)
        if field in ds:
            return cache_file(ds[field])
    else:
        return (get_arff_file(dataset_id, subset="training"), get_arff_file(dataset_id, subset="test"), get_arff_file(dataset_id, subset="validation"))
    return None


def get_arff_file_from_url(url):
    return cache_file(url)


def create_arff_file(data, labels, name='dummy'):
    tmp = tempfile.NamedTemporaryFile(mode='w+', bufsize=-1, suffix='.arff', prefix='training_', dir=None, delete=False)
    tmp.write("@RELATION "+name+"\n")
    tmp.write("\n")
    shape = np.shape(data)
    for i in xrange(shape[1]):
        tmp.write("@ATTRIBUTE dim"+str(i)+" NUMERIC\n")
    tmp.write("@ATTRIBUTE class {")
    tmp.write(",".join([str(l) for l in set(labels)])) # write all unique class labels, comma-separated
    tmp.write("}\n")
    tmp.write("\n")
    tmp.write("@DATA\n")
    for x,l in zip(data, labels):
        tmp.write(",".join([str(s) for s in x]))
        tmp.write(",")
        tmp.write(str(l))
        tmp.write("\n")
    return tmp.name



def cache_file(url):
    """Returns a filepath to the seleceted url.

    If the selected url is not in local cache, it will be downloaded.

    Parameters
    ==========
    url : string
        url starting with "/"
    """
    filename = url
    while filename[-1] == "/":
        filename = filename[:-1]
    filename = unquote_plus(filename.split("/")[-1])
    filepath = settings.BACKEND_FILE_CACHE_DIR+"/"+filename

    if not os.path.isfile(filepath):
        with open(filepath, "wb") as file:
            print("PPH:", url)
            data = api.load(url, raw=True)
            #print(data)
            file.write(data)

    return filepath


if __name__ == "__main__":
    jobid = None

    jobid = int(input("Preprocessing Job ID: "))
    if jobid > 0: process_preprocessing_job(jobid)

    jobid = int(input("Job ID: "))
    while jobid == None or jobid > 0:
        if jobid is not None: process_job(jobid)
        if len(argv)<2:
            jobid = int(input("Job ID: "))
        else:
            jobid = int(argv[1])
