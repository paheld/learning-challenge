#!/usr/bin/env python
import os
import sys

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

#import sys
#sys.path.insert(0, os.path.abspath('.'))

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.django_settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
