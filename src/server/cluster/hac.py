__author__ = 'cbraune'

import numpy as np
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as pl
from tempfile import mkstemp


def cluster(data, k=3, linkage='single', linkage_distance='euclidean', **kwargs):
    n, _ = np.shape(data)

    if k > n:
        return 'Number of cluster centers larger than number of data points. That makes no sense!'

    if linkage_distance == 'manhattan':
        linkage_distance = 'cityblock'

    z = sch.linkage(data, method=linkage, metric=linkage_distance)
    prediction = sch.fcluster(z, k, criterion='maxclust')

    _, file_name = mkstemp(suffix=".png", prefix='tmp_cla_hac_')

    pl.figure()
    p = min(100, max(k * 2, 30))
    sch.dendrogram(z, p=p, truncate_mode='lastp')
    pl.xticks([])
    pl.xlabel("Maximally 30 <= k*2 <= 100 non-singletons are shown. (Here: %i)" % p)
    pl.savefig(file_name)

    return prediction, [file_name]