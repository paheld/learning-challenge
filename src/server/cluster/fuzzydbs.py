__author__ = 'cbraune'

from cluster.main import main

def cluster(data, eps=1, minptsmin=5, minptsmax=10, **_):

    print(data)

    prediction, images = main(eps, minptsmin, minptsmax, data, createImage=True)

    return prediction, [images]
