from __future__ import absolute_import

import cluster.kmeans
import cluster.kmedian
import cluster.hac
import cluster.mst
import cluster.spectral
import cluster.gmm
import cluster.affinity
import cluster.dbscan
import cluster.optics
import cluster.meanshift
import cluster.triclust

import cluster.diana
import cluster.comeans
import cluster.fuzzyhac
import cluster.snn
import cluster.fuzzydbs
import cluster.studentem
import cluster.spc

import cluster.ca2016.fuzzy_dbscan as fuzzy_dbscan_2016
import cluster.ca2016.som as som_2016
import cluster.ca2016.nscabdt as nscabdt_2016
import cluster.ca2016.ascdt as ascdt_2016
import cluster.ca2016.ant as ant_2016
import cluster.ca2016.spc as spc_2016

import cluster.ca2017.kbcht as kbcht_2017
import cluster.ca2017.Evolutionary_Clustering as ea_2017
import cluster.ca2017.flame as flame_2017
import cluster.ca2017.SOM as som_2017
import cluster.ca2017.birch as birch_2017
import cluster.ca2017.khopca as khopca_2017
import cluster.ca2017.pycure as cure_2017

import numpy as np
from time import time
from sklearn.metrics import homogeneity_completeness_v_measure


def cluster_func(func):
    def do_cluster(dataset, *args, **kwargs):
        (_, _, _, _, data, label) = dataset

        start = time()
        result = func(data, *args, **kwargs)
        time_taken = time() - start

        rest = []
        pic_names = []

        if isinstance(result, str):
            error = {'message': result,
                     'pictures': [],
                     'success': False}
            return error
        elif isinstance(result, tuple):
            if len(result) == 2:
                result, pic_names = result
            else:
                result, pic_names = result[0], result[1]
                rest = result[2:]

        print(pic_names)

        h, c, _ = homogeneity_completeness_v_measure(label, result)
        k = len(np.unique(result))  # no of clusters found

        result = {'score_test': k,
                  'score_validation': min(h, c),
                  'training_labels': [],
                  'test_labels': result,
                  'validation_labels': result,
                  'extra_scores_test': {'Number of clusters found:': '%3.0f' % k},
                  'extra_scores_validation': {'Homogenity': '%5.4f' % h,
                                              'Completeness': '%5.4f' % c},
                  'message': 'Training time: %2.3fms' % (time_taken*1000),
                  'pictures': pic_names,
                  'success': True}
        if len(rest) > 0 and isinstance(rest, dict):
            for k,v in rest:
                result['extra_scores_validation'][k] = v
        return result

    return do_cluster