__author__ = 'cbraune'

from sklearn.cluster import KMeans
import numpy as np
from sklearn.metrics import homogeneity_completeness_v_measure, silhouette_score
from time import time
from json import loads
from sklearn.metrics.pairwise import manhattan_distances

# Idea from Mathieu Blondel <mathieu@mblondel.org>, https://gist.github.com/mblondel
class KMedians(KMeans):

    def _e_step(self, X):
        self.labels_ = manhattan_distances(X, self.cluster_centers_).argmin(axis=1)

    def _average(self, X):
        return np.median(X, axis=0)

def cluster(data, k=3, *args, **kwargs):
    n, _ = np.shape(data)

    if k > n:
        return 'Number of cluster centers larger than number of data points. That makes no sense!'

    if 'init' in kwargs:
        if kwargs['init'] == 'random':
            init = 'random'
        elif kwargs['init'] == 'points':
            init_points = loads(kwargs['init_points'])
            try:
                init_points = np.array(init_points)
            except:
                return 'Cluster definition incoherent with dataset. Probably number of dimension for at least one point wrong.'
            else:
                if len(init_points) != k:
                    return 'Number of cluster centers does not match number of clusters.'
                else:
                    init = init_points


    k_medians = KMedians(init=init, n_clusters=k, n_init=1)
    return k_medians.fit_predict(data)
