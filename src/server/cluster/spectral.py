__author__ = 'cbraune'

import numpy as np
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as pl
from tempfile import mkstemp
from sklearn.cluster import SpectralClustering as SpectralClustering


def cluster(data, k=3, affinity='nearest_neighbors', affinity_k=10, affinity_gamma=1.0, assign='discretize', **_):

    if affinity == 'knn':
        affinity = 'nearest_neighbors'

    sc = SpectralClustering(n_clusters=k, n_init=1, affinity=affinity,
                            gamma=affinity_gamma, n_neighbors=affinity_k, assign_labels=assign)
    sc.fit(data)
    prediction = sc.labels_

    return prediction