__author__ = 'cbraune'

from sklearn.cluster import MeanShift as MeanShift


def cluster(data, bandwidth=None, minbinfreq=1, clusterall=True, binseeding=False, **_):

    ms = MeanShift(bandwidth=bandwidth, cluster_all=clusterall, seeds=None, bin_seeding=binseeding, min_bin_freq=minbinfreq)

    ms.fit(data)
    prediction = ms.labels_

    return prediction
