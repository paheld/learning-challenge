import os
working_dir = os.path.dirname(os.path.abspath(__file__))

from subprocess import PIPE, Popen
from time import time
from sklearn.metrics import homogeneity_completeness_v_measure
import numpy as np
from scipy.io.arff import loadarff

def cluster(data, nocluster=3, k=3, linkage='single', **_):

    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    _, test_file, validation_file = data
    true_label = []

    start = time()
    command = ["java", "-Xmx1g", "-jar",  working_dir+"/../lib/kHAC_1.0.jar", test_file, str(int(nocluster)), linkage, str(k)]
    proc = Popen(command, stdout=PIPE, stderr=PIPE)
    output, err = proc.communicate()

    if proc.returncode != 0:
        error['message'] = "Something went wrong when executing the jar file! \n\nCommand:\n({})\n\n---\n{}".format(command, err)
        return error

    lines = output.split("\n")
    prediction = map(int, lines[:-1])
    time_taken = time() - start

    arff, _ = loadarff(validation_file)

    labels = map(int, [x[-1] for x in arff])

    h, c, _ = homogeneity_completeness_v_measure(prediction, labels)
    k = len(np.unique(prediction))  # no of clusters found

    result = {'score_test': k,
              'score_validation': min(h, c),
              'training_labels': [],
              'test_labels': prediction,
              'validation_labels': prediction,
              'extra_scores_test': {'Number of clusters found:': '%3.0f' % k},
              'extra_scores_validation': {'Homogenity': '%5.4f' % h,
                                          'Completeness': '%5.4f' % c},
              'message': 'Training time: %2.3fms' % (time_taken*1000),
              'pictures': [],
              'success': True}

    return result
