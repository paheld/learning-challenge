__author__ = 'cbraune'

from scipy.spatial.distance import squareform, pdist
from scipy.sparse.csgraph import minimum_spanning_tree,connected_components
import matplotlib.pyplot as pl
from tempfile import mkstemp

def cluster(data, k=3, d='euclidean', d_weights=None, d_p=None):
    if d == 'manhattan':
        d = 'cityblock'

    dm = squareform(pdist(data, metric=d, w=d_weights, p=d_p))
    mst = minimum_spanning_tree(dm)
    min_tree = mst.toarray()

    _, file_name_1 = mkstemp(suffix=".png", prefix='tmp_cla_mst_')

    fig = pl.figure()
    pl.title('Minimum Spanning Tree')
    ax = fig.add_subplot(111)
    ax.scatter(data[:, 0], data[:, 1])
    edge_list = []
    for i in range(len(min_tree)):
        for j in range(len(min_tree[i])):
            if min_tree[i, j] > 0.0:
                edge_list.append((i, j))
    for edge in edge_list:
        i, j = edge
        ax.plot([data[i, 0], data[j, 0]], [data[i, 1], data[j, 1]], c='r')
    pl.savefig(file_name_1)
    pl.close()

    for _ in range(k-1):  # find k-1 largest edges and remove them
        cur_max = -1
        cur_pos = (-1, -1)
        for i in range(len(min_tree)):
            for j in range(len(min_tree[i])):
                if min_tree[i, j] > cur_max:
                    cur_pos = (i,j)
                    cur_max = min_tree[i, j]
        min_tree[cur_pos[0], cur_pos[1]] = 0.0  # delete edge
    _, prediction = connected_components(min_tree)

    _, file_name_2 = mkstemp(suffix=".png", prefix='tmp_cla_mst_')

    fig = pl.figure()
    pl.title('After removing the k-1 longest edges')
    ax = fig.add_subplot(111)
    ax.scatter(data[:, 0], data[:, 1])
    edge_list = []
    for i in range(len(min_tree)):
        for j in range(len(min_tree[i])):
            if min_tree[i, j] > 0.0:
                edge_list.append((i, j))
    for edge in edge_list:
        i, j = edge
        ax.plot([data[i, 0], data[j, 0]], [data[i, 1], data[j, 1]], c='r')
    pl.savefig(file_name_2)
    pl.close()

    return prediction, [file_name_1, file_name_2]