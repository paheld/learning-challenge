from __future__ import print_function, division, unicode_literals, absolute_import, generators
from sklearn.cluster import AffinityPropagation
from scipy.spatial.distance import pdist, squareform
import matplotlib.pyplot as pl
import numpy as np
from tempfile import mkstemp
from itertools import cycle

__author__ = 'cbraune'

color_codes = ['#bfa600', '#33cc85', '#23548c', '#f7bfff', '#b39e86', '#6d7356', '#165950', '#403830', '#0065d9',
               '#ee80ff', '#ffc480', '#cfe673', '#3df2da', '#1d2873', '#370040', '#b2742d', '#c2f200', '#b6e6f2',
               '#4059ff', '#6f0080', '#331b00', '#118000', '#0d2b33', '#8f86b3', '#c700e6', '#663600', '#20f200',
               '#23778c', '#140d33', '#f28100', '#1a331f', '#39c3e6', '#2900cc', '#ccc599', '#00400d', '#2d4159',
               '#9360bf', '#665c1a', '#4d9975', '#73a8e6', '#564359']
colors = cycle(color_codes)


def cluster(data, metric='euclidean', preferences=50, max_iter=500, damping=0.5, metric_weights=None, metric_p=None, **kwargs):

    if metric_weights is not None:
        metric_weights = map(int, metric_weights.split(","))
        if len(metric_weights) is not data.shape[1]:
            return "Something is wrong with the weight vector."

    if metric == 'manhattan': metric = 'cityblock'

    if damping >= 1:
        damping = 0.99999

    if damping < 0.5:
        damping = 0.5

    w = np.array(metric_weights) if metric == 'wminkowski' else None
    p = metric_p if (metric in ('wminkowski', 'minkowski') and metric_p is not None) else None

    dm = squareform(pdist(data, metric=metric, w=w, p=p))
    affinity = -np.square(dm)
    ap = AffinityPropagation(max_iter=max_iter, damping=damping, preference=preferences, affinity='precomputed')
    prediction = ap.fit_predict(affinity)

    dims = range(data.shape[1])
    file_names = []

    if len(dims) == 2:
        b, a = 1, 0

        cluster_centers_indices = ap.cluster_centers_indices_
        if cluster_centers_indices is None:
            cluster_centers_indices = []
            print("No clusters found!!!")
        else:
            _, file_name = mkstemp(suffix=".png", prefix='tmp_cla_aff_')
            fig = pl.figure()
            ax = fig.add_subplot(111)
            n_clusters_ = len(cluster_centers_indices)
            for k, col in zip(range(n_clusters_), colors):
                class_members = prediction == k
                cluster_center = data[cluster_centers_indices[k]]
                pl.plot(data[class_members, 0], data[class_members, 1], color=col, marker='.')
                pl.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col, markeredgecolor='k', markersize=14)
                for x in data[class_members]:
                    pl.plot([cluster_center[0], x[0]], [cluster_center[1], x[1]], col)
            pl.title('Estimated number of clusters: %d' % n_clusters_)
            ax.axis('tight')
            pl.xlabel("Dim"+str(a))
            pl.ylabel("Dim"+str(b))
            pl.savefig(file_name)
            pl.close()
            file_names.append(file_name)
    return prediction, file_names