__author__ = 'cbraune'

from sklearn.cluster import DBSCAN as DBSCAN


def cluster(data, eps=0.5, minpts='nearest_neighbors', metric='euclidean', metric_p=None, **_):

    algo = 'auto'
    if metric in ['correlation', 'mahalanobis', 'seuclidean', 'sqeuclidean', 'yule', 'cosine']:
        algo = 'brute'

    db = DBSCAN(eps=eps, min_samples=minpts, metric=metric, algorithm=algo, p=metric_p)
    db.fit(data)
    prediction = db.labels_

    return prediction
