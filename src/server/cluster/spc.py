from __future__ import print_function, division, unicode_literals, absolute_import, generators

from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform
import numpy as np
import math as m
# from arff import convert_arff_to_matrix

def calculate_probability(S, J, t, n):
    P = np.zeros((n, n), dtype='f')
    for i in range(n):
        for j in range(n):
            if S[i] == S[j]:
                P[i, j] = 1 - m.exp(-(J[i, j] / t))
    return P

def wolff(iterations, points, spins, temperature, neighbors, N, J):
    # create spin vector (S), by assigning a random value to each point
    S = np.random.random_integers(1, spins, points)
    #print "Spin vector:\n"
    #print S
    #print "\n"

    for _ in xrange(iterations):
        # select random point
        xi = np.random.randint(points)

        frontier = [xi]
        closed = []
        
        # new random spin
        spin = np.random.randint(1, spins + 1)

        while frontier:
            active_x = frontier.pop(0)

            # generate list of neighbors with same spin
            candidates = [p for p in N[active_x, 1:neighbors + 1] if S[p] == S[active_x]]

            P = calculate_probability(S, J, temperature, points)

            for c in candidates:
                if P[active_x, c] > 0.0 and np.random.random_sample() <= P[active_x, c]:
                    if c not in closed:
                        S[c] = spin
                        closed.append(c)
                        frontier.append(c)

            if active_x not in closed:
                S[active_x] = spin
                closed.append(active_x)
    return S

def classify(S, points, N, neighbors):
    done = []
    clusters = [0] * points

    cluster = 0

    for i in xrange(points):
        if i in done:
            continue

        clusters[i] = cluster
        done.append(i)

        queue = [i]

        while queue:
            xi = queue.pop(0)
            candidates = [p for p in N[xi, 1:neighbors + 1] if S[p] == S[i]]
            for c in candidates:
                if c not in done:
                    clusters[c] = cluster
                    done.append(c)
                    queue.append(c)

        cluster += 1

    return clusters


def spc(data, neighbors=11, spins=20, iterations=500, temperature=0.05):
    # print "Data:\n"
    # print data
    # print "\n"

    # number of points
    points = data.shape[0]

    # calculate distance matrix for all point pairs (D)
    D = squareform(pdist(data, 'euclidean'))

    # print "Distance matrix:\n"
    # print D
    # print "\n"

    # sort neighbors after distance and create neighborhood matrix (N)
    N = np.argsort(D)

    # print "Nearest neighbors:\n"
    # print N
    # print "\n"

    # calculate fix coupling matrix (J)
    J = np.zeros((points, points), dtype='f')
    for p in range(points):
        # build mean over distances to nearest neighbors
        mean = np.mean([D[p, x] for x in N[p, 1:neighbors + 1]])
        # calculate interactions between point p and it's neighbors
        for n in range(neighbors):
            J[p, N[p, n + 1]] = m.exp(-(D[p, N[p, n + 1]] / (2 * mean**2))) / neighbors

    # print "Coupling Matrix:\n"
    # print J
    # print "\n"

    # perform Wolff algorithm (Monte Carlo)
    S = wolff(iterations, points, spins, temperature, neighbors, N, J)

    # print "Updated spin matrix:"
    # print S
    # print "\n"

    result = classify(S, points, N, neighbors)

    return result

def cluster(data, neighbors=11, spins=20, iterations=500, temperature=0.05, **_):

    prediction = spc(data, neighbors=neighbors, spins=spins, iterations=iterations, temperature=temperature)

    return prediction


if __name__ == "__main__":
    from sklearn.datasets import make_blobs
    point_list, labels = make_blobs(n_samples=300, shuffle=False, cluster_std=0.01)
    #point_list = convert_arff_to_matrix("data/iris.arff")[:,:4]
    for t in np.linspace(0.001, 0.002, 10):
        print(t, end=" ")
        clust = spc(point_list, neighbors=15, spins=2, iterations=100, temperature=t)
        print(clust)
		
