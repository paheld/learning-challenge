#changed copy for Submission
import numpy as np
import scipy.spatial.distance as spd

metricstring = 'euclidean'

def DIANAbyK(data, k):
    #for values between 1 and number of items

    datat = np.transpose(data)
    if(k >len(data) or k < 1):
        raise NameError("k must be  1 <= k <= number of samples")

    labels = clusterDIANA(datat, k)

    for i in range(len(labels[0])):
        print(labels.item(i))
    return labels

def DIANAbyDC(data, dc):
    #for values between 0 and 1
    datat = np.transpose(data)
    if(dc <= 0 or dc > 1):
        raise NameError("k must be  0 < k <= 1 ")
    labels = clusterDIANA(datat, dc)

    for i in range(len(labels[0])):
        print(labels.item(i))
    return labels



def clusterDIANA(data, *args):
    #outputList containing element labels
    classLabelList = np.zeros([1,data.shape[1]])
    #matrix containing distances d(i,j)
    #distMat = calcDistMat(data)
    distMat = spd.squareform(spd.pdist(np.transpose(data), metric=metricstring))
    #number of inputelements
    nInputElements = data.shape[1]
    #Sum of distances to cluster and splinter
    dhSums = np.zeros([2,nInputElements])


    #terminate after a number of clusters is reached
    if ((len(args) == 1 and args[0] >= 1)):

        nClusters = 1
        clusterList =  [];
        clusterDiameters = [-1]
        clusterList.append(list(range(nInputElements)))
        data_diam = 0

        #outer loop for number of clusters
        while (nClusters < args[0]):


            #--find cluster with largest diameter
            ind = -1
            diameter = -1
            for clusterIndex in range(len(clusterList)):

                if(clusterDiameters[clusterIndex] == -1):
                    for elementi in clusterList[clusterIndex]:
                        for elementj in clusterList[clusterIndex]:
                            if( distMat[elementi, elementj] > diameter):
                                diameter = distMat[elementi,elementj]
                                ind = clusterIndex
                            if(distMat[elementi, elementj] > clusterDiameters[clusterIndex]):
                                clusterDiameters[clusterIndex] = distMat[elementi, elementj]
                else:
                    if(clusterDiameters[clusterIndex] > diameter):
                        diameter = clusterDiameters[clusterIndex]
                        ind = clusterIndex
            #--ind is index of cluster with largest diameter

            if(len(clusterList) == 1): data_diam = clusterDiameters[0]

            # split cluster
            activeCluster = clusterList[ind]    #indices of elements in the cluster
            splitCluster = []                   #indices of the elements in the split group
            nElementsNONSplit = len(activeCluster)
            nElementsSplit = 0
            dhSums[:,:] = 0

            #sum(d(i,j) for all nonSplitElements)
            for elementi in activeCluster:
                for elementj in activeCluster:
                    dhSums[0,elementi] += distMat[elementi, elementj]

            #find initial splitElement

            initSIndex = -1
            dissimilarity = -1

            for elementi in range(len(activeCluster)):
                if(dhSums[0,activeCluster[elementi]] > dissimilarity):
                    dissimilarity = dhSums[0,activeCluster[elementi]]
                    initSIndex = elementi

            for elementi in activeCluster:
                dhSums[0,elementi] -= distMat[elementi,activeCluster[initSIndex]]
                dhSums[1,elementi] += distMat[elementi,activeCluster[initSIndex]]

            nElementsNONSplit = nElementsNONSplit -1
            nElementsSplit = nElementsSplit +1

            splitCluster.append(activeCluster[initSIndex])
            del activeCluster[initSIndex]



            Dh = 1
            #add elements to splinter group if closer
            while(Dh > 0):
                Dh = -1
                DhIndex = -1
                for elementi in range(len(activeCluster)):
                    current_Dh = (dhSums[0,activeCluster[elementi]] / (nElementsNONSplit)-1) - (dhSums[1,activeCluster[elementi]] / nElementsSplit)
                    if(current_Dh > Dh):
                        Dh = current_Dh
                        DhIndex = elementi

                if(Dh <= 0):
                    continue

                #DhIndex points to the element most similar to the splinter group

                for elementi in activeCluster:
                    dhSums[0,elementi] -= distMat[elementi,activeCluster[DhIndex]]
                    dhSums[1,elementi] += distMat[elementi,activeCluster[DhIndex]]

                nElementsNONSplit = nElementsNONSplit -1
                nElementsSplit = nElementsSplit +1

                splitCluster.append(activeCluster[DhIndex])
                del activeCluster[DhIndex]
                #print("----- %.05s seconds ------onemoreelement" % (time.time() - start_time))

            divCoeff = 0

            split_diam = 0
            for i in range(len(splitCluster)):
                for j in range( len(splitCluster)):
                    if(distMat[splitCluster[i],splitCluster[j]] > split_diam): split_diam = distMat[splitCluster[i],splitCluster[j]]
            active_diam = 0
            for i in range(len(activeCluster)):
                for j in range(len(activeCluster)):
                    if(distMat[activeCluster[i],activeCluster[j]] > active_diam): active_diam = distMat[activeCluster[i],activeCluster[j]]

            sum_diam = active_diam * len(activeCluster)+ split_diam * len(splitCluster)

            for i in range(len(clusterList)):
                if(i != ind): sum_diam += len(clusterList[i]) * clusterDiameters[i]


            clusterList.append(activeCluster)
            clusterDiameters.append(active_diam)
            clusterList.append(splitCluster)
            clusterDiameters.append(split_diam)
            del clusterList[ind]
            del clusterDiameters[ind]

            nClusters += 1
        #fill Labellist
        for label in range(nClusters):
            for sampleElement in clusterList[label]:
                classLabelList[0,sampleElement] = label


        return classLabelList

    #-----------------------------------------------------------------------------------------------------


    #-----------------------------------------------------------------------------------------------------

    elif (len(args) == 1 and args[0] < 1 and args[0] > 0):
        nClusters = 0
        clusterList =  [];
        clusterDiameters = [-1]
        clusterList.append(list(range(nInputElements)))
        data_diam = 0

        #outer loop for number of clusters
        while (nClusters < args[0]):



            #--find cluster with largest diameter
            ind = -1
            diameter = -1
            for clusterIndex in range(len(clusterList)):

                if(clusterDiameters[clusterIndex] == -1):
                    for elementi in clusterList[clusterIndex]:
                        for elementj in clusterList[clusterIndex]:
                            if( distMat[elementi, elementj] > diameter):
                                diameter = distMat[elementi,elementj]
                                ind = clusterIndex
                            if(distMat[elementi, elementj] > clusterDiameters[clusterIndex]):
                                clusterDiameters[clusterIndex] = distMat[elementi, elementj]
                else:
                    if(clusterDiameters[clusterIndex] > diameter):
                        diameter = clusterDiameters[clusterIndex]
                        ind = clusterIndex
            #--ind is index of cluster with largest diameter

            if(len(clusterList) == 1): data_diam = clusterDiameters[0]

            # split cluster
            activeCluster = clusterList[ind]    #indices of elements in the cluster
            splitCluster = []                   #indices of the elements in the split group
            nElementsNONSplit = len(activeCluster)
            nElementsSplit = 0
            dhSums[:,:] = 0

            #sum(d(i,j) for all nonSplitElements)
            for elementi in activeCluster:
                for elementj in activeCluster:
                    dhSums[0,elementi] += distMat[elementi, elementj]

            #find initial splitElement

            initSIndex = -1
            dissimilarity = -1

            for elementi in range(len(activeCluster)):
                if(dhSums[0,activeCluster[elementi]] > dissimilarity):
                    dissimilarity = dhSums[0,activeCluster[elementi]]
                    initSIndex = elementi

            for elementi in activeCluster:
                dhSums[0,elementi] -= distMat[elementi,activeCluster[initSIndex]]
                dhSums[1,elementi] += distMat[elementi,activeCluster[initSIndex]]

            nElementsNONSplit = nElementsNONSplit -1
            nElementsSplit = nElementsSplit +1

            splitCluster.append(activeCluster[initSIndex])
            del activeCluster[initSIndex]



            Dh = 1
            #add elements to splinter group if closer
            while(Dh > 0):
                Dh = -1
                DhIndex = -1
                for elementi in range(len(activeCluster)):
                    current_Dh = (dhSums[0,activeCluster[elementi]] / (nElementsNONSplit)-1) - (dhSums[1,activeCluster[elementi]] / nElementsSplit)
                    if(current_Dh > Dh):
                        Dh = current_Dh
                        DhIndex = elementi

                if(Dh <= 0):
                    continue

                #DhIndex points to the element most similar to the splinter group

                for elementi in activeCluster:
                    dhSums[0,elementi] -= distMat[elementi,activeCluster[DhIndex]]
                    dhSums[1,elementi] += distMat[elementi,activeCluster[DhIndex]]

                nElementsNONSplit = nElementsNONSplit -1
                nElementsSplit = nElementsSplit +1

                splitCluster.append(activeCluster[DhIndex])
                del activeCluster[DhIndex]
                #print("----- %.05s seconds ------onemoreelement" % (time.time() - start_time))

            divCoeff = 0

            split_diam = 0
            for i in range(len(splitCluster)):
                for j in range( len(splitCluster)):
                    if(distMat[splitCluster[i],splitCluster[j]] > split_diam): split_diam = distMat[splitCluster[i],splitCluster[j]]
            active_diam = 0
            for i in range(len(activeCluster)):
                for j in range(len(activeCluster)):
                    if(distMat[activeCluster[i],activeCluster[j]] > active_diam): active_diam = distMat[activeCluster[i],activeCluster[j]]

            sum_diam = active_diam * len(activeCluster)+ split_diam * len(splitCluster)

            for i in range(len(clusterList)):
                if(i != ind): sum_diam += len(clusterList[i]) * clusterDiameters[i]
            if(args[0] < (sum_diam/(nInputElements*data_diam)) ):

                clusterList.append(activeCluster)
                clusterDiameters.append(active_diam)
                clusterList.append(splitCluster)
                clusterDiameters.append(split_diam)
                del clusterList[ind]
                del clusterDiameters[ind]
            else:
                activeCluster.append(splitCluster)
                nClusters = len(clusterList)
        #fill Labellist
        for label in range(nClusters):
            for sampleElement in clusterList[label]:
                classLabelList[0,sampleElement] = label

        return classLabelList
    else:
        return classLabelList



def cluster(data, k=3, metric='euclidean', **_):

    global metricstring
    if metric == 'manhattan':
        metric = 'cityblock'
    metricstring = metric


    if k < 1:
        prediction = DIANAbyDC(data, k)
    else:
        prediction = DIANAbyK(data, k)
    return prediction[0]
