import numpy

def cluster_points(X, centers):
  clusters = []
  for x in X:
      clustername = 0
      distance = numpy.linalg.norm(x-centers[0])
      i = -1
      for y in centers:
          i = i + 1
          newdistance = numpy.linalg.norm(x-y)
          if(distance > newdistance):
              distance = newdistance
              clustername = i
      clusters.append(clustername)
  return clusters

def cluster_point(x, centers):
  clusters 
  clustername = 0
  distance = numpy.linalg.norm(x-centers[0])
  i = -1
  for y in centers:
      i = i + 1
      newdistance = numpy.linalg.norm(x-y)
      if(distance > newdistance):
           distance = newdistance
           clustername = i
  clusters = clustername
  return clusters

def reevaluate_centers(clusters,X):
    max = numpy.max(clusters)
    cluster = [None]*(max+1)
    i = 0
    newcenters = []
    while(i<len(X)):
        try:
            cluster[clusters[i]]+=[X[i]]
        except TypeError:
            cluster[clusters[i]]=[X[i]]
        i+=1
    for c in cluster:
        newcenters.append(getmiddle(c))
    return newcenters

def getmiddle(samplelist):
    middle = numpy.sum(samplelist,0)/len(samplelist)
    return middle

#def cluster_points(X, mu):
#    clusters  = {}
#    for x in X:
#        bestmukey = min([(i[0], linalg.norm(x-mu[i[0]])) \
#                    for i in enumerate(mu)], key=lambda t:t[1])[0]
#        try:
#            clusters[bestmukey].append(x)
#        except KeyError:
#            clusters[bestmukey] = [x]
#    return clusters
 
#def reevaluate_centers(mu, clusters):
#    newmu = []
#    keys = sorted(clusters.keys())
#    for k in keys:
#        newmu.append(mean(clusters[k], axis = 0))
#    return newmu
 
def has_converged(mu, oldmu):
    return (set([tuple(a) for a in mu]) == set([tuple(a) for a in oldmu]))

def is_equal(mu, oldmu):
    i = 0
    ret = True
    while(i<len(mu)):
        ret &= mu[i]==oldmu[i]
        i+=1

    return ret

def find_centers(mustlink,constraints, X, K,G):
    # Initialize to K random centers
    i = 0
    points = numpy.random.permutation(X)
    oldcenters = []
    while(i<K):
        oldcenters.append(points[i])
        i+=1
    centers = list(oldcenters)
    oldcenters[0] = points[1]
    clusters = []
    i=0
    while ((not has_converged(centers, oldcenters)) and i<G):
        oldcenters = list(centers)
        # Assign all points in X to clusters
        clusters = cluster_points(X, oldcenters)
        newclusters = reassign_clusters(clusters,mustlink,constraints,oldcenters, X)
        # Reevaluate centers
        if(newclusters is None):
            centers = reevaluate_centers(clusters,X)
        else:
            centers = reevaluate_centers(newclusters,X)
        i+=1
    return clusters 

def reassign_clusters(clusters, mustlink, constraints, centers, X):
    newmustlink = []
    done = False
    for link in mustlink:
        i = 0
        for newlink in newmustlink:
            done |= link[0] in newlink
            if(link[0] in newlink):
                newmustlink[i] += link[1]
            done |= link[1] in newlink
            if(link[1] in newlink):
                newmustlink[i] += link[0]
            i+=1
        if(not done):
            newmustlink.append(link)
    try:
        for link in newmustlink:
            sumdistance = [0]*(len(link))
            changed = False
            for element1 in link:
                for element2 in link:
                    if(clusters[X.index(element1)] != clusters[X.index(element2)]):
                        changed = True
                        sumdistance[link.index(element1)] += numpy.linalg.norm(element1-centers[clusters[X.index(element1)]])
            if(changed):
                for element in link:
                    clusters[X.index(element)] = clusters[X.index(link[sumdistance.index(numpy.amin(sumdistance))])]
        for con in constraints:
            if(clusters[X.index(con[0])] == clusters[X.index(con[1])]):
                distance1 = numpy.linalg.norm(con[0]-centers[clusters[X.index(con[0])]]) + numpy.linalg.norm(con[0]-centers[clusters[X.index(con[1])]])
                distance2 = numpy.linalg.norm(con[1]-centers[clusters[X.index(con[0])]]) + numpy.linalg.norm(con[1]-centers[clusters[X.index(con[1])]])
                if(distance1>distance2):
                    zorro = []
                    i = 0
                    anz = len(centers)
                    while(i<anz):
                        data = centers[i]
                        if(not is_equal(data.tolist(),centers[clusters[X.index(con[0])]])):
                           zorro.append(data)
                        i+=1
                    
                    clusters[X.index(con[0])] = getIndex(centers,zorro[numpy.random.random_integers(0,len(zorro)-1,1)[0]].tolist())
                else:
                    zorro = []
                    i = 0
                    anz = len(centers)
                    while(i<anz):
                        data = centers[i]
                        if(not is_equal(data.tolist(),centers[clusters[X.index(con[1])]])):
                           zorro.append(data)
                        i+=1
                    clusters[X.index(con[1])] = getIndex(centers,zorro[numpy.random.random_integers(0,len(zorro)-1,1)[0]])
        return clusters
    except ValueError as arg:
        raise arg
        #print "FEHLERANFANG:"
        #print "arg.args",arg.args
        #print "arg.message",arg.message
        #print "len(zorro): ", len(zorro)
        #print "len(zorro)-1: ", len(zorro)-1
        #print "Punkte in Constrains oder Mustlink falsch angegeben!"
        #print "zorro: ",zorro
        #print "clusters: ",clusters
        #print "centers: ",centers
        #print "FEHLERENDE"
        
def getIndex(centers,point):
    i = 0
    for c in centers:
        if(is_equal(c,point)):
            return i
        i+=1
    return -1