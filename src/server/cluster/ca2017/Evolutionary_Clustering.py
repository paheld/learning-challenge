import numpy as np
import time as time
from scipy.spatial import KDTree as kd
from scipy.spatial import minkowski_distance as md

import cluster.ca2017.eac_stuff.Fitness as ft
import cluster.ca2017.eac_stuff.Mutation as mt
import cluster.ca2017.eac_stuff.Rekombination as rk
import cluster.ca2017.eac_stuff.Selection as sl
from cluster.ca2017.eac_stuff import Evol_Alg as alg


def cluster(data_matrix, cluster, generations, size, sample_size, fit, sel, mut, rek, **kwargs):
    fit_args = []
    if fit == "min-clique":
        fit_args = [kwargs.get("fit_mink_dist_const", 1)]
    elif fit == "density_sum":
        fit_args = [kwargs.get("fit_mink_dist_const", 1), kwargs.get("fit_cut_dist", 1)]
    elif fit == "knn_sum":
        fit_args = [kwargs.get("fit_knnk", 1)]

    sel_args = [kwargs.get("sel_size", 1)]

    mut_args = [kwargs.get("mut_amount", 0)]

    rek_args = []
    if rek == "n_point_crossover":
        rek_args = [kwargs.get("rek_cuts", 1), kwargs.get("rek_children", 1), kwargs.get("rek_mapping", 1)]
    elif rek == "uniform-crossover":
        rek_args = [kwargs.get("rek_probability", 1.0), kwargs.get("rek_children", 1), kwargs.get("rek_mapping", 1)]

    return ea(data_matrix, cluster, generations, size, sample_size, fit, fit_args, sel,
              sel_args, mut, mut_args, rek, rek_args)


def ea(data_matrix, cluster, generations, size, sample_size, fit, fit_args, sel,
       sel_args, mut, mut_args, rek, rek_args):
    """ function to initialise the evolutionary algorithm

    :param data_matrix: the data to be clustered
    :param cluster: the amount of searched clusters
    :param generations: the number of generations
    :param size: the size of the population
    :param sample_size: the size of a random subsample of the data
    :param fit: the name of the fitness function
    :param fit_args: the arguments in a list of the fitness function
    :param sel: the name of the selection function
    :param sel_args: the arguments in a list of the selection function
    :param mut: the name of the mutation function
    :param mut_args: the arguments in a list of the mutation function
    :param rek: the name of the recombining function
    :param rek_args: the arguments in a list of the recombining function
    :return: a list result where result[i] is the cluster of data[i]
    """

    # create the form of the data we worked with
    data = np.zeros((data_matrix.shape[0], len(data_matrix[0])-1))
    for i in range(data.shape[0]):
        for k in range(data.shape[1]):
            data[i][k] = data_matrix[i][k]

    # set time when the algorithm has to be finished
    end_seconds = time.time() + 180

    # copy the data given for the reduction
    given_data = data

    # fit sample_size if smaller values are given
    if sample_size <= data.shape[0]:
        data = data[np.random.randint(0, len(data), size=sample_size)]

    tree = kd(data)

    # create initialization
    def init_wrapper(dat):
        return alg.init_random(dat, cluster=cluster)
    alg_init = init_wrapper

    # =====================================create fitness======================================
    alg_fit = None
    if fit == 'centroid-distance':
        def centroid_wrapper(da, population):
            return ft.fit_centroid_distance(da, population, cluster=cluster)

        alg_fit = centroid_wrapper
    elif fit == 'convex_hull_volume':
        def convex_wrapper(da, p):
            return ft.fit_convex_hull_volume(da, p, cluster=cluster)

        alg_fit = convex_wrapper
    elif fit == 'min-clique':
        # evaluate distance matrix
        minkowski_dist = fit_args[0]
        graph = create_distance_graph(data, minkowski_dist)

        def min_clique_wrapper(da, p):
            return ft.fit_min_clique(da, p, cluster=cluster, graph=graph)
        alg_fit = min_clique_wrapper

    elif fit == 'density_sum':
        kind = fit_args[0]
        density = fit_args[1]

        graph = create_distance_graph(data, kind)

        def density_wrapper(da, p):
            return ft.fit_density_sum(da, p, cluster=cluster, graph=graph, max_distance=density)
        alg_fit = density_wrapper

    elif fit == 'knn_sum':
        k = fit_args[0]
        # evaluate distance matrix
        graph = create_knn_graph(data, tree, k)

        def knn_wrapper(da, p):
            return ft.fit_knn_sum(da, p, graph, k, cluster=cluster)
        alg_fit = knn_wrapper

    elif fit == 'davies_bouldin':

        def db_wrapper(da, p):
            return ft.fit_davies_bouldin_index(da, p, cluster=cluster)
        alg_fit = db_wrapper
    # ====================================create selection======================================
    alg_sel = None
    if sel == 'tournament_selection':
        # determine arg
        tournament_size = sel_args[0]

        def tournament_wrapper(p, f, s):
            return sl.sel_tournament_selection(p, f, s, tournament_size=tournament_size)

        alg_sel = tournament_wrapper
    elif sel == 'random_selection':
        alg_sel = sl.sel_random_selection

    # =====================================create mutation=======================================

    alg_mut = None
    if mut == 'change_one_assignment_probability':
        mutation_probability = mut_args[0]

        def change_wrapper(da, p):
            return mt.mut_change_one_assignment(da, p, cluster=cluster, probability=mutation_probability)

        alg_mut = change_wrapper

    elif mut == 'change_one_assignment_fixed':
        mutation_amount = mut_args[0]

        def mutate_wrapper(da, p):
            return mt.mut_change_one_assignment_fixed_children(da, p, cluster=cluster, children=mutation_amount)

        alg_mut = mutate_wrapper

    # ======================================create recombination=================================
    alg_rek = None
    if rek == 'n_point_crossover':
        n = rek_args[0]
        children = rek_args[1]
        use_map = rek_args[2]

        def n_point_wrapper(da, p):
            return rk.rek_n_point_crossover_mapping(da, p, amount_of_children=children, cluster=cluster, n=n,
                                                    mapping=use_map)

        alg_rek = n_point_wrapper

    elif rek == 'uniform-crossover':
        probability = rek_args[0]
        children = rek_args[1]
        use_map = rek_args[2]

        def uniform_wrapper(da, p):
            return rk.rek_uniform_crossover(da, p, amount_of_children=children, cluster=cluster, probability=probability,
                                            mapping=use_map)
        alg_rek = uniform_wrapper

    # start algorithm
    result = alg.evol_alg(data, generations, size, alg_fit, alg_sel, alg_mut, alg_rek, alg_init, end_time=end_seconds)

    # label the rest of the data with knn with k if sample_size is smaller
    if sample_size <= data.shape[0]:
        clustered_data = []
        k = 5
        # label every data point in the given set
        for i in range(len(given_data)):
            d, indices = tree.query(given_data[i], k)
            # point was clustered
            if d[0] == 0:
                clustered_data.append(result[indices[0]])
            # point needs to be specified
            else:
                cluster_count = np.zeros(cluster)
                for l in range(k):
                    cluster_count[result[indices[l]]] += 1
                clustered_data.append(np.argmax(cluster_count))
        result = np.array(clustered_data)
    return result


def create_distance_graph(data, dist):
    """ function to create a distance matrix

    :param data: the data points for the graph
    :param dist: the minkowski-distance that should be used
    :return: a distance matrix
    """
    graph = np.zeros((len(data), len(data)))
    for i in range(len(data)):
        for k in range(i + 1, len(data)):
            graph[i, k] = md(data[i], data[k], dist)
            graph[k, i] = graph[i, k]
    return graph


def create_knn_graph(data, tree, k):
    """ function to create a knn-graph

    :param data: the data points for the graph
    :param tree: a KDTree containing the data points
    :param k: the amount of neighbours that should be looked at
    :return: a knn-graph
    """
    graph = np.zeros((len(data), len(data)))
    for i in range(len(data)):
        d, indices = tree.query(data[i], k)
        for l in range(len(indices)):
            if not d[l] == 0:
                graph[i][indices[l]] = k - l
    return graph
