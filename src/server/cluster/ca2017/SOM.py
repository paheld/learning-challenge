import sklearn.utils as utils
import sklearn.base as base
from sklearn.utils.validation import check_random_state
import logging
import numpy as np
from scipy.spatial.distance import cityblock, pdist, squareform, cdist
import matplotlib.pyplot as plt
from scipy.special import binom
from sklearn.cluster import KMeans, AgglomerativeClustering

# Set up a specific logger with our desired output level
my_logger = logging.getLogger('SOM')
my_logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
my_logger.addHandler(handler)



######## Init Functions
def random_weight_init(X, n, random_state): return utils.resample(X, n_samples=n, random_state=random_state)


# Learning Functions


# Neighborhood Functions
def exponential_decay(sigma, lambda_, iteration): return sigma * np.exp(-iteration/lambda_)


# sampling generators
def linear_sampler(X, n, **kwargs):

    for i, x in enumerate(X):
        if i >= n:
            return
        yield x

def random_sampler(X, n, random_state):
    random_samples = utils.resample(X, n_samples=n, random_state=random_state)

    for rand_sample in random_samples:
        yield rand_sample

sampling_functions = {
    'random': random_sampler,
    'linear': linear_sampler
}


class SOM(base.BaseEstimator, base.ClusterMixin):

    def __init__(self, x=4, y=4, max_samples=100, max_iterations=3, lrate=0.05, sampling_method_name="random", distance_method="euclidean",
                 random_state=0, second_level_cluster_name='kmean', n_clusters=4, single_step=0, **kwargs):
        my_logger.info('Creating SOM')
        # algorithm parameters
        self.x = x
        self.y = y
        self.max_samples = max_samples
        self.max_iterations = max_iterations
        self.lrate = lrate
        self.sampling_method_name = sampling_method_name
        self.sampling_method = sampling_functions[self.sampling_method_name]
        self.distance_method = distance_method
        self.random_state_init = random_state
        self.second_level_cluster_name = second_level_cluster_name
        self.n_clusters = n_clusters
        self.single_step = single_step

        self.kwargs = kwargs
        self.neighbourhood_function = exponential_decay
        self.init_function = random_weight_init

        self.labels_ = None
        self.random_state = random_state


    def get_coord_dist_indx(self, i, j, n): return int(binom(n, 2) - binom(n - i, 2) + (j - i - 1))

    def draw_weights(self, ax):

        for i, neuron_weight in enumerate(self.weights):
            neighbor_idxs = np.where(self.neighbor_dist_mtr[i] <= 1)[0]
            for idx in neighbor_idxs:
                linepoints = np.vstack((self.weights[i], self.weights[idx]))
                ax.plot(linepoints[:, 0], linepoints[:, 1], c="black")

            ax.scatter(neuron_weight[0], neuron_weight[1], s=30, c="black")

        return

    def draw_neurons(self, ax):

        for i, neuron_coord in enumerate(self.coords):
            neighbor_idxs = np.where(self.neighbor_dist_mtr[i] <= 1)[0]
            for idx in neighbor_idxs:
                linepoints = np.vstack((self.coords[i], self.coords[idx]))
                ax.plot(linepoints[:, 0], linepoints[:, 1], c="black")

            ax.scatter(neuron_coord[0], neuron_coord[1], s=30, c="black")

        return

    def draw_updates(self, ax, bmu_idx, bmu_range):

        # plot grid
        circle1 = plt.Circle(self.coords[bmu_idx], bmu_range, color='r', fill=False)
        ax.add_artist(circle1)

        return

    def draw_memory(self, ax, low=0, up=-1):

        for node_idx, memories in enumerate(self.memories):
            memories = memories[low:up]
            if not len(memories) > 2:
                return
            old_position = memories[0]
            for memory in memories:
                    linepoints = np.vstack((old_position, memory))
                    ax.plot(linepoints[:, 0], linepoints[:, 1], c="black")
                    old_position = memory

                #linepoints = np.vstack((neuron['weight'], last_memory[-1]))
                #ax1.plot(linepoints[:, 0], linepoints[:, 1], c=colors[i])
        return

    def draw_training_set(self, ax):
        ax.scatter(self.X_[:, 0], self.X_[:, 1], s=10, c='b')
        return

    def fit(self, X, y=None):

        xx, yy = np.meshgrid(np.array(range(self.x)), np.array(range(self.y)))
        self.coords = np.array([[x, y] for x, y in zip(xx.reshape(-1), yy.reshape(-1))])
        self.weights = None
        self.memories = [[] for x, y in zip(xx.reshape(-1), yy.reshape(-1))]

        self.coord_distances = pdist(self.coords, 'euclidean')  # always use euclidean distances here
        self.n = self.coords.shape[0]

        self.sigma_0 = np.max(self.coords) / 2.0
        my_logger.debug('Sigma: %s' % self.sigma_0)

        self.lambda_ = self.max_iterations / np.log(self.sigma_0)
        my_logger.debug('Lambda: %s' % self.lambda_)

        # precalcuate distances to each node and cache them
        neighbor_distances = []
        for i, coord in enumerate(self.coords):
            neighbor_distance = cdist(self.coords, np.array([coord]), 'euclidean').reshape(-1)
            neighbor_distances.append(neighbor_distance)
        self.neighbor_dist_mtr = np.vstack(tuple(neighbor_distances))

        # set second level cluster
        if self.second_level_cluster_name == 'kmean':
            self.second_cluster_level = KMeans(n_clusters=self.n_clusters, random_state=self.random_state,
                                               **self.kwargs)
        elif self.second_level_cluster_name == 'hac':
            self.second_cluster_level = AgglomerativeClustering(n_clusters=self.n_clusters, **self.kwargs)
        else:
            raise ValueError('Unknown second level cluster name:  %s' % self.second_level_cluster_name)

        # Check that X and y have correct shape
        X = utils.check_array(X)

        self.X_ = X
        self.y_ = y

        self.random_state_generator = check_random_state(self.random_state)

        self.weights = self.init_function(X, self.n, self.random_state_generator)
        self.memories = [[neuron_idx] for neuron_idx in self.weights]

        my_logger.debug('Input-values: %s' % X)

        if self.single_step:
            return self.fit_generator(X)



        # traverse input vectors
        for iteration in range(self.max_iterations):

            sample_generator = self.sampling_method(X, self.max_samples, self.random_state_generator)
            sigma_t = self.neighbourhood_function(self.sigma_0, self.lambda_, iteration)
            squared = (2 * np.square(sigma_t))
            L = self.lrate * np.exp(-iteration / self.lambda_)

            my_logger.info('Iteration: %s\tSigma_0: %s\tlambda: %s\tSigma_t: %s\tL-rate: %s' % (
            iteration, self.sigma_0, self.lambda_, sigma_t, L))

            for train_vec_idx, training_vector in enumerate(sample_generator):
                my_logger.debug('Training-Vector: %s' % training_vector)

                # Calculate BMU
                bmu_idx = np.argmin(cdist(self.weights, np.array([training_vector]), 'euclidean'))

                # get neighbors of BMU
                neighbor_indexes = np.where(self.neighbor_dist_mtr[bmu_idx] <= sigma_t)[0]

                # update neighbors
                weights_vector = np.array([self.weights[i] for i in neighbor_indexes])
                theta_mtx = np.diag(
                    [np.exp(np.square(self.neighbor_dist_mtr[bmu_idx][i]) / squared) for i in neighbor_indexes])

                new_weights = np.matmul(theta_mtx, training_vector - weights_vector) * L + weights_vector

                for j, i in enumerate(neighbor_indexes):
                    self.memories[i].append(self.weights[i].copy())
                    self.weights[i] = new_weights[j]


        self.second_cluster_level.fit(self.weights)

        weight_labels = self.second_cluster_level.labels_

        # create labels for each point in X
        X_labels = []
        for x in X:
            dist_2 = np.sum((self.weights - x) ** 2, axis=1)
            min_i = np.argmin(dist_2)
            label = weight_labels[min_i]
            X_labels.append(label)

        self.labels_ = np.array(X_labels)

        if self.second_level_cluster_name == 'kmean':
            self.cluster_centers_ = self.second_cluster_level.cluster_centers_

        return self

    def fit_generator(self, X, y=None):

        # traverse input vectors
        for iteration in range(self.max_iterations):

            sample_generator = self.sampling_method(X, self.max_samples, self.random_state_generator)
            sigma_t = self.neighbourhood_function(self.sigma_0, self.lambda_, iteration)
            squared = (2 * np.square(sigma_t))
            L = self.lrate * np.exp(-iteration / self.lambda_)

            my_logger.info('Iteration: %s\tSigma_0: %s\tlambda: %s\tSigma_t: %s\tL-rate: %s' % (iteration, self.sigma_0, self.lambda_, sigma_t, L))

            for train_vec_idx, training_vector in enumerate(sample_generator):
                my_logger.debug('Training-Vector: %s' % training_vector)

                # Calculate BMU
                bmu_idx = np.argmin(cdist(self.weights, np.array([training_vector]), 'euclidean'))

                # get neighbors of BMU
                neighbor_indexes = np.where(self.neighbor_dist_mtr[bmu_idx] <= sigma_t)[0]

                # update neighbors
                weights_vector = np.array([self.weights[i] for i in neighbor_indexes])
                theta_mtx = np.diag([np.exp(np.square( self.neighbor_dist_mtr[bmu_idx][i]) / squared) for i in neighbor_indexes])

                new_weights = np.matmul(theta_mtx, training_vector-weights_vector) * L + weights_vector

                for j, i in enumerate(neighbor_indexes):
                    self.memories[i].append(self.weights[i].copy())
                    self.weights[i] = new_weights[j]
                # if self.single_step == 2:
                #     yield training_vector

            # if self.single_step == 1:
            #     yield iteration

        self.second_cluster_level.fit(self.weights)

        weight_labels = self.second_cluster_level.labels_
        # create labels for each point in X
        for x in X:
            dist_2 = np.sum((self.weights - x) ** 2, axis=1)
            min_i = np.argmin(dist_2)

        #self.labels_ =

        #self.cluster_centers_ = self.weights
        if self.second_level_cluster_name == 'kmean':
            self.cluster_centers_ = self.second_cluster_level.cluster_centers_

        return self

    def update_neighbor(self, bmu_idx, neighbor_index, training_vector, squared, L):
        neighbor_distance = self.neighbor_dist_mtr[bmu_idx][neighbor_index]
        my_logger.debug('Neighbor: %s with distance %s' % (neighbor_index, neighbor_distance))

        # update the neighbor
        theta = np.exp(np.square(neighbor_distance) / squared)
        adjusted_weight_vector = self.weights[neighbor_index] + theta * L * (training_vector - self.weights[neighbor_index])

        my_logger.debug('Adjusted Weight-Vector: %s' % adjusted_weight_vector)
        self.memories[neighbor_index].append(self.weights[neighbor_index])
        self.weights[neighbor_index] = adjusted_weight_vector
        return


def cluster(data, *args, **kwargs):
    try:
        # yeah its cool B-)
        kwargs["linkage"] = kwargs["second_level_cluster_name_linkage"]
        del kwargs["second_level_cluster_name_linkage"]
    except KeyError:
        pass
    som = SOM(*args, **kwargs)
    return som.fit_predict(data)