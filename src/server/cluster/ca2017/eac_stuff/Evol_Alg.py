import numpy as np
import time as time
import numpy.random as rng


def evol_alg(data, gen, pop, fit, sel, mut, rek, init, end_time):
    """ This function runs an Evolutionary Algorithm

    :param data: The data, that needs to be clustered
    :param gen: The number of generations, that should be run.
    :param pop: The size of the population of the Algorithm
    :param fit: The function to evaluate the fitness of the population
    :param sel: The function that selects pop- individuals in every iteration
    :param mut: The function that mutates an Individual
    :param rek: The function that recombines multiple individuals
    :param init: A function that initialises individuals
    :return:
    """
    # start creating a population
    population = []
    for i in range(pop):
        population.append(init(data))
    # start evolution
    for g in range(gen):
        # mutate
        population += mut(data, population)
        # recombining
        population += rek(data, population)
        # select
        fitness = fit(data, population)
        population = sel(population, fitness, pop)

        # break after 3 minutes
        if time.time() > end_time:
            break

    # select best algorithms
    fitness = fit(data, population)

    # look for max
    best = np.argmax(fitness)

    # get return value
    return population[best]


def init_random(data, cluster=7):
    """ initialising a random individual

    :param data: the data ,that should be clustered
    :param cluster: the amount of searched clusters
    :return: one created individual
    """
    return np.random.randint(0, high=cluster, size=data.shape[0])
