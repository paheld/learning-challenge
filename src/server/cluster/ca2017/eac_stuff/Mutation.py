import numpy as np


def mut_change_one_assignment(data, pop, cluster=7, probability=0.8):
    """ a function for a mutation

    :param data: the data that should be clustered
    :param pop: the current population
    :param cluster: the amount of clusters
    :param probability: the probability an individual gets mutated
    :return: mutated individuals
    """
    result = []
    for i in range(len(pop)):
        # create mutation if the probability is reached
        if np.random.rand() < probability:
            # copy and create random mutant
            mutant = np.copy(pop[i])
            mutant[np.random.randint(0, len(mutant))] = np.random.randint(0, cluster)
            result.append(mutant)
    return result


def mut_change_one_assignment_fixed_children(data, pop, cluster=4, children=30):
    """ a function for a mutation, where a fixed amount of children is produced

    :param data: the data that should be clustered
    :param pop: the current population
    :param cluster: the amount of clusters
    :param children: the amount of mutated children that should be created
    :return: mutated individuals
    """
    result = []
    for i in range(children):
        # copy and change one assignment
        mutant = np.copy(pop[np.random.randint(0, len(pop))])
        mutant[np.random.randint(0, len(mutant))] = np.random.randint(0,cluster)
        result.append(mutant)
    return result