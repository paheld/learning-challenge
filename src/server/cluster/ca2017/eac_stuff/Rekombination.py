import random as srng
import numpy as np
import numpy.random as rng


def rek_n_point_crossover_mapping(data, pop, amount_of_children=30, cluster=7, n=5, mapping=1):
    """ create children from parents of the population

    :param data: data, that should be clustered
    :param pop: current population in the algorithm
    :param amount_of_children: children that should be produced
    :param cluster: the amount of cluster that is searched
    :param n: the amount of cuts
    :param mapping: boolean if mapping for possibly faster convergence is activated
    :return: a list of new children
    """

    children = []
    for i in range(amount_of_children):
        # size of an individual
        s_i = data.shape[0]
        # create cuts (sorted list of cutting variables)
        cuts = srng.sample(xrange(1, s_i), n)
        cuts.append(0)
        cuts.append(s_i)
        cuts.sort()

        # create child
        child = np.copy(pop[rng.randint(0, len(pop))])
        other = pop[rng.randint(0, len(pop))]

        # get mapping and use on child
        if mapping == 1:
            m = util_generate_mapping_between_individuals_i1_to_i2(child, other, cluster)
            for k in range(len(child)):
                child[k] = m[child[k]]
        # create child from cuts
        for k in range(0, len(cuts)-1, 2):
            child[cuts[k]:cuts[k+1]] = other[cuts[k]: cuts[k+1]]

        children.append(child)
    return children


def rek_uniform_crossover(data, pop, amount_of_children=30, cluster=7, probability=0.5, mapping=1):
    """ create children from parents of the population

    :param data: data, that should be clustered
    :param pop: current population in the algorithm
    :param amount_of_children: children that should be produced
    :param cluster: the amount of cluster that is searched
    :param probability: probability of a swap
    :param mapping: boolean if mapping for possibly faster convergence is activated
    :return: a list of new children
    """
    children = []
    for i in range(amount_of_children):
        child = np.copy(pop[rng.randint(0, len(pop))])
        other = pop[rng.randint(0, len(pop))]

        if mapping == 1:
            m = util_generate_mapping_between_individuals_i1_to_i2(child, other, cluster)
            for k in range(len(child)):
                child[k] = m[child[k]]

        for k in range(len(child)):
            if rng.rand() <= probability:
                child[k] = other[k]
        children.append(child)
    return children


def util_generate_mapping_between_individuals_i1_to_i2(ind1, ind2, cluster):
    """ generating a mapping

    This function should create a mapping between two individuals by counting
    appearances of the cluster numbers that co-occur and assign the most appearing
    one.

    :param ind1: individual from
    :param ind2: individual to
    :param cluster:  amount of clusters
    :return: a dictionary as a mapping
    """
    mapping = {}
    # create counter
    count = np.zeros((cluster, cluster))

    # count appearance
    for i in range(len(ind1)):
        count[ind1[i], ind2[i]] += 1

    # create map
    for i in range(cluster):
        mapping[i] = np.argmax(count[i, :])

    return mapping

