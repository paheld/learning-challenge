import numpy as np
import numpy.random as rng


def sel_tournament_selection(population, fit, pop_size, tournament_size=5):
    """ implementation of tournament selection

    The tournament selection takes tournament_size individuals and adds the
    one with the best fitness to the next generations of the population

    :param population: population after the current generation
    :param fit: a list of the fitness, where fit[i] is the fitness of population[i]
    :param pop_size: the amount of individuals that should be selected
    :param tournament_size: the amount of individuals that will be compared
    :return: a new generation of individuals
    """
    res = []
    for i in range(pop_size):
        # create the indices of a tournament
        tournament = []
        for k in range(tournament_size):
            tournament.append(rng.randint(0, len(population)))
        # append the the individual with the largest fitness
        res.append(population[tournament[np.argmax(fit[tournament])]])
    return res


def sel_random_selection(population, fit, pop_size):
    """ random selection

    this function randomly selects pop_size individuals

    :param population: population after the current generation
    :param fit: a list of the fitness, where fit[i] is the fitness of population[i]
    :param pop_size: the amount of individuals that should be selected
    :return: a new generation of individuals
    """
    res = []
    for i in range(pop_size):
        res.append(population[rng.randint(0, len(population))])
    return res
