# coding=utf-8
import numpy as np
from scipy import spatial as spatial


def fit_centroid_distance(data, population, cluster=7):
    """ Determines a K-Means-like Fitness.

    Therefore the centroid for each cluster is determined by
    adding all data points and dividing it by the size of the
    cluster. After that the the distances to the centroid are
    subtracted from 0.
    The maximization of this fitness leads to minimizing the
    distance to the centroid.

    :param data: The data, that needs to be clustered
    :param population: The current population, in the Evolutionary Algorithm
    :param cluster: The amount of clusters in the Evolutionary Algorithm
    :return: A list of the fitness for the population, where res[i] is the fitness
             for the individual population[i]
    """
    res = []
    for i in range(len(population)):
        individual = population[i]
        # create separated list for indices
        clust = get_cluster_indices(individual, cluster)
        # evaluate centroids
        centroids = []
        for c in range(cluster):
            tmp = data[clust[c], :].sum(axis=0)
            tmp /= len(clust[c])
            centroids.append(tmp)
        # measure distance to centroids
        fitness = 0
        for k in range(len(individual)):
            cl = individual[k]
            fitness -= np.linalg.norm(data[k] - centroids[cl])
        res.append(fitness)
    return np.array(res)


def fit_convex_hull_volume(data, population, cluster=7):
    """ Determines  a fitness with regards to the spatial expansion of the cluster

    Therefore the volume of each cluster is subtracted from 0 if it exists.
    This should lead to clusters, that have their data in a small volume

    :param data: The data, that needs to be clustered
    :param population: The current population, in the Evolutionary Algorithm
    :param cluster: The amount of clusters in the Evolutionary Algorithm
    :return: A list of the fitness for the population, where res[i] is the fitness
             for the individual population[i]
    """
    res = []
    for i in range(len(population)):
        individual = population[i]

        clust = get_cluster_indices(individual, cluster)

        # volume of convex hulls
        fit = 0
        for k in range(cluster):
            if len(clust[k][0]) > data.shape[1] + 1:
                try:
                    fit -= spatial.ConvexHull(data[clust[k], :][0]).volume
                except:
                    print("There were collinear points on convex hull")

        res.append(fit)
    return np.array(res)


def fit_min_clique(data, population, cluster=7, graph=None):
    """ Determines a fitness, which should create maximal separated clusters

     Therefore subtract all pairwise distances of points within a cluster.
     This should lead to a clustering, where the distances within the cluster
     get really small and relatively well separated from the other clusters.

    :param data: The data, that needs to be clustered
    :param population: The current population, in the Evolutionary Algorithm
    :param cluster: The amount of clusters in the Evolutionary Algorithm
    :param graph: A len(population)x len(population) Matrix containing the pairwise distances of all data points
    :return: A list of the fitness for the population, where res[i] is the fitness
             for the individual population[i]
    """
    res = []

    for i in range(len(population)):
        individual = population[i]

        clust = get_cluster_indices(individual, cluster)

        # volume of convex hulls
        fit = 0

        # calculate all pairwise distances
        for cl in range(len(clust)):
            for k in range(len(clust[cl][0])):
                for l in range(k + 1, len(clust[cl][0])):
                    fit -= graph[clust[cl][0][k], clust[cl][0][l]]

        res.append(fit)
    return np.array(res)


def fit_density_sum(data, population, cluster=3, max_distance=0.3, graph=None):
    """ Determines a sum of near points

    Therefore we take a look at the distances between the points in the cluster.
    if it is within the max_distance , max_distance - dist is added to the
    fitness. Additionally we punish distances bigger than that by the distance
    to get compact and connected clusters.

    :param data: The data, that needs to be clustered
    :param population: The current population, in the Evolutionary Algorithm
    :param cluster: The amount of clusters in the Evolutionary Algorithm
    :param max_distance: the distance until a neighbor is important
    :param graph: A len(population)x len(population) Matrix containing the pairwise distances of all data points
    :return: A list of the fitness for the population, where res[i] is the fitness
             for the individual population[i]
    """
    res = []

    for i in range(len(population)):
        individual = population[i]
        # get cluster indices
        clust = get_cluster_indices(individual, cluster)

        fit = 0

        for cl in range(len(clust)):
            # go through all pairs in the cluster
            for k in range(len(clust[cl][0])):
                for l in range(k + 1, len(clust[cl][0])):
                    # if below max distance add distance else subtract
                    tmp = graph[clust[cl][0][k], clust[cl][0][l]]
                    if tmp <= max_distance:
                        fit += max_distance - tmp
                    else:
                        fit -= tmp
        res.append(fit)
    return np.array(res)


def fit_knn_sum(data, population, graph, k, cluster=3):
    """ a function that runs a sum on the k Nearest neighbors

    The idea is that it is better if we have our nearest neighbor
    in the same cluster. So we sum every k that is in our cluster.
    We have a punish term for every outgoing edge of (k*(k-1))/2.
    If we would leave it out there exist lokal maxima, where cluster
    are in distinct areas, so it does create connected clusters.

    :param data: The data, that needs to be clustered
    :param population: The current population, in the Evolutionary Algorithm
    :param graph: the knn graph matrix
    :param k: the k from the knn graph
    :param cluster: The amount of clusters in the Evolutionary Algorithm
    :return: A list of the fitness for the population, where res[i] is the fitness
             for the individual population[i]
    """
    res = []
    for i in range(len(population)):
        individual = population[i]

        clust = get_cluster_indices(individual, cluster)

        fit = 0

        for cl in range(len(clust)):
            for g in range(len(clust[cl][0])):
                sum_k = 0
                # sum every knn we have for each pair in our cluster
                for l in range(len(clust[cl][0])):
                    if l != g:
                        sum_k += graph[clust[cl][0][g], clust[cl][0][l]]
                sum_k -= ((k - 1) * k) / 2
                fit += sum_k

        res.append(fit)
    return np.array(res)


def fit_davies_bouldin_index(data, population, cluster=7):
    """ this function is a typical function for cluster quality
    as on this wikipedia page : https://en.wikipedia.org/wiki/Cluster_analysis

    So we evaluate:

    1/n * sum_{i=1}^{n} max_{j != i}((o_i + o_j)/d(c_i,c_j))

    Therefore let
    n be the number of clusters
    c_x the centroid of the cluster x
    o_x the average distance for all points in the cluster to c_x
    d(c_i, c_j) the distance between the centroids

    this measure is better the smaller it gets, so we multiply it with -1 to
    make it a maximization problem

    :param data: The data, that needs to be clustered
    :param population: The current population, in the Evolutionary Algorithm
    :param cluster: The amount of clusters in the Evolutionary Algorithm
    :return: A list of the fitness for the population, where res[i] is the fitness
             for the individual population[i]
    """
    res = []
    for i in range(len(population)):
        individual = population[i]

        # determine centroids
        c_size = np.zeros(cluster)
        centroids = np.zeros((cluster, data.shape[1]))
        for k in range(len(individual)):
            c_size[individual[k]] += 1
            centroids[individual[k]] += data[k]
        for k in range(cluster):
            if not c_size[k] == 0:
                centroids[k] /= c_size[k]

        # determine average distances to centroids
        average_distances = np.zeros(cluster)
        for k in range(len(individual)):
            average_distances[individual[k]] += np.linalg.norm(centroids[individual[k]] - data[k])
        for k in range(len(average_distances)):
            if not c_size[k] == 0:
                average_distances /= c_size[k]

        # evaluate the given sum
        fitness = 0
        for k in range(cluster):
            # go for all not empty clusters
            if not c_size[k] == 0:
                d_max = 0
                # search for the max
                for l in range(cluster):
                    if not l == k and not c_size[k] == 0:
                        tmp = (average_distances[k] + average_distances[l]) / np.linalg.norm(centroids[l] + centroids[k])
                        if tmp > d_max:
                            d_max = tmp
                fitness += d_max
        fitness /= cluster

        # make it a maximization problem
        fitness *= -1
        res.append(fitness)
    return np.array(res)


def get_cluster_indices(individual, cluster=7):
    """ get cluster indices

    Create a list of lists, where list[i] contains all
    indices, where the cluster is i

    :param individual: individual ,that is looked for
    :param cluster: amount of clusters
    :return: list<list<int>> where the indices are of the clusters are saved
    """
    cl = []
    for c in range(cluster):
        cl.append(np.where(individual == c))
    return cl
