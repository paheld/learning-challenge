from __future__ import print_function, division, unicode_literals, absolute_import, generators
from sklearn import mixture
import matplotlib.pyplot as pl
from matplotlib import cm
import numpy as np
from tempfile import mkstemp

__author__ = 'cbraune'


def cluster(data, k=3, covariance='diag', initialisation=None, **_):

    if initialisation is not None:
        initialisation = ''.join(initialisation)
    else:
        initialisation = ''

    if covariance not in ['full', 'spherical', 'diag', 'tied']:
        return 'Covariance matrix type must be either "full", "diag" or "spherical"!'

    g = mixture.GMM(n_components=k, covariance_type=covariance, n_init=1, n_iter=1000, init_params=initialisation)
    g.fit(data)

    prediction = g.predict(data)

    dims = range(data.shape[1])
    file_names = []

    print(dims)

    while len(dims) >= 2:
        a, b = dims.pop(), dims.pop()

        _, file_name = mkstemp(suffix=".png", prefix='tmp_cla_gmm_')
        fig = pl.figure()
        ax = fig.add_subplot(111)
        x = np.linspace(np.min(data[:, a]), np.max(data[:, a]))
        y = np.linspace(np.min(data[:, b]), np.max(data[:, b]))
        xx, yy = np.meshgrid(x, y)
        xxx = np.c_[xx.ravel(), yy.ravel()]

        # if data has more than two dimension one _could_ do two dimensional plots.
        # however, the data matrix would have to be padded to fit the original data
        # format. Since we take dimension from in the middle, we cannot just pad to
        # the right, but need to know how much left and right padding is needed.
        # if data.shape[1] > 2:
        #     padding = np.zeros(data.shape[0], data.shape[1]-2)

        dummy_data = np.zeros((xxx.shape[0], data.shape[1]))
        dummy_data[:, a] = xxx[:, 0]
        dummy_data[:, b] = xxx[:, 1]

        print(data.shape)
        print(xxx.shape)
        print(dummy_data.shape)

        z = np.log(-g.score_samples(dummy_data)[0])
        z = z.reshape(xx.shape)

        cs = ax.contour(xx, yy, z, 10, cmap=cm.coolwarm)
        ax.clabel(cs, inline=True, fontsize=8)
        ax.scatter(data[:, a], data[:, b], s=10, marker='.', alpha=0.67)
        ax.axis('tight')
        pl.xlabel("Dim"+str(a))
        pl.ylabel("Dim"+str(b))
        pl.savefig(file_name)
        pl.close()
        file_names.append(file_name)
    return prediction, file_names