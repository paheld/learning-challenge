from __future__ import print_function, division, unicode_literals, absolute_import, generators

from cluster.COMeansClustering import *
from json import loads
import numpy as np


def cluster(data, mustlink="[]", cannotlink="[]",k=3, maxiter=100, **_):

    if not mustlink:
        mustlink = "[]"

    if not cannotlink:
        cannotlink = "[]"

    X = [list(x) for x in data]
    try:
        mustlink = loads(mustlink)
    except:
        return "Syntax Error in JSON code for 'must links'."

    try:
        cannotlink = loads(cannotlink)
    except:
        return "Syntax Error in JSON code for 'cannot links'."

    print(mustlink)
    print(cannotlink)
    try:
        init_points = np.array(mustlink)
    except:
        return 'Cluster definition incoherent with dataset. Probably number of dimension for at least one point wrong.'

    prediction = find_centers(mustlink, cannotlink, X, k, maxiter)

    return prediction
