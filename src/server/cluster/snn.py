from numpy import *
import math

def snn_clustering(data, k, Eps, MinPts):
    k += 1
    n = data.shape[0]
    d = data.shape[1]
    similarity_matrix = zeros( (n, n) )
    distanz_matrix = zeros( (n, n) )
    k_nearest = zeros( (n, k) ) - 1
    #1.
    for i in range(n):
        for j in range(n):
            z = 0
            for l in range(d):
                z += (data[i, l] - data[j, l])**2    
            distanz_matrix[i, j] = math.sqrt(z)
            similarity_matrix[i, j] = 1/(1 + distanz_matrix[i, j])

    #2.
    for i in range(n):
        for j in range(n):
            counter = 0
            tmp = j
            tmp2 = 0
            while(counter < k and tmp >= 0):
                if(k_nearest[i, counter] < 0):
                    k_nearest[i, counter] = tmp
                    tmp = -1
                elif(similarity_matrix[i,k_nearest[i, counter]] < similarity_matrix[i, tmp]):
                    tmp2 = k_nearest[i, counter]
                    k_nearest[i, counter] = tmp
                    tmp = tmp2
                else:
                    counter += 1
            if(tmp >= 0):
                similarity_matrix[i, tmp] = 0

    #return similarity_matrix

    #3.
    snn_graph = zeros( (n, k, 2) )
    snn = 0
    counter = 0
    for i in range(n):
        for j in k_nearest[i]:
            for x in range(k):
                for y in range(k):
                    if (k_nearest[i, x] == k_nearest[j, y]):
                        snn += 1
            if(snn > 0):
                snn_graph[i,counter] = array( [j, snn] )
                snn = 0
                counter +=1
        counter = 0
                    
    #return snn_graph

    #4.
    snn_density = zeros( (n) )
    density_counter = 0
    for i in range(n):
        for j in range(k):
            if(snn_graph[i, j, 1] >= Eps):
                density_counter += 1
        snn_density[i] = density_counter
        density_counter = 0
            
    #return snn_density

    #5
    core_points = []
    for i in range(n):
        if(snn_density[i] >= MinPts):
            core_points.append(i)
    if len(core_points) == 0:
        # no point is a core point...
        return -ones((n), int)

    #return core_points

    #6
           
    lable_array = zeros( (n), int )
    lable_counter = 2
    lable_array[core_points[0]] = 1
    for i in range(1, len(core_points)):
        for j in range(i):
            if(distanz_matrix[core_points[i], core_points[j]] <= Eps):
                lable_array[core_points[i]] = lable_array[core_points[j]]
                break
        if(lable_array[core_points[i]] == 0):
            lable_array[core_points[i]] = lable_counter
            lable_counter += 1

    #return lable_array
            
    #7+8
    for i in range(n):
        if(i == core_points[0]):
            continue
        else:
           min_dis_i_j =  distanz_matrix[i, core_points[0]]
           nearest_core_point = core_points[0]
        for j in range(len(core_points)):
            if(i == core_points[j]):
                break
            if(min_dis_i_j > distanz_matrix[i,core_points[j]]):
                min_dis_i_j = distanz_matrix[i,core_points[j]]
                nearest_core_point = j
        if(min_dis_i_j <= Eps):
            lable_array[i] = lable_array[nearest_core_point]

    #return lable_array

    lable_list = []

    for i in lable_array:
        lable_list.append(i)

    return lable_list



def cluster(data, k=3, eps=1, minpts=5, **_):

    prediction = snn_clustering(data, k, eps, minpts)

    return prediction




















    
