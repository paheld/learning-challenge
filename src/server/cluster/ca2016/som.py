from __future__ import print_function, division, unicode_literals, absolute_import, generators

from sklearn.neighbors import DistanceMetric
from random import randint
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import DBSCAN
import random
import time
import math
from sklearn.utils.extmath import cartesian
import matplotlib

#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from tempfile import mkstemp


# function that clusters data based on self organizing maps
# Parameters: 
#     X : numpy.array
#         array of data
#     tMax : int
#         maximum number  of iterations
#     numberOfNeurons : int
#         should not be too high
#
#  lerningrate parameter:
#     eStart : float
#         must be higher then eEnd
#     eEnd : float
#         must be smaller then eStart
def som_clustering_withPlot(X, tMax=1000, numberOfNeurons=100, eStart=0.8, eEnd=0.5, *_, **__):
    # scale the data(0,1) -> neurons are better spread 
    X = MinMaxScaler().fit_transform(X)
    dist = DistanceMetric.get_metric('euclidean')
    dimensions = len(X[0])
    N = [[random.random() for i in range(dimensions)] for x in range(numberOfNeurons)]
    dataLen = len(X) - 1
    usedNeurons = np.array([], dtype=int)

    # goes from 0 to tMax and take a random input from the data 
    for i in range(tMax):
        randomInput = [X[randint(0, dataLen)]]
        # distance from neurons to input
        distances = dist.pairwise(N, randomInput)
        # finds the neuron, which has the smallest distance
        winnerNIndex = np.argmin(distances)
        randomInput = np.array(randomInput)
        # the learning rate gets smaller after every iteration, so the neurons moves fewer 
        learningRate = eStart * pow((eEnd / eStart), ((i + 1) / tMax))
        # learningRate = (tMax-i)/tMax
        # distances from winner neuron to all other neurons
        distances = dist.pairwise(N, [N[winnerNIndex]])
        winnerNeiborsIndex = [winnerNIndex]
        Reucl = []

        #  for j  in range(len(N)) :
        # Reucl.append(pow(pow(np.array(N[j] - np.array(N[winnerNIndex])),2) + pow(np.array(N[j] - np.array(N[winnerNIndex])),4)),.5) 
        # iterates over all neurons 
        for k in range(len(distances) - 1):
            if k == winnerNIndex:
                continue
            value = distances[k]
            # checks, which neuron is near enough
            if value < learningRate * ((1 / numberOfNeurons) * 5):
                winnerNeiborsIndex.append(k)

        # iterates over all neurons, which are near enough the winner neuron and move them in the direction from the random input    
        for k in winnerNeiborsIndex:
            # Entfernungsgewichtungsfunktion:
            h = math.exp(-pow(distances[k], 2) / (2 * pow(learningRate, 2)))
            # labels all neurons, which are near enough the winner neuron 
            usedNeurons = np.append(usedNeurons, k)
            winnerN = np.array(N[winnerNIndex])
            # moves the neuron k dependent on the learning rate and the Entfernungsgewichtungsfunktion
            N[k] = (winnerN + (learningRate * h) * (randomInput - winnerN))[0]

        usedNeurons = np.unique(usedNeurons)
    # saves only the neurons which was moved, this deletes the noise neurons 
    N = [N[i] for i in usedNeurons]
    # make dbscan with neurons
    neuronCluster = DBSCAN(eps=((1 / numberOfNeurons) * 5), min_samples=1).fit_predict(N)
    # data to cluster
    labels = []
    predict = {}
    for c in set(neuronCluster):
        predict.update({c: []})
    # data to cluster
    for x in X:
        input = [x]
        distances = dist.pairwise(N, input)
        cluster = neuronCluster[np.argmin(distances)]
        labels.append(cluster)
        predict[cluster].append(x)

    colors = plt.cm.Spectral(np.linspace(0, 1, len([x for x in predict.values() if len(x) > 0])))
    i = 0
    for val in predict.values():
        if len(val) == 0:
            continue
        plt.plot([x[0] for x in val], [x[1] for x in val], 'o', markerfacecolor=colors[i])
        i = 1 + i
    plt.plot([x[0] for x in N], [x[1] for x in N], 'bs', )
    _, file_name = mkstemp(suffix=".png", prefix='tmp_cla_hac_')
    plt.savefig(file_name)
    plt.close()
    return labels, file_name
