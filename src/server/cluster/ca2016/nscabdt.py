#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import logging

import math
import random
import six.moves.queue as queue
import array
from matplotlib.tri import Triangulation
import matplotlib.pyplot as pl
from tempfile import mkstemp

logger = logging.getLogger(__name__)

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"


def einfaches_clustering(data):
    clustering(data, False)


def tolles_clustering_mit_visualisierung(data):
    clustering(data, True)


def clustering(data, plot, *_, **__):
    fileName0 = ''
    fileName1 = ''
    # Triangulate the Data
    delaunayEdges = getDelaunayTriangulation(data)
    # Repack the edges into a sorted dictionary. This is build the following way:
    # {node0: [nodeConnectedToNode0, edgeLength],... ; ...}
    organizedEdges = organizeEdges(delaunayEdges, data)
    if (plot):
        f1 = plotData(convertOrganizedEdgesToEdges(organizedEdges), data, 'nscabdt_tri.png')
    # Get the global Mean length of the edges
    globalMean = calculateGlobalMean(delaunayEdges, data)
    # Get the global Standard Deviation of the edges length
    globalStaDeviation = calculateGlobalStaDeviation(delaunayEdges, globalMean, data)
    # Remove all edges which are to long, to form clusters
    removeLongEdges(organizedEdges, globalMean, globalStaDeviation)
    if (plot):
        f2 = plotData(convertOrganizedEdgesToEdges(organizedEdges), data, 'nscabdt_clusters.png')
    # Build the clusters from the edges. Each connected graph is a cluster. Single nodes get
    # will be ignored and later get added to the noise cluster
    clusters = getClusters(organizedEdges)
    # convert the clusters into a list of labels
    if (plot):
        return getListOfLabelsFromClusters(clusters, data), [f1, f2]
    return getListOfLabelsFromClusters(clusters, data)


def getDelaunayTriangulation(data):
    # Only use the first two dimensions and triangulate based on those
    tri = Triangulation(data[:, 0], data[:, 1])
    # return the edges from the triangulation
    edges = tri.edges.tolist()
    return sorted(list(map(sorted, edges)))


def organizeEdges(delaunayEdges, data):
    # Sort the inner and then the outer list
    j = 0
    edgeDictionary = {}
    # Add each edge to the dictionary {fromNode: [toNode, length]}.
    for i in range(0, len(delaunayEdges)):
        edge = delaunayEdges[i]
        q = edge[1]
        while (edge[0] > j):
            j += 1
        if (j in edgeDictionary):
            edgeDictionary[j].append([q, distance(j, q, data)])
        else:
            edgeDictionary[j] = [[q, distance(j, q, data)]]
    return edgeDictionary


def distance(p, q, data):
    return math.sqrt((data[p][0] - data[q][0]) ** 2 + (data[p][1] - data[q][1]) ** 2)


def removeLongEdges(organizedEdges, globalMean, globalStaDeviation):
    for node in organizedEdges:
        edges = organizedEdges[node]
        localMean = 0
        localMean = calculateLocalMean(edges)
        f = calculateF(localMean, globalMean, globalStaDeviation)
        # Remove all edges longer than f(p_i)
        for i in reversed(range(0, len(edges))):
            length = edges[i][1]
            if length >= f:
                edges.pop(i)


def calculateLocalMean(edges):
    sumOfLength = 0.0
    for edge in edges:
        sumOfLength += edge[1]
    mean = sumOfLength / len(edges)
    return mean


def calculateGlobalMean(edges, data):
    sumOfLength = 0.0
    for edge in edges:
        sumOfLength += distance(edge[0], edge[1], data)
    mean = sumOfLength / len(edges)
    return mean


def calculateGlobalStaDeviation(edges, globalMean, data):
    sumOfVariations = 0
    for edge in edges:
        sumOfVariations += (globalMean - distance(edge[0], edge[1], data)) ** 2
    return math.sqrt(sumOfVariations / len(edges))


def calculateF(localMean, globalMean, globalStaDeviation):
    return globalMean + (globalStaDeviation * (globalMean / localMean))


def getClusters(edges):
    clusters = []
    visited = []
    # We need a dictionary with keys for both ends of the edge to be able to do our search
    doubleEdges = getTwoWayEdges(edges)
    for node in list(doubleEdges.keys()):
        # We only consider Nodes not visited before and which aren't noise
        if (len(doubleEdges[node]) > 0) and (not node in visited):
            cluster = buildClusterFromSubgraph(node, doubleEdges, visited)
            clusters.append(cluster)
    return clusters


def getTwoWayEdges(edges):
    # Each toNode gets added as a key in the dict with all arriving edges
    doubleEdges = dict(edges)
    for node in reversed(list(edges.keys())):
        for edge in edges[node]:
            node2 = edge[0]
            length = edge[1]
            if (node2 in doubleEdges):
                doubleEdges[node2].append([node, length])
            else:
                doubleEdges[node2] = [[node, length]]
    return doubleEdges


def buildClusterFromSubgraph(node, edges, visited):
    q = queue.Queue()
    q.put(node)
    cluster = []
    # Do a broadth-search on the current subgraph
    while not q.empty():
        currentNode = q.get()
        if not (currentNode in visited):
            visited.append(currentNode)
            cluster.append(currentNode)
            for edge in edges[currentNode]:
                q.put(edge[0])
    return cluster


def getListOfLabelsFromClusters(clusters, data):
    listOfLabels = [0] * len(data)
    for i in range(0, len(clusters)):
        for datum in clusters[i]:
            listOfLabels[datum] = i + 1
    return listOfLabels


def plotData(edges, data, fileName):
    _, fileName = mkstemp(suffix=fileName, prefix='tmp_cla_nscabdt_')

    fig = pl.figure()
    pl.plot(data[:, 0], data[:, 1], 'o')
    for edge in edges:
        pl.plot([data[:, 0][edge[0]], data[:, 0][edge[1]]], [data[:, 1][edge[0]], data[:, 1][edge[1]]], color='b')
    pl.savefig(fileName)
    pl.close(fig)

    return fileName


def convertOrganizedEdgesToEdges(organizedEdges):
    edges = []
    for node in list(organizedEdges):
        for edge in organizedEdges[node]:
            edges.append([node, edge[0]])
    return edges

##############################################################
#                                                            #
# Beispielnutzung                                            #
#                                                            #
##############################################################
# from sklearn import datasets
# rawData = datasets.make_blobs(n_samples=5000, n_features=101, cluster_std=1.0, centers=5, shuffle=True, random_state=1)
# data = rawData[0]
# model = einfaches_clustering(data)
# model = tolles_clustering_mit_visualisierung(data)
##############################################################
#                                                            #
# Einzelschritte von einfaches_clustering fÃŒrs debugging     #
#                                                            #
##############################################################
# delaunayEdges = getDelaunayTriangulation(data)
# organizedEdges = organizeEdges(delaunayEdges, data)
# globalMean = calculateGlobalMean(delaunayEdges, data)
# globalStaDeviation = calculateGlobalStaDeviation(delaunayEdges, globalMean, data)
# removeLongEdges(organizedEdges, globalMean, globalStaDeviation)
# clusters = getClusters(organizedEdges)
# model = getListOfLabelsFromClusters(clusters)
