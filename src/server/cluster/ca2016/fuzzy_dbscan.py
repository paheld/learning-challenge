#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import logging

logger = logging.getLogger(__name__)

__author__ = "Joel Dierkes und Magnus Rafn Tiedemann"

from scipy.spatial.distance import cdist


class Datapoint:
    # Eigene Datenstruktur zum repraesentieren der mehrdimensionalen Daten

    def __init__(self, coord):
        # coord: Koordinaten
        #
        # Initialisierung

        self.coord = coord
        self.visited = False
        # noch nicht besucht

        self.clus = None
        self.membership = -1
        # keine Clusterzugehoerigkeit oder Membership

        self.core = False
        self.nei = []
        self.numNeigh = -1
        # kein Corepoint und keine Nachbarn

    def getNeigh(self):
        # return: die berechnetten Nachbarn

        return self.nei

    def __repr__(self):
        # toString zur Ausagbe

        st = "DataPoint("
        for i in self.coord:
            st += str(i) + ","
        st = st[:-1] + ") -> " + str(self.clus) + ", " + str(self.membership)
        return st


# Source:     Gloria Bordogna, Dino Ienco, 'Fuzzy Core DBScan Clustering Algorithm',
#             'Information Processing and Management of Uncertainty in Knowledge-Based Systems',
#             pp. 100 - 116,  2014

# Beispiel:  Clus = approxfuzzycoredbscan('euclidean', Data, 0.999, 5, 35, False)

def cluster(data, dist='euclidean', eps=1.0, minmin=1, minmax=5, *_, **__):
    return approxfuzzycoredbscan(dist, data, eps, minmin, minmax, False)


def approxfuzzycoredbscan(dist, Data, eps, minmin, minmax, ausg=False):
    # dist:             selbst gewaehlte Distanzfunktion, oder String
    #                   moeglich: 'euclidean', 'minkowski', 'cityblock', uvm. ;
    #                   Siehe Seite:
    #                   http://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.spatial.distance.cdist.html
    # Data:             numpy.matrix oder numpy.array mit Datenpunkten
    # eps:              Epsilon
    # minmin:           Schranke (drunter) ab der Membership immer 0 ist
    # minmax:           Schranke (drueber)ab der Membership immer 1 ist
    # ausg(Optional):   Ausgabe von Zwischenwerten
    #
    # Hauptfunktion

    if ausg: print("[+] Start...")
    C = 0
    # Clusterindex

    if ausg: print("[+] Berechne Datenpunkte...")
    Datapoints = [Datapoint(i) for i in Data.tolist()]
    # wandle Datenpunkte in eigenen Datentypen um

    if ausg: print("[+] Berechne Abstaende...")
    getnearest(dist, Data, Datapoints, eps, ausg)
    # berechne Distanzen zwischen allen Datenpunkten, speichere Indizes der Epsilon-Nachbarschaft in eigenem Datentyp
    print("fertig")
    for i in Datapoints:
        if not i.visited:
            # mache fuer alle nicht besuchten Datenpunkte

            i.visited = True
            Neigh = i.getNeigh()
            # gib Indizes der Nachbarn zurueck

            if len(Neigh) <= minmin:
                i.clus = -2
            # pruefe ob kein Corepoint, dann markiere als nicht Corepoint

            else:
                # wenn Corepoint

                C += 1
                # Cluster Id zuordnen

                expandClusterFuzzyCore(Datapoints, i, Neigh, C, minmin, minmax, ausg=ausg)
                # durchsuche die Nachbarschaft

    for i in Datapoints:
        if i.clus == -2:
            # fuer alle nicht Corepoints

            i.clus = 0
            # sagt Datenunkt i ist Noise

            Neigh = i.getNeigh()
            # lade Nachbarschaft in Zwischenspeicher

            for j in Neigh:
                if Datapoints[j].core:
                    # fuer alle Corepoints in der Nachbarschaft

                    if Datapoints[j].membership > i.membership:
                        i.membership = Datapoints[j].membership
                        i.clus = Datapoints[j].clus
                        # wenn Membership groesser als bereits vorhandene, weise Punkt diesem Cluster zu
                        # i.membership ist Zwischenspeicher

    Clusters = []
    for i in Datapoints:
        Clusters.append(i.clus)
    # erstelle Liste mit Clusterzugehoerigkeit

    return Clusters
    # gib diese zurueck


def getnearest(dist, Data, Datapoints, eps, ausg):
    # dist:            selbst gewaehlte Distanzfunktion
    # Data: Liste mit allen Datenpunkten
    # Datapoints:      Datenpunkte als Liste eines eigenen Datentypes
    # eps:             Epsilon
    # ausg(Optional):  Ausgabe von Zwischenwerten
    #
    # berechnet und speichert die Epsilon-Nachbarschaft aller Datenpunkte als Indizes
    #
    # wird nicht benutzt, da cdist von scipy extrem schneller

    if ausg: print("[+] Berechne Abstandsmatrix...")
    abs = cdist(Data, Data, dist)
    # Berechnet Abstandsmatrix

    if ausg: print("[+] Eintragen...")
    for i in range(len(abs)):
        if ausg: print(i)
        for j in range(i + 1, len(abs)):
            # berechne und speicher fuer alle anderen Datenpunkte die Epsilon-Nachbarschaft

            if abs[i, j] < eps:
                Datapoints[i].nei.append(j)
                Datapoints[j].nei.append(i)


def getnearest2(dist, Datapoints, eps, ausg):
    # dist:             selbst gewaehlte Distanzfunktion
    # Datapoints:       Datenpunkte als Liste eines eigenen Datentypes
    # eps:              Epsilon
    # ausg(Optional):   Ausgabe von Zwischenwerten
    #
    # berechnet und speichert die Epsilon-Nachbarschaft aller Datenpunkte als Indizes
    #
    # wird nicht benutzt, da cdist von scipy extrem schneller

    l = len(Datapoints)
    for i, j in enumerate(Datapoints):
        if ausg: print("get Nearest Datapoint: " + str(i))
        j.nei.append(i)
        # fuege dich selbst hinzu (Abstand = 0)

        for w in range(i + 1, l):
            # berechne und speicher fuer alle anderen Datenpunkte die Epsilon-Nachbarschaft
            if dist(j.coord, Datapoints[w].coord) < eps:
                j.nei.append(w)
                Datapoints[w].nei.append(i)


def expandClusterFuzzyCore(Datapoints, p, neightpts, C, minmin, minmax, ausg=False):
    # Datapoints:      Datenpunkte als Liste eines eigenen Datentypes
    # p:               Aktuell betrachteter Corepoint
    # neightpts:       Epsilon-Nachbarschaft von Corepoint-p
    # C:               Cluster-Id
    # minmin:          Schranke (drunter) ab der Membership immer 0 ist
    # minmax:          Schranke (drueber)ab der Membership immer 1 ist
    # ausg(Optional):  Ausgabe von Zwischenwerten
    #
    # fuege alle Datenpunkte, die zu diesem Cluster gehoeren, hinzu

    p.membership = calcmemb(len([Datapoints[m] for m in neightpts]), minmin, minmax)
    # berechne Membership fuer Corepoint p

    p.clus = C
    p.core = True
    w = 0
    for i in neightpts:
        # fuer alle Nachbarn

        w += 1
        if ausg: print(str(Datapoints[i]) + "  :  " + str(w))
        if not Datapoints[i].visited:
            # fuer alle nicht besuchten Datenpunkte

            Datapoints[i].visited = True
            neightpts2 = Datapoints[i].getNeigh()
            # lade Epsilon-Nachbarschaft in Zwischenspeicher

            if len(neightpts2) > minmin:
                # wenn Corepoint

                # neightpts2.remove(i)
                neightpts = mergewithoutdoub(neightpts, neightpts2)
                # vereinige die Indizes der Nachbarschaft und der Nachbarschaft des aktuellen Punktes

                mem = calcmemb(len(neightpts), minmin, minmax)
                # berechne aktuelle Membership

                if mem >= Datapoints[i].membership:
                    # wenn aktuelle Membership die grÃ¶sste ist, dann

                    Datapoints[i].membership = mem
                    # setzte Membership

                    Datapoints[i].clus = C
                    # setzte Cluster-Id C

                    Datapoints[i].core = True
                    # merkiere als Core-Point

            else:
                Datapoints[i].clus = -2
                # nicht Corepoint

    return 0


def calcmemb(Neigh, minmin, minmax):
    # Neigh:           Groesse der Epsilon-Nachbarschaft
    # minmin:          Schranke (drunter) ab der Membership immer 0 ist
    # minmax:          Schranke (drueber)ab der Membership immer 1 ist
    #
    # Weiter Informationen: siehe Paper Seite 103

    if Neigh >= minmax:
        return 1
    elif Neigh <= minmin:
        return 0
    else:
        return float(Neigh - minmin) / (minmax - minmin)


def mergewithoutdoub(x, y):
    # x: Liste, zu der nicht schon vorhandene Elemente hinzugefuegt werden sollen
    # y: Liste, der hinzuzufuegenden Elemente
    #
    # Vereinigt x und y
    # optimiert durch Dictionary

    seen = set()
    for i in x:
        seen.add(i)
    for i in y:
        if i not in seen:
            seen.add(i)
            x.append(i)
    return x
