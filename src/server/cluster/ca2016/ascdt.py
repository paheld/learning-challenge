#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import logging

logger = logging.getLogger(__name__)

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import sys

from matplotlib.tri import Triangulation
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.cm as plcm
from tempfile import mkstemp
import math
import itertools
import threading

#def data_constructor (clazz):
#    def data_decorator (*args, **kwargs):
#        clazz.__my_variable = None
#        res = clazz (*args, **kwargs)
#        return res
#    return data_decorator

#@data_constructor
class Data:
    Cached_Global_Mean = None
    Cached_Global_Variation = None
    Cached_Edge_Length = {}
    Cached_Edge_Global_Cut_Value = {}
    Cached_Mean = {}
    Cached_Mean_Variation = None

#    @staticmethod
#    def do_something ():
#        Data.Cached_Global_Mean

#    @staticmethod
#    @property
#    def MyVariable () -> str:
#        return Data.__my_variable

#    @staticmethod
#    @MyVariable.setter
#    def MyVariable (value: str) -> None:
#        Data.__my_variable = value    

#Data.MyVariable = "Foo"

class AlgoHelper:
    def __init__(self):
        edges = []

# Point
def point_key(point):
    value = ''
    try:
        value = str(point[0, 0]) + '-' + str(point[0, 1])
    finally:
        return value

# Edge lengths
def edge_key(edge):
    return str(min(edge[0], edge[1])) + "-" + str(max(edge[0], edge[1]))

def edge_length(edge, points):
    key = str(min(edge[0], edge[1])) + "-" + str(max(edge[0], edge[1]))
    if key not in Data.Cached_Edge_Length:
        Data.Cached_Edge_Length[key] = np.linalg.norm(points[edge[0]] - points[edge[1]])

    return Data.Cached_Edge_Length[key]

# Definition 1 

def neighbors_rek(tri, points, point, parent, k, m):
    list_to_return = []

    if k == m: # Letzter Schritt
        for e in tri.edges:
            allowed_1 = (point_key(points[e[0]]) == point_key(point)) and (point_key(points[e[1]]) != point_key(parent))
            allowed_2 = (point_key(points[e[1]]) == point_key(point)) and (point_key(points[e[0]]) != point_key(parent))

            if allowed_1 or allowed_2:
                list_to_return.append(e)
    else:
        for e in tri.edges:
            allowed_1 = (point_key(points[e[0]]) == point_key(point)) and (point_key(points[e[1]]) != point_key(parent))
            allowed_2 = (point_key(points[e[1]]) == point_key(point)) and (point_key(points[e[0]]) != point_key(parent))

            if allowed_1 and not allowed_2:
                # For each element wir have to duplicate actual edge
                tmpes = neighbors_rek(tri, points, points[e[1]], point, k, m+1)

                for obj in tmpes:
                    list_to_return.append([e, obj])
            elif allowed_2 and not allowed_1:
                # For each element wir have to duplicate actual edge
                tmpes = neighbors_rek(tri, points, points[e[0]], point, k, m+1)

                for obj in tmpes:
                    list_to_return.append([e, obj])

    return list_to_return

def neighbors(tri, points, point, k):
    list_to_return = []

    return neighbors_rek(tri, points, point, None, k, 1)

# Definition 2
def global_mean(tri, ps):
    if Data.Cached_Global_Mean == None:
        Data.Cached_Global_Mean = sum([edge_length(e, ps) for e in tri.edges]) / len(tri.edges)

    return Data.Cached_Global_Mean

# Definition 3 # TODO
def mean(tri, ps, point, k, nbs = None):
    if k == 1:
        if nbs == None:
            nbs = neighbors(tri, ps, point, k);

        count_of_edges = len(nbs)
        if count_of_edges <= 0:
            return 0, 0

        if point_key(point) not in Data.Cached_Mean:
            Data.Cached_Mean[point_key(point)] = sum([edge_length(e, ps) for e in nbs]) / count_of_edges, count_of_edges

        return Data.Cached_Mean[point_key(point)]
    elif k == 2:
        if nbs == None:
            nbs = neighbors(tri, ps, point, k)

        count_of_edges = 0
        sum_of_edges = 0

        for el in nbs:
            if isinstance(el, list):
                count_of_edges += 2
                sum_of_edges += edge_length(el[0], ps)
                sum_of_edges += edge_length(el[1], ps) + edge_length(el[0], ps)
            else:
                count_of_edges += 1
                sum_of_edges += edge_length(el, ps)

        if count_of_edges <= 0:
            return 0, 0

        return sum_of_edges / count_of_edges, count_of_edges

# Definition 4
def global_variation(tri, ps):
    glob_mean = global_mean(tri, ps)

    if Data.Cached_Global_Variation == None:
        Data.Cached_Global_Variation = math.sqrt(sum([pow(edge_length(e, ps) - glob_mean, 2) for e in tri.edges]) / (len(tri.edges) - 1))

    return     Data.Cached_Global_Variation

# Definition 5
def local_variation(tri, ps, point):
    mn, count_of_edges = mean(tri, ps, point, 1)
    if count_of_edges <= 0:
        return 0

    if count_of_edges == 1:
        return 0

    return math.sqrt(sum([pow(edge_length(e, ps) - mn, 2) for e in tri.edges]) / (count_of_edges-1))

# Definition 6
def mean_variation(tri, ps):
    if Data.Cached_Mean_Variation == None:
        Data.Cached_Mean_Variation = sum([local_variation(tri, ps, p) for p in ps]) / len(ps)
    return Data.Cached_Mean_Variation

# Definition 7 und 8
def global_cut_value(tri, ps, point):
    mn, _ = mean(tri, ps, point, 1)

    if point_key(point) not in Data.Cached_Edge_Global_Cut_Value:
        Data.Cached_Edge_Global_Cut_Value[point_key(point)] = global_mean(tri, ps) + (global_mean(tri, ps) / mn) * global_variation(tri, ps)

    return Data.Cached_Edge_Global_Cut_Value[point_key(point)]

def global_long_edges(tri, ps, point):
    list_to_return = []
    glb_cut_val = global_cut_value(tri, ps, point)

    for e in tri.edges:
        if point_key(ps[e[0]]) == point_key(point) or point_key(ps[e[1]]) == point_key(point):
            if edge_length(e, ps) >= glb_cut_val:
                list_to_return.append(e)

    return list_to_return

def global_other_edges(tri, ps, point):
    list_to_return = []
    glb_cut_val = global_cut_value(tri, ps, point)

    for e in tri.edges:
        if point_key(ps[e[0]]) == point_key(point) or point_key(ps[e[1]]) == point_key(point):
            if edge_length(e, ps) < glb_cut_val:
                list_to_return.append(e)

    return list_to_return

# Defintion 9
def local_cut_value(tri, ps, point, beta, nbs = None):
    mn, _ = mean(tri, ps, point, 2, nbs)
    return mn + beta * mean_variation(tri, ps)

def local_long_edges(tri, ps, point, beta):
    nbs = neighbors(tri, ps, point, 2)

    lcutvalue = local_cut_value(tri, ps, point, beta, nbs)

    list_to_return = []

    for el in nbs:
        if isinstance(el, list):
            if (edge_length(el[1], ps) + edge_length(el[0], ps)) >= lcutvalue:
                list_to_return.append(el[0])
                list_to_return.append(el[1])
        else:
            if edge_length(el, ps) >= lcutvalue:
                list_to_return.append(el)
    return list_to_return

def goto_neighborhood(tri, ps, point, idx, actual_index, index_label_helper):
    nachb = neighbors(tri, ps, point, 1)

    index_label_helper[idx] = actual_index

    for nach in nachb:
        if (nach[0] != idx) and nach[0] in index_label_helper:
            index_label_helper[idx] = index_label_helper[nach[0]]
        elif (nach[1] != idx) and nach[1] in index_label_helper:
            index_label_helper[idx] = index_label_helper[nach[1]]

        actual_index = index_label_helper[idx]

        if (nach[0] == idx) and nach[1] not in index_label_helper:
            goto_neighborhood(tri, ps, ps[nach[1]], nach[1], actual_index, index_label_helper)
        elif (nach[1] == idx) and nach[0] not in index_label_helper:
            goto_neighborhood(tri, ps, ps[nach[0]], nach[0], actual_index, index_label_helper)

def find_clusters(tri, ps):
    index_label_helper = {}
    actual_index = 1

    for i in range(0, len(ps)):
        if i not in index_label_helper:
            goto_neighborhood(tri, ps, ps[i], i, actual_index, index_label_helper)
            actual_index += 1

    return index_label_helper


def cluster(points, beta, *_, **__):
    points = np.matrix(points)
    # Should I ignore higher dimensions?
    points = points[:, 0:2]

    calculated_point_info_mean1 = {}
    calculated_point_info_mean2 = {}
    calculated_edges = []

    # Erster Schritt, Trianguliere
    tri = Triangulation(points[:, 0].getA1(), points[:, 1].getA1())
    # Berechne GlobalMean und GlobalVariation und für jeden Punkt Mean1

    glb_long_edges = []
    for pt in points:
        glb_long_edges.extend(global_long_edges(tri, points, pt))

    # Entferne alle Global Long Edges?
    for e in tri.edges:
        append = True
        for e2 in glb_long_edges:
            if edge_key(e) == edge_key(e2):
                append = False
                break
        if append:
            calculated_edges.append(e)

    helper = AlgoHelper()
    helper.edges = calculated_edges

    # Zweiter Schritt
    # Berechne Durchschnittsabweichung, und für jeden Punkt Mean2 und long edges
    lcl_lng_edges = []
    second_calculated_edges = []

    for pt in points:
        lcl_lng_edges.extend(local_long_edges(helper, points, pt, beta))

    # Entferne Long Edges
    for e in helper.edges:
        append = True
        for e2 in lcl_lng_edges:
            if edge_key(e) == edge_key(e2):
                append = False
                break
        if append:
            second_calculated_edges.append(e)



    # Here noch local_link_edges finden, aber erst später

    # Finde Cluster, bzw ordne grob zu
    helper.edges = second_calculated_edges

    index_label_helper = find_clusters(helper, points)

    labels = [index_label_helper[i] if i in index_label_helper else 0 for i in range(0, len(points))]
    return labels

    # for i in range(0, len(points)):
    #     try:
    #         print(index_label_helper[i])
    #         pass
    #     except:
    #         print(0)

        #fig = pl.figure()
        #pl.title("Delaunay triangulation")
        #pl.gca().set_aspect('equal')
        #pl.triplot(tri)
        #pl.plot(points[:, 0], points[:, 1], 'o')
        #pl.savefig("Triangulation.png")
        #pl.close(fig)

        #fig = pl.figure()
        #pl.title("Reduced graph")
        #pl.gca().set_aspect('equal')
        #for se in second_calculated_edges:
        #    pl.plot([points[se[0], 0], points[se[1], 0]], [points[se[0], 1], points[se[1], 1]])
        #pl.plot(points[:, 0], points[:, 1], 'o')
        #pl.savefig("ReducedGraph.png")
        #pl.close(fig)

def read_arff_file(path):
    # Read arff file
    points = []

    start_reading = False

    arff_file = open(sys.argv[1], 'r')
    for line in arff_file:
        if start_reading:
            splitted_string = line.split(',')
            point = [float(x) for x in splitted_string]
            point.pop()
            points.append(point)

        if line == '@DATA\n':
            start_reading = True

    return np.matrix(points)

def main():
    # Read arff file
    data = read_arff_file(sys.argv[1])

    # Cluster
    cluster(data, float(sys.argv[2]))


if __name__ == "__main__":
    main()