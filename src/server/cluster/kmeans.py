__author__ = 'cbraune'

from sklearn.cluster import KMeans
import numpy as np
from sklearn.metrics import homogeneity_completeness_v_measure, silhouette_score
from time import time
from json import loads

def cluster(data, k=3, *args, **kwargs):

    n, _ = np.shape(data)

    if k > n:
        return 'Number of cluster centers larger than number of data points. That makes no sense!'

    if 'init' in kwargs:
        if kwargs['init'] == 'random':
            init = 'random'
        elif kwargs['init'] == 'points':
            init_points = loads(kwargs['init_points'])
            print(init_points)
            try:
                init_points = np.array(init_points)
            except:
                return 'Cluster definition incoherent with dataset. Probably number of dimension for at least one point wrong.'
            else:
                if len(init_points) != k:
                    return 'Number of cluster centers does not match number of clusters.'
                else:
                    init = init_points


    k_means = KMeans(init=init, n_clusters=k, n_init=1)
    return k_means.fit_predict(data)