"""
Django settings for server project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import settings

from django.conf import global_settings

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '78lcw@a7snl4ihfgw#1n8(55d4g$rv@3#*$c0lc2o+_9&0(yjp'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = settings.DJANGO_DEBUG

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #'debug_toolbar',
    'webapp',
    'raven.contrib.django.raven_compat',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'server.urls'

WSGI_APPLICATION = 'server.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.sqlite3',
    #     'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    # }
   'default': {
       'ENGINE': 'django.db.backends.mysql',      # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
       'NAME': settings.DJANGO_DB_DATABASE,       # Or path to database file if using sqlite3.
       # The following settings are not used with sqlite3:
       'USER': settings.DJANGO_DB_USERNAME,
       'PASSWORD': settings.DJANGO_DB_PASSWORD,
       'HOST': settings.DJANGO_DB_HOSTNAME,
       # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
       'PORT': '',                      # Set to empty string for default.
       'OPTIONS': {'charset': 'utf8mb4'},
   }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Berlin'

USE_I18N = False

USE_L10N = False

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = settings.DJANGO_STATIC_ROOT

MEDIA_ROOT = settings.DJANGO_MEDIA_ROOT
MEDIA_URL = "/media/"

LOGIN_REDIRECT_URL = '/'

_list_type = type(global_settings.TEMPLATE_CONTEXT_PROCESSORS)
TEMPLATE_CONTEXT_PROCESSORS = _list_type(list(global_settings.TEMPLATE_CONTEXT_PROCESSORS) + [
    "webapp.processors.general_processor",
])

RAVEN_CONFIG = {
    'dsn': settings.RAVEN_dsn,
}