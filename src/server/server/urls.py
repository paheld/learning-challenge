from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from webapp import views, admin as _

from django.contrib import admin

try:
   admin.autodiscover()
except:
    pass


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'server.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^env\.json/$', views.get_env, name="env"),
    url(r'^ok\.json/$', views.api_ok, name="api_ok"),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {"next_page": "/"}),
    url(r'^actions/add_group/$', views.add_group, name="add_group"),
    url(r'^actions/get_groups\.json/$', views.get_groups, name="get_groups"),
    url(r'^actions/get_time\.json/$', views.get_time, name="get_time"),
    url(r'^actions/get_round/(?P<pk>[0-9]+).json/$', views.get_round, name="get_round"),
    url(r'^actions/start_round/(?P<round_id>[0-9]+).json/$', views.start_round, name="start_round"),
    url(r'^actions/stop_round/(?P<round_id>[0-9]+).json/$', views.stop_round, name="stop_round"),
    url(r'^actions/get_free_jobs/(?P<round_id>[0-9]+)/(?P<dataset_id>[0-9]+).json/$', views.get_free_jobs, name="get_free_jobs"),
    url(r'^actions/get_job_params/(?P<jobid>[0-9]+).json/$', views.get_job_params, name="get_job_params"),
    url(r'^actions/get_preprocessing_job_params/(?P<ppid>[0-9]+).json/$', views.get_preprocessing_job_params, name="get_preprocessing_job_params"),
    url(r'^actions/get_job_details/(?P<jobid>[0-9]+).json/$', views.get_job_details, name="get_job_details"),
    url(r'^actions/submit_job/(?P<jobid>[0-9]+)\.json/$', views.submit_job, name="submit_job"),
    url(r'^actions/rerun_job/(?P<jobid>[0-9]+)\.json/$', views.rerun_job, name="rerun_job"),
    url(r'^actions/cancel_job/(?P<jobid>[0-9]+)\.json/$', views.cancel_job, name="cancel_job"),
    url(r'^actions/score_list/(?P<round_id>[0-9]+)\.json/$', views.score_list, name="score_list"),
    url(r'^actions/score_list_total\.json/$', views.score_list_total, name="score_list_total"),
    url(r'^actions/load_job_list-(?P<roundid>[0-9]+).json/$', views.load_job_list, name="load_job_list"),
    url(r'^actions/update_job_details/(?P<jobid>[0-9]+).json/$', views.update_job_details, name="update_job_details"),
    url(r'^actions/update_preprocessing_job_details/(?P<ppid>[0-9]+).json/$', views.update_preprocessing_job_details, name="update_preprocessing_job_details"),
    url(r'^actions/add_job_picture/(?P<jobid>[0-9]+).json/$', views.add_job_picture, name="add_job_picture"),
    url(r'^actions/get_preprocessing/(?P<preprocessing_id>[0-9]+).json/$', views.get_preprocessing, name="get_preprocessing"),
    url(r'^actions/add_dataset/$', views.add_dataset, name="add_dataset"),
    url(r'^actions/clone_dataset/$', views.clone_dataset, name="clone_dataset"),
    url(r'^actions/get_datasets\.json/$', views.get_datasets, name="get_datasets"),
    url(r'^actions/create_user/$', views.create_user, name="create_user"),
    url(r'^actions/edit_round/$', views.edit_round, name="edit_round"),
    url(r'^actions/update_dataset_default_dimension/$', views.update_dataset_default_dimension, name="update_dataset_default_dimension"),
    url(r'^actions/ping/$', views.ping, name="ping"),
    url(r'^actions/ping_preprocessing/$', views.ping_preprocessing, name="ping_preprocessing"),
    url(r'^actions/ping_job/(?P<job_id>[0-9]+)\.json/$', views.ping_job, name="ping_job"),
    url(r'^actions/create_job/$', views.create_job, name="create_job"),
    url(r'^actions/submit_preprocessing/$', views.submit_preprocessing, name="submit_preprocessing"),
    url(r'^actions/join_group/(?P<group_name>[^/]+)/$', views.join_group, name="join_group"),
    url(r'^data/(?P<pk>[0-9]+)-full\.json/', views.get_full_dataset_json, name="full_json_dataset"),
    url(r'^data/preprocessing/(?P<pk>[0-9]+)-full\.json/', views.get_full_preprocessing_json, name="full_preprocessing_json"),
    url(r'^data/preprocessing/(?P<pk>[0-9]+)\.json/', views.get_preprocessing_json, name="preprocessing_json"),
    url(r'^data/preprocessing/(?P<pk>[0-9]+)-(?P<subset>[^/]+)\.arff/', views.get_preprocessing_arff, name="preprocessing_arff"),
    url(r'^data/(?P<pk>[0-9]+)-full\.json/', views.get_full_dataset_json, name="full_json_dataset"),
    url(r'^data/(?P<name>[^/]+)-full\.json/', views.get_full_dataset_json, name="full_json_dataset"),
    url(r'^data/(?P<pk>[0-9]+)\.json/', views.get_dataset_json, name="json_dataset"),
    url(r'^data/(?P<name>[^/]+)\.json/', views.get_dataset_json, name="json_dataset"),
    url(r'^data/(?P<pk>[0-9]+)-(?P<subset>[^/]+)\.arff/', views.get_dataset_arff, name="get_dataset_arff"),
    url(r'^data/(?P<name>[^/]+)-(?P<subset>[^/]+)\.arff/', views.get_dataset_arff, name="get_dataset_arff"),
    url(r'/$', views.webapp, name="webapp"),
    url(r'^$', views.webapp, name="webapp"),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


