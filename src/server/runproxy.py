import sys

from twisted.internet import reactor
from twisted.web import proxy, server

import daemon

from config_example import *
try:
    from config import *
except ImportError:
    pass

def run():
    site = server.Site(proxy.ReverseProxyResource(BACKUP_SERVER, BACKUP_SERVER_PORT, ''))
    reactor.listenTCP(TORNADO_PORT, site)
    reactor.run()

if __name__ == "__main__":
    if len(sys.argv) == 2:
        d = daemon.Daemon(TORNADO_PIDFILE, run)
        daemon.deamon_manager(d)
    else:
        run()