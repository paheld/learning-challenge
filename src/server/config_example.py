#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is an example configuration. Please copy this file to ``config.py`` and change the settings to your local
environment.
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

import tempfile
from datetime import timedelta

import logging

import os

LOG_LEVEL = logging.INFO

##############################
#
# Backend related stuff
#
##############################

SERVER_URI = "http://cla/"
BACKUP_SERVER = "pascal.iws.cs.uni-magdeburg.de"
BACKUP_SERVER_PORT = 8000

BACKEND_USERNAME = "cbraune"
"""The username for backend access to server. Need admin rights."""

BACKEND_PASSWORD = "poseidon0815"
"""Password for the backend user"""

BACKEND_PING_TIME = 15
"""Average delay between two pings."""

BACKEND_PIDFILE = "/tmp/learning-challenge-worker.pid"
"""PID file of the worker process"""

BACKEND_RUNFILE = "/tmp/learning-challenge-worker.running"
"""File that indicates, that the workers are running. Will also be created if worker process is not a daemon."""

BACKEND_FILE_CACHE_DIR = tempfile.gettempdir()
"""Directory to store tmp files, like dataset files"""

BACKEND_WEKA_PATH = 'C:\\Progra~1\\Weka-3-6\\'
"""Path to Weka's jar file. With trailing (back)slash!"""

BACKEND_TIMEOUT_TIME = timedelta(minutes=3)
"""Maximum Time to run a job"""

TMPDIR = os.environ.get("TMPDIR", "/tmp")
"""Temporary Directory for images"""

BACKEND_DELETE_FILES_AFTER_UPLOAD = True
"""Should files be deleted after upload?"""

##############################
#
# Django related stuff
#
##############################


DJANGO_DB_USERNAME = "foo"
"""Username for MySQL-Server, needed by Django Server Instance only."""

DJANGO_DB_PASSWORD = "bar"
"""Password for MySQL-Server, needed by Django Server Instance only."""

DJANGO_DB_DATABASE = "learning-challenge"
"""MySQL-Database name"""

DJANGO_DB_HOSTNAME = "localhost"
"""MySQL Server name"""

DJANGO_DEBUG = True
"""Run Django in debug mode?"""

DJANGO_MEDIA_ROOT = "./MEDIA/"
"""Directory for job Images."""

DJANGO_STATIC_ROOT = "./STATIC/"
"""Directory for static files."""


##############################
#
# Website Stuff
#
##############################

VISUALIZATION_DIMENSION_DEFAULT = 4
"""How many dimensions are plotted as default"""


##############################
#
# Tornado Stuff
#
##############################

TORNADO_PORT = 8888
"""Port of the tornado server"""

TORNADO_PIDFILE = "/tmp/learning-challenge-server.pid"
"""PID file of the server process"""


##############################
#
# Google Analytics
#
##############################

ANALYTICS_TRACKING_ID = "UA-52811236-3"

DIMENSION_HOSTNAME_LABEL = "dimension1"
DIMENSION_USERNAME_LABEL = "dimension2"
DIMENSION_GROUP_LABEL = "dimension3"
DIMENSION_ROUND_LABEL = "dimension4"
DIMENSION_DATASET_LABEL = "dimension5"
DIMENSION_ALGORITHM_LABEL = "dimension6"


#############################
#
# Sentry
#
#############################

RAVEN_dsn = ''