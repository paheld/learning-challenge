#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
To use the configuration please include **this** file. It will include the config defaults from the
```config_example.py``` and will overwrite the values with data from a local ```config.py```.
"""

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import copy

__author__ = "Pascal Held"
__email__ = "paheld@gmail.com"

from config_example import *

import six
from six.moves.queue import Queue
import os

import docutils.core

try:
    from config import *
except ImportError:
    pass

os.environ["TMPDIR"] = TMPDIR
try:
    os.makedirs(TMPDIR)
except OSError:
    pass


from algorithms import *

def rst2html(obj):
    if isinstance(obj, (list, tuple)):
        for o in obj:
            rst2html(o)
    if isinstance(obj, dict):
        for key, value in obj.copy().items():
            if key in ["short_desc", "description"]:
                obj[key+"_html"] = docutils.core.publish_parts(value, writer_name='html')['html_body']
            rst2html(value)

rst2html(algorithms)
rst2html(distances)

json_types = []
json_types.extend(six.integer_types)
json_types.extend(six.string_types)
json_types.append(six.text_type)
json_types.extend([float, bool, dict, tuple, list])
json_types = tuple(json_types)


def remove_not_jsonable(obj):
    if isinstance(obj, list):
        i=0
        while i<len(obj):
            if not isinstance(obj[i], json_types):
                del obj[i]
            else:
                remove_not_jsonable(obj[i])
                i += 1
    if isinstance(obj, dict):
        for key in list(obj.keys()):
            if not isinstance(obj[key], json_types):
                del obj[key]
            else:
                remove_not_jsonable(obj[key])


algorithms_jsonable = copy.deepcopy(algorithms)
remove_not_jsonable(algorithms_jsonable)

preprocessing_jsonable = copy.deepcopy(preprocessing)
remove_not_jsonable(preprocessing_jsonable)

TYPE_TRAINING = 0
TYPE_TEST = 1
TYPE_VALIDATION = 2

msg_queue = Queue()