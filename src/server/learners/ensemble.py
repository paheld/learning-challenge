__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score as accuracy_score
from time import time as time

def do_stuff(dataset, *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    if 'enstype' in kwargs and 'n' in kwargs:
        if kwargs['enstype'] == 'random':
            clf = RandomForestClassifier(n_estimators=kwargs['n'])
        elif kwargs['enstype'] == 'extra':
            clf = ExtraTreesClassifier(n_estimators=kwargs['n'])
        elif kwargs['enstype'] == 'ada':
            clf = AdaBoostClassifier(DecisionTreeClassifier(max_depth=kwargs['enstype_maxdepth']), n_estimators=kwargs['n'])
    else:
        error['message'] = 'No valid classifier type specified.'
        return error


    t = time() # Train the KNN Classifier on the training data and labels
    clf.fit(training_data, training_label)
    training_time = time() - t

    training_prediction = clf.predict(training_data) # get a prediction on the training data (this should be obvious)

    t = time() # get labels for test data
    test_prediction = clf.predict(test_data)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = clf.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': None,
               'success': True}
    return result