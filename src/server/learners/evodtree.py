#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import random
from time import time
from sklearn.metrics import accuracy_score

nodeTraverseCounter = 0


# Erstellt einen Entscheidungsbaum, basierend auf den übergebenen Daten
# Parameter: 
# data: Die Trainingsdaten für die Klassifizierung
# labels: Die Klassifizierung der Trainingsdaten
# generationSize: Die Populationsgröße einer Genration (default 100)
# thresholdFitness: Die minimal zu erreichende Fitnes (default 0)
# maxGenerations: Die maximale Anzahl an Generationen (default 50)
# treeSizeBias: Faktor wie hart größere Bäume bestraft werden sollen um overfitting einzudämmen. Mit 0 wird die Größe nicht bestraft (default 0.1)
# Rückgabe: 
# Es wird der trainierte Entscheidungsbaum zurückgegeben (der Klasse DecisionTree)
def einfaches_klassifizieren_training(data, labels, generationSize=100, thresholdFitness=0, maxGenerations=100,
                                      treeSizeBias=0.1):
    numberOfAttributes = len(data[0])
    minMax = getMinMax(data, numberOfAttributes)
    population = generateInitialPopulation(labels, minMax, numberOfAttributes, generationSize)
    for i in range(0, maxGenerations):
        population = generateNextGeneration(data, labels, minMax, numberOfAttributes, population, generationSize,
                                            treeSizeBias)
        # print("Generation " + str(i) + ", Best Fitness: " + str(
        #     fitness(data, labels, population[0], treeSizeBias)) + " with the size: " + str(population[0].size))
        if (isStoppingCriteriaReached(data, labels, population[0], thresholdFitness, treeSizeBias)):
            break
    return DecisionTree(population[0])


# Findet für jedes Attribut den geringsten und den höchsten möglichen Wert
# Parameter: 
# data: Trainingsdaten
# numberOfAttributes: Anzahl der Attribute
# Rückgabe: 
# Gibt eine Liste von zweielementigen Listen (Tupel) zurück. Bei jedem Tupel ist für das 
# entsprechende Attribut an erster Stelle der geringste und an zweiter Stelle der hächste Wert 
def getMinMax(data, numberOfAttributes):
    minMaxTuple = [0] * numberOfAttributes
    for i in range(0, numberOfAttributes):
        minMaxTuple[i] = [data[0][i], data[0][i]]
    for datum in data:
        for i in range(0, numberOfAttributes):
            if (minMaxTuple[i][0] > datum[i]):
                minMaxTuple[i][0] = datum[i]
            if (minMaxTuple[i][1] < datum[i]):
                minMaxTuple[i][1] = datum[i]
    return minMaxTuple


# Generiert eine zufällige Initialpopulation
# Parameter: 
# labels: Liste der möglichen Klassen 
# minMax: Minimal/Maximalwerte der Attribute (s. Rückgabe von getMinMax)
# numberOfAttributes: Anzahl der Attribute
# generationSize: Größe der zu generierenden Population
# Rückgabe: 
# Gibt eine Liste von Bäumen (der Klasse Node) zurück
def generateInitialPopulation(labels, minMax, numberOfAttributes, generationSize):
    population = []
    for i in range(0, generationSize):
        population.append(generateRandomTree(labels, minMax, numberOfAttributes))
    return population


# Generiert einen Zufälligen Baum. Dessen Wurzelknoten hat nur ein Kind, das immer gewählt wird.
# Ansonsten ist es ein Binärbaum. 
# Parameter: 
# labels: Liste der möglichen Klassen 
# minMax: Minimal/Maximalwerte der Attribute (s. Rückgabe von getMinMax)
# numberOfAttributes: Anzahl der Attribute
# Rückgabe: 
# Ein Baum (der Klasse Node)
def generateRandomTree(labels, minMax, numberOfAttributes):
    node = Node(None, minMax[0][0], 0, 0, False)
    node.addChild(generateRandomNode(node, 1, minMax, numberOfAttributes, labels))
    return node


# Generiert einen zufälligen Knoten eines Baumes. Dieser hat entweder zwei Kinder oder ein Blatt.
# Parameter: 
# parent: Elternknoten
# level: Aktuelle Ebene
# minMax: Minimal/Maximalwerte der Attribute (s. Rückgabe von getMinMax)
# numberOfAttributes: Anzahl der Attribute
# labels: Liste der möglichen Klassen 
# Rückgabe:
# Ein Knoten (der Klasse Node)
def generateRandomNode(parent, level, minMax, numberOfAttributes, labels):
    if (random.uniform(0, 1) < 1 / (1 + level * 0.1)):
        attribute = random.randrange(0, numberOfAttributes)
        threshold = random.uniform(minMax[attribute][0], minMax[attribute][1])
        node = Node(parent, threshold, attribute, -1, False)
        node.addChild(generateRandomNode(parent, level + 1, minMax, numberOfAttributes, labels))
        node.addChild(generateRandomNode(parent, level + 1, minMax, numberOfAttributes, labels))
        return node
    else:
        label = random.choice(labels)
        node = Node(parent, 0, 0, label, True)
        return node


# Generiert anhand einer Population die nächste Population
# Parameter: 
# data: Trainingsdaten
# labels: Klassifizierung der Daten
# minMax: Minimal/Maximalwerte der Attribute (s. Rückgabe von getMinMax)
# numberOfAttributes: Anzahl der Attribute
# population: Die Ausgangspopulation
# generationSize: Die Größe einer Population
# treeSizeBias: Faktor wie hart größere Bäume bestraft werden sollen um overfitting einzudämmen. Mit 0 wird die Größe nicht bestraft 
# Rückgabe:
# Gibt eine Liste von Bäumen (der Klasse Node) zurück
def generateNextGeneration(data, labels, minMax, numberOfAttributes, population, generationSize, treeSizeBias):
    crossoverChilds = []
    mutatedChilds = []
    for i in range(0, int(math.ceil(generationSize / 2))):
        crossoverChilds += crossover(population)
        mutatedChilds += mutate(population, minMax, numberOfAttributes)
    return select(data, labels, population, crossoverChilds, mutatedChilds, generationSize, treeSizeBias)


# Crossover-Operator. Es werden zwei zufällige Knoten zweier zufälliger Bäume der Population vertauscht
# Parameter: 
# population: Die Ausgangspopulation
# Rückgabewert: 
# Zwei Kinder (der Klasse Node)
def crossover(population):
    treeIndex1 = random.randrange(0, len(population))
    treeIndex2 = random.randrange(0, len(population))

    tree1 = population[treeIndex1].deepCopy(None)
    tree2 = population[treeIndex2].deepCopy(None)

    nodeIndex1 = random.randrange(0, tree1.size, 1)
    nodeIndex2 = random.randrange(0, tree2.size, 1)

    node1 = traverseTree(tree1, nodeIndex1)
    node2 = traverseTree(tree2, nodeIndex2)

    parentNode1 = node1.parent
    parentNode2 = node2.parent

    if (parentNode1 != None and parentNode2 != None):
        parentNode1.children[parentNode1.children.index(node1)] = node2
        parentNode2.children[parentNode2.children.index(node2)] = node1

        node1.parent = parentNode2
        node2.parent = parentNode1

        node1.updateSize()
        node2.updateSize()

    return [tree1, tree2]


# Mutationsoperator. Es wird zum einen bei einem zufälligen Knoten eines zufälligen Baumes das
# referenzierte Attribute zufällig verändert und zum anderen bei einem anderen zufälligen Knoten
# die Position des Splits
# Parameter: 
# population: Die Ausgangspopulation
# minMax: Minimal/Maximalwerte der Attribute (s. Rückgabe von getMinMax)
# numberOfAttributes: Anzahl der Attribute
# Rückgabewert: 
# Zwei Kinder (der Klasse Node)
def mutate(population, minMax, numberOfAttributes):
    treeIndex1 = random.randrange(0, len(population))
    treeIndex2 = random.randrange(0, len(population))

    tree1 = population[treeIndex1].deepCopy(None)
    tree2 = population[treeIndex2].deepCopy(None)

    nodeIndex1 = random.randrange(0, tree1.size)
    nodeIndex2 = random.randrange(0, tree2.size)

    node1 = traverseTree(tree1, nodeIndex1)
    node2 = traverseTree(tree2, nodeIndex2)

    step = minMax[node1.attribute][1] - minMax[node1.attribute][0]
    node1.threshold += random.choice([-step, step])

    node2.attribute = random.randrange(0, numberOfAttributes)

    return [tree1, tree2]


# Selektiert aus der Elternpopulation und zwei Kindpopulation eine neue Generation
# Parameter
# data: Trainingsdaten
# labels: Klassifizierung der Daten
# population: Die Ausgangspopulation
# crossoverChilds: Durch Crossover generierte Kinder
# mutatedChilds: Durch Mutation generierte Kinder
# generationSize: Die Größe einer Population
# treeSizeBias: Faktor wie hart größere Bäume bestraft werden sollen um overfitting einzudämmen. Mit 0 wird die Größe nicht bestraft 
# Rückgabe:
# Gibt eine Liste von Bäumen (der Klasse Node) zurück
def select(data, labels, population, crossoverChilds, mutatedChilds, generationSize, treeSizeBias):
    population += crossoverChilds
    population += mutatedChilds
    population.sort(key=lambda x: fitness(data, labels, x, treeSizeBias))

    newPopulation = []
    for i in range(0, generationSize):
        newPopulation.append(population[i])
    return newPopulation


# Prüft ob das Endkriterium erreicht wurde
# Parameter
# data: Trainingsdaten
# labels: Klassifizierung der Daten
# tree: Baum für den dies überprüft wird
# thresholdFitness: Die minimal zu erreichende Fitnes
# treeSizeBias: Faktor wie hart größere Bäume bestraft werden sollen um overfitting einzudämmen. Mit 0 wird die Größe nicht bestraft 
# Rückgabe: 
# Boolescher Wert
def isStoppingCriteriaReached(data, labels, tree, thresholdFitness, treeSizeBias):
    return fitness(data, labels, tree, treeSizeBias) < thresholdFitness


# Berechnet die fitness eines Baumes bezüglich der Daten. Diese ist in unserem Fall zu minimieren und beschreibt die
# Anzahl der fehlklassifizierten Daten. Zusätzlich wird noch ein Strafterm für die größe des Baumes aufaddiert
# Parameter: 
# data: Trainingsdaten
# labels: Klassifizierung der Daten
# tree: Baum für den dies berechnet wird
# treeSizeBias: Faktor wie hart größere Bäume bestraft werden sollen um overfitting einzudämmen. Mit 0 wird die Größe nicht bestraft 
# Rückgabe: 
# Fitness als reeler Zahlenwert
def fitness(data, labels, tree, treeSizeBias):
    prediction = totallyRedundantPredictBecauseWeDidntBotherToRewriteEverythingIntoAWrapper(tree, data)
    score = evaluate(prediction, labels)
    score += tree.size * treeSizeBias
    return score


# Berechnet die Anzahl der fehlklassifizierten Daten in prediction (bzgl labels)
# Parameter: 
# prediction: (Von uns erstellte) Klassifizierung der Daten
# labels: Korrekte Klassifizierung der Daten
# Rückgabe: 
# Anzahl der fehlklassifizierten Daten
def evaluate(prediction, labels):
    score = 0
    for i in range(0, len(labels)):
        if prediction[i] != labels[i]:
            score += 1
    return score


# Klassifiziert Daten mithilfe eines Baumes
# Parameter: 
# root: Der Baum
# data: Trainingsdaten
# Rückgabe:
# Eine Liste von Labeln
def totallyRedundantPredictBecauseWeDidntBotherToRewriteEverythingIntoAWrapper(root, data):
    labels = []
    for datum in data:
        labels.append(root.classifyDatum(datum))
    return labels


# Klassifiziert Daten mithilfe eines Entscheidungsbaumes
# Parameter: 
# model: Der Entscheidungsbaum
# data: Trainingsdaten
# Rückgabe: 
# Eine Liste von Labeln
def einfaches_klassifizieren(model, data):
    labels = model.predict(data)
    return labels


# Traversiert einen Baum und gibt das "index"-te Element zurück
# Parameter:
# root: Der zu traversierende Baum
# index: der Knoten, der zurückgegeben werden soll
# Rückgabe: 
# Ein Knoten der Klasse Node
def traverseTree(root, index):
    global nodeTraverseCounter
    nodeTraverseCounter = index
    return root.traverse()


# Wrapper für einen Baum der Klasse Node
class DecisionTree:
    def __init__(self, root):
        self.root = root

    # Klassifiziert Daten mithilfe des Baumes
    # Parameter:
    # data: Trainingsdaten
    # Rückgabe:
    # Eine Liste von Labeln
    def predict(self, data):
        labels = []
        for datum in data:
            labels.append(self.root.classifyDatum(datum))
        return labels


# Klasse für einen Baum
class Node:
    def __init__(self, parent, threshold, attribute, label, isLeaf):
        self.threshold = threshold
        self.attribute = attribute
        self.label = label
        self.isLeaf = isLeaf
        self.children = []
        self.size = 1
        self.parent = parent
        if (self.parent != None):
            self.parent.updateSize()

    # Klassifiziert ein Datum. Dazu wird entweder das Label dieses Knoten zurückgegeben, oder
    # Anhand des Attributs und des thresholds (splits) diese Funktion rekursiv bei dem entsprechenden
    # Kindknoten aufgerufen
    # Parameter:
    # datum: Das zu klassifizierende Datum
    # Rückgabe
    # Das entsprechende Label
    def classifyDatum(self, datum):
        if (not self.isLeaf):
            if (len(self.children) == 1):
                return self.children[0].classifyDatum(datum)
            val = datum[self.attribute]
            if (val < self.threshold):
                return self.children[0].classifyDatum(datum)
            else:
                return self.children[1].classifyDatum(datum)
        else:
            return self.label

    # Updated die Größe des Baumes, wenn sich etwas an den Kindern verändert. Dies wird auch rekursiv
    # auf den Eltern aufgerufen
    def updateSize(self):
        self.size = 1
        for child in self.children:
            if (child != None):
                self.size += child.size
        if (self.parent != None):
            self.parent.updateSize()

    # Fügt einen Kindknoten zu dem baum hinzu
    # Parameter:
    # child: Hinzuzufügender Kindknoten
    def addChild(self, child):
        self.children.append(child)
        self.updateSize()

    # Gibt eine komplette Kopie des Knotens zurück (mit Verweis auf Kopien seiner Kindknoten)
    # Parameter:
    # newParent: Kopierter, neuer Elternknoten
    def deepCopy(self, newParent):
        copy = Node(newParent, self.threshold, self.attribute, self.label, self.isLeaf)
        copy.size = self.size
        for child in self.children:
            if (child != None):
                copy.addChild(child.deepCopy(copy))
        return copy

    # Traversiert einen Baum und gibt das Element zurück, falls der nodeTraverseCounter 0 erreicht
    # Rückgabe:
    # Ein Knoten der Klasse Node
    def traverse(self):
        global nodeTraverseCounter
        if (nodeTraverseCounter == 0):
            return self
        if (self.children == []):
            return None
        for child in self.children:
            nodeTraverseCounter -= 1
            if (child != None):
                node = child.traverse()
                if (node != None):
                    return node
        return None


# Beispielnutzung
# from sklearn import datasets
# rawData = datasets.make_blobs(1000,20,10)
# data = rawData[0]
# labels = rawData[1]
# model = einfaches_klassifizieren_training(data, labels)
# labels_predicted = einfaches_klassifizieren(model, data)


def do_stuff(dataset, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    gen_size = kwargs['gensize']
    max_generations = kwargs['generations']
    tree_size_bias = kwargs['treesizebias']

    t = time()  # get labels for test data
    model = einfaches_klassifizieren_training(training_data, training_label, generationSize=gen_size,
                                              maxGenerations=max_generations, treeSizeBias=tree_size_bias)
    training_prediction = einfaches_klassifizieren(model, training_data)
    training_time = time() - t

    t = time()  # get labels for test data
    test_prediction = einfaches_klassifizieren(model, test_data)
    time_test = time() - t

    t = time()  # get labels for validation data
    validation_prediction = einfaches_klassifizieren(model, validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result = {'score_test': score_test,  # Kriegen Studis zu sehen
              'score_validation': score_validation,  # kommt in die Highscore-Tabelle
              'training_labels': training_prediction,
              'test_labels': test_prediction,
              'validation_labels': validation_prediction,
              'extra_scores_test': {"time": "%2.3fms" % (time_test * 1000)},
              'extra_scores_validation': {"time": "%2.3fms" % (time_validation * 1000)},
              'message': 'Training time: %2.3fms' % (training_time * 1000),
              'pictures': [],
              'success': True}
    return result
