#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
working_dir = os.path.dirname(os.path.abspath(__file__))

from subprocess import PIPE, Popen
from time import time
from sklearn.metrics import accuracy_score
from scipy.io.arff import loadarff


def do_stuff(data, iterations=1, **_):

    error = {'score_test':None, # Kriegen Studis zu sehen
           'score_validation':None, # kommt in die Highscore-Tabelle
           'training_labels':None,
           'test_labels':None,
           'validation_labels':None,
           'extra_scores_test':None,
           'extra_scores_validation':None,
           'pictures': None,
           'success': False}

    training_file, test_file, validation_file = data

    result = {'pictures': [],
              'success': True}

    start = time()

    for file, field in zip([training_file, test_file, validation_file], ["training_labels", "test_labels", "validation_labels"]):
        command = ["java", "-Xmx1g", "-jar",  working_dir+"/../../lib/adtree.jar", training_file, file, str(iterations)]
        print(" ".join(command))
        proc = Popen(command, stdout=PIPE, stderr=PIPE)
        output, err = proc.communicate()
        output = output.decode()

        if proc.returncode != 0:
            error['message'] = "Something went wrong when executing the jar file! \n\nCommand:\n({})\n\n---\n{}".format(" ".join(command), err)
            return error

        lines = output.split("\n")[2:]  # adtree.jar produces two annoying lines :-(
        prediction = list(map(int, lines[:-1]))
        time_taken = time() - start
        result[field] = prediction

        arff, _ = loadarff(file)
        labels = list(map(int, [x[-1] for x in arff]))

        if field == "training_labels":
            result['message'] = 'Training time: %2.3fms' % (time_taken * 1000)
        elif field == "test_labels":
            result['extra_scores_test'] = {"time":"%2.3fms"%(time_taken*1000)}
            result["score_test"] = accuracy_score(labels, prediction)
        elif field == "validation_labels":
            result['extra_scores_validation'] = {"time":"%2.3fms"%(time_taken*1000)}
            result["score_validation"] = accuracy_score(labels, prediction)

    return result
