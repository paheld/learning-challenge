#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import pandas as pd
from time import time

from scipy.spatial import distance
from sklearn.metrics import accuracy_score
from matplotlib import pyplot as plt
import numpy as np
import math

# Programmierprojekt: Erweiterung LDA
# Annahme: Label und features numerisch
# grundsätzlicher Aufbau folgt dem Beispiel auf der Vorlesungsseite

# einfaches_klassifizieren_training: trainiert einen Klassifikator (LDA)
# je nach Belegung des Parameters art werden die eingegebenen Daten (z.B. x1, x2) zuvor ergänzt:
#    keine Angabe/art = lda: keine Ergänzung
#    art = qda: Ergänzung der Daten um Quadrat der Daten (x1,x2,x1²,x2²)
#    art = cos: Ergänzung der cos-Werte der Daten (x1,x2,cos(x1),cos(x2))
#    art = log: Ergänzung der log-Werte der Daten (x1,x2,log(x1),log(x2))
    
def einfaches_klassifizieren_training(data, label, art = 'lda'):

 # Ergänzung der Daten
    
    f = len(data[1])
    
    if art == 'qda':
        data2 = []
        for j in range(len(data)):
            neu = np.zeros(len(data[1]))
            for k in range(len(data[1])):
                neu[k]= data[j,k]*data[j,k]
            data2 = np.append([data2],[data[j],neu])
        data2 = data2.reshape(((len(data)),f*2))

        
    if art == 'cos':
        data2 = []
        for j in range(len(data)):
            neu = np.zeros(len(data[1]))
            for k in range(len(data[1])):
                neu[k]= math.cos(data[j,k])
            data2 = np.append([data2],[data[j],neu])
        data2 = data2.reshape(((len(data)),f*2))

        
    if art == 'log':
        data2 = []
        for j in range(len(data)):
            neu = np.zeros(len(data[1]))
            for k in range(len(data[1])):
                neu[k]= math.log(data[j,k])
            data2 = np.append([data2],[data[j],neu])
        data2 = data2.reshape(((len(data)),f*2))

    if art!='lda':    
        data=data2      
    f = len(data[1])
    klassenanzahl = len(list(set(label)))
 
# Durchführung LDA 
    
    mean_vectors = []
    for cl in np.unique(label): # range(1,(klassenanzahl+1)):
        mean_vectors.append(np.mean(data[label==cl], axis=0))
        # print('Mean Vector class %s: %s\n' %(cl, mean_vectors[cl-1]))

    S_W = np.zeros((f,f))
    for cl,mv in zip(range(1,f), mean_vectors):
        class_sc_mat = np.zeros((f,f))                 
        for row in data[label == cl]:
            row = row.reshape(f,1)
            mv = mv.reshape(f,1)
            class_sc_mat += (row-mv).dot((row-mv).T)
            S_W += class_sc_mat
    # print('within-class Scatter Matrix:\n', S_W)
                    
    overall_mean = np.mean(data, axis=0)

    S_B = np.zeros((f,f))
    for i,mean_vec in enumerate(mean_vectors):  
        n = data[label==i+1,:].shape[0]
        mean_vec = mean_vec.reshape(f,1) 
        overall_mean = overall_mean.reshape(f,1) 
        S_B += n * (mean_vec - overall_mean).dot((mean_vec - overall_mean).T)
    # print('between-class Scatter Matrix:\n', S_B)

    eig_vals,eig_vecs = np.linalg.eig(S_W.dot(S_B))
 
    
    modell = eig_vecs,eig_vals,art,mean_vectors
    
    return modell
    
# einfaches_klassifizieren: nutzt das zuvor trainierte Modell um weitere Daten zu klassifizieren und gibt ein Label zurück
# daten: einzelner zu klassifizierender Datensatz

def einfaches_klassifizieren(modell, daten):
    
 # "Aufteilen" des übergebenen Modells in mehrere Variablen   
    eig_vals = modell[1]
    eig_vecs = modell[0]
    art = modell[2]
    mean_vecs = modell[3]

# ggf. Testdatensatz wie Trainingsdaten ergänzen

    if art == 'qda':
        neu = np.zeros(len(daten[0]))
        for k in range(len(daten[0])):
            neu[k]= daten[0,k]*daten[0,k]
        daten = np.append(daten,neu)
        #print('\ndaten:\n', daten)
        
        
    if art == 'cos':
        neu = np.zeros(len(daten[0]))
        for k in range(len(daten[0])):
            neu[k]= math.cos(daten[0,k])
        daten = np.append(daten,neu)
        #print('\ndaten:\n', daten)
        
    if art == 'log':
        neu = np.zeros(len(daten[0]))
        for k in range(len(daten[0])):
            neu[k]= math.log(daten[0,k])
        daten = np.append(daten,neu)
        #print('\ndaten:\n', daten)
        
# Label bestimmen 
   
    dst_vec = np.zeros(2)
    for i in range(2):
        mean_sc = mean_vecs[i-1]   
        dst = distance.euclidean(daten,mean_sc)
        dst_vec[i-1]=dst
    label=dst_vec.argmin()+1
    
    return label
    
#Quellen:
    #Literatur aus Seminar
        #T. Hastie et al., The Elements of Statistical Learning, Second Edition
    #weitere Quellen
       # http://scikit-learn.org/0.16/modules/generated/sklearn.lda.LDA.html
       # http://sebastianraschka.com/Articles/2014_python_lda.html
       # http://www.saedsayad.com/lda.htm


def do_stuff(dataset, art='lda', *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}


    t = time() # Train the LDA
    model = einfaches_klassifizieren_training(training_data, training_label, art)
    training_time = time() - t

    training_prediction = [einfaches_klassifizieren(model, td) for td in training_data]

    t = time() # get labels for test data
    test_prediction = [einfaches_klassifizieren(model, td) for td in test_data]
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = [einfaches_klassifizieren(model, td) for td in validation_data]
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': [],
               'success': True}
    return result