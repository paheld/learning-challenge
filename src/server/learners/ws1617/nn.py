#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators

import logging

from sys import argv
import numpy as np
from numpy import double, argmax
from sklearn import preprocessing
from sklearn.metrics import accuracy_score

logger = logging.getLogger(__name__)
import re
from time import time


def tanh(X, deriv=False):
    if(deriv):
        return 1 - np.tanh(X) * np.tanh(X)
    return np.tanh(X)


def exp_func(x, deriv=False):
    if(deriv==True):
        return (x*(1-x))

    return 1/(1+np.exp(-x))


def appElem(_array, _elem):
    #print (np.ndim(_array))
    #print(np.ndim(_elem))
    return np.array([[elem for elem in np.append(array, np.array([_elem]))] for array in _array])


class NN:

    syn = list()
    buffer = list()
    delta = list()
    func = 0
    epsilon = 0.0
    debug = 0


    def __init__(self,tData,tLables,hidden,function = tanh, epsilon = 0.01, print_error = False):

        eingang = len(tData[0])+1
        ausgang = len(np.unique(tLables))
        self.func = function
        self.epsilon = epsilon
        self.debug=print_error
        #Random init für Synapsen/Gewichtsmatrizen
        self.syn.append(np.random.random((eingang ,hidden[0])))
        for i in range(0,len(hidden)-1):
            self.syn.append(np.random.random((hidden[i],hidden[i+1])))
        self.syn.append(np.random.random((hidden[-1],ausgang)))

        #Training
        for i in range(50001):
            self.feed_forward(tData)
            self.back_prop(tLables)


            #Debug
            if self.debug and (i%5000) == 0:
                #Umwandlung Int => Array für Fehlerberechnung mit mehreren Ausgängen
                Y = np.zeros((len(tLables),len(np.unique(tLables))))
                for i in range(len(tLables)):
                    Y[i][int(tLables[i])] = 1
                print ('Error: ' + str(np.mean(np.abs(Y - self.buffer[-1]))))
                '''for i in range(len(Y)):
                    _err = Y[i] - self.buffer[-1][i]
                    for elem in _err:
                        print('{0:.7f}'.format(elem), end='   ')
                    print(tLables[i])
               '''


    def predict(self,data):
        self.feed_forward(data)
        temp = argmax(self.buffer[-1],1)
        return temp

    def feed_forward(self,data):
        self.buffer = list()#reset
        data = appElem(data,1) #Add 1 for bias
        self.buffer.append(data)#Eingabewert
        for layer in self.syn:
            self.buffer.append(self.func(np.dot(self.buffer[-1],layer)))
        pass

    def back_prop(self,lables):
        self.delta = list() #Reset

        #Umwandlung Int => Array für Fehlerberechnung mit mehreren Ausgängen
        Y = np.zeros((len(lables),len(np.unique(lables))))
        for i in range(len(lables)):
            Y[i][int(lables[i])] = 1

        #Backprop
        error = Y - self.buffer[-1]
        self.delta.append(self.func(self.buffer[-1] , deriv=True)* error)

        for layer in range(len(self.syn)-1,0,-1):
            error = self.delta[-1].dot(self.syn[layer].T)
            self.delta.append(self.func(self.buffer[layer], deriv=True)*error)


        #Update
        self.delta.reverse()
        for i in range(len(self.delta)):
            temp = self.buffer[i].T.dot(self.delta[i])
            self.syn[i] += temp * self.epsilon

        pass



class Arff:

    features = 0
    lables = 0

    def __init__(self, document):
        #TODO: Parse other components

        #Read file into string and drop empty lines
        file = open(document)
        file = str(file.read()).split(sep='\n')
        while '' in file:
            file.remove('')

        #Split auf Headerteil und Datenteil
        header_part = file[:file.index("@DATA")]
        data_part = file[file.index("@DATA")+1:]

        #Split Data part in data and lables
        for i in range(len(data_part)):
            data_part[i] = data_part[i].split(',')
        data_part = np.array(data_part).astype(double)
        #print(data_part)
        self.lables = data_part[:,-1]
        self.features = data_part[:,:-1]

    def getData(self):
        return self.features,self.lables


def auswertung(pred,lab):
    richtig,falsch = 0,0
    for i in range(len(lab)):
        if(pred[i]==lab[i]):
            richtig+=1
        else:
            falsch+=1

    print('Richtig klassifiziert: {0}\nFalsch klassifiziert: {1} '.format(richtig,falsch))


def do_stuff(dataset, neuron_list="10,10,10", epsilon=0.01, *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    neuron_list = re.sub(r"[^0-9,]", "", neuron_list)
    neuron_list = re.sub(r",+", ",", neuron_list) or "1"
    hLayers = list(map(int, neuron_list.split(",")))

    error = {'score_test': None,  # Kriegen Studis zu sehen
             'score_validation': None,  # kommt in die Highscore-Tabelle
             'training_labels': None,
             'test_labels': None,
             'validation_labels': None,
             'extra_scores_test': None,
             'extra_scores_validation': None,
             'pictures': None,
             'success': False}


    t = time()
    data = training_data
    lable = training_label

    # Map label to integers
    lableMap = np.unique(lable)
    for l in range(len(lable)):
        for i in range(len(lableMap)):
            if (lable[l] == lableMap[i]):
                lable[l] = i
                # Init/Train the NN
    data = preprocessing.normalize(data)
    nn = NN(data, lable, hLayers, epsilon=epsilon)
    training_time = time() - t

    training_prediction = nn.predict(data)

    t = time()  # get labels for test data
    data = preprocessing.normalize(test_data)
    test_prediction = nn.predict(data)
    time_test = time() - t

    t = time()  # get labels for validation data
    data = preprocessing.normalize(validation_data)
    validation_prediction = nn.predict(data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result = {'score_test': score_test,  # Kriegen Studis zu sehen
              'score_validation': score_validation,  # kommt in die Highscore-Tabelle
              'training_labels': training_prediction,
              'test_labels': test_prediction,
              'validation_labels': validation_prediction,
              'extra_scores_test': {"time": "%2.3fms" % (time_test * 1000)},
              'extra_scores_validation': {"time": "%2.3fms" % (time_validation * 1000)},
              'message': 'Training time: %2.3fms' % (training_time * 1000),
              'pictures': [],
              'success': True}
    return result


if __name__ == '__main__':
    #print('Begin')

    #Get Number of hidden Nodes
    if(len(argv)> 3):
        hLayers = argv[3].split(',')
        #hLayers[0] = hLayers[0][1:]
        #hLayers[-1] = hLayers[-1][:-1]
        for i in range(len(hLayers)):
            hLayers[i] = int(hLayers[i])
    else:
        hLayers = [10,10,10]


    #Parse Lernrate
    if(len(argv)>4):
        lernrate = np.double(argv[4])
    else:
        lernrate = 0.01

    if len(argv)>5:
        pErr = True
    else:
        pErr = False

    #Lade ARFF files
    file1 = Arff(argv[1])
    file2 = Arff(argv[2])
    data,lable = file1.getData()

    #Map label to integers
    lableMap = np.unique(lable)
    for l in range(len(lable)):
        for i in range(len(lableMap)):
            if(lable[l] == lableMap[i]):
                lable[l] = i

    # Init/Train the NN
    preprocessing.normalize(data)
    nn = NN(data,lable,hLayers,epsilon=lernrate, print_error=pErr)

    #Bewerte Performance
    '''print('Trainingsset: ')
    for l in range(len(lable)):
        lable[l] = lableMap[int(lable[l])]
    auswertung(nn.predict(data), lable)

    data,lable = file2.getData()
    print('Testset: ')
    auswertung(nn.predict(data), lable)'''

    #Gebe Lable für Testset aus
    data = file2.features
    for l in nn.predict(data):
        print(l)

    #print('End')
