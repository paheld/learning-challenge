# This python file implements the main control flow of the VFDT algorithm.

import math
import operator
import learners.VfdtNode as t

# This is a class used as a wrapper around the VfdtTree module to learn a tree and to store it.
# This class also provides functionality to apply the learnt tree on a given data set and to
# print the tree to standard out.
class VfdtAlgorithm:

    # Constructor of a VfdtAlgorithm instance.
    #
    # numPointsBeforeQualityComputation: Number of training instances that have to have arrived in a node between each
    #                                    calculation of split quality. See VFDT paper for further explanation. This
    #                                    value has to be at least 1, but consider: This mode will be not active for 1
    #                                    as input!
    def __init__(self, numPointsBeforeQualityComputation):
        self.tree = None
        # Set minimal number of instances that have to be in a leaf to (re)consider splitting it.
        self.numPointsBeforeQualityComputation = numPointsBeforeQualityComputation
        return

    # This method implements the VFDT learning algorithm.
    # Bad parameters are expected to be caught before this method is called.
    #
    # trainingData:             Numpy matrix of data instances that shall be used for training. Rows are instances,
    #                           cols are attributes. Data is expected to be discretized such that only
    #                           numberOfAttributeValues different values for each attribute are possible. Attributes
    #                           values are expected to be in {0,...,numberOfAttributeValues-1} . This kind of data
    #                           can be achieved by nBin-preprocessing of the data before handing it over to this
    #                           function. The last column is expected to be the class label.
    # numberOfAttributeValues:  This is the number of possible values of each attribute. Attributes are expected to
    #                           be discretized as described for parameter trainingData in the main python file.
    #                           Must be at least 1.
    # delta:                    Accepted error that a split is not done for the best possible attribute. It is to be
    #                           in (0,1]. The smaller, the later decisions / splits are done and therefore the
    #                           longer the algorithm needs to adapt the model. If this parameter is not set
    #                           appropriately, an error message is returned.
    # tieBreakingThreshold:     Parameter to enforce splits if the algorithm can't settle down on deciding what
    #                           attribute is to be chosen for splitting a node if several attributes are equally
    #                           strong in their split quality. This is done to prevent the algorithm from taking too
    #                           much time / instances to decide. This parameter can have any value but consider:
    #                           values <= 0 will switch off tie breaking. See paper "Tie Breaking in Hoeffding
    #                           Trees" by Geoffrey Holmes, Richard Kirkby, and Bernhard Pfahringer for further
    #                           details on the issue of tie breaking.
    # qualityMeasure:           What measure should be used for calculation of split quality?
    #
    # return: None
    def trainModel(self, trainingData, numberOfAttributeValues, delta, tieBreakingThreshold, qualityMeasure):
        # Initiate the tree
        self.tree = t.VfdtNode(len(trainingData[0]) - 1, numberOfAttributeValues)

        # Add instances to the tree and test for split ability of relevant nodes.
        for instance in range(0, len(trainingData)):
            node = self.tree.addInstance(trainingData[instance], self.numPointsBeforeQualityComputation)
            # If there is a leaf we might have to analyse it's split quality.
            if node is None:
                continue

            # Compute split qualities of each attribute
            qualities = {}
            for attribute in node.allowedSplitAttributes:
                quality = t.VfdtNode.computeSplitQuality(qualityMeasure, node, attribute)
                qualities[str(attribute)] = quality

            # Sort split qualities
            sortedQualities = sorted(qualities.items(), key = operator.itemgetter(1))

            # If only one split attribute is left, split node on it.
            # (zero remaining split attributes are not possible)!
            if len(qualities) == 1:
                node.split(int(sortedQualities[0][0]), sortedQualities[0][1])
                continue

            ## Compute Hoeffding Bound ##
            # Compute number of classes in node that might be split
            numberOfClasses = len(node.computeClassFrequencies())
            # Compute number of instances in node that might be split
            numberOfInstances = node.computeContainedInstances()
            hoeffdingBound = self.computeHoeffding(numberOfClasses, delta, numberOfInstances, qualityMeasure)

            # More than one possible split attributes are left.
            # Calculate the best attribute with smallest index
            indexOfLastQuality = len(sortedQualities)-1
            bestSplitQuality = sortedQualities[indexOfLastQuality][1]
            smallestAttributeIndex = int(sortedQualities[indexOfLastQuality][0])
            for attribute in range(indexOfLastQuality - 1, -1, -1):
                currentSplitQuality = sortedQualities[attribute][1]
                currentAttributeIndex = int(sortedQualities[attribute][0])
                # If current split quality is smaller than best split quality,
                # this and following attribute/s are not relevant.
                if currentSplitQuality < bestSplitQuality:
                    break

                # If current split quality is the same as the best split quality,
                # # check the index of the current attribute. If the index is smaller
                # than the former known smallest index, replace it now.
                if smallestAttributeIndex > currentAttributeIndex:
                    smallestAttributeIndex = currentAttributeIndex

            # Compute quality difference with respect to the known smallest attribute index.
            # The index of the chosen second best attribute doesn't really matter, but it's split quality does.
            secondBestAttributeIndex = int(sortedQualities[indexOfLastQuality][0])
            if secondBestAttributeIndex == smallestAttributeIndex:
                secondBestAttributeIndex = int(sortedQualities[indexOfLastQuality-1][0])
            qualityDifference = sortedQualities[smallestAttributeIndex][1] - sortedQualities[secondBestAttributeIndex][1]
            hoeffdingFulfilled = qualityDifference > hoeffdingBound
            # If the best split attribute fulfills Hoeffding Bound or tie is detected split node on it.
            if hoeffdingFulfilled or hoeffdingBound < tieBreakingThreshold: # This is the "logical compression" of "A or (not A and B)".
                node.split(smallestAttributeIndex, sortedQualities[smallestAttributeIndex][1])

        return

    # This method computes the Hoeffding Bound.
    # Bad parameters are expected to be caught before this method is called.
    #
    # numberOfClasses:      The number of classes that are in the node for which the Hoeffding Bound is calculated for. This
    #                       can't be 0 (impossible by design, if inserted training data is ok).
    # delta:                Accepted error that a split is not done for the best possible attribute. It is to be
    #                       in (0,1]. The smaller, the later decisions / splits are done and therefore the
    #                       longer the algorithm needs to adapt the model. If this parameter is not set
    #                       appropriately, an error message is returned.
    # numberOfInstances:    The number of training instances that have been placed so far into the node the Hoeffding
    #                       Bound is calculated for.
    # qualityMeasure:       What measure should be used for calculation of split quality?
    #
    # return: Hoeffding Bound as floating point.
    @staticmethod
    def computeHoeffding(numberOfClasses, delta, numberOfInstances, qualityMeasure):
        R = None
        # Range for Information Gain
        if qualityMeasure == t.VfdtNode.infoGainName:
            # numberOfClasses can't be zero! There is at least one.
            R = math.log(numberOfClasses, 2)
        # Range for Gini Coefficient
        elif qualityMeasure == t.VfdtNode.giniCoeffName:
            R = 1.0
        # numberOfInstances can't be zero for this function will only be called if there have been instances in a leaf.
        # delta can't be zero for it is forbidden when starting up algorithm.
        return math.sqrt((R*R*math.log(1/delta, math.e)) / (2*numberOfInstances))

    # Applies the learnt model on a given evaluation set. A model must be learnt before this method is to be called,
    # otherwise it will return an error message.
    # Bad parameters are expected to be caught before this method is called.
    #
    # evaluationData: Numpy matrix of evaluation data. Rows are instances, cols are attributes. Data is
    #                 expected to be discretized as described for trainingData. There is no column of
    #                 class labels expected in this matrix ;)
    #
    # return: Returns a list of class labels for each instance in evaluationData in the same ordering as evaluationData.
    def applyModel(self, evaluationData):
        if self.tree is None:
            return "No model is learnt but a model is tried to be applied!"

        # Classify instances one by one
        classification = [None for i in range(0, len(evaluationData))]
        for instance in range(0, len(evaluationData)):
            classification[instance] = self.tree.classifyInstance(evaluationData[instance])
        return classification

    # Print the decision tree to standard out (delegating to VfdtNode.printTree()).
    #
    # return: None
    def printTree(self):
        self.tree.printTree()
        return