__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

import settings
from sklearn.metrics import accuracy_score
from time import time as time
from subprocess import PIPE, Popen

structure_learners = {#'genetic':'weka.classifiers.bayes.net.search.local.GeneticSearch',
                      'hill':'weka.classifiers.bayes.net.search.local.HillClimber',
                      'K2':'weka.classifiers.bayes.net.search.local.K2',
                      #'lagd':'weka.classifiers.bayes.net.search.local.LAGDHillClimber',
                      #'rep':'weka.classifiers.bayes.net.search.local.RepeatedHillClimber',
                      'tan':'weka.classifiers.bayes.net.search.local.TAN',
                      'tabu':'weka.classifiers.bayes.net.search.local.TabuSearch',
                      #'annealing':'weka.classifiers.bayes.net.search.local.SimulatedAnnealing',
                      'score':'weka.classifiers.bayes.net.search.local.LocalScoreSearchAlgorithm'}

scorer = {
    "entropy": "ENTROPY",
    "bayes": "BAYES",
    "mdl": "MDL",
    "aic": "AIC"
}

def do_stuff(dataset, *args, **kwargs):
    #print dataset
    training_file, test_file, validation_file = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    if 'structure' in kwargs and kwargs['structure'] in structure_learners.keys():
        sl = structure_learners[kwargs['structure']]

    if not "structure_maxparents" in kwargs:
        kwargs["structure_maxparents"] = 2

    add_params = []
    add_params += ['--']
    if kwargs['structure'] == "K2":
        if kwargs['structure_defaultstructure'] == 'empty':
            add_params += ["-N"]
        add_params += ['-P', str(kwargs['structure_maxparents'])]
    add_params += ['-S', scorer[kwargs['scorer']]]

    t = time()
    command_train = ["java", "-Xmx1g", "-cp",  settings.BACKEND_WEKA_PATH+"weka.jar", "weka.classifiers.bayes.BayesNet"]
    command_train += ["-t", training_file, "-T", training_file, "-p", "0"]
    if not ('use_adtree' in kwargs and kwargs['use_adtree'] == 'No'):
        command_train += ["-D"] if not 'use_adtree' in kwargs else []

    command_train += ["-Q", sl] + add_params
    command_train += ["-E", "weka.classifiers.bayes.net.estimate.SimpleEstimator"]# -- -A 1.0"

    print(" ".join(command_train))

    proc = Popen(command_train, stdout=PIPE, stderr=PIPE)
    output, err = proc.communicate()
    if proc.returncode != 0:
        error['message'] = err
        return error
    lines = output.split("\n")
    prediction_train = []
    for line in lines:
        res = line.split(None)
        if len(res) == 4 or len(res) == 5 and res[0] != 'inst#':
            prediction_train.append( res[2].split(":")[1] )
    training_time = time() - t

    t = time()
    command_test = ["java", "-Xms1g", "-Xmx4g", "-cp",  settings.BACKEND_WEKA_PATH+"weka.jar", "weka.classifiers.bayes.BayesNet"]
    command_test += ["-t", training_file, "-T", test_file, "-p", "0"]
    if not ('use_adtree' in kwargs and kwargs['use_adtree'] == 'No'):
        command_test += ["-D"] if not 'use_adtree' in kwargs else []
    command_test += ["-Q", sl] + add_params
    command_test += ["-E", "weka.classifiers.bayes.net.estimate.SimpleEstimator"]# -- -A 1.0"

    proc = Popen(command_test, stdout=PIPE, stderr=PIPE)
    output, err = proc.communicate()
    if proc.returncode != 0:
        error['message'] = err
        return error

    lines = output.split("\n")
    prediction_test = []
    test_label = []
    for line in lines:
        res = line.split(None)
        if len(res) == 4 or len(res) == 5 and res[0] != 'inst#':
            test_label.append( res[1].split(":")[1] )
            prediction_test.append( res[2].split(":")[1] )
    time_test = time() - t

    t = time()
    command_validation = ["java", "-Xmx1g", "-cp",  settings.BACKEND_WEKA_PATH+"weka.jar", "weka.classifiers.bayes.BayesNet"]
    command_validation += ["-t", training_file, "-T", validation_file, "-p", "0"]
    if not ('use_adtree' in kwargs and kwargs['use_adtree'] == 'No'):
        command_validation += ["-D"] if not 'use_adtree' in kwargs else []
    command_validation += ["-Q", sl] + add_params
    command_validation += ["-E", "weka.classifiers.bayes.net.estimate.SimpleEstimator"]# -- -A 1.0"

    proc = Popen(command_validation, stdout=PIPE, stderr=PIPE)
    output, err = proc.communicate()
    if proc.returncode != 0:
        error['message'] = err
        return error

    lines = output.split("\n")
    prediction_validation = []
    validation_label = []
    for line in lines:
        res = line.split(None)
        if len(res) == 4 or len(res) == 5 and res[0] != 'inst#':
            validation_label.append( res[1].split(":")[1] )
            prediction_validation.append( res[2].split(":")[1] )
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, prediction_test)
    score_validation = accuracy_score(validation_label, prediction_validation)

    command_train = ["java", "-Xmx1g", "-cp",  settings.BACKEND_WEKA_PATH+"weka.jar", "weka.classifiers.bayes.BayesNet"]
    command_train += ["-t", training_file, "-T", training_file]
    if not ('use_adtree' in kwargs and kwargs['use_adtree'] == 'No'):
        command_train += ["-D"] if not 'use_adtree' in kwargs else []

    command_train += ["-Q", sl] + add_params
    command_train += ["-E", "weka.classifiers.bayes.net.estimate.SimpleEstimator"]# -- -A 1.0"

    proc = Popen(command_validation, stdout=PIPE, stderr=PIPE)
    output, err = proc.communicate()
    if proc.returncode != 0:
        error['message'] = err
        return error

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':[int(x) for x in prediction_train],
               'test_labels':[int(x) for x in prediction_test],
               'validation_labels':[int(x) for x in prediction_validation],
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000)+"\n\n"+output+"\n\nSTDERR:\n"+err,
               'pictures': [],
               'success': True}
    return result

if __name__ == "__main__":
    #ds = ("C:\Users\cbraune\digits_cla.arff", "C:\Users\cbraune\Digits-test.arff", "C:\Users\cbraune\digits-validation.arff")
    #ds = ("C:\Users\cbraune\ecoli-training.arff", "C:\Users\cbraune\e.coli.arff", "C:\Users\cbraune\e.coli.arff")

    #keywords = {"structure":"tabu", "structure_defaultstructure":"NaiveBayes", "structure_maxparents":"1", "scorer":"entropy"}
    #result = do_stuff(ds, **keywords)
    #for k in result:
    #    print k, result[k]
    pass