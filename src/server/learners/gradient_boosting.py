import numpy as np
import sklearn
from sklearn.tree import DecisionTreeClassifier
from scipy.optimize import minimize, minimize_scalar
from time import time
from sklearn.metrics import accuracy_score
import warnings

warnings.filterwarnings("ignore",category=DeprecationWarning)


'''
   Zu minimierende Funktion 1
'''
def GetMinFunc(x, y, h):
    return lambda b: np.sum([(y[i] - b * h(x[i])) ** 2 for i in range(0, np.alen(y))])

'''
   Zu minimierende Funktion 2
'''
def GetMinFunc2(x, y, h, lossF, F):
    return lambda p: np.sum([lossF(y[i], F([x[i]])[0] + p * h(x[i])[0]) for i in range(0, np.alen(y))])

'''
   Erzeugt eine Funktion die alle Funktionswerte in F aufsummiert
   F: Ein Array aus Funktionen ((R^d)^n -> R^n, wobei n die Anzahl der Datenpunkte sind und d die Anzahl der Datenpunkte)
      Die Funktionen "labeln" ein Uebergebenes Array (Die Ergebnisse sind Real-Values)
   return: Linearkombination von F
'''
def GetFunction(F):
    return lambda x: np.sum([f(x) for f in F], axis=0)

'''
   Liefert eine Componente der Linearkombination fuer das Boosting
   Notwendig damit die Variablen pmin und h im Stack liegen und nicht ueberschrieben werden koennen

   x ist ein Array von Datenpunkten
'''
def GetLinierComponent(pmin, h):
    return lambda x: pmin * h(x)

'''
    Liefert F0 fuer die Linearkombination fuer jeden Datenpunkt
'''
def GetInitFunction(c):
    return lambda x: [c for i in x]

def GetF0Func(lossFunc, y):
    return lambda p: np.sum([lossFunc(yi, p) for yi in y])

def GradientBoostingTraining(data, label, weakLearners, lossF, F0, M):
    # Trainiert die schwachen Lerner, Array der label-Funktionen der Schwachen Lerner
    learners = [learner.fit(data, label).predict for learner in weakLearners]
    if(F0 == -3.141592653589793):
        func = GetF0Func(lossF[0], label)
        res = minimize(func, [0], method='nelder-mead')
        F0 = res.x[0]
    anz = np.alen(data);
    F = [GetInitFunction(F0)];   
    for m in range(1, M):
        # Berechnen der Fehlerwerte fuer mit der aktuellen Labelfunktion
        y = [-lossF[1](label[i], GetFunction(F)([data[i]])[0]) for i in range(0, np.alen(label))]
        learnersMin = None
        pmin = 0
        min = float("inf")
        for h in learners:            
            toMinFunc = GetMinFunc(data, y, h)
            # Minimieren der alten Fehlerwerte
            res = minimize(toMinFunc, [0], method='nelder-mead')
            b = res.x[0]
            val = toMinFunc(b)
            if(val < min):
                min = val
                pmin = b
                learnersMin = h
        
        toMinFunc = GetMinFunc2(data, label, h, lossF[0], GetFunction(F))
        # Minimieren der neuen Fehlerwerte (pmin so bestimmen, dass der Fehler so klein wie moeglich ist)
        res = minimize(toMinFunc, [0], method='nelder-mead')
        pmin = res.x[0]    
        # neuen Werte der Boosting Funktion hinzufuegen
        F.append(GetLinierComponent(pmin, learnersMin))
    return GetFunction(F);

'''
    Labelt uebergebene Daten
    model: das Modell das von GradientBoostingTraining zurueckgegeben wird
    data: die zu labelnen Daten
    return "real-value" label
'''
def GradientBoosting(model, data):
    return model(data)
    


'''
    Erzeugt die schwachen Lerner
    J anzahl max Blaetter
'''
def GetWeakLearner(J):
    return [DecisionTreeClassifier(max_depth=i) for i in range(2, J + 1)]

'''
   Liefert das Tupel (d,l), sodass l die labels sind und d die Datenpunkte
   Dabei werden nur Datenpunke der Klasse l1, l2 zurueckgeben
   Label:   1: data gehoert zur 1. Klasse, -1 data gehoert zur zweiten Klasse
'''
def GetClasses(data, label, l1, l2):
    arr = np.array([])    
    d = []
    l = []
    for i in range(0, np.alen(data)):
        if(label[i] == l1):
            l.append(1)
            d.append(data[i])
        else:
            if(label[i] == l2):
                l.append(-1)
                d.append(data[i])
    return (d, l)

'''
   Liefert ein Modell zum Klassifizieren, dabei wird One vs One benutzt, um das Mehrklassenproblem zu loesen
   Dabei wird fuer JEDES Paar Klassen ein Modell erstellt.
   data: Trainingsdaten
   label: label der Daten {-1,1}
   h: Array der Lerner
   lossF: die loss function und deren Ableitung
   F0: F0
   M: Anzahl durchlaeufe
   return: das Modell zum Klassifizieren
'''
def OneVsOneTraining(data, label, h, lossF, F0, M):
    numClasses = len(np.unique(label))
    learners = []    
    for i in range(0, numClasses):
        learners.append([])
        for j in range(i + 1, numClasses):            
            (dataij, labelij) = GetClasses(data, label, i, j)
            learners[i].append(GradientBoostingTraining(dataij, labelij, h, lossF, F0, M))
    return learners;

'''
   Klassifiziert data mithilfe des Modells
   Dabei wird OneVsOne benutzt. Jeder Datenpunkt wird fuer jedes Paar an Klassen gelabelt
   Dabei gilt, -1 entspricht Klasse 2, 1 entspricht Klasse 1
   Das label Entspricht der Klasse mit den hoechsten Treffern
'''
def OneVsOne(model, data):
    label = np.empty(np.alen(data))    
    for i in range(0, np.alen(data)):
        votes = np.zeros_like(label)
        for j in range(0, len(model)):
            for k in range(0, len(model[j])):
                # Voting fuer                
                votes[j] += np.sum([1 for l in GradientBoosting(model[j][k], [data[i]]) if l > 0])
                votes[j+k+1] += np.sum([1 for l in GradientBoosting(model[j][k], [data[i]]) if l < 0])
        label[i] = np.argmax(votes)
    return label


'''
   Liefert ein Modell zum Klassifizieren, dabei wird One vs All benutzt, um das Mehrklassenproblem zu loesen
   Dabei wird fuer jede Klassen ein Modell erstellt. Label 1 falls in der Klasse, sonst -1
   data: Trainingsdaten
   label: label der Daten {-1,1}
   h: Array der Lerner
   lossF: die loss function und deren Ableitung
   F0: F0
   M: Anzahl durchlaeufe
   return: das Modell zum Klassifizieren
'''
def OneVsAllTraining(data, label, h, lossF, F0, M):   
    learners = []
    for i in np.unique(label):
        labeli = [1 if l == i else -1 for l in label]
        learners.append(GradientBoostingTraining(data, labeli, h, lossF, F0, M))
    return learners

def OneVsAll(model, data):
    '''
    label = np.empty(np.alen(data))        
    for i in range(0, np.alen(data)):
        maxI = 0
        maxV = GradientBoosting(model[0], [data[i]])[0]
        for j in range(1, len(model)):
            l = GradientBoosting(model[j], [data[i]])[0]
            if(l > maxV):
                maxV = l
                maxI = j
        label[i] = maxI    
    return label
    '''
    label = [GradientBoosting(m, data) for m in model]   
    return np.argmax(label, axis = 0) 
    
'''
Regression
'''
def RegressionTraining(data, label, h, lossF, F0, M):
    return GradientBoostingTraining(data, label, h, lossF, F0, M)

def Regression(model, data):
    return np.round(GradientBoosting(model, data))

import math;
# Es wird ein Parameter l uebergeben, welcher entscheidet welche Fehlerfunktion benutzt wird
# Zur Auswahl stehen Linear(l=1), Quadratisch(l=2) und Logaritmisch(l=3)
# Das Ergebnis der Funktion ist sowohl die Fehlerfunktion als auch ihre Ableitung
def getLossFunction(l):
    l = int(l)
    if(l == 1):
        # linear error function
        F = lambda y, y1: math.fabs(y - y1);
        f = lambda y, y1: (y1 -y );
        return [F, f];
    elif(l == 2):
        # quadratic error function
        F = lambda y, y1: math.pow(y - y1, 2)/2;
        f = lambda y, y1: y1-y;
        return [F, f];
    elif(l == 3):
        # logarithmic error function
        F = lambda y, y1: math.log(math.fabs(y - y1));
        f = lambda y, y1: 1 / (y1-y);
        return [F, f];
    else:
        raise Exception('Lossfunction nicht vorhanden!')


'''
    Trainiiert mit den angegebenen Parametern
    Parameter: model (Das Rueckgabewert von MainTraining)
    Parameter: data (Zu labelne Date)
    lossFunction: Linear(l=1), Quadratisch(l=2) und Logaritmisch(l=3)
    multiClassType: (Gibt an welche Methode benutzt werden soll, um multiclass Probleme zu loesen)
    Return: ein numpy-Array der erzeugen Labels
'''
def MainTrainig(data, label, multiClassType="OvA", lossFunction=2, maxLeafs=15, F0=0, Iteration=5):
    learners = GetWeakLearner(maxLeafs)
    trainingFunc = None
    if(multiClassType == "OvA"):
        trainingFunc = OneVsAllTraining
    else:
        if(multiClassType == "OvO"):
            trainingFunc = OneVsOneTraining
        else:
            if(multiClassType == "Regression"):
                trainingFunc = RegressionTraining
    lossF = getLossFunction(lossFunction)
    return trainingFunc(data, label, learners, lossF, F0, Iteration)

'''
    Labelt mit dem angegebenen Model die Daten    
    Parameter: model (Das Rueckgabewert von MainTraining)
    Parameter: data (Zu labelne Date)
    lossFunction: Linear(l=1), Quadratisch(l=2) und Logaritmisch(l=3)
    multiClassType: (Gibt an welche Methode benutzt werden soll, um multiclass Probleme zu loesen)
    Return: ein numpy-Array der erzeugen Labels
'''
def Main(model, data, multiClassType="OvA", lossFunction=2, maxBlattanzahl="15", F0=0, Iterationen="5"):
    classFunc = None
    if(multiClassType == "OvA"):
        classFunc = OneVsAll
    else:
        if(multiClassType == "OvO"):
            classFunc = OneVsOne
        else:
            if(multiClassType == "Regression"):
                classFunc = Regression
    return classFunc(model, data)

"""
Modell = MainTrainig([[2,5],[1,4],[7,8], [0,0]], [0,1,0,1], F0=-3.141592653589793, multiClassType="OvA")

print(Main(Modell, [[1, 4], [3, 10], [99, 200], [45, 344], [1204, 1243], [-5,-7]], multiClassType="OvA"))
"""





'''
Zum schoen aussehen aber ohne Funktion:
def MyLambda(label):
    return {lambda x: 1/2*np.log(1+np.mean(label) / 1-np.mean(label)  )}


def LKTreeBoost(data,label,J,M):
    F = [MyLambda(label)]
    for m in range(0,M):
        y_ = [2*label[i]/(1+np.exp(2*label[i]*GetFunction(F)(data[i])))]
        R = [sklearn.tree.DecisionTreeClassifier(max_leaf_nodes=j).fit(data,y_) for j in range(1,J+1)]
        gamma = [np.sum([y_]) / np.sum( )]
'''


def do_stuff(dataset, *_, **kwargs):
    (trd, trl, ted, tel, vad, val) = dataset
    mct = kwargs['mct']
    lf = kwargs['loss']
    maxleafs = kwargs['maxleafs']
    f0 = kwargs['f0']
    iterations = kwargs['iter']

    t = time()
    model = MainTrainig(trd, trl, multiClassType=mct, lossFunction=lf, maxLeafs=maxleafs, F0=f0, Iteration=iterations)
    training_prediction = Main(model, trd, multiClassType=mct, lossFunction=lf, maxBlattanzahl=maxleafs, F0=f0, Iterationen=iterations)
    time_training = time() - t

    t = time() # get labels for test data
    test_prediction = Main(model, ted, multiClassType=mct)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = Main(model, vad, multiClassType=mct)
    time_validation = time() - t

    score_test = accuracy_score(tel, test_prediction)
    score_validation = accuracy_score(val, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(time_training*1000),
               'pictures': [],
               'success': True}
    return result
