# In this project the VFDT Algorithm introduced in the paper "Mining High-Speed Data Streams" by
# Pedro Domingos and Geoff Hulten is implemented by Arne Schneck.
#
# To use the algorithm just have a look at the main function of this python file.

import learners.VfdtAlgorithm as alg
import learners.VfdtNode as node

from learners.PropositionForDiscretization import discretizeData

from time import time
from sklearn.metrics import accuracy_score
import numpy as np

# This function applies the VFDT algorithm on data. Inserted data is expected to be discretized (see trainingData
# parameter description). The VFDT algorithm is applied on training data and evaluation data. The classification of the
# data given for evaluation is printed at standard out after classification is done.
# One hint for the current frontend: Users should be aware of the discretization that is applied on the data before it
# is handed over to this function.
#
# trainingData:                     Numpy matrix of data instances that shall be used for training. Rows are instances,
#                                   cols are attributes. Data is expected to be discretized such that only
#                                   numberOfAttributeValues different values for each attribute are possible. Attributes
#                                   values are expected to be in {0,...,numberOfAttributeValues-1} . This kind of data
#                                   can be achieved by nBin-preprocessing of the data before handing it over to this
#                                   function. The last column is expected to be the class label. If no data is given,
#                                   this function returns an error message accordingly.
#                                   No missing values are expected!
# evaluationData:                   Numpy matrix of evaluation data. Rows are instances, cols are attributes. Data is
#                                   expected to be discretized as described for trainingData. There is no column of
#                                   class labels expected in this matrix ;) If no data is given, this function returns
#                                   an error message accordingly.
#                                   No missing values are expected!
# numberOfAttributeValues:          This is the number of possible values of each attribute. Attributes are expected to
#                                   be discretized as described for parameter trainingData. Must be at least 1.
# numPointsBeforeQualityComputation: Number of training instances that have to have arrived in a node between each
#                                   calculation of split quality. See VFDT paper for further explanation. This value has
#                                   to be at least 1, but consider: This mode will be not active for 1 as input! Default
#                                   value is 10 .
# delta:                            Accepted error that a split is not done for the best possible attribute. It is to be
#                                   in (0,1]. The smaller, the later decisions / splits are done and therefore the
#                                   longer the algorithm needs to adapt the model. If this parameter is not set
#                                   appropriately, an error message is returned. Default value is 0.05
# tieBreakingThreshold:             Parameter to enforce splits if the algorithm can't settle down on deciding what
#                                   attribute is to be chosen for splitting a node if several attributes are equally
#                                   strong in their split quality. This is done to prevent the algorithm from taking too
#                                   much time / instances to decide. This parameter can have any value but consider:
#                                   values <= 0 will switch off tie breaking. See paper "Tie Breaking in Hoeffding
#                                   Trees" by Geoffrey Holmes, Richard Kirkby, and Bernhard Pfahringer for further
#                                   details on the issue of tie breaking. Default value is 0.2 .
# qualityMeasure:                   What measure should be used for calculation of split quality? Default is
#                                   "InformationGain", later maybe GiniCoefficient will also be available. See VFDT
#                                   paper for interesting idea of using Gini Coefficient.
#
# return: Returns a list of class labels for each instance in evaluationData in the same ordering as evaluationData.
def main(trainingData, evaluationData, numberOfAttributeValues, numPointsBeforeQualityComputation = 10, delta = 0.05,
            tieBreakingThreshold = 0.2, qualityMeasure = node.VfdtNode.infoGainName):
    ### Are parameters ok? ###
    # Does training data exist? If no instances are given or no attribute (but maybe class label) per instance,
    # return error message.
    if trainingData.shape[0] == 0 or trainingData.shape[1] <= 1:
        return "No training data is given!"
    # Does evaluation data exist?
    if evaluationData.shape[0] == 0 or evaluationData.shape[1] == 0:
        return "No evaluation data is given!"
    if trainingData.shape[1]-1 != evaluationData.shape[1]:
        return "Training data and evaluation data have different dimensions!"
    if numberOfAttributeValues < 1:
        return "numberOfAttributeValues is to be >= 1 !"
    if numPointsBeforeQualityComputation < 1:
        return "numPointsBeforeQualityComputation is be >= 1 !"
    # Delta ok?
    if delta <= 0 or delta > 1:
        return "Parameter delta is to be in (0,1]"
    if qualityMeasure != node.VfdtNode.infoGainName:
        return "Unknown quality measure!"

    # Instantiate the VFDT algorithm
    vfdt = alg.VfdtAlgorithm(numPointsBeforeQualityComputation)
    # Learn the model
    vfdt.trainModel(trainingData, numberOfAttributeValues, delta, tieBreakingThreshold, qualityMeasure)

    # TODO If model shall be shown to the user: adapt / use this.
    #vfdt.printTree()

    return vfdt.applyModel(evaluationData)


def do_stuff(dataset, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset

    error = {'score_test': None,
             'score_validation': None,
             'training_labels': None,
             'test_labels': None,
             'validation_labels': None,
             'extra_scores_test': None,
             'extra_scores_validation': None,
             'pictures': None,
             'success': False}

    nbins = kwargs['nbins']

    dtrd = discretizeData(training_data, nbins)
    dted = discretizeData(test_data, nbins)
    dvad = discretizeData(validation_data, nbins)

    trainingset = np.concatenate((dtrd, training_label.reshape((len(training_label),1))), axis=1)

    nmin = kwargs['nmin']
    delta = kwargs['delta']
    tiethr = kwargs['tiethr']

    t = time()
    training_prediction = main(trainingset, training_data, nbins, nmin, delta, tiethr)
    if isinstance(training_prediction, basestring):
        error['message'] = "Problem occurred: " + training_prediction
        return error
    training_time = time() - t

    t = time()
    test_prediction = main(trainingset, test_data, nbins, nmin, delta, tiethr)
    if isinstance(test_prediction, basestring):
        error['message'] = "Problem occurred: " + test_prediction
        return error
    time_test = time() - t

    t = time()
    validation_prediction = main(trainingset, validation_data, nbins, nmin, delta, tiethr)
    if isinstance(validation_prediction, basestring):
        error['message'] = "Problem occurred: " + validation_prediction
        return error
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result = {'score_test': score_test,  # Kriegen Studis zu sehen
              'score_validation': score_validation,  # kommt in die Highscore-Tabelle
              'training_labels': training_prediction,
              'test_labels': test_prediction,
              'validation_labels': validation_prediction,
              'extra_scores_test': {"time": "%2.3fms" % (time_test * 1000)},
              'extra_scores_validation': {"time": "%2.3fms" % (time_validation * 1000)},
              'message': 'Training time: %2.3fms' % (training_time * 1000),
              'pictures': [],
              'success': True}
    return result



# TODO Execute VFDT algorithm with parameters
#trainingSet = someDiscretizedNumpyMatrix
#evaluationSet = someDiscretizedNumpyMatrix
#numberOfBins = 100? 60? Whatever makes sense for the data.
#nmin = 10 is default
# delta = 0.05 is default
# tieThreshold = 0.2 is default
#result = main(trainingSet, evaluationSet, numberOfBins, nmin, delta, tieThreshold)
# if isinstance(result, basestring):
#     print "Problem occurred: " , result
# else:
#     # Print classification
#     for i in result:
#         print i
#
#     # TODO
#     # Store classification in file, therefore it has to be an array of int,
#     # which is not default in my implementation as strings are used for class labels
#     # (doesn't of course make sense for the expected numpy matrix).
#     #for i in range(0,len(classification)):
#     #    classification[i] = int(classification[i])
#     #np.savetxt("classificationVfdt.txt", classification, fmt="%1.3f")