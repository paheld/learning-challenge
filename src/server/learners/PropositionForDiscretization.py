import numpy as np
import learners.Pair as p


# n: number of bins
# data: numpy matrix of instances to be discretized. Last column is class label.
def discretizeData(data, n):
    if n <= 0:
        return "n has to be bigger than 0 !"
    dataShape = data.shape

    # If no data is given, just return empty data
    if dataShape[0] == 0 or dataShape[1] == 0:
        # error["message"] = "No data given!"
        # return error
        print("No data given")
        return data

    ### Iterate through data and compute min and max value for each attribute except the class label (last column). ###
    # Create a min-max-pair for each attribute initiated with values of first instance
    minMaxValues = [p.Pair(data[0][attribute], data[0][attribute]) for attribute in range(0, dataShape[1] - 1)]

    # Iterate through data and compute min and max values
    for attribute in range(0, dataShape[1] - 1):
        minMax = minMaxValues[attribute]
        for instance in range(0, dataShape[0]):
            attributeValue = data[instance][attribute]
            # Store instance's value as min or max if necessary
            if attributeValue < minMax.first:
                minMax.first = attributeValue
            if attributeValue > minMax.second:
                minMax.second = attributeValue

    ###       Compute bins out of min and max values and number of bins n         ###
    ### For every attribute there are n bins as thresholds max values of that bin ###
    binMatrix = [[-1 for binInd in range(0, n)] for attribute in range(0, dataShape[1] - 1)]
    # Iterate over all attributes
    for attribute in range(0, dataShape[1] - 1):
        # Create range of a bin of that attribute
        attrRange = minMaxValues[attribute].second - minMaxValues[attribute].first
        binRange = attrRange / n
        # Create bins
        for binInd in range(0, n - 1):
            binMatrix[attribute][binInd] = minMaxValues[attribute].first + (binInd + 1) * binRange
        # Taking care of (not researched) problem: There is an issue with values bigger
        # than threshold (or bin) at last index due to float computation (I guess).
        # Maybe this is not necessary
        binMatrix[attribute][n - 1] = minMaxValues[attribute].second

    ### Discretize data by putting it into the bins ###
    # Initialize dicretized data
    discretizedData = np.zeros(dataShape, dtype=int)
    # Discretize data
    for attribute in range(0, dataShape[1] - 1):  # Iterate over attributes
        attributesBins = binMatrix[attribute]
        # print attributesBins
        for instance in range(0, dataShape[0]):  # Iterate over instances
            value = data[instance][attribute]
            # Which bin fits?
            for binInd in range(0, len(attributesBins)):
                if value <= attributesBins[binInd]:
                    discretizedData[instance][attribute] = binInd
                    break

    # Add class label to the matrix of discretized data
    for instance in range(0, dataShape[0]):
        discretizedData[instance][dataShape[1] - 1] = data[instance][dataShape[1] - 1]
    return discretizedData
