__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

import numpy as np
import settings
from sklearn.metrics import accuracy_score
from time import time as time

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis as QDA


def do_stuff(dataset, *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    priors = [float(x) for x in kwargs['priors'].split(",")]
    if kwargs['type'] == 'lda':
        clf = LDA(priors=priors)
    else:
        if 'type_regularisation' in kwargs:
            reg = kwargs['type_regularisation']
        else:
            reg = 0.0
            print("reg_param not given, falling back to default value!")
        clf = QDA(priors=priors, reg_param=reg)

    t = time() # Train the LDA
    clf.fit(training_data,training_label)
    training_time = time() - t

    training_prediction = clf.predict(training_data) # get a prediction on the training data (this should be obvious)

    t = time() # get labels for test data
    test_prediction = clf.predict(test_data)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = clf.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': [],
               'success': True}
    return result