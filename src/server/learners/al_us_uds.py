from __future__ import print_function, unicode_literals, generators, absolute_import

import learners.activelearner as al
import numpy as np
from sklearn.naive_bayes import GaussianNB as nb
from sklearn.tree import DecisionTreeClassifier as dt
from sklearn.cross_validation import train_test_split

from time import time
from sklearn.metrics import accuracy_score

__author__ = 'Dominik Lang'

oracle_penalty = 0.995

def run_al(sampling, method, data, cnum, budget, bandwidth, processes=2, labeled_set=None):
    """
    Method to perform the implemented active learning
    :param sampling: string, what active learner should be used. Options are 'US' for uncertainty sampling and 'UDS' for
    uncertainty diversity sampling
    :param method: string, method of uncertainty to be used. For both sampling strategies 'margin' and 'entropy are
    available, uncertainty sampling also allows 'random'
    :param data: numpy matrix containing the data to be used
    :param cnum: int, number of class labels in the data
    :param budget: int, how many instances will be sampled from the data
    :param bandwidth: float (0.1 - 1.0) , bandwidth used by kernel density estimation
    :param processes: int, number of processes to be used
    :param labeled_set: numpy matrix, initial labeled set
    :return: returns the feature vector aswell as the target vector of the actively sampled dataset. This separation is
    to allow easier use in combination with sklearn classifiers
    """
    data = data.tolist()
    if labeled_set is None:
        labeled_set = get_initial_labeled(data, 2)
        labeled_set = labeled_set.tolist()
        for l in labeled_set:
            data.remove(l)
    else:
        labeled_set = labeled_set.tolist()

    if sampling == 'US':
        learner = al.US(data, labeled_set, cnum, budget, bandwidth, processes)
        ret = learner.run(method)
        X = np.matrix(ret[0])
        y = np.array(ret[1])
        return X, y
    elif sampling == 'UDS':
        learner = al.US(data, labeled_set, cnum, budget, bandwidth, processes)
        ret = learner.run(method)
        X = np.matrix(ret[0])
        y = np.array(ret[1])
        return X, y


def load_dataset(dataset='iris'):
    """
    Load one of the UCI datasets shipped with the program
    :param dataset: string, name of a dataset. Options are 'iris', 'ecoli', 'bupa' and 'glass'
    :return: tuple, position 0 contains the dataset, position 1 contains the cnum (number of class labels)
    """

    if dataset == 'iris':
        fn = 'iris.csv'
        cnum = 3
    elif dataset == 'ecoli':
        fn = 'ecoli.csv'
        cnum = 8
    elif dataset == 'bupa':
        fn = 'bupa.csv'
        cnum = 2
    elif dataset == 'glass':
        fn = 'glass.csv'
        cnum = 6

    else:
        print('Invalid parameter: dataset')
        return None

    file = open(fn)
    a = []
    for line in file:
        line = line.rstrip()
        sline = line.split(',')
        if '?' in sline:
            continue
        tmp = []
        for e in sline:
            tmp.append(eval(e))
        a.append(tmp)
    m = np.array(a)
    ret = (m, cnum)
    return ret


def get_initial_labeled(data, n):
    """
    If no labeled set has been passed to the active learner, select the first n occurances of each class label
    and combine those instances to form the initial labeled set
    :param data: numpy matrix containing the data
    :param n: integer, how many instances of each class label should be selected?
    :return: the initial labeled set
    """
    data = np.array(data)
    labels = []
    labeled = []
    ret = []
    for i in range(0, data.shape[0]):
        row = data[i]
        yc = row[row.shape[0]-1]
        lexists = False
        for label in labels:
            i = labels.index(label)
            if label == yc:
                lexists = True
                if len(labeled[i]) < n:
                    labeled[i].append(row)
        if not lexists:
            labels.append(yc)
            labeled.append([row])

    for l in labeled:
        for sl in l:
            ret.append(sl)
    ret = np.array(ret)
    return ret


def benchmark(X, y, tX, ty, classifier='DT'):
    """
    Train a classifier (default: Decision Tree) on the given training and test data
    :param X: feature vector used for training
    :param y: target vector used for training
    :param tX: feature vector used for testing
    :param ty: target vector used for testing
    :param classifier: string, what classifier to use. Options 'NB' for Naive Bayes and 'DT' for Decision Tree
    :return: float, accuracy score of the classifier
    """
    if classifier == 'NB':
        clf = nb()
    elif classifier == 'DT':
        clf = dt(random_state=0)
    clf.fit(X, y)
    score = clf.score(tX, ty)
    return score


def demo():
    """
    Short demo, loading the iris data set, performing uncertainty diversity sampling using margin-based uncertainty
    on it and printing out the string representations of the returned feature and target vectors
    :return: None
    """
    d = load_dataset('iris')
    unlabeled = d[0]
    cnum = d[1]
    alldata = np.matrix(unlabeled)
    X, y = run_al('UDS', 'margin', alldata, cnum, 20, 0.5, 4)
    print(str(X))
    print(str(y))


def demo_classify():
    """
    Short demo, loading the ecoli data set, performing uncertainty diversity sampling using margin-based uncertainty
    on it, then training a decision tree on the sampled set and evaluating the decision tree on a randomly selected
    test set from the data
    P.S.: I'm aware that this is no accurate split. This demo is just intended to show how the output of the active
    learning can be used
    :return: None
    """
    d = load_dataset('ecoli')
    unlabeled = d[0]
    cnum = d[1]
    alldata = np.matrix(unlabeled)
    X, y = run_al('UDS', 'margin', alldata, cnum, 20, 0.5, 4)
    all_f = alldata[:,:-1]
    all_t = np.array(alldata[:, -1])
    X_train, X_test, y_train, y_test = train_test_split(all_f, all_t, test_size=0.33)
    print(str(benchmark(X, y, X_test, y_test)))



def do_stuff(dataset, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    classifier = kwargs['classifier']  # DT or NB
    sampling = kwargs['sampling']      # US or UDS
    method = kwargs['method']          # margin, entropy or random
    budget = kwargs['budget']          # number of queries allowed 0..len(data)
    bw = kwargs['bw']
    procs = 1

    if classifier == 'NB':
        clf = nb
    elif classifier == 'DT':
        clf = dt(random_state=0)
    else:
        error['message'] = "Invalid classifier. was '"+classifier+"' should be in ['DT', 'NB']."
        return error

    cs = len(set(np.unique(training_label)) | set(np.unique(test_label)) | set(np.unique(validation_label)))  # of cls.
    labeled = np.concatenate((training_data, training_label.reshape((len(training_label), 1))), axis=1)
    print("CS = "+str(cs))
    if budget > len(test_data):
        error['message'] = "There are less data points available for querying than you asked for. Maximal number is %i." % len(test_data)
        return error

    t = time()
    run_al(sampling, method, test_data, cs, budget, bw, processes=procs, labeled_set=labeled)
    training_time = time() - t

    t = time() # get labels for test data
    test_prediction = clf.predict(test_data)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = clf.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    penalty = (oracle_penalty**budget)

    result = {'score_test':score_test*penalty, # Kriegen Studis zu sehen
              'score_validation':score_validation*penalty, # kommt in die Highscore-Tabelle
              'training_labels':training_label,
              'test_labels':test_prediction,
              'validation_labels':validation_prediction,
              'extra_scores_test':{"time":"%2.3fms"%(time_test*1000), "true_accuracy": round(score_test,3)},
              'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000), "true_accuracy":round(score_validation,3)},
              'message': 'Training time: %2.3fms'%(training_time*1000),
              'pictures': [],
              'success': True}
    return result
