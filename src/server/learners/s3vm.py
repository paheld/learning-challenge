#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators
from docutils.nodes import docinfo
import settings
import numpy as np
from sklearn.metrics import accuracy_score
from time import time as time
from sklearn.semi_supervised import LabelPropagation, LabelSpreading
from learners.qns3vm import QN_S3VM
from itertools import combinations
import random

__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

def do_stuff(dataset, *_, **kwargs):
    (X_train_l, L_train_l, X_train_u, L_test, X_train_u2, L_test2) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}
    kernel = kwargs['kernel']
    lam = kwargs['penalty_1']
    lam_U = kwargs['penalty_2']

    args = {'lam':lam, 'lamU':lam_U}

    if kernel == 'rbf':
        args['kernel_type'] = 'RBF'
        args['sigma'] = kwargs['kernel_gamma']
    elif kernel == 'linear':
        args['kernel_type'] = 'Linear'
    else:
        error['message'] = 'Unsupported kernel type!'
        return error
    #print(args)
    labels = np.unique(L_train_l)
    predictions_1 = np.zeros((len(X_train_u), len(labels)))
    predictions_2 = np.zeros((len(X_train_u2), len(labels)))

    t = time() # get labels for test data
    #print(L_train_l)
    #print(X_train_l)
    for i,j in combinations(labels,2):                      # get all attribute combinations
        train_l = np.array(L_train_l, dtype='int')          # training labels are a copy of training labels... :P
        mask = np.bitwise_or(train_l==i, train_l==j)        # get True only if label is in one of the combinations
        #print(mask)
        train_l = train_l[mask]                             # get only the training labels for the two labels
        X_l = np.array(X_train_l)[mask]                     # and only the corresponding points as well
        #print(X_l)
        train_l[train_l==i]=-1                              # first label is zero
        train_l[train_l==j]=1                               # second label is one
        #print((i,j,train_l))
        #print( np.shape(X_l) )
        #print( np.shape(train_l) )
        #print( np.shape(X_train_u) )

        model = QN_S3VM(list(X_l), list(train_l), list(X_train_u), random.Random(), **args) # QN_S3VM needs data to be in lists, not arrays...
        model.train()

        preds = model.getPredictions(X_train_u)
        preds2 = model.getPredictions(X_train_u2)
        #print((np.unique(preds), preds))
        #print(preds2)
        for idx,p in enumerate(preds):
            if p == 1:
                predictions_1[idx][j] += 1
            else:
                predictions_1[idx][i] += 1

        for idx,p in enumerate(preds2):
            if p == 1:
                predictions_2[idx][j] += 1
            else:
                predictions_2[idx][i] += 1
    #np.set_printoptions(linewidth=140)
    #print(predictions_1)
    p1 = np.argmax(predictions_1, axis=1)
    p2 = np.argmax(predictions_2, axis=1)
    #print(p1)
    time_test = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(L_test, p1)
    score_validation = accuracy_score(L_test2, p2)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':L_train_l,
               'test_labels':p1,
               'validation_labels':p2,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"Status": "Done!"}, # time not measured separately
               'message': '',
               'pictures': [],
               'success': True}
    return result

if __name__ == '__main__':
    from sklearn import datasets
    iris = datasets.load_digits()
    random_unlabeled_points = np.where(np.random.random_integers(0, 1,size=len(iris.target)))
    labels = np.copy(iris.target)
    labels[random_unlabeled_points] = -1
    X_train_l = iris.data[labels!=-1]
    L_train_l = labels[labels!=-1]
    X_train_u = iris.data[labels==-1]
    #print (L_train_l)
    dataset = (X_train_l, L_train_l, X_train_u, iris.target[labels==-1], X_train_u, iris.target[labels==-1])
    result = do_stuff(dataset, penalty_1=1, penalty_2=0.5, kernel='linear')
    print(result)