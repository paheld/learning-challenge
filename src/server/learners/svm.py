#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators
import numpy as np
import scipy.spatial.distance as ssd
import settings
from sklearn.metrics import accuracy_score
from time import time as time
from sklearn.svm import SVC

__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

dists = {
    "euclidean": ssd.euclidean,
    "weuclidean": ssd.wminkowski,
    "seuclidean": ssd.seuclidean,
    "sqeuclidean": ssd.sqeuclidean,
    "hamming": ssd.hamming,
    "manhattan": ssd.cityblock,
    "chebyshev": ssd.chebyshev,
    "minkowski": ssd.minkowski,
    "wminkowski": ssd.wminkowski
}


def do_stuff(dataset, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    c = kwargs['penalty']
    kernel = kwargs['kernel']

    if kernel == 'linear':
        clf = SVC(C=c, kernel=str('linear'))
    elif kernel == 'poly':
        clf = SVC(C=c, kernel=str('poly'), degree=kwargs['kernel_degree'], gamma=kwargs['kernel_gamma'], coef0=kwargs['kernel_coef0'])
    elif kernel == 'sigmoid':
        clf = SVC(C=c, kernel=str('sigmoid'), gamma=kwargs['kernel_gamma'], coef0=kwargs['kernel_coef0'])
    elif kernel == 'rbf':
        clf = SVC(C=c, kernel=str('rbf'), gamma=kwargs['kernel_gamma'])

    # *****************************************************************************************************************
    # DO NOT EDIT BELOW HERE.
    # HERE BE DRAGONS!
    # *****************************************************************************************************************

    t = time() # Train the LDA
    clf.fit(training_data,training_label)
    training_time = time() - t

    training_prediction = clf.predict(training_data) # get a prediction on the training data (this should be obvious)

    t = time() # get labels for test data
    test_prediction = clf.predict(test_data)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = clf.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': [],
               'success': True}
    return result