#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators
import settings
import numpy as np
from sklearn.metrics import accuracy_score
from time import time as time
from sklearn.semi_supervised import LabelPropagation, LabelSpreading

__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

def do_stuff(dataset, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}
    kernel = kwargs['kernel']
    alpha = kwargs['alpha']

    Classifier = LabelPropagation if kwargs['algo'] == 'propagation' else LabelSpreading

    if kernel == 'rbf':
        clf = Classifier(kernel=str('rbf'), alpha=alpha, gamma=float(kwargs['kernel_gamma']))
    elif kernel == 'knn':
        clf = Classifier(kernel=str('knn'), alpha=alpha, n_neighbors=int(kwargs['kernel_neighbors']))
    else:
        error['message'] = 'Unsupported kernel type!'
        return error

    data_for_testing = np.concatenate((training_data, test_data))
    label_for_testing = np.concatenate((training_label, np.ones_like(test_label)*-1))
    data_for_validation = np.concatenate((training_data, validation_data))
    label_for_validation = np.concatenate((training_label, np.ones_like(validation_label)*-1))

    true_label_for_testing = np.concatenate((training_label, test_label))
    true_label_for_validation = np.concatenate((training_label, validation_label))

    t = time() # get labels for test data
    clf.fit(data_for_testing, label_for_testing)
    test_prediction = clf.predict(data_for_testing)
    time_test = time() - t

    t = time() # get labels for validation data
    clf.fit(data_for_validation, label_for_validation)
    validation_prediction = clf.predict(data_for_validation)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(true_label_for_testing, test_prediction)
    score_validation = accuracy_score(true_label_for_validation, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_label,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': '',
               'pictures': [],
               'success': True}
    return result