# This python file implements a VFDT decision tree that can be used for the VFDT algorithm.

import math as m

# Class that represents a VFDT decision tree or it's nodes.
class VfdtNode:

    # Static counter for node IDs
    nodeId = 0
    # Static constants
    nodeNameEmpty = "EMPTY"
    infoGainName = "InformationGain"
    giniCoeffName = "GiniCoefficient"

    # Constructor of a VfdtNode instance (recursive implementation)
    #
    # numberOfAttributes:       Count of attributes that will be used in this decision tree. This is used to set up an
    #                           appropriate amount of counters for each attribute.
    # numberOfAttributeValues:  This is the number of possible values of each attribute. Attributes are expected to
    #                           be discretized as described for parameter trainingData in the main python file.
    #                           Must be at least 1.
    # parent:                   Parent node of this node. If this is the root node and the tree is initiated by calling
    #                           this method the parent parameter is not to be used but the default value of None shall
    #                           be used (don't give parent parameter if this is going to be the root node).
    def __init__(self, numberOfAttributes, numberOfAttributeValues, parent=None):
        # Parent node
        self.parent = parent

        # Initiate counter matrix. Counters stay on the value they had at split time.
        self.counterMatrix = [[{} for j in range(0, numberOfAttributeValues)] for i in range(0, numberOfAttributes)]

        # Initiate edges leading to nodes (dictionary of attribute value to VfdtNode)
        # e.g. children[attributeValue] -> childNodeReachedByAttributeValue
        self.children = {}

        # Initiate set of allowed split attributes
        self.allowedSplitAttributes = set()
        for i in range(0,numberOfAttributes):
            self.allowedSplitAttributes.add(i)

        # Initiate name of this node as empty.
        # A leaf is named according to most frequent class, an inner node according to split attribute.
        self.name = VfdtNode.nodeNameEmpty

        # This measure stores the split quality of a node such that it does not have to be recomputed for certain cases.
        # For inner nodes it contains the split quality computed by one the attribute split quality measures, if this
        # node is a leaf it contains the frequency of the biggest class in this leaf (which is in [0,1]).
        self.splitQuality = 0.0

        self.id = VfdtNode.nodeId
        VfdtNode.nodeId += 1 # Increment ID counter

        return


    # This method takes an instance as a list of attribute values of which the last element is the class value of this
    # instance and adds it to this tree. The name of the leaf this instance will be placed in might be reset.
    #
    # instance:                         A numpy array that contains attribute values in the first columns and the class
    #                                   label of this instance in the last column.
    # numPointsBeforeQualityComputation: Number of training instances that have to have arrived in a node between each
    #                                   calculation of split quality. See VFDT paper for further explanation. This value
    #                                   has to be at least 1, but consider: This mode will be not active for 1 as input!
    #
    # return: None or a node that might be split up after having added this instance.
    def addInstance(self, instance, numPointsBeforeQualityComputation):
        # Small adaption to original algorithm: Add this instance to this node no matter if it is a leaf or not.
        # Advantage of that is an always up to date counter state of an inner node that can be queried
        # (e.g. for getting to know the biggest class of the parent of a leaf in case of leaf name == nodeNameEmpty).
        # Increment counters
        for attribute in range(0,len(instance)-1): # last element is class value
            ia = instance[attribute]
            print("ia="+str(ia))
            print("self.cm="+str(self.counterMatrix[attribute]))
            counts = self.counterMatrix[attribute][ia]
            key = str(instance[len(instance)-1])
            if key in counts:
                counts[key] += 1
            else:
                counts[key] = 1

        # This is a leaf
        if len(self.children) == 0:
            # Recompute most frequent class and set the name to the name of this class.
            # Also compute the count of all instances in this leaf.
            # (This way of computation only works if no values are missing,
            # for only one attribute is taken into account for counting.)
            classes = self.computeClassFrequencies()
            maxCount = -1
            maxClass = ""
            for classValue in classes:
                if maxCount < classes[classValue]:
                    maxCount = classes[classValue]
                    maxClass = str(classValue)
            self.name = maxClass

            # Compute count of instances in this node. This can never be zero for there was at least one instance that
            # was placed into that leaf!
            containedInstances = self.computeContainedInstances()

            # Set the split quality of this leaf.
            self.splitQuality = float(maxCount) / containedInstances

            # If instances of more than one class are contained in this leaf and
            # if enough instances have arrived in this node and
            # if there is at least one more possible split attribute
            # set this node to be making sense for (re-)computation of split quality.
            if len(classes) > 1 and containedInstances % numPointsBeforeQualityComputation == 0 and len(self.allowedSplitAttributes) > 0:
                return self

            return

        # This node is an inner node. Find relevant child and pass it to this child.
        # The split attribute is the name of this node.
        return self.children[str(instance[int(self.name)])].addInstance(instance, numPointsBeforeQualityComputation)


    # This method computes the class frequencies of this node.
    #
    # return: Dictionary of class label mapping to class frequency e.g. for two instances of class 0, 3 instances of
    # class 1 and 0 instances of class 2 the result will be a dictionary containing these values:
    # 0: 2
    # 1: 3
    # As can be seen, not occurring classes are not represented with counters.
    def computeClassFrequencies(self):
        classes = {}
        attribute = self.counterMatrix[0] # Choose one (first) attribute
        for classValues in attribute:
            for classValue in classValues:
                if str(classValue) in classes:
                    classes[str(classValue)] += classValues[str(classValue)]
                else:
                    classes[str(classValue)] = classValues[str(classValue)]
        return classes

    # Computes the number of training instances that have been placed in this node during training.
    #
    # return: Number of instances that have been placed in this node during training as integer.
    def computeContainedInstances(self):
        # Compute sizes of classes in this node
        classes = self.computeClassFrequencies()
        containedInstances = 0
        for classValue in classes:
            containedInstances += classes[classValue]
        return containedInstances

    # This method splits up a node by adding a certain amount of children to it and renaming this node.
    #
    # splitAttribute:   Index of the attribute that is used as the split attribute. Index in this case is the column of
    #                   the numpy matrix of training data.
    # splitQuality:     Split quality of the split attribute. This is used for debugging purposes to memorize the split
    #                   quality that was relevant for a node during split.
    #
    # return: None
    def split(self, splitAttribute, splitQuality):
        # Change this node's name into the name of the split attribute
        self.name = str(splitAttribute)

        # Set split quality for this inner node (constant after split!)
        self.splitQuality = splitQuality

        # Add child nodes to this node for each value of the split attribute
        for attributeValue in range(0,len(self.counterMatrix[splitAttribute])):
            # This is possible for every attribute has the same amount of values in this implementation.
            node = VfdtNode(len(self.counterMatrix), len(self.counterMatrix[0]), self)

            # Compute biggest class in child node
            maxClass = VfdtNode.computeBiggestClass(self.counterMatrix[splitAttribute][attributeValue])

            # Set name of child node (if there is any class in it according to it's parent node)
            if maxClass != "":
                node.name = maxClass

            # Disable this split attribute for child node
            splitAttributeSet = set()
            splitAttributeSet.add(splitAttribute)
            node.allowedSplitAttributes = self.allowedSplitAttributes.difference(splitAttributeSet)

            # Add child to this node
            self.children[str(attributeValue)] = node
        return

    # This method takes an instance as a numpy array of attribute values and uses the model / VFDT decision tree that is
    # learnt to predict the class of this instance. If no model is learnt this method must not be called!
    #
    # instance: A numpy array that contains attribute values. The last column of this instance is not
    #           supposed to contain a class label of course.
    #
    # return: Class label that is predicted for this instance.
    def classifyInstance(self, instance):
        # If this node is an empty leaf return the biggest class of it's parent node.
        # An empty root node is not possible by design.
        if self.name == VfdtNode.nodeNameEmpty:
            return VfdtNode.computeBiggestClass(self.parent.computeClassFrequencies())

        # If this is a leaf with content return the name of this leaf.
        if len(self.children) == 0:
            return self.name

        # This is an inner node; pass the instance down the tree.
        # The name of this node is the name of the split attribute,
        # which is the index of the split attribute in the list of
        # attributes of this instance.
        return self.children[str(instance[int(self.name)])].classifyInstance(instance)

    # This method prints this node / tree (and all of it's sub nodes / trees) to standard out in the format:
    # -{depthOfNode-times} + " " + nodeName + " " + nodeId + " " + nodeSplitQuality + " " + nodeAllowedSplitAttributes
    #
    # depth: Integer that is used to identify on which level of the tree the current recursive method call is.
    #
    # return: None
    def printTree(self, depth = 0):
        depthString = ""
        for i in range(0, depth):
            depthString += "-"
        print(depthString, " ", self.name, " " , self.id ,  " ", self.splitQuality, " ", self.allowedSplitAttributes)

        if len(self.children) != 0:
            for child in self.children:
                self.children[child].printTree(depth+1)
        return

    # Method to compute the Information Gain of a node.
    #
    # node:         Node for which the Information Gain is to be computed.
    # attribute:    Index of the attribute which the Information Gain is to be computed for. The index identifies an
    #               attribute in the matrix of training data.
    #
    # return: Information Gain as floating point.
    @staticmethod
    def computeInformationGain(node, attribute):
        informationGain = VfdtNode.computeEntropy(node)
        for attributeValue in range(0,len(node.counterMatrix[attribute])):
            # Compute number of instances in this node with value "attributeValue" of attribute "attribute"
            numberOfFulfillingInstances = 0
            classes = node.counterMatrix[attribute][attributeValue]
            for className in classes:
                numberOfFulfillingInstances += classes[className]

            # Compute number of all instances in this node. This can't be zero for there was at least one instance that
            # was placed in that node.
            containedInstances = node.computeContainedInstances()

            # Compute entropy after virtual split of this attribute
            virtualEntropy = 0
            for className in classes:
                # numberOfFulfillingInstances will never be 0 for every entry in classes dictionary has at least
                # one instance in this node.
                probabilityOfThisClass = float(classes[className]) / numberOfFulfillingInstances
                # probabilityOfThisClass can never be zero, for there is at least one instance for this class.
                virtualEntropy -= probabilityOfThisClass * m.log(probabilityOfThisClass, 2)

            # Compute information gain for this value of the attribute
            weight = float(numberOfFulfillingInstances) / containedInstances
            informationGain -= weight * virtualEntropy
        return informationGain

    # Not implemented yet!
    # Method to compute the Gini Coefficient of a node.
    #
    # node:         Node for which the Gini Coefficient is to be computed.
    # attribute:    Index of the attribute which the Gini Coefficient is to be computed for. The index identifies an
    #               attribute in the matrix of training data.
    #
    # return: Gini Coefficient as floating point.
    @staticmethod
    def computeGiniCoefficient(node, attribute):
        # TODO Implement for later usages if desired.
        return

    # Method to compute the entropy of a node.
    #
    # node: Node for which the entropy is to be computed.
    #
    # return: Entropy as floating point.
    @staticmethod
    def computeEntropy(node):
        # Compute sizes of classes. Every entry in this dictionary has at least one instance in this leaf, for regular
        # usage of this implementation of the VFDT algorithm does not allow class count of zero (a count is only created
        # for instances that arrive in a node and have this relevant class value).
        classes = node.computeClassFrequencies()
        # Compute all instances in a node. This can't ne zero for regular usage of algorithm, for split quality is only
        # computed for non empty leafs.
        containedInstances = node.computeContainedInstances()
        # Compute entropy
        entropy = 0.0
        for classValue in classes:
            # This is never undefined or zero for the algorithm doesn't allow it! See above comments.
            probabilityOfThisClass = float(classes[classValue]) / containedInstances
            entropy -= probabilityOfThisClass * m.log(probabilityOfThisClass, 2)
        return entropy

    # This method delegates the computation of the split quality to the appropriate methods.
    # Bad parameters are expected to be already called
    #
    # qualityMeasure:   What measure should be used for calculation of split quality? Default is
    #                   "InformationGain", later maybe GiniCoefficient will also be available. See VFDT
    #                   paper for interesting idea of using Gini Coefficient.
    # node:             Node for which the entropy is to be computed.
    # attribute:        Index of the attribute which the split quality is to be computed for. The index identifies an
    #                   attribute in the matrix of training data.
    #
    # return: Split quality of this node for this attribute as floating point.
    @staticmethod
    def computeSplitQuality(qualityMeasure, node, attribute):
        if qualityMeasure == VfdtNode.infoGainName:
            return VfdtNode.computeInformationGain(node, attribute)
        elif qualityMeasure == VfdtNode.giniCoeffName:
            return VfdtNode.computeGiniCoefficient(node, attribute)

    # This method computes the label of the biggest class out of a dictionary of class frequencies.
    #
    # classFrequencies: Dictionary of class label pointing to class frequency. For further details have a look at method
    #                   computeClassFrequencies()
    #
    # return: Label of the biggest class in classFrequencies or empty string.
    @staticmethod
    def computeBiggestClass(classFrequencies):
        maxClassSize = -1
        maxClass = ""
        for classValue in classFrequencies:
            if maxClassSize < classFrequencies[classValue]:
                maxClassSize = classFrequencies[classValue]
                maxClass = str(classValue)
        return maxClass