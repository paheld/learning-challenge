__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

import numpy as np
from sklearn.neighbors import KNeighborsClassifier as knn
from sklearn.metrics import accuracy_score as accuracy_score
import settings
from time import time as time

def do_stuff(dataset, k=3, weights='uniform', distance='euclidean', *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    p = None
    dweights = None
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}
    if not 1 <= k <= len(training_label):
        error['message'] = "Parameter 'k' is too large. There are not enough training points available."
        return error
    if weights not in ['uniform', 'distance']:
        error['message'] = "Stop hacking the source code. You may only specify 'uniform' or 'distance' as weight parameter."
        return error
    if distance not in settings.distances:
        error['message'] = "Whatever you were trying to achieve, '"+distance+"' is not a valid distance measure."
        return error
    if distance in ['minkowski', 'wminkowski', 'weuclidean']:
        if distance in ['wminkowski', 'minkowski']:
            if 'distance_p' not in kwargs:
                error['message'] = "'p' is missing"
            else:
                p = kwargs['distance_p']
        if distance in ['wminkowski', 'weuclidean']:
            if 'distance_weights' not in kwargs:
                error['message'] = "'distance_weights' is missing"
            else:
                dweights = np.array(map(float, kwargs['distance_weights'].split(",")))
    #print(kwargs)


    t = time() # Train the KNN Classifier on the training data and labels
    if distance in ['manhattan', 'cityblock']:
        KNN = knn(n_neighbors=k, metric=distance, weights=weights, algorithm='brute').fit(training_data, training_label)
    else:
        KNN = knn(n_neighbors=k, metric=distance, weights=weights, algorithm='brute', w=dweights, p=p).fit(training_data, training_label)
    training_time = time() - t

    training_prediction = KNN.predict(training_data) # get a prediction on the training data (this should be obvious)

    # print(training_data)
    # print(validation_data)

    t = time() # get labels for test data
    test_prediction = KNN.predict(test_data)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = KNN.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': [],
               'success': True}
    return result
