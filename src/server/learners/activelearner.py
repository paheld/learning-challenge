from copy import deepcopy
import math
import random
import operator
import multiprocessing
from sklearn.metrics.pairwise import euclidean_distances
import warnings
import numpy as np
import learners.classifier

__author__ = 'Dominik Lang'


class ActiveLearner:
    """
    Class modeling an active learner
    Note: dataset variable of an ActiveLearning object stores the data the learner is using in form of a list of lists
            i.e. it'll look something like this: [[unlabeled],[class_1],[class_2],...,[class_cnum]]
    """
    def __init__(self, unlabeled, labeled, cnum,  budget, bandwidth, processes=2):
        """
        constructor of the active learner
        :param unlabeled: List containing instances as lists of features, data to be treated as unlabeled data
        :param labeled: List containing instances as lists of features, data to be treated as initial labeled data
        :param cnum: integer, number of different class labels in the data
        :param budget: integer, how many labelings can be 'bought' by the learner
        :param bandwidth: float, bandwidth for the probability estimation (value between 0 and 1)
        :return: None
        """
        self.processes = processes

        assert budget > 0, 'Budget must be larger than 0'
        assert cnum > 0, 'Number of classes must be larger than 0'
        assert 0 < bandwidth <= 1, 'Bandwidth must be between 0.1 and 1.0'

        if budget > len(unlabeled):
            warnings.warn('Budget exceeds size of dataset')
            budget = len(unlabeled)-1

        self.cnum = cnum
        self.budget = budget
        self.labels = []
        self.dataset = []
        self.bandwidth = bandwidth
        self.dataset.append(deepcopy(unlabeled))

        for i in range(1, cnum+1):
            self.dataset.append([])
        self.setup_labeled(deepcopy(labeled))

    def run(self, method):
        pass

    def step(self, method):
        pass

    def setup_labeled(self, labeled):
        """
        Method to correctly distribute the instances in the different subsets in self.dataset
        :param labeled: List containing instances as lists of features, data to be treated as initial labeled data
        :return: None
        """
        for instance in labeled:
            yc = instance[-1]  #label of the instance
            lexists = False
            ind = None
            for l in self.labels:
                if l == yc:
                    ind = self.labels.index(l)
                    lexists = True
                    break
            if lexists is False:
                self.labels.append(yc)
                ind = self.labels.index(yc)
            assert ind is not None
            self.insert(instance, ind)

    def move(self, dset, index):
        """
        Moves an element from the unlabeled set to the correct labeled set
        :param dset: the set from which an instance should be taken
        :param index: index of item in the unlabeled set to move
        :return: None
        """
        candidate = dset[index]  #selected row from unlabeled set
        yc = candidate[-1]  #label of the candidate
        lexists = False
        ind = None
        for l in self.labels:
            if l == yc:
                ind = self.labels.index(l)
                lexists = True
                break
        if lexists is False:
            self.labels.append(yc)
            ind = self.labels.index(yc)
        assert ind is not None
        self.insert(candidate, ind)
        self.delete(dset, index)

    def insert(self, candidate, index):
        """
        Method to insert the passed candidate instance into the correct labeled set
        :param candidate:   an element from the unlabeled set
        :param index:   index of the labeled set where the element should be put
        :return:
        """
        index += 1
        assert len(candidate) > 1
        row = candidate[:-1]
        if self.dataset[index] == 0:
            self.dataset[index] = []
            self.dataset[index].append(np.array(row))
        else:
            self.dataset[index].append(np.array(row))

    def delete(self, dset, index):
        """
        :param dset:  dataset from which to delete the instance
        :param index: index of instance to remove
        """
        dset.remove(dset[index])


class US(ActiveLearner):
    """
    Class implementing an uncertainty sampling active learner. Uncertainty Sampling is based selecting instances for
    which the learner is uncertain about it's correct class membership.
    """
    def __init__(self, unlabeled, labeled, cnum,  budget, bandwidth, processes=1):
        """
        :param unlabeled: List containing instances as lists of features, data to be treated as unlabeled data
        :param labeled: List containing instances as lists of features, data to be treated as initial labeled data
        :param cnum: integer, number of different class labels in the data
        :param budget: integer, how many labelings can be 'bought' by the learner
        :param bandwidth: float, bandwidth for the probability estimation (value between 0 and 1)
        :return: None
        """
        ActiveLearner.__init__(self, unlabeled, labeled, cnum,  budget, bandwidth, processes=1)
        self.has_labels = False

    def step(self, method):
            """
            Method to perform one step of uncertainty sampling.There're two different measures of uncertainty
            implemented here:
            - Entropy-based sampling uses shannon entropy (base 10 logarithm), considering all classes, to estimate
            uncertainty. Higher entropy means higher uncertainty
            - Margin-based sampling uses the difference ('margin') between the highest and second highest class post-
            erior probability to measure uncertainty. A smaller margin means higher uncertainty.
            Random sampling is also implemented to allow comparison. It randomly chooses an instance from the data
            :param method: String, the method of uncertainty sampling. Viable options are 'margin', 'entropy' and
            'random'. (Default is 'margin')

            """
            has_empty = False
            if not self.has_labels:
                for s in self.dataset:
                    if len(s) == 0:
                        has_empty = True
                if not has_empty:
                    self.has_labels = True

            unlabeled = self.dataset[0]
            lset = self.dataset[1:]

            if has_empty:
                i = random_sample(unlabeled)

            else:
                func = None
                if method == 'margin':
                    func = margin_sample
                elif method == 'entropy':
                    func = entropy_sample

                if func is not None:
                    q = multiprocessing.Queue()
                    chunks = divide_data(unlabeled, int(len(unlabeled)/self.processes))
                    processes = []
                    for chunk in chunks:

                        p = multiprocessing.Process(target=func, args=(chunk, lset, self.bandwidth, q,))
                        processes.append(p)
                    for p in processes:
                        p.start()

                    rets = [q.get() for p in processes]
                    rets.sort(key=operator.itemgetter(0))
                    if method == 'entropy':
                        i = rets[-1][1]
                    elif method == 'margin':
                        i = rets[0][1]

                    for process in processes:
                        process.terminate()

                if method == 'random':
                    i = random_sample(unlabeled)

            assert i is not None
            index = self.dataset[0].index(i)
            self.move(self.dataset[0], index)

    def run(self, method='margin'):
        """
        Runs the active learning process of this instance. Continues until budget is spent.
        :param method: String, the method of uncertainty sampling. Viable options are 'margin', 'entropy' and 'random'
        :return: list containing the actively sampled set form the data. At index 0, the feature vector, at index 1
        the target vector (the class labels) and at index 2 the remaining unlabeled instances. The separation of feature
        and target vector was chosen to ease the use of the returned list in combination with sklearn classifiers
        """
        fvector = []
        tvector = []
        while self.budget != 0:
            self.step(method)
            self.budget -= 1

        for i in range(1, len(self.dataset)):
            fvector += self.dataset[i]
            tmp = np.empty(len(self.dataset[i]))
            tmp.fill(self.labels[i-1])
            tvector = np.concatenate((tvector, tmp), axis=0)

            assert len(fvector) == len(tvector)
        return [fvector, tvector, self.dataset[0]]


class UDS(ActiveLearner):
    """
    Class implementing uncertainty diversity sampling, i.e. sampling based on uncertainty as well as diversity.
    Diversity is a criterion defined by Chen et al. (2010) in their paper 'Multi-class support vector machine active
    learning for music annotation'

    """
    def __init__(self, unlabeled, labeled, cnum,  budget, bandwidth, processes=2):
        ActiveLearner.__init__(self, unlabeled, labeled, cnum,  budget, bandwidth, processes=2)
        self.has_labels = False

    def step(self, method):
        """
        Method to perform one step of uncertainty diversity sampling.There're two different measures of uncertainty
        implemented here:
        - Entropy-based sampling uses shannon entropy (base 10 logarithm), considering all classes, to estimate
        uncertainty.
        - Margin-based sampling uses the difference ('margin') between the highest and second highest class post-
        erior probability to measure uncertainty.

        Diversity measures the minimum distance from a candidate instance x to the instances in the pool of already
        acquired labeled instances.
        Combining Diversity with Uncertainty aims to prevent sampling to close to the current labeled pool, thereby
        balancing the sampling process a bit.

        :param method: String, the method of uncertainty sampling. Viable options are 'margin' and 'entropy'
        (Default is 'margin')
        """
        has_empty = False
        if not self.has_labels:
            for s in self.dataset:
                if len(s) == 0:
                    has_empty = True
            if not has_empty:
                self.has_labels = True

        unlabeled = self.dataset[0]
        lset = self.dataset[1:]

        if has_empty:
            i = random_sample(unlabeled)

        else:
            func = None
            if method == 'margin':
                func = margin_div_sampling
            elif method == 'entropy':
                func = entropy_div_sample

            if func is not None:
                q = multiprocessing.Queue()
                chunks = divide_data(unlabeled, int(len(unlabeled)/self.processes))
                processes = []
                for chunk in chunks:

                    p = multiprocessing.Process(target=func, args=(chunk, lset, self.bandwidth, q,))
                    processes.append(p)
                for p in processes:
                    p.start()

                rets = [q.get() for p in processes]
                rets.sort(key=operator.itemgetter(0))
                if method == 'entropy':
                    i = rets[-1][1]
                elif method == 'margin':
                    i = rets[0][1]

                for process in processes:
                    process.terminate()

        assert i is not None
        index = self.dataset[0].index(i)
        self.move(self.dataset[0], index)

    def run(self, method='margin'):
        """
        Runs the active learning process of this instance. Continues until budget is spent.
        :param method: String, the method of uncertainty sampling. Viable options are 'margin', 'entropy'
        :return: list containing the actively sampled set form the data. At index 0, the feature vector, at index 1
        the target vector (the class labels) and at index 2 the remaining unlabeled instances. The separation of feature
        and target vector was chosen to ease the use of the returned list in combination with sklearn classifiers
        """
        fvector = []
        tvector = []
        while self.budget != 0:
            self.step(method)
            self.budget -= 1

        for i in range(1, len(self.dataset)):
            fvector += self.dataset[i]
            tmp = np.empty(len(self.dataset[i]))
            tmp.fill(self.labels[i-1])
            tvector = np.concatenate((tvector, tmp), axis=0)

            assert len(fvector) == len(tvector)
        return [fvector, tvector, self.dataset[0]]

"""
Utility functions used by the learners
"""


def entropy_sample(unlabeled, lset, bandwidth, q):
    """
    Method to perform entropy-based uncertainty sampling
    :param unlabeled: data pool to sample from
    :param lset:  labeled sets in forms of nested lists
    :param bandwidth: the bandwidth for the KDE
    :param q: Queue object, needed for getting the result of the process
    :return: None, result is a list containing the highest entropy score and it's corresponding instance
    """
    estimators = []
    kdebot = classifier.pw_get_bottom_kdes(lset, bandwidth)
    for y in lset:
        Ly = np.matrix(y)
        est = classifier.pw_get_top_kde(Ly, bandwidth)
        estimators.append(est)

    emax = None
    maxinst = None

    for row in unlabeled:
        e = 0
        x = row[:-1]
        x = np.array(x)
        for i in range(0, len(lset)):
            p = classifier.pw_get_prob(estimators[i], kdebot, x)
            assert p <= 1
            e += p * math.log10(p)
        e = -e
        if emax is None:
            emax = e
            maxinst = row
        elif e > emax:
            emax = e
            maxinst = row
    q.put([emax, maxinst])


def margin_sample(unlabeled, lset, bandwidth, q):
    """
    Method to perform margin-based uncertainty sampling
    :param unlabeled: data pool to sample from
    :param lset:  labeled sets in forms of nested lists
    :param bandwidth: the bandwidth for the KDE
    :param q: Queue object, needed for getting the result of the process
    :return: None, result is a list containing the smalles margin and it's corresponding instance
    """

    estimators = []
    kdebot = classifier.pw_get_bottom_kdes(lset, bandwidth)
    for y in lset:
        Ly = np.matrix(y)
        est = classifier.pw_get_top_kde(Ly, bandwidth)
        estimators.append(est)

    minmargin = None
    mminst = None

    cgp = classifier.pw_get_prob
    for row in unlabeled:
        x = np.array(row[:-1])
        probs = []
        for i in range(0, len(lset)):
            probs.append(cgp(estimators[i], kdebot, x))
        probs = sorted(probs)
        margin = probs[-1] - probs[-2]
        if minmargin is None:
            minmargin = margin
            mminst = row
        elif margin < minmargin:
            minmargin = margin
            mminst = row
    q.put([minmargin, mminst])


def random_sample(unlabeled):
    """
    Function to randomly sample an instance from a dataset
    :param unlabeled: the data pool to choose from
    :return: instance from the dataset
    """
    ret = random.choice(unlabeled)
    return ret


def entropy_div_sample(unlabeled, lset, bandwidth, q):
    """
    Method to perform entropy-based uncertainty sampling
    :param unlabeled: data pool to sample from
    :param lset:  labeled sets in forms of nested lists
    :param bandwidth: the bandwidth for the KDE
    :param q: Queue object, needed for getting the result of the process
    :return: None, result is a list containing the highest entropy_diversity score and it's corresponding instance
    """
    estimators = []
    kdebot = classifier.pw_get_bottom_kdes(lset, bandwidth)
    for y in lset:
        Ly = np.matrix(y)
        est = classifier.pw_get_top_kde(Ly, bandwidth)
        estimators.append(est)

    edivmax = None
    maxinst = None

    for row in unlabeled:
        e = 0
        x = row[:-1]
        x = np.array(x)
        mindist = None
        for i in range(0, len(lset)):
            p = classifier.pw_get_prob(estimators[i], kdebot, x)
            assert p <= 1
            e += p * math.log10(p)
            for inst in lset[i]:
                dist = euclidean_distances(x, inst)
                if mindist is None:
                    mindist = dist
                elif mindist > dist:
                    mindist = dist
        e = -e
        ediv = e + mindist
        if edivmax is None:
            edivmax = ediv
            maxinst = row
        elif ediv > edivmax:
            edivmax = ediv
            maxinst = row
    q.put([edivmax, maxinst])


def margin_div_sampling(unlabeled, lset, bandwidth, q):
    """
    Method to perform entropy-based uncertainty sampling
    :param unlabeled: data pool to sample from
    :param lset:  labeled sets in forms of nested lists
    :param bandwidth: the bandwidth for the KDE
    :param q: Queue object, needed for getting the result of the process
    :return: None, result is a list containing the lowest margins_diversity score and it's corresponding instance
    """
    estimators = []
    kdebot = classifier.pw_get_bottom_kdes(lset, bandwidth)
    for y in lset:
        Ly = np.matrix(y)
        est = classifier.pw_get_top_kde(Ly, bandwidth)
        estimators.append(est)

    min_mdiv = None
    candidate = None

    cgp = classifier.pw_get_prob
    for row in unlabeled:
        x = np.array(row[:-1])
        probs = []
        mindist = None
        for i in range(0, len(lset)):
            probs.append(cgp(estimators[i], kdebot, x))
            for inst in lset[i]:
                dist = euclidean_distances(x, inst)
                if mindist is None:
                    mindist = dist
                elif mindist > dist:
                    mindist = dist

        probs = sorted(probs)
        margin = probs[-1] - probs[-2]
        mdiv = margin - mindist

        if min_mdiv is None:
            min_mdiv = mdiv
            candidate = row
        elif mdiv < min_mdiv:
            min_mdiv = mdiv
            candidate = row
    q.put([min_mdiv, candidate])


def divide_data(l, n):
    """
    Utility function to seperate a dataset into evenly sized chunks. Used for parallelization
    :param l: list, to be cut into chunks
    :param n: numeric, size of a chunk
    :return: list, the chunks
    """
    ret = []
    for i in range(0, len(l), n):
        ret.append(l[i:i+n])
    return ret

