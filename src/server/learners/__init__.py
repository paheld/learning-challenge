from __future__ import print_function, division, unicode_literals, absolute_import, generators

__author__ = 'cbraune'


import learners.knn
import learners.dtree
import learners.bayesnet
import learners.naivebayes
import learners.ldaqda
import learners.svm
import learners.labels
import learners.s3vm
import learners.al
import learners.ensemble
import learners.bestofthree

# 21.01.2016
import learners.gradient_boosting
import learners.ensemble_project
import learners.decision_tree_multiclass

# 28.01.2016
import learners.evodtree
import learners.kdeclassify
import learners.al_us_uds
import learners.dbscan_label_propagation
import learners.VFDT

# 17.01.2016
import learners.ws1617.LVQ
import learners.ws1617.NBTree
import learners.ws1617.adtree
import learners.ws1617.extendedLDA
import learners.ws1617.kde
import learners.ws1617.kde.kde

import learners.ws1617.kde2
import learners.ws1617.nn
