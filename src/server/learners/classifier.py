import numpy as np
from sklearn.neighbors.kde import KernelDensity

__author__ = 'Dominik Lang'

"""
This file is merely a container for capsuling the posterior estimation using Kernel Density Estimation (also called the
'Parzen-Window Method'
"""


def pw_get_top_kde(x, bandwidth):
    """
    Utility method to get the 'top' element of the posterior estimation formula
    :param x: data belonging to one class label
    :param bandwidth: bandwidth of the kernels
    :return: a fitted KernelDensity object
    """
    ret = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(x.reshape(-1, 1))
    return ret


def pw_get_bottom_kdes(classes, bandwidth):
    """

    :param classes: nested lists containing the different labeled sets (corresponding to 'lset' in activelearner.py)
    :param bandwidth:bandwidth of the kernels
    :return: list of fitted KernelDensity objects for the different labeled sets (classes)
    """
    kdes = []
    for x in classes:
        x = np.matrix(x)
        k = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(x.reshape(-1, 1))
        kdes.append(k)
    return kdes


def pw_get_prob(top, bot, x):
    """
    Compute the class posterior probability of an instance x
    :param top: result of pw_get_top_kde
    :param bot: result of pw_get_bottom_kdes
    :param x: instance which's class posterior is to be estimated
    :return: the class posterior probability
    """
    t = top.score(x.reshape(-1, 1))
    b = 0
    for k in bot:
        b += k.score(x.reshape(-1, 1))
    return t/b
