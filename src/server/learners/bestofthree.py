__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
import algorithms
from sklearn.metrics import accuracy_score as accuracy_score
from time import time as time
from collections import Counter

def do_stuff(dataset, *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    kwargs1 = {key[6:]:value for key, value in kwargs.items() if key.startswith('algo1_')}
    kwargs2 = {key[6:]:value for key, value in kwargs.items() if key.startswith('algo2_')}
    kwargs3 = {key[6:]:value for key, value in kwargs.items() if key.startswith('algo3_')}

    res1 = algorithms.algorithms[kwargs['algo1']]['worker'](dataset, **kwargs1)
    res2 = algorithms.algorithms[kwargs['algo2']]['worker'](dataset, **kwargs2)
    res3 = algorithms.algorithms[kwargs['algo3']]['worker'](dataset, **kwargs3)

    trl1 = [x for x in res1['training_labels']]
    trl2 = [x for x in res2['training_labels']]
    trl3 = [x for x in res3['training_labels']]

    tel1 = [x for x in res1['test_labels']]
    tel2 = [x for x in res2['test_labels']]
    tel3 = [x for x in res3['test_labels']]

    val1 = [x for x in res1['validation_labels']]
    val2 = [x for x in res2['validation_labels']]
    val3 = [x for x in res3['validation_labels']]

    training_prediction   = [Counter([c1, c2, c3]).most_common(1)[0][0] for c1,c2,c3 in zip(trl1, trl2, trl3)]
    test_prediction       = [Counter([c1, c2, c3]).most_common(1)[0][0] for c1,c2,c3 in zip(tel1, tel2, tel3)]
    validation_prediction = [Counter([c1, c2, c3]).most_common(1)[0][0] for c1,c2,c3 in zip(val1, val2, val3)]

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{},
               'extra_scores_validation':{},
               'message': '',
               'pictures': None,
               'success': True}
    print(result)
    return result