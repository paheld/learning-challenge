__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

import settings
from sklearn.metrics import accuracy_score
from time import time as time
from sklearn.naive_bayes import GaussianNB, MultinomialNB
import numpy as np
from scipy.sparse import issparse


def do_stuff(dataset, *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    nb = GaussianNB() if kwargs['distribution'] == 'gaussian' else MultinomialNB()

    if kwargs['distribution'] != 'gaussian' and (np.any(np.array(training_data) < 0) or np.any(np.array(test_data) < 0) or np.any(np.array(validation_data) < 0)):
        error['message'] = "Negative Values are not allowed for multinomial distributions! And one of the three sets contains negative values..."
        return error

    t = time() # Train the KNN Classifier on the training data and labels
    nb.fit(training_data,training_label)
    training_time = time() - t

    training_prediction = nb.predict(training_data) # get a prediction on the training data (this should be obvious)

    t = time() # get labels for test data
    test_prediction = nb.predict(test_data)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = nb.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': [],
               'success': True}
    return result