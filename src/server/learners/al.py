#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, unicode_literals, absolute_import, generators
import numpy as np
import scipy.spatial.distance as ssd
import settings
from sklearn.metrics import accuracy_score
from time import time as time
from sklearn.svm import SVC

__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

dists = {
    "euclidean": ssd.euclidean,
    "weuclidean": ssd.wminkowski,
    "seuclidean": ssd.seuclidean,
    "sqeuclidean": ssd.sqeuclidean,
    "hamming": ssd.hamming,
    "manhattan": ssd.cityblock,
    "chebyshev": ssd.chebyshev,
    "minkowski": ssd.minkowski,
    "wminkowski": ssd.wminkowski
}

oracle_penalty = 0.995


def do_stuff(dataset, *_, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    c = kwargs['penalty']
    kernel = kwargs['kernel']


    if kernel == 'linear':
        clf = SVC(C=c, kernel=str('linear'))
    elif kernel == 'poly':
        clf = SVC(C=c, kernel=str('poly'), degree=kwargs['kernel_degree'], gamma=kwargs['kernel_gamma'], coef0=kwargs['kernel_coef0'])
    elif kernel == 'sigmoid':
        clf = SVC(C=c, kernel=str('sigmoid'), gamma=kwargs['kernel_gamma'], coef0=kwargs['kernel_coef0'])
    elif kernel == 'rbf':
        clf = SVC(C=c, kernel=str('rbf'), gamma=kwargs['kernel_gamma'])


    oracle_queries = kwargs['queries']
    oracle_queries = max(0, oracle_queries)
    if oracle_queries > len(test_data):
        error['message'] = "There are less data points available for querying than you asked for. Maximal number is %i." % len(test_data)
        return error

    no_classes = len(np.unique(training_label))
    k = int(no_classes * (no_classes-1)/2)
    t = time()
    clf.fit(training_data,training_label)
    td = np.copy(test_data)
    tl = np.copy(test_label)

    tr_d = np.copy(training_data)
    tr_l = np.copy(training_label)
    for _ in range(oracle_queries):
        df = clf.decision_function(test_data)       # distance to decision boundary for all SVCs (multiclass stuff)
        j = np.argmin(np.abs(df))                   # gets index in flattened array
        idx = j // k                                # integer division, gets index in non-flattened array

        # add data point that lies closest to _any_ decision boundary to training data
        training_data = np.concatenate( (training_data, [test_data[idx]]) )
        training_label = np.concatenate( (training_label, [test_label[idx]]) )
        #print(training_data)
        #print(np.size(training_data))
        mask = np.ones(len(test_data), dtype=bool)
        mask[[idx]] = False
        #print(mask)
        test_data = np.array(test_data)[mask]
        test_label = np.array(test_label)[mask]
        clf.fit(training_data,training_label)
    training_prediction = clf.predict(tr_d)
    training_time = time() - t


    t = time() # get labels for test data
    test_prediction = clf.predict(td)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = clf.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(tl, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    penalty = (oracle_penalty**oracle_queries)

    result =  {'score_test':score_test*penalty, # Kriegen Studis zu sehen
               'score_validation':score_validation*penalty, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000), "true_accuracy": round(score_test,3)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000), "true_accuracy":round(score_validation,3)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': [],
               'success': True}
    return result