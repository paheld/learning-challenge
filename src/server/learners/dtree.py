__author__ = 'Christian Braune'
__email__ = "christian.braune79@gmail.com"

import numpy as np
from sklearn import tree
from sklearn.metrics import accuracy_score as accuracy_score
import settings
from time import time as time
from sklearn.externals.six import StringIO
import pydot
import tempfile
import codecs

def do_stuff(dataset, criterion='gini', splitter='best', max_depth=None, *args, **kwargs):
    (training_data, training_label, test_data, test_label, validation_data, validation_label) = dataset
    error = {'score_test':None, # Kriegen Studis zu sehen
               'score_validation':None, # kommt in die Highscore-Tabelle
               'training_labels':None,
               'test_labels':None,
               'validation_labels':None,
               'extra_scores_test':None,
               'extra_scores_validation':None,
               'pictures': None,
               'success': False}

    t = time() # Train the KNN Classifier on the training data and labels
    dtree = tree.DecisionTreeClassifier(
        criterion=criterion, splitter=splitter, max_depth=max_depth
    ).fit(training_data, training_label)
    training_time = time() - t

    training_prediction = dtree.predict(training_data) # get a prediction on the training data (this should be obvious)

    t = time() # get labels for test data
    test_prediction = dtree.predict(test_data)
    time_test = time() - t

    t = time() # get labels for validation data
    validation_prediction = dtree.predict(validation_data)
    time_validation = time() - t

    # for both test and validation data get the accuracy scores
    score_test = accuracy_score(test_label, test_prediction)
    score_validation = accuracy_score(validation_label, validation_prediction)

    # generate graphical output for the decision tree
    dot_data = StringIO()
    wrapper = codecs.StreamReaderWriter(dot_data, codecs.lookup("utf-8").streamreader, codecs.lookup("utf-8").streamwriter) # weil windows und solaris nicht so gut koennen
    tmpfile = tempfile.NamedTemporaryFile(mode='w+b', bufsize=-1, suffix='.jpg', prefix='dtree_tmp_', dir=None, delete=False)
    tree.export_graphviz(dtree, out_file=wrapper)
    graph = pydot.graph_from_dot_data(dot_data.getvalue())
    graph.write_jpg(tmpfile.name)

    result =  {'score_test':score_test, # Kriegen Studis zu sehen
               'score_validation':score_validation, # kommt in die Highscore-Tabelle
               'training_labels':training_prediction,
               'test_labels':test_prediction,
               'validation_labels':validation_prediction,
               'extra_scores_test':{"time":"%2.3fms"%(time_test*1000)},
               'extra_scores_validation':{"time":"%2.3fms"%(time_validation*1000)},
               'message': 'Training time: %2.3fms'%(training_time*1000),
               'pictures': [tmpfile.name],
               'success': True}
    return result