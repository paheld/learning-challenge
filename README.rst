Introduction
============

Resources
---------

- Git - Repository: https://bitbucket.org/paheld/learning-challenge
- Documentation: http://learning-challenge.readthedocs.org


Requirements
------------

- django
- Matplotlib
- SciPy
- NumPy
- PyMySQL
- six
- pydot
- docutils